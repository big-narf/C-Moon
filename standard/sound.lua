local std = require("standard") --loads the engine's basic functions and definitions

--Synth def

synth = { volume = 1, voices = {} }

function synth:note_on(channel, key, velocity) end

function synth:note_off(channel, key) end

---returns a buffer with b_size for a mono sound stuff
---comment
---@param right SharedBuffer
---@param left SharedBuffer
function synth:render(right, left) end

-- Oscilator def

return soundfont

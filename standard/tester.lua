local std = require("standard") --loads the engine's basic functions and definitions

tester = {}
tester.__index = tester

---@param properties table
function tester:new(properties)
	print("created tester object")

	--[[local attch = {
		image = properties.image,
		mesh = properties.mesh,
		frag_shader = properties.frag_shader,
		vert_shader = properties.vert_shader,
	}
	std.create_partial("renderer", "sprite", "render", nil, attch)]]

	--std.new_hook(self, std.controller.timer(100), tester.waiter)

	--std.new_hook(self, std.controller.updater(), tester.update)
	local shoot_signal = { target = nil, signal_type = "Space" }

	std.new_hook(self, std.controller.trigger(shoot_signal), tester.on_shoot)
end

function tester:waiter(delay)
	print("delay is", delay)
end

function tester:update()
	repeat
		print("updating")
		coroutine.yield()
	until false
end

function tester:on_shoot(signal) -- monitors spacebar
	print("trigger pulled!")
	coroutine.yield()
	repeat
		print("charging")
		local signal = coroutine.yield()
	until signal < 0.5 --checks if space is not released
	print("shooting!!!!!!")
end

-- This is a standard library that will wrap around the low level engine API. regular applications are supposed to require this module instead of the "cmoon" api
local eng = require("cmoon") --loads the engine's basic functions and definitions

---@class Object

--NOTE: Sets up hidden tables and functions that are not meant to be manipulated directly by the application

---@type Hook[]
local CALLBACKS = setmetatable({}, { __mode = "k" })

---@type Object[]
local OBJECTS = {}

---@type table<Object,Hook[]>
local OBJ_HOOKS = setmetatable({}, { __mode = "kv" })

---@type table<Object, string>
local PARENTS = setmetatable({}, { __mode = "k" })

local LAST_OBJ_ID = 1
local LAST_HOOK_ID = 1

local standard = {}

---standard constructor callback
---@param group string
---@param identifier string
---@param parent string?
---@param properties table
---@return number
function standard.create(group, identifier, parent, properties)
	local mdl = require(group)
	print(identifier)
	local obj = setmetatable({}, mdl[identifier])
	PARENTS[obj] = parent
	OBJ_HOOKS[obj] = {}
	obj:new(properties)

	local index = 1
	while OBJECTS[index] do
		index = index + 1 --finds first index that's nil
	end
	OBJECTS[index] = obj
	return index
end

function standard.destroy(id)
	local obj = OBJECTS[id]
	if not obj then
		return
	end
	OBJECTS[id] = nil

	--kills obj hooks.
	local h_list = OBJ_HOOKS[obj]
	if h_list then
		for _, hook in ipairs(h_list) do
			hook:kill()
		end
	end
	OBJ_HOOKS[obj] = nil

	local parent = PARENTS[obj]
	PARENTS[obj] = nil
	print("object destroyed!")
end

function standard.process(dt)
	for i, h in ipairs(CALLBACKS) do
		if not h:try_call(dt) then
			print("Found dead hook :", i)
			CALLBACKS[i] = nil
		end
	end
end

--NOTE: hook related stuff
local controllers = {}

---comment
---@return fun(dt :number):boolean?
function controllers.updater()
	local func = function(dt)
		return true
	end
	return func
end

---comment
---@param delay number
---@return fun(dt :number):number?
function controllers.timer(delay)
	local wait = delay
	local func = function(dt)
		wait = wait - dt
		if wait < 0 then
			return delay
		else
			return nil
		end
	end
	return func
end

---comment
---@param signal_id table
---@return fun(dt :number):number?
function controllers.trigger(signal_id)
	local func = function(dt)
		local signal = eng:search_signal(signal_id)
		if signal then
			return signal
		else
			return nil
		end
	end
	return func
end

--NOTE: Callback utilities

---@class Hook
---@field trigger fun(dt :number):boolean
---@field co thread
local hook = {}
hook.__index = hook
hook.__gc = function()
	print("hook got gc'd")
end

---Creates a new hook registered on the relevant tables
---@param obj Object
---@param trigger fun(dt :number):any?
---@param func function
---@return Hook
function hook.new(obj, trigger, func)
	local h = setmetatable({}, hook)
	local co = coroutine.create(function(value)
		func(obj, value)
	end)
	h.trigger, h.co = trigger, co

	local h_list = OBJ_HOOKS[obj]
	if h_list then
		table.insert(h_list, h)
	end
	table.insert(CALLBACKS, h)
	return h
end

---tries to call this hook, return true if the hook is alive and false if its dead
---@param dt number
---@return boolean
function hook:try_call(dt)
	if self:is_dead() then
		return false
	end
	local value = self.trigger(dt)
	if value then
		local success, n_trigger = coroutine.resume(self.co, value)
		if n_trigger then
			self.trigger = n_trigger
		end -- if received new trigger, substitutes trigger
		if not success then
			print("Hook error'd out")
			self:kill()
			return false
		end --if hook errors kill hook
	end
	return true
end

function hook:is_dead()
	return not self.co
end

---kill this hook
function hook:kill()
	self.co, self.trigger = nil, nil
end

--NOTE: Object stuff

---introduces a class definition with regular utility functions
---@return Object
local function def_class()
	local obj = {}
	obj.__index = {}

	return obj
end

--NOTE: EXPORTS

---comment
---@param bytes string
function standard.resource(bytes)
	return eng:new_resource(bytes)
end

---maybe change this later to be a specific function for each module
---@param module string
---@param id string
---@param group string
---@param parent string?
---@param args table
function standard.create_partial(module, id, group, parent, args)
	eng:create_partial(module, id, group, parent, args)
end

standard.new_hook = hook.new

standard.controller = controllers

function standard.instantiate(info)
	eng:instantiate(info)
end

function standard.eliminate(name)
	eng:eliminate(name)
end

return standard

local std = require("standard") --loads the engine's basic functions and definitions

sprite = {}
sprite.__index = sprite

---@param properties table
function sprite:new(properties)
	local attch = {
		image = properties.image,
		mesh = properties.mesh,
		frag_shader = properties.frag_shader,
		vert_shader = properties.vert_shader,
	}
	std.create_partial("renderer", "sprite", "render", nil, attch)
end

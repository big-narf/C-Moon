local iterator = {}

function as_iter(table)
	return setmetatable({ ipairs(table) }, iterator)
end

function iterator:pairs()
	return table.unpack(self)
end

function iterator:for_each(predicate)
	for _, value in table.unpack(self) do
		predicate(value)
	end
end

function iterator:filter(predicate)
	local prev_fn = self.next
	self.next = function(iter)
		local v = { prev_fn(iter) }
		while #v > 0 and predicate(table.unpack(v)) do
			v = { next(iter) }
		end
		return table.unpack(v)
	end
end

function iterator:map(predicate)
	local prev_fn = self.next
	self.next = function(iter)
		return predicate(prev_fn(iter))
	end
end

function iterator:zip(other)
	local this_next = self.next
	local other_next = other.next
end

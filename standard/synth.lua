---@class synth
---@field samples integer[]
---@field presets table<integer,preset>
---@field insts table<integer,instrument>
local synth = {}

function synth:get_instrument()
	local preset = self.presets[1]
	for index, value in ipairs(preset.reg_inst) do
	end
end

--- returns a closure that will yeild an output sample for the next iteration
---@param samples number[]
---@param start_index integer
---@param end_index integer
---@return fun(number) :number?
local function oscillator(samples, start_index, end_index)
	local cur_index = start_index
	return function(pitch_ratio)
		if cur_index >= end_index then
			return nil
		else
		end
		cur_index = cur_index + pitch_ratio
	end
end

---this is a thing that indexes into an array of instruments
---@class preset
---@field reg_gens table[]
---@field reg_inst integer[]
local preset = {}

---This is a thing that indexes into the samples array
---@class instrument
---@field reg_headers table[]
---@field reg_gens table[]
local inst_region = {}

---returns an iterator over the note's samples (receiving the current state of the channel)
---@param inst_region any
---@param preset_region any
---@return fun(channel_state :table):integer?
function create_voice(inst_region, preset_region)
	local voice = function(channel_state) end
	return voice
end

use std::collections::{HashMap, HashSet};

use rusqlite::types::{FromSql, FromSqlError};

use cmoon_commons::*;
use serde::Deserialize;

use crate::error::*;

use crate::context::ENGINE_CORE;
use crate::infra::{TaggedSignal, ToDyn};

/**
* Game Object is an extensive type central object for the engine. It acts as a container for data
* types of the engine (non-state) and keeps references to the partial objects it holds on the
* different modules, acting as a common bridge between them.
* Engine messages follow the path: Sender => EngineCore => GameObject => PartialObject for all messages addressed at extensive objects.
*/
#[derive(Default)]
pub struct GameObject {
    transform: Transform,
    properties: Vec<(EngString, DynValue)>,
    partials: HashSet<GlobalAdress>,
    signals: EngMap<TaggedSignal>,
}

impl GameObject {
    ///Creates a single gameobject and inserts it to the envoirement
    pub fn new<I: IntoIterator<Item = (EngString, Property)>>(
        fields: I,
        abs_transform: Transform,
    ) -> EngResult<Self> {
        let properties = fields.into_iter().map(|(s, p)| (s, p.to_dyn())).collect();
        //Core object creation
        Ok(GameObject {
            transform: abs_transform,
            properties,
            partials: Default::default(),
            signals: EngMap::new(),
        })
    }

    pub fn attach_partial(&mut self, partial: GlobalAdress) {
        self.partials.insert(partial);
    }

    pub fn remove_partial(&mut self, partial: GlobalAdress) -> bool {
        self.partials.remove(&partial)
    }

    ///Inserts the signal into the signal table or superposes with existing similar signal.
    pub fn add_signal(&mut self, id: &str, signal: TaggedSignal) {
        self.signals.insert_or(id, signal, TaggedSignal::superpose);
    }

    ///Evaluates the signal at the current moment, returns a clone if the signal is alive
    pub fn find_signal(&mut self, id: &str, current_moment: usize) -> Option<Signal> {
        let signal = self.signals.get_mut(id)?;
        if signal.eval(current_moment) {
            self.signals.remove(id);
            None
        } else {
            Some(signal.as_signal())
        }
    }

    pub fn get_properties(&self) -> Vec<(EngString, DynValue)> {
        self.properties.clone()
    }

    pub fn get_transform(&self) -> Transform {
        self.transform.clone()
    }

    ///Add the delta transform to the current transform. All non-instantiate transform operations
    ///are relative, for code simplification.
    pub fn add_transform(&mut self, delta: Transform) {
        self.transform += delta;
    }
}

///Debug functions
impl GameObject {
    pub fn inspect_partials(&self) -> String {
        let msg = self.partials.iter().fold(String::default(), |mut s, a| {
            s.push_str(&a.to_string());
            s
        });
        msg
    }
}

impl Drop for GameObject {
    fn drop(&mut self) {
        self.partials
            .iter()
            .try_for_each(|adress| ENGINE_CORE.get().unwrap().destroy_partial(adress.clone()))
            .expect("OBJ DROP ERROR!!!!!");
    }
}

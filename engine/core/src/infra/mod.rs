use cmoon_commons::Signal;

///Wrapper around a signal with the emmission moment.
pub struct TaggedSignal {
    moment: usize,
    signal: Signal,
}

impl TaggedSignal {
    pub fn new(signal: Signal, moment: usize) -> Self {
        Self { moment, signal }
    }

    ///Evaluates self and superposes with the new signal. if self is a dead signal, other is
    ///assigned to self
    pub fn superpose(&mut self, other: Self) {
        let frames_passed = other.moment - self.moment;
        self.signal.adv_compose(other.signal, frames_passed);
        self.moment = other.moment;
    }

    ///Returns true if the signal is dead at the current moment.
    pub fn eval(&mut self, cur_moment: usize) -> bool {
        let frames_passed = cur_moment - self.moment;
        self.moment = cur_moment;
        self.signal.advance(frames_passed)
    }

    pub fn as_signal(&self) -> Signal {
        self.signal.clone()
    }
}

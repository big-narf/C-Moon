use std::env;
use std::path::PathBuf;
use std::sync::{Arc, OnceLock};
use std::thread::JoinHandle;
use std::time;

use winit::application::ApplicationHandler;
use winit::event::WindowEvent;
use winit::event_loop::{ActiveEventLoop, EventLoop, EventLoopProxy};
use winit::keyboard::KeyCode;
use winit::window::{Window, WindowId};

use cmoon_commons::*;

mod context;
mod elements;
mod error;
mod infra;

use context::{Core, Synchronizer, ENGINE_CORE};

#[cfg(test)]
mod tests;

static ASSET_PATH: OnceLock<PathBuf> = OnceLock::new();

static LIBRARIES_PATH: OnceLock<PathBuf> = OnceLock::new();

fn main() {
    //sets up assets path
    let assets_p = env::args()
        .nth(1)
        .and_then(|arg| arg.parse().ok())
        .or_else(|| Some(env::current_exe().ok()?.parent()?.join("assets.db")))
        .expect("FAILED TO FIND ASSET PATH");
    ASSET_PATH.set(assets_p).unwrap();

    //sets up libraries path
    let libs_p = env::current_exe()
        .ok()
        .and_then(|p| Some(p.parent()?.join("modules")))
        .expect("FAILED TO GET LIBS PATH");
    LIBRARIES_PATH.set(libs_p).unwrap();

    //starts main loop
    let event_loop = EventLoop::with_user_event()
        .build()
        .expect("Error creating event loop");

    let mut game = Executer::new(&event_loop);
    event_loop.run_app(&mut game).expect("EVENT LOOP ERROR");
}

#[derive(Debug)]
pub struct Executer {
    handles: Vec<JoinHandle<()>>,
    window: Option<Window>,
    frame_time: time::Instant,
}

#[derive(Clone, Debug, Default)]
pub enum TestEvent {
    #[default]
    IDK,
}

impl Executer {
    pub fn new(event_loop: &EventLoop<TestEvent>) -> Self {
        Self {
            handles: Default::default(),
            window: Default::default(),
            frame_time: time::Instant::now(),
        }
    }
}

impl ApplicationHandler<TestEvent> for Executer {
    fn resumed(&mut self, event_loop: &ActiveEventLoop) {
        //sets up the engine core with the operational parameters creating the thread assignments
        let window = event_loop
            .create_window(Window::default_attributes())
            .unwrap();
        let core = context::ENGINE_CORE.get_or_init(|| Core::new(&window).unwrap());

        self.window = window.into();

        //main thread keeps busy managing the window.
        //passes the thread assignments to the core and launches it.
        self.handles = core.execute().unwrap();
    }

    fn about_to_wait(&mut self, _event_loop: &ActiveEventLoop) {}

    fn window_event(
        &mut self,
        event_loop: &ActiveEventLoop,
        window_id: WindowId,
        event: WindowEvent,
    ) {
        match event {
            WindowEvent::CloseRequested => {
                self.window = None;
                event_loop.exit();
            }

            WindowEvent::RedrawRequested => {
                //NOTE: THIS WILL MAKE SURE THIS WILL CONTINUALLY BE CALLED.
                self.window.as_ref().map(|w| w.request_redraw());

                let core = ENGINE_CORE.get().unwrap();

                //NOTE: THIS MAY LEAP A WHOLE SECOND IF THE FRAME CHOKES UP ENOUGH.
                if self.frame_time.elapsed().subsec_millis() >= core.frame_delta() {
                    core.swap().unwrap();
                    self.frame_time = std::time::Instant::now();
                }
            }
            WindowEvent::KeyboardInput {
                device_id,
                event,
                is_synthetic,
            } => {
                if event.physical_key == KeyCode::Space {
                    let info = InstantiateInfo::default()
                        .with_id("tester")
                        .with_name("thing");
                    //context::ENGINE_CORE.instantiate(info).unwrap();
                }
                if event.physical_key == KeyCode::Enter {
                    let name = "thing/";
                    //context::ENGINE_CORE.eliminate(name).unwrap();
                }
            }
            _ => (),
        }
    }

    fn user_event(&mut self, event_loop: &ActiveEventLoop, event: TestEvent) {
        let dt = self.frame_time.elapsed().subsec_millis();

        self.frame_time = time::Instant::now();
        //self.updater.refresh();
    }

    /*
    fn suspended(&mut self, event_loop: &ActiveEventLoop) {
        //joins all the threads for finishing execution
        //ENGINE_CORE.signal_close();
        self.handles
            .drain(..)
            .try_for_each(|handle| handle.join())
            .expect("ERROR: Thread join Error.");
    }*/

    fn exiting(&mut self, event_loop: &ActiveEventLoop) {
        ENGINE_CORE.get().unwrap().finish().unwrap();

        self.handles
            .drain(..)
            .try_for_each(|handle| handle.join())
            .expect("ERROR: Thread join Error.");
    }
}

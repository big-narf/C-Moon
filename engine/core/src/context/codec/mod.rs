use std::io::{BufRead, BufReader, Read};

use cmoon_commons::AssetData;

use super::*;
use crate::error::*;

///High level decoder function
pub fn decode(blob: &[u8]) -> EngResult<AssetData> {
    let mut reader = BufReader::new(blob);
    let mut head_byte: u8 = 0;
    reader
        .read_exact(std::slice::from_mut(&mut head_byte))
        .cast()?;
    match head_byte {
        SIMPLE_BYTE => {
            let mut chunk_tag: u8 = Default::default();
            let mut chunk_size: [u8; 4] = Default::default();
            let mut data = Vec::new();

            reader
                .read_exact(std::slice::from_mut(&mut chunk_tag))
                .cast()?;
            reader.read_exact(&mut chunk_size).cast()?;
            reader.read_to_end(&mut data).cast()?;
            let dynval = convert(chunk_tag, &data)?;
            Ok(AssetData::Simple(dynval))
        }
        COMPOSITE_BYTE => {
            let mut map = EngMap::default();
            let mut name_buff = Vec::new();
            let mut chunk_tag: u8 = Default::default();
            let mut size_buff: [u8; 4] = Default::default();
            let mut data: Vec<u8> = Default::default();

            //NOTE: this is ugly and C pilled, but if it works I am touching this ever again, fuck this
            //shit...
            loop {
                name_buff.clear();
                data.clear();

                let bytes_read = reader
                    .read_until(ID_TERMINATOR_BYTE, &mut name_buff)
                    .cast()?;
                if bytes_read == 0 {
                    break;
                }
                name_buff.pop(); //gets rid of terminator

                let name = std::str::from_utf8(&name_buff)
                    .map_err(|_| {
                        CODEC_ERROR
                            .clone()
                            .with_root_cause("field name is not valid utf8")
                    })?
                    .to_owned();
                reader
                    .read_exact(std::slice::from_mut(&mut chunk_tag))
                    .cast()?;
                reader.read_exact(&mut size_buff).cast()?;
                let size = u32::from_le_bytes(size_buff) as usize;
                data.resize(size, 0);
                reader.read_exact(&mut data).cast()?;
                let dynval = convert(chunk_tag, &data)?;

                map.replace(name, dynval);
            }
            Ok(AssetData::Composite(map))
        }
        _ => todo!("implement!!!!"),
    }
}

fn convert(chunk_tag: u8, raw: &[u8]) -> EngResult<DynValue> {
    match chunk_tag {
        NUMBER_TAG => {
            let n: f64 = f64::from_le_bytes(
                raw.try_into()
                    .map_err(|_| CODEC_ERROR.clone().with_root_cause("malformed number size"))?,
            );
            Ok(DynValue::Number(n))
        }
        RAW_TEXT_TAG => {
            let s = String::from_utf8(raw.to_owned())
                .map_err(|_| CODEC_ERROR.clone().with_root_cause("invalid utf8"))?;
            Ok(DynValue::Text(s))
        }
        COMPRESS_TEXT_TAG => {
            let s = String::from_utf8(
                inflate::inflate_bytes(raw)
                    .map_err(|_| CODEC_ERROR.clone().with_root_cause("decompression failed"))?,
            )
            .map_err(|_| CODEC_ERROR.clone().with_root_cause("invalid utf8"))?;
            Ok(DynValue::Text(s))
        }
        RAW_DATA_TAG => Ok(DynValue::Data(raw.into())),
        COMPRESS_DATA_TAG => {
            let d = inflate::inflate_bytes(raw)
                .map_err(|_| CODEC_ERROR.clone().with_root_cause("decompression failed"))?;
            Ok(DynValue::Data(d.into()))
        }
        _ => Err(CODEC_ERROR
            .clone()
            .with_root_cause(format!("invalid tag:{}", chunk_tag))),
    }
}

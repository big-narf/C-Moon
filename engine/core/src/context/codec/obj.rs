use std::io::{BufRead, Read};

use bytemuck::{Pod, Zeroable};
use inflate::inflate_bytes;

use super::*;
use crate::error::*;

fn compressed_decoder(blob: &[u8]) -> Result<AssetMap, OBJError> {
    let mut map = AssetMap::default();

    let mut name = String::new();
    let mut chunk_data = ChunkData::default();
    let mut buffer: Vec<u8> = Vec::new();
    let mut name_size: usize = 0;

    //NOTE: this is ugly and C pilled, but if it works I am touching this ever again, fuck this
    //shit...
    loop {
        name.clear();
        buffer.clear();
        name_size = blob.read_line(&mut name)?;
        if name_size == 0 {
            break;
        }
        blob.read_exact(chunk_data.as_mut())?;
        buffer.resize(chunk_data.chunk_size(), 0);
        blob.read_exact(&mut buffer)?;
        let dynval = chunk_data.convert(&buffer).unwrap();
        map.insert(name, dynval);
    }
    Ok(map)
}

#[repr(C)]
#[derive(Default, Clone, Copy, Zeroable, Pod)]
struct ChunkData {
    variant: u32,
    chunk_size: u32,
}

impl AsMut<[u8]> for ChunkData {
    fn as_mut(&mut self) -> &mut [u8] {
        bytemuck::bytes_of_mut(self)
    }
}

impl ChunkData {
    const NUMBER_TAG: u32 = 1;
    const RAW_TEXT_TAG: u32 = 2;
    const COMPRESS_TEXT_TAG: u32 = 3;
    const RAW_DATA_TAG: u32 = 4;
    const COMPRESS_DATA_TAG: u32 = 5;
    fn chunk_size(&self) -> usize {
        self.chunk_size as usize
    }

    fn convert(&self, raw: &[u8]) -> Result<DynValue, ()> {
        match self.variant {
            Self::NUMBER_TAG => {
                let n: f64 = *bytemuck::try_from_bytes(raw).unwrap();
                Ok(DynValue::Number(n))
            }
            Self::RAW_TEXT_TAG => {
                let s = String::from_utf8(raw.to_owned()).unwrap();
                Ok(DynValue::Text(s))
            }
            Self::COMPRESS_TEXT_TAG => {
                let s = String::from_utf8(inflate_bytes(raw).unwrap()).unwrap();
                Ok(DynValue::Text(s))
            }
            Self::RAW_DATA_TAG => Ok(DynValue::Data(raw.to_owned())),
            Self::COMPRESS_DATA_TAG => Ok(DynValue::Data(inflate_bytes(raw).unwrap())),
            _ => Err(()),
        }
    }
}

use std::collections::HashMap;
use std::io::Read;
use std::ops::Deref;
use std::sync::Mutex;
use std::sync::Weak;

use fallible_iterator::FallibleIterator;

use rusqlite::{named_params, Connection, Row};
use serde::Deserialize;

use cmoon_commons::*;

use crate::error::*;
use crate::ASSET_PATH;

use super::*;

///This is a wrapper around the database that caches retrieved blobs as weak references
pub struct AssetManager {
    db: Mutex<Connection>,
    cache: DashMap<u32, AssetData>,
}

impl AssetManager {
    pub fn new() -> EngResult<Self> {
        let path = ASSET_PATH.get().ok_or_else(|| NO_CONTENT.clone())?;
        let db = Connection::open(path).cast()?.into();
        Ok(Self {
            db,
            cache: Default::default(),
        })
    }

    pub fn get_config(&self) -> EngResult<EngineConfig> {
        let lock = self.db.lock().cast()?;

        let encoded: String = lock
            .query_row(GRAB_CONFIG, (), |row| row.get("Encoded"))
            .cast()?;

        let config = serde_json::from_str(&encoded)
            .map_err(|je| BAD_CONFIG.clone().with_root_cause(je.as_msg()))?;
        Ok(config)
    }
    pub fn find_assemblies(&self, lang: &str) -> EngResult<HashMap<String, String>> {
        let lock = self.db.lock().cast()?;
        let res = lock
            .prepare_cached(GRAB_ASSEMBLIES)
            .cast()?
            .query(named_params! {":lang":lang})
            .cast()?
            .map(|row| {
                let ident: String = row.get("Assembly_ID")?;
                let code: String = row.get("Code")?;
                Ok((ident, code))
            })
            .collect()
            .cast()?;
        Ok(res)
    }

    ///Internal function that will find all the fields for all the objects bellow the root
    pub fn find_objects(
        &self,
        root_id: EngString,
        root_name: EngString,
        root_transform: Transform,
        root_args: Vec<Property>,
    ) -> EngResult<Vec<(EngString, ObjMeta)>> {
        let lock = self.db.lock().cast()?;

        let mut tree_finder = lock.prepare_cached(REC_GRAB_TREE).cast()?;

        //NOTE: SERIALIZES SHIT SO THEN IF CAN DESERIALIZE AFTERWARDS
        let root_transform = serde_json::to_string(&root_transform).cast()?;
        let root_args = serde_json::to_string(&root_args).cast()?;

        println!("name is :{root_name} id is : {root_id}");
        let rows = tree_finder
            .query(named_params! {
                ":root":root_id.deref(),
                ":root_name":root_name.deref(),
                ":root_transform":root_transform,
                ":root_args":root_args,
            })
            .cast()?;

        let meta: Vec<_> = rows
            .and_then(move |row: &Row<'_>| -> SQLResult<_> {
                let name: EngString = row.get_ref("tree_path")?.as_str()?.into();

                let behaviours: Vec<BehaviourMeta> = row.get_json("Behaviours")?;

                let transforms: Vec<Transform> = row.get_json("Transforms")?;

                //NOTE: CHANGE THIS
                //get params
                let raw: &str = row.get_ref("Args")?.as_str()?;
                let args: Vec<Property> = serde_json::from_str(&raw)
                    .map_err(|je| {
                        MALFORMED_BLUEPRINT
                            .clone()
                            .with_root_cause(format!("ARGS:{}", je.as_msg()))
                    })
                    .cast_sql()?;

                let json_mask: Vec<(EngString, PropertyType)> = row.get_json("Parametrics")?;
                let params = validate_args(args, json_mask)?;

                let statics: HashMap<EngString, Property> = row.get_json("Statics")?;

                let fields = params.into_iter().chain(statics.into_iter()).collect();
                let meta = ObjMeta {
                    transforms,
                    fields,
                    behaviours,
                };
                Ok((name, meta))
            })
            .collect::<SQLResult<_>>()
            .cast()?;

        if meta.is_empty() {
            Err(OBJECT_NOT_FOUND.clone().with_root_cause(root_id))
        } else {
            Ok(meta)
        }
    }

    /// Returns an appropriate resource for the given id, checks for cache first.
    pub fn find_resource(&self, id: u32) -> EngResult<AssetData> {
        match self.cache.get(&id) {
            Some(pair) => Ok(pair.pair().1.clone()), //return if cached
            None => {
                //grabs the file and decodes it to a table
                let lock = self.db.lock().cast()?;
                let res = lock
                    .prepare_cached(GRAB_RESOURCES)
                    .cast()?
                    .query_row(named_params! {":resource":id}, |row| {
                        let blob = row.get_ref("Data")?.as_blob()?;
                        super::codec::decode(blob).cast_sql()
                    })
                    .cast()?;
                self.cache.insert(id, res.clone()); //caches the table
                Ok(res)
            }
        }
    }

    pub fn clear_cache(&self) {
        self.cache.clear();
    }
}

///Convenience trait for SQL rows
trait GetJSON<'row, T> {
    fn get_json(&'row self, id: &str) -> Result<T, SQLError>;
}

impl<'row, T: Deserialize<'row>> GetJSON<'row, T> for Row<'row> {
    fn get_json(&'row self, id: &str) -> Result<T, SQLError> {
        let raw = self.get_ref(id)?.as_str()?;
        serde_json::from_str(raw)
            .map_err(|e| {
                MALFORMED_BLUEPRINT
                    .clone()
                    .with_root_cause(format!("{id}::{}", e.as_msg()))
            })
            .cast_sql()
    }
}

///Parses and validates json for parametrics
fn validate_args(
    args: Vec<Property>,
    mask: Vec<(EngString, PropertyType)>,
) -> SQLResult<HashMap<EngString, Property>> {
    if args.len() != mask.len() {
        let cause = format!("PARAMS# : {} != ARGS# : {}", mask.len(), args.len());
        Err(SQLError::UserFunctionError(Box::from(
            MISMATCHED_PARAMS.clone().with_root_cause(cause),
        )))
    } else {
        Iterator::zip(mask.into_iter(), args.into_iter())
            .map(|((field_name, field_type), value)| {
                if value.get_type() == field_type {
                    Ok((field_name, value))
                } else {
                    let cause = format!("{field_name} : {field_type} != {}", value.get_type());
                    Err(SQLError::UserFunctionError(Box::from(
                        MISMATCHED_PARAMS.clone().with_root_cause(cause),
                    )))
                }
            })
            .collect()
    }
}

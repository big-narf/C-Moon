use std::collections::HashMap;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::{Arc, Mutex, OnceLock, RwLock};
use std::thread::JoinHandle;

use dashmap::{DashMap, DashSet};
use winit::raw_window_handle::*;
use winit::window::Window;

use cmoon_commons::*;

use crate::elements::*;
use crate::error::*;
use crate::infra::{TaggedSignal, ToDyn};

mod asset;
mod codec;
mod misc;
mod worker;
pub use misc::*;

pub static ENGINE_CORE: OnceLock<Core> = OnceLock::new();

pub struct Core {
    frame_delta: u32,
    frame_id: AtomicUsize,
    synchro: Arc<Synchronizer>,
    pub asset_manager: asset::AssetManager,
    mdls: DashMap<ModuleType, ModuleHandle>,

    objects: DashMap<EngString, GameObject>,

    ambients: DashSet<GlobalAdress>,
    amb_signals: DashMap<EngString, Signal>,
}

///Engine Ops methods
impl Core {
    ///Initializes the core with operational parameters
    pub fn new(window: &Window) -> EngResult<Self> {
        let asset_manager = asset::AssetManager::new()?;

        //creates core handle
        let display_handle = window.display_handle().cast()?.as_raw();
        let window_handle = window.window_handle().cast()?.as_raw();

        let core_handle = CoreHandle {
            display_handle,
            window_handle,

            signal_emitter: emit_signal,
            signal_searcher: search_signal,

            partial_creator: create_partial,
            partial_destroyer: destroy_partial,

            instantiator: instantiate,
            eliminator: eliminate,

            transform_getter: get_transform,
            transform_setter: set_transform,
        };

        let config = asset_manager.get_config()?;
        let handles: DashMap<_, _> = LOADED_LIBRARIES
            .iter()
            .map(|lib| match lib.mdl_type()? {
                ModuleType::Runtime(lang) => {
                    let load = asset_manager.find_assemblies(&lang)?;
                    Self::link_library(core_handle.clone(), &config, lib, Some(load))
                }
                _ => Self::link_library(core_handle.clone(), &config, lib, None),
            })
            .collect::<Result<_, _>>()?;

        let synchro = Synchronizer::new(config.n_threads).into();

        let core = Self {
            frame_delta: 017,
            frame_id: 0.into(),
            synchro,
            asset_manager,
            mdls: handles,

            objects: Default::default(),

            ambients: Default::default(),
            amb_signals: Default::default(),
        };

        //NOTE: INITIAL OBJECT STUFF, PROLLY MOVE THIS TO A SEPARATE FUNCTION.
        let info = CreateInfo::default()
            .with_mdl(config.init_mdl)
            .with_group(config.init_group)
            .with_id(config.init_id)
            .attach_many(config.init_args.into_iter().map(|(s, p)| (s, p.to_dyn())));

        core.create_partial(info)?;

        Ok(core)
    }

    ///Main scope of engine execution. Launches the worker threads and keeps the main window event
    ///loop going.
    pub fn execute(&self) -> EngResult<Vec<JoinHandle<()>>> {
        let assign: Vec<_> = self.mdls.iter().map(|pair| pair.value().clone()).collect();

        //initializes the first cycle
        assign.iter().try_for_each(|mdl| mdl.commence_phase())?;

        //launches threads
        let handles = (0..self.synchro.n_threads())
            .into_iter()
            .map(|id| worker::Worker::new(id, assign.clone(), self.synchro.clone()).launch())
            .collect();

        self.synchro.reset();
        println!("THREADS UNBLOCKED!!!!");
        Ok(handles)
    }

    ///Receives the a module dynlib handle and links it against the ENGINE_CORE
    fn link_library(
        core_handle: CoreHandle,
        config: &EngineConfig,
        mdl: &'static DynModule,
        load: Option<HashMap<String, String>>,
    ) -> EngResult<(ModuleType, ModuleHandle)> {
        // Identifies the type of the module
        let m_type = mdl.mdl_type()?;

        let n_threads = config.n_threads;
        let m_config = config.mdlconfigs.get(&m_type.to_string()).ok_or_else(|| {
            MDL_CONFIG_NOT_PRESENT
                .clone()
                .with_root_cause(m_type.clone())
        })?;

        //grabs master handle to module context
        let handle = mdl.init(core_handle, n_threads, m_config.clone(), load)?;
        Ok((m_type, handle))
    }

    pub fn swap(&self) -> EngResult<()> {
        if self.synchro.check() {
            self.conclude()?;

            //NOTE: INCLUDE IMPORTANT CORE OPS HERE.
            self.frame_id.fetch_add(1, Ordering::SeqCst);
            self.commence()?;
            self.synchro.reset();
        };
        Ok(())
    }

    fn commence(&self) -> EngResult<()> {
        self.mdls.iter().try_for_each(|pair| pair.commence_phase())
    }
    fn conclude(&self) -> EngResult<()> {
        self.mdls.iter().try_for_each(|pair| pair.conclude_phase())
    }

    pub fn finish(&self) -> EngResult<()> {
        while !self.synchro.check() {}
        self.synchro.finish();
        Ok(())
    }

    pub fn frame_delta(&self) -> u32 {
        self.frame_delta
    }
}

///Runtime called functions
impl Core {
    ///Routes messages between modules
    pub fn emit_signal(&self, id: SignalID, signal: Signal) -> EngResult<()> {
        match id {
            SignalID::Ambient(name) => {
                println!("Ambient received signal {:?}", signal);
                self.amb_signals.insert(name, signal);
            }
            SignalID::Object { target, id } => {
                let mut obj = self
                    .objects
                    .get_mut(&target)
                    .ok_or_else(|| OBJECT_NOT_FOUND.clone().with_root_cause(&target))?;
                println!("Object {} received signal {:?}", target, signal);
                let moment = self.frame_id.load(std::sync::atomic::Ordering::SeqCst);
                let signal = TaggedSignal::new(signal, moment);
                obj.add_signal(&id, signal);
            }
        }
        Ok(())
    }

    pub fn search_signal(&self, id: &SignalID) -> Option<Signal> {
        match id {
            SignalID::Ambient(name) => self.amb_signals.get(name).map(|s| s.clone()), //NOTE: THIS
            //IS WRONG
            SignalID::Object { target, id } => {
                let mut obj = self.objects.get_mut(target)?;
                obj.find_signal(&id, 0).map(|s| s.clone())
            }
        }
    }

    fn create_partial(&self, info: CreateInfo) -> EngResult<()> {
        let parent = info.parent();
        match parent {
            Some(s) => {
                let mut obj = self
                    .objects
                    .get_mut(s)
                    .ok_or_else(|| OBJECT_NOT_FOUND.clone().with_root_cause(s))?;
                let (mdl, code, args) = info.into_parts();

                let handle = self
                    .mdls
                    .get(&mdl)
                    .ok_or_else(|| MDL_NOT_FOUND.clone().with_root_cause(mdl.clone()))?;
                let id = handle.create(code, args)?;
                let g_adress = GlobalAdress { mdl, id };
                obj.attach_partial(g_adress);
            }
            None => {
                let (mdl, code, args) = info.into_parts();
                let handle = self
                    .mdls
                    .get(&mdl)
                    .ok_or_else(|| MDL_NOT_FOUND.clone().with_root_cause(mdl.clone()))?;
                let id = handle.create(code, args)?;
                let g_adress = GlobalAdress { mdl, id };
                self.ambients.insert(g_adress);
            }
        }
        Ok(())
    }
    pub fn destroy_partial(&self, adress: GlobalAdress) -> EngResult<()> {
        let (mdl, id) = (adress.mdl, adress.id);
        self.mdls
            .get(&mdl)
            .ok_or_else(|| MDL_NOT_FOUND.clone().with_root_cause(mdl.clone()))?
            .destroy(id)
    }

    pub fn instantiate(&self, info: InstantiateInfo) -> EngResult<()> {
        //Generates non-duped name
        let mut counter = 0;
        let base = info.root_name();
        let mut name: EngString = format!("{base}/").into();
        while self.objects.contains_key(&name) {
            counter += 1;
            name = format!("{}_{}/", &base, counter).into();
        }

        let list = self
            .asset_manager
            .find_objects(info.obj_id, name, info.transform, info.args)?;

        println!("creating {} objects", list.len());
        list.into_iter()
            .try_for_each(|(name, meta)| -> EngResult<()> {
                let (fields, behaviours) = (meta.fields, meta.behaviours);
                //creates core object and inserts into context
                let abs_transform = meta
                    .transforms
                    .into_iter()
                    .reduce(|acc, val| acc + val)
                    .unwrap_or_default();
                self.objects
                    .insert(name.clone(), GameObject::new(fields, abs_transform)?);

                //caches the properties of the obj
                let properties = self
                    .objects
                    .get(&name)
                    .ok_or_else(|| SOMETHING_WENT_WRONG.clone())?
                    .get_properties();
                //sends create info for the tier one partials
                //NOTE: THIS IS A QUICK AND DIRTY SHIT, NEED TO REDO THIS LATER

                println!("got obj");
                behaviours.into_iter().try_for_each(|b_meta| {
                    println!("behaviour meta is {b_meta:?}");
                    let info = CreateInfo::default()
                        .with_mdl(ModuleType::Runtime(b_meta.lang))
                        .with_group(b_meta.assembly)
                        .with_id(b_meta.identifier)
                        .with_parent(Some(&name))
                        .attach_many(properties.clone());
                    self.create_partial(info)?;
                    println!("pushed mdl create");
                    Ok(())
                })?;

                Ok(())
            })?;
        Ok(())
    }

    pub fn eliminate(&self, instance: &str) -> EngResult<()> {
        self.objects.retain(|k, v| !k.starts_with(instance));
        Ok(())
    }

    pub fn get_transform(&self, obj: &str) -> Option<Transform> {
        self.objects.get(obj).map(|obj| obj.get_transform())
    }

    ///Adds the delta transform to every object bellow instance in the hierarchy
    pub fn change_transform(&self, instance: &str, delta: Transform) -> EngResult<()> {
        self.objects
            .iter_mut()
            .filter(|pair| pair.key().starts_with(instance))
            .for_each(|mut obj| obj.add_transform(delta.clone()));

        Ok(())
    }
}

fn emit_signal(id: SignalID, signal: Signal) {
    match ENGINE_CORE.get().unwrap().emit_signal(id, signal) {
        Err(e) => println!("{:?}", e.to_string()),
        _ => (),
    };
}

fn search_signal(signal_name: &SignalID) -> Option<Signal> {
    ENGINE_CORE.get().unwrap().search_signal(signal_name)
}

fn create_partial(info: CreateInfo) -> EngResult<()> {
    ENGINE_CORE.get().unwrap().create_partial(info)
}

fn destroy_partial(adress: GlobalAdress) -> EngResult<()> {
    ENGINE_CORE.get().unwrap().destroy_partial(adress)
}

fn instantiate(info: InstantiateInfo) -> EngResult<()> {
    ENGINE_CORE.get().unwrap().instantiate(info)
}

fn eliminate(instance: &str) -> EngResult<()> {
    ENGINE_CORE.get().unwrap().eliminate(instance)
}

fn get_transform(instance: &str) -> Option<Transform> {
    ENGINE_CORE.get().unwrap().get_transform(instance)
}

fn set_transform(instance: &str, delta: Transform) -> EngResult<()> {
    ENGINE_CORE.get().unwrap().change_transform(instance, delta)
}

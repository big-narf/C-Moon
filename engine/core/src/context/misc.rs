use std::sync::atomic::{AtomicBool, AtomicUsize, Ordering};
use std::sync::Barrier;

use lazy_static::lazy_static;

use libloading::{Library, Symbol};

use walkdir::WalkDir;

use cmoon_commons::*;

use crate::error::*;
use crate::LIBRARIES_PATH;

use super::*;

lazy_static! {
    pub(super) static ref LOADED_LIBRARIES: Vec<DynModule> =
        load_libraries().expect("FATAL ERROR: Module initialization failed!");
}

fn load_libraries() -> EngResult<Vec<DynModule>> {
    let libs_path = LIBRARIES_PATH
        .get()
        .ok_or_else(|| LIB_LOAD_ERROR.clone().with_root_cause("Path error"))?;

    println!("Looking for modules in: {:?}", libs_path);

    Ok(WalkDir::new(libs_path)
        .into_iter()
        .filter_map(|entry| {
            let entry = entry.ok()?;
            let file_path = entry.path();
            let lib = unsafe { Library::new(file_path).ok() }?.into();
            println!("Loading lib: {:?}", file_path.file_stem()?);
            Some(lib)
        })
        .collect())
}

///this is a utility wrapper around the unsafe library functions
pub struct DynModule(Library);

impl From<Library> for DynModule {
    fn from(value: Library) -> Self {
        Self(value)
    }
}

impl DynModule {
    pub fn init(
        &self,
        core_handle: CoreHandle,
        n_threads: usize,
        config: ModuleConfig,
        load: Option<HashMap<String, String>>,
    ) -> EngResult<ModuleHandle> {
        unsafe {
            let func: Symbol<
                '_,
                unsafe extern "C" fn(
                    CoreHandle,
                    usize,
                    ModuleConfig,
                    Option<HashMap<String, String>>,
                ) -> EngResult<ModuleHandle>,
            > = self
                .0
                .get(b"initialize_module")
                .map_err(|e| LIB_LOAD_ERROR.clone().with_root_cause("initialize_module?"))?;
            func(core_handle, n_threads, config, load)
        }
    }

    pub fn mdl_type(&self) -> EngResult<ModuleType> {
        unsafe {
            let func: Symbol<'_, unsafe extern "C" fn() -> ModuleType> = self
                .0
                .get(b"module_type")
                .map_err(|e| LIB_LOAD_ERROR.clone().with_root_cause("module_type?"))?;
            Ok(func())
        }
    }
}

#[derive(Debug)]
pub struct Synchronizer {
    n_threads: usize,
    frame_barrier: Barrier,
    n_idle: AtomicUsize,
    end_flag: AtomicBool,
}

impl Synchronizer {
    pub fn new(n_threads: usize) -> Self {
        Self {
            n_threads,
            frame_barrier: Barrier::new(n_threads + 1),
            n_idle: Default::default(),
            end_flag: Default::default(),
        }
    }
}

impl Synchronizer {
    pub fn threads_waiting(&self) -> usize {
        self.n_idle.load(Ordering::SeqCst)
    }

    pub fn n_threads(&self) -> usize {
        self.n_threads
    }

    ///Resets the counter and unblocks the slaves
    pub fn reset(&self) {
        self.n_idle.store(0, Ordering::SeqCst);
        self.frame_barrier.wait();
    }

    pub fn finish(&self) {
        self.end_flag.store(true, Ordering::SeqCst);
        self.frame_barrier.wait();
    }

    ///Checks if the frame can be reseted already
    pub fn check(&self) -> bool {
        let v = self.n_idle.load(Ordering::SeqCst);
        v >= self.n_threads
    }

    pub fn slave_wait(&self) -> bool {
        if self.end_flag.load(Ordering::SeqCst) {
            true
        } else {
            let n = self.n_idle.fetch_add(1, Ordering::SeqCst);
            self.frame_barrier.wait();
            false
        }
    }
}

pub struct ObjMeta {
    pub fields: Vec<(EngString, Property)>,
    pub transforms: Vec<Transform>,
    pub behaviours: Vec<BehaviourMeta>,
}

#[derive(Clone)]
pub struct Assignment {
    inner: Vec<ModuleHandle>,
}

impl Assignment {
    ///Returns an iterator that will process the create and destroy events for each module
    pub fn alter_phase(&self, thread_id: usize) -> EngResult<()> {
        self.inner
            .iter()
            .try_for_each(|mdl| mdl.alter_phase(thread_id))
    }

    ///Returns an iterator that will process the update for all the elements in each module
    pub fn process_phase(&self, thread_id: usize, dt: u32) -> EngResult<()> {
        self.inner
            .iter()
            .map(|mdl| mdl.process_phase(thread_id, dt))
            .collect()
    }
}

impl From<Vec<ModuleHandle>> for Assignment {
    fn from(value: Vec<ModuleHandle>) -> Self {
        Self { inner: value }
    }
}

use std::thread;

use std::sync::Arc;

use cmoon_commons::*;

use super::*;

pub struct Worker {
    id: usize,
    load: Assignment,
    synchro: Arc<Synchronizer>,
}

impl Worker {
    pub fn new(id: usize, load: Vec<ModuleHandle>, synchro: Arc<Synchronizer>) -> Self {
        Self {
            id,
            load: load.into(),
            synchro,
        }
    }

    /// This is effectively the main function of each thread.
    /// When called consumes the worker and returns a handle to the spawn thread.
    pub fn launch(self) -> thread::JoinHandle<()> {
        let thread_id = self.id;
        let load = self.load;
        thread::Builder::new()
            .name(format!("Worker_{thread_id}"))
            .spawn(move || {
                println!("launching thread : {thread_id}!!!");

                //worker look
                while !self.synchro.slave_wait() {
                    load.process_phase(thread_id, 10)
                        .thread_fail(&format!("PROCESS FAILURE AT THREAD {thread_id}."));

                    load.alter_phase(thread_id)
                        .thread_fail(&format!("ALTER FAILURE AT THREAD {thread_id}."));
                    //phase change sync
                    //²broadcast.wait();
                    println!("worker {thread_id} finished frame");
                }
            })
            .unwrap()
    }
}

trait ThreadFail<T> {
    fn thread_fail(self, msg: &str) -> T;
}

impl<T> ThreadFail<T> for EngResult<T> {
    fn thread_fail(self, msg: &str) -> T {
        match self {
            Ok(v) => v,
            Err(e) => {
                eprintln!("{e}");
                panic!("{msg}");
            }
        }
    }
}

use std::error::Error;
pub use std::io::{Error as IOError, Result as IOResult};
use std::sync::PoisonError;

pub use libloading::Error as LoadError;
pub use rusqlite::{Error as SQLError, Result as SQLResult};
pub use serde_json::Error as JError;
pub use winit::raw_window_handle::HandleError as WindowError;

//codec errors

use lazy_static::lazy_static;

use cmoon_commons::*;

///Converts DB error to engine error
pub trait IntoEngError {
    fn into_eng_error(self) -> EngineError;
}

impl IntoEngError for SQLError {
    fn into_eng_error(self) -> EngineError {
        match self {
            Self::UserFunctionError(e) => e.into_eng_error(),
            _ => SOMETHING_WENT_WRONG.clone(),
        }
    }
}

impl IntoEngError for Box<dyn Error + Send + Sync> {
    fn into_eng_error(self) -> EngineError {
        if let Some(e) = self.downcast_ref::<EngineError>() {
            e.clone()
        } else {
            SOMETHING_WENT_WRONG
                .clone()
                .with_root_cause("Couldnt downcast SQL error!")
        }
    }
}

impl<T> IntoEngError for PoisonError<T> {
    fn into_eng_error(self) -> EngineError {
        SYNC_ERROR.clone().with_root_cause(self)
    }
}

impl IntoEngError for WindowError {
    fn into_eng_error(self) -> EngineError {
        WINDOW_HANDLE_FAIL.clone().with_root_cause(self)
    }
}

impl IntoEngError for IOError {
    fn into_eng_error(self) -> EngineError {
        let kind = self.kind();
        CODEC_ERROR.clone().with_root_cause(kind)
    }
}

impl IntoEngError for JError {
    fn into_eng_error(self) -> EngineError {
        CODEC_ERROR.clone().with_root_cause("JSON")
    }
}

pub trait FromEngError {
    fn from_eng_error(value: EngineError) -> Self;
}

impl FromEngError for SQLError {
    fn from_eng_error(value: EngineError) -> Self {
        Self::UserFunctionError(Box::from(value))
    }
}

pub trait AsMsg {
    fn as_msg(self) -> String;
}

impl AsMsg for JError {
    fn as_msg(self) -> String {
        let collumn = self.column();
        let line = self.line();
        match self.classify() {
            serde_json::error::Category::Io => format!("IOerr@{line}:{collumn}"),
            serde_json::error::Category::Eof => format!("EOF@{line}:{collumn}"),
            serde_json::error::Category::Data => format!("BadType@{line}:{collumn}"),
            serde_json::error::Category::Syntax => format!("BadSyntax@{line}:{collumn}"),
        }
    }
}

pub trait Cast {
    type Target;
    fn cast(self) -> Self::Target;
}

impl<T, E: IntoEngError> Cast for Result<T, E> {
    type Target = EngResult<T>;
    fn cast(self) -> Self::Target {
        self.map_err(|e| e.into_eng_error())
    }
}

pub trait CastSQL<T> {
    fn cast_sql(self) -> SQLResult<T>;
}

impl<T> CastSQL<T> for EngResult<T> {
    fn cast_sql(self) -> SQLResult<T> {
        self.map_err(|e| SQLError::from_eng_error(e))
    }
}

impl<T> CastSQL<T> for IOResult<T> {
    fn cast_sql(self) -> SQLResult<T> {
        self.map_err(|e| SQLError::UserFunctionError(Box::new(e)))
    }
}

macro_rules! static_error {
    ($name:ident, $code:expr, $msg:expr) => {
        lazy_static! {
            pub static ref $name: EngineError = EngineError::default()
                .from_core()
                .with_specifics($code, $msg);
        }
    };
}

//NOTE: 91XX SERIES (INIT ERRORS)
static_error!(LIB_LOAD_ERROR, 101, "Failed to load library");
static_error!(
    MDL_CONFIG_NOT_PRESENT,
    102,
    "Couldn't find configuration for a loaded module"
);
static_error!(NO_CONTENT, 103, "Game content missing or corrupted.");
static_error!(BAD_CONFIG, 104, "Config not found or misformed");
static_error!(
    WINDOW_HANDLE_FAIL,
    105,
    "Failed to grab handle to the window"
);

//NOTE: 92XX SERIES (OPS ERRORS)
static_error!(MDL_NOT_FOUND, 201, "No loaded module of the given type");
static_error!(
    SYNC_ERROR,
    202,
    "Synchronization error occured on the core!"
);

static_error!(CODEC_ERROR, 203, "An error occurred while decoding!");

static_error!(
    UNRECOGNIZED_FORMAT,
    204,
    "The codec format was not recognized"
);

static_error!(OBJECT_NOT_FOUND, 301, "No object with the given id");

static_error!(MALFORMED_BLUEPRINT, 302, "Blueprint has a malformed field");

static_error!(
    MISMATCHED_PARAMS,
    304,
    "Instantiation parameters do not match blueprint"
);

static_error!(
    SOMETHING_WENT_WRONG,
    999,
    "Something went wrong. IDK what tho"
);

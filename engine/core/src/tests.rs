use std::thread::JoinHandle;
use std::time::Instant;

use winit::application::ApplicationHandler;
use winit::event_loop::{ActiveEventLoop, EventLoop};
use winit::platform::x11::EventLoopBuilderExtX11;
use winit::window::Window;

use context::*;

use cmoon_commons::*;

use crate::*;

#[test]
fn grab_resource_test() {
    ASSET_PATH
        .set(dirs::home_dir().unwrap().join("test/assets.db"))
        .unwrap();
    let manager = AssetManager::new().unwrap();
    let start = std::time::Instant::now();
    let res = manager.find_resource(5).unwrap();
    let elapsed = start.elapsed();
    println!("DECODED IN {} millis", 1000.0 * elapsed.as_secs_f64());
    //println!("RES IS {:?}", res);
}

//#[test]
fn run_test() {
    ASSET_PATH
        .set(dirs::home_dir().unwrap().join("test/assets.db"))
        .unwrap();
    LIBRARIES_PATH
        .set(dirs::home_dir().unwrap().join("engine_test/engine"))
        .unwrap();
    let event_loop = EventLoop::with_user_event()
        .with_any_thread(true)
        .build()
        .expect("Error creating event loop");

    let mut game = TestApp::new(&event_loop);
    event_loop.run_app(&mut game).expect("EVENT LOOP ERROR");
}

#[derive(Debug)]
pub struct TestApp {
    loop_proxy: EventLoopProxy<TestEvent>,
    handles: Vec<JoinHandle<()>>,

    window: Option<Window>,
    synchro: Option<Arc<Synchronizer>>,
    frame_time: time::Instant,
}

#[derive(Clone, Debug, Default)]
pub enum TestEvent {
    #[default]
    IDK,
}

impl TestApp {
    pub fn new(event_loop: &EventLoop<TestEvent>) -> Self {
        Self {
            loop_proxy: event_loop.create_proxy(),
            handles: Default::default(),
            window: Default::default(),
            synchro: Default::default(),
            frame_time: time::Instant::now(),
        }
    }
}

impl ApplicationHandler<TestEvent> for TestApp {
    fn resumed(&mut self, event_loop: &ActiveEventLoop) {
        //sets up the engine core with the operational parameters creating the thread assignments
        let window = event_loop
            .create_window(Window::default_attributes())
            .unwrap();
        let assignments = context::ENGINE_CORE.initialize(&window).unwrap();

        let synchro = Synchronizer::new(assignments.len()).into();
        self.window = window.into();

        //main thread keeps busy managing the window.
        //passes the thread assignments to the core and launches it.
        self.handles = context::ENGINE_CORE
            .execute(assignments, &synchro)
            .expect("EXECUTION ERROR!");

        self.synchro = Some(synchro);
    }

    fn about_to_wait(&mut self, _event_loop: &ActiveEventLoop) {
        match self.synchro.as_ref() {
            Some(s) => {
                if s.check() {
                    s.reset();
                    let dt = self.frame_time.elapsed();
                    println!("DT: {} secs", dt.as_secs_f64());
                    self.frame_time = Instant::now();
                };
            }
            None => (),
        }
    }

    fn window_event(
        &mut self,
        event_loop: &ActiveEventLoop,
        window_id: WindowId,
        event: WindowEvent,
    ) {
        match event {
            WindowEvent::CloseRequested => {
                self.window = None;
                event_loop.exit();
            }
            WindowEvent::KeyboardInput {
                device_id,
                event,
                is_synthetic,
            } => {
                if event.physical_key == KeyCode::Space {
                    let info = InstantiateInfo::default()
                        .with_id("tester")
                        .with_name("thing");
                    context::ENGINE_CORE.instantiate(info).unwrap();
                }
                if event.physical_key == KeyCode::Enter {
                    let name = "thing/";
                    context::ENGINE_CORE.eliminate(name).unwrap();
                }
                if event.physical_key == KeyCode::ShiftRight {
                    let signal = Signal::default();
                    let id = SignalID {
                        signal_type: "TEST_SIGNAL".to_string(),
                        object: None,
                    };
                    context::ENGINE_CORE.emit_signal(id, signal).unwrap();
                }
            }
            _ => (),
        }
    }

    fn user_event(&mut self, event_loop: &ActiveEventLoop, event: TestEvent) {
        let dt = self.frame_time.elapsed().subsec_millis();
        ENGINE_CORE.update_timestep(dt);

        self.frame_time = time::Instant::now();
    }

    fn suspended(&mut self, event_loop: &ActiveEventLoop) {
        //joins all the threads for finishing execution
        self.handles
            .drain(..)
            .try_for_each(|handle| handle.join())
            .expect("ERROR: Thread join Error.");
    }

    fn exiting(&mut self, event_loop: &ActiveEventLoop) {
        //ENGINE_CORE.signal_close();

        let synchro = self.synchro.take().unwrap();
        while !synchro.check() {}
        synchro.finish();
        self.handles
            .drain(..)
            .try_for_each(|handle| handle.join())
            .expect("ERROR: Thread join Error.");
    }
}

#!/bin/sh

mode=$1
target=$2

set -e

if [ ! -d "$target" ]; then
    mkdir -p "$target"
fi


cd core || exit 1
chmod +x operations.sh
./operations.sh "$mode" "$target"
cd .. || exit 1

mkdir -p "$target/modules"
for mdl in modules/*/; do
    if [ -d "$mdl" ]; then
        cd "$mdl" || exit 1
        #chmod +x operations.sh
        ./operations.sh "$mode" "$target/modules"
        cd ../.. || exit 1
    fi
done

#cd modules || exit 1
#chmod +x operations.sh
#./operations.sh "$mode" "$target/modules"
#cd .. || exit 1

exit 0

use std::collections::HashMap;
use std::ops::Deref;
use std::sync::{Arc, Weak};

use mlua::{prelude::*, Variadic};

use cmoon_commons::*;

use super::*;

///This is a thread local context
#[derive(Debug)]
pub struct ThreadCtx {
    thread_id: usize,
    lua: Lua,

    destroy_list: Mutex<Vec<usize>>,

    instantiator_fn: LuaFunction,
    destructor_fn: LuaFunction,
    process_fn: LuaFunction,
}

impl ThreadCtx {
    pub fn new(std_mdl: &str, api: EngAPI, thread_id: usize) -> LuaResult<Self> {
        let lua = Lua::new_with(
            LuaStdLib::ALL_SAFE,
            LuaOptions::new().catch_rust_panics(false),
        )?;

        lua.globals().set(
            "print",
            LuaFunction::wrap(|args: LuaMultiValue| {
                let val: String = args
                    .into_iter()
                    .filter_map(|v| v.to_string().ok())
                    .map(|mut s| {
                        s.push_str("    ");
                        s
                    })
                    .collect();
                println!("{val}");
                Ok(())
            }),
        )?;

        //Module loader
        let c_cache = api.code_cache.clone(); //NOTE: CHANGE THIS LATER!!!
        let mdl_loader = lua.create_function(move |lua, mdl_name: String| {
            let assembly = c_cache.get(&mdl_name).ok_or_else(|| {
                LuaError::external(CODE_ASSEMBLY_UNKNOWN.clone().with_root_cause(&mdl_name))
            })?;
            let env: LuaTable = lua.create_table()?;
            let env_meta = lua.create_table()?;
            env_meta.set("__index", lua.globals())?;
            env.set_metatable(Some(env_meta));
            let chunk = lua
                .load(assembly)
                .set_environment(env.clone())
                .set_name(&mdl_name);
            let locals: Option<LuaTable> = chunk.eval()?;
            match locals {
                Some(l) => {
                    println!("ENG DEBUG: loaded assembly :{} successefully", mdl_name);
                    Ok(l)
                }
                None => Ok(env),
            }
        })?;

        let api_loader = lua.create_function(move |lua, _: String| Ok(api.clone()))?;

        //Searcher function
        let searcher = LuaFunction::wrap(move |identifier: String| {
            let loader: LuaFunction = if identifier == "cmoon" {
                api_loader.clone()
            } else {
                mdl_loader.clone()
            };
            Ok(loader)
        });
        lua.globals()
            .get::<LuaTable>("package")?
            .get::<LuaTable>("searchers")?
            .push(searcher)?;

        let require_fn: LuaFunction = lua.globals().get("require")?;
        let _: LuaTable = require_fn.call(std_mdl)?;

        let instantiator_fn = lua
            .named_registry_value("constructor")
            .lua_context("INSTANTIATOR FUNCTION COULDN'T BE FOUND")?;
        let destructor_fn = lua
            .named_registry_value("destructor")
            .lua_context("DESTRUCTOR FUNCTION COULDN'T BE FOUND")?;
        let process_fn = lua
            .named_registry_value("process")
            .lua_context("PROCESS FUNCTION COULDN'T BE FOUND")?;

        Ok(Self {
            thread_id,
            lua,

            destroy_list: Default::default(),

            instantiator_fn,
            destructor_fn,
            process_fn,
        })
    }

    /// creates the object inside the lua state, returns the local adress
    pub fn add_obj(
        &self,
        group: EngString,
        id: EngString,
        parent: Option<EngString>,
        attch: EngMap<DynValue>,
    ) -> LuaResult<(usize, usize)> {
        let properties = attch.from_eng(&self.lua)?;
        let local = self.instantiator_fn.call((
            group.as_str(),
            id.as_str(),
            parent.as_ref().map(|s| s.as_str()),
            properties,
        ))?;
        Ok((self.thread_id, local))
    }

    pub fn mark_destroy(&self, local_id: usize) -> EngResult<()> {
        let mut lock = self.destroy_list.lock().cast()?;
        lock.push(local_id);
        Ok(())
    }

    ///destroy all objects marked for destruction on the current thread
    pub fn remove_garbage(&self) -> LuaResult<()> {
        let mut lock = self.destroy_list.lock().unwrap();
        let r = lock
            .drain(..)
            .try_for_each(|id| self.destructor_fn.call(id));
        r
    }

    pub fn process(&self, dt: u32) -> LuaResult<()> {
        self.process_fn.call(dt)
    }
}

///DEBUG STUFF
impl ThreadCtx {
    pub fn valid_id(&self) -> Option<usize> {
        let get_fn: LuaFunction = self.lua.named_registry_value("get_id_fn").unwrap();
        get_fn.call(()).unwrap()
    }
}

///this is an object that will hold all the low level lua API needed.
#[derive(Clone, Debug)]
pub struct EngAPI {
    code_cache: Arc<HashMap<String, String>>,

    signal_emitter: fn(SignalID, Signal),
    signal_searcher: fn(&SignalID) -> Option<Signal>,

    partial_creator: fn(CreateInfo) -> EngResult<()>,
    partial_destroyer: fn(GlobalAdress) -> EngResult<()>,

    instantiator: fn(InstantiateInfo) -> EngResult<()>,
    eliminator: fn(&str) -> EngResult<()>,
}

impl EngAPI {
    pub fn new(core: CoreHandle, code_cache: &Arc<HashMap<String, String>>) -> Self {
        Self {
            code_cache: code_cache.clone(),
            signal_emitter: core.signal_emitter,
            signal_searcher: core.signal_searcher,
            partial_creator: core.partial_creator,
            partial_destroyer: core.partial_destroyer,
            instantiator: core.instantiator,
            eliminator: core.eliminator,
        }
    }
}

impl LuaUserData for EngAPI {
    fn add_methods<M: LuaUserDataMethods<Self>>(methods: &mut M) {
        //NOTE: CALLBACK RELATED

        methods.add_method("constructor_fn", |lua, _, f: LuaFunction| {
            lua.set_named_registry_value("constructor", f)
        });

        methods.add_method("destructor_fn", |lua, _, f: LuaFunction| {
            lua.set_named_registry_value("destructor", f)
        });

        methods.add_method("process_fn", |lua, _, f: LuaFunction| {
            lua.set_named_registry_value("process", f)
        });

        //NOTE: GO RELATED
        methods.add_method("instantiate", |_, this, info: LuaValue| {
            let info = ToEng::to_eng(info)?;
            (this.instantiator)(info).into_lua_err()
        });
        methods.add_method("eliminate", |_, this, instance: String| {
            (this.eliminator)(&instance).into_lua_err()
        });

        //NOTE: PARTIAL RELATED

        methods.add_method(
            "create_partial",
            |_,
             this,
             (mdl, id, group, parent, attch): (
                String,
                String,
                String,
                Option<String>,
                LuaValue,
            )| {
                let mdl = mdl.parse().into_lua_err()?;
                let attch: EngMap<DynValue> = ToEng::to_eng(attch)?;
                let info = CreateInfo::default()
                    .with_mdl(mdl)
                    .with_id(id)
                    .with_group(group)
                    .with_parent(parent)
                    .attach_many(attch);
                (this.partial_creator)(info).into_lua_err()
            },
        );

        methods.add_method("destroy_partial", |_, this, (mdl, id): (String, usize)| {
            let mdl = mdl.parse().into_lua_err()?;
            let g_adress = GlobalAdress { mdl, id };
            (this.partial_destroyer)(g_adress).into_lua_err()
        });

        //NOTE: SIGNAL RELATED API

        methods.add_method(
            "emmit_signal",
            |_, this, (id, signal): (LuaValue, LuaValue)| {
                let id = SignalID::to_eng(id)?;
                let signal = Signal::to_eng(signal)?; //NOTE: THIS NEEDS WORK.
                (this.signal_emitter)(id, signal);
                Ok(())
            },
        );

        methods.add_method("search_signal", |lua, this, id: LuaValue| {
            let id = SignalID::to_eng(id)?;
            match (this.signal_searcher)(&id) {
                Some(s) => s.from_eng(lua),
                None => LuaValue::Nil.into_lua(lua),
            }
        });
        //NOTE: TYPES RELATED STUFF
    }
}

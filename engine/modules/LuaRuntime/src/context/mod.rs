use std::collections::HashMap;
use std::sync::{Arc, Mutex, MutexGuard, RwLock};

use crossbeam::queue::SegQueue;
use dashmap::DashMap;
use mlua::prelude::*;

use cmoon_commons::*;

//use crate::elements::*;

use crate::infra::*;

mod api;
use api::*;

#[derive(Debug)]
pub struct Runtime {
    //lua states
    context_set: Vec<ThreadCtx>,

    //partial related stuff
    id_table: DashMap<usize, (usize, usize)>, // usize to LocalAdress map
    create_queue: SegQueue<ScheduledCreate>,
    //caches
    //code_cache: Arc<HashMap<String, String>>,
    //bin_cache: Arc<HashMap<u32, ResourceRef>>,
}

impl Runtime {
    pub fn new(
        handle: CoreHandle,
        n_threads: usize,
        config: ModuleConfig,
        load: Option<HashMap<String, String>>,
    ) -> EngResult<Self> {
        //println!("FOUND ASSEMBLIES: {:?}", code_cache.keys());
        let code_cache = Arc::from(load.unwrap());

        let std_mdl: String = config
            .get("init")
            .ok_or_else(|| INIT_BAD_CONFIG.clone().with_root_cause("init"))?;

        let api = EngAPI::new(handle, &code_cache);
        let context_set = (0..n_threads)
            .map(|thread_id| ThreadCtx::new(&std_mdl, api.clone(), thread_id))
            .collect::<Result<_, _>>()
            .cast()?;
        Ok(Self {
            context_set,
            id_table: Default::default(),
            create_queue: Default::default(),
        })
    }
}

impl cmoon_commons::Module for Runtime {
    fn get_type() -> ModuleType {
        crate::MDL_TYPE.clone()
    }

    fn alter(&self, thread_id: usize) -> EngResult<()> {
        //println!("ALTERING");
        let ctx = self.get_context(thread_id)?;

        //create new objects
        while let Some(scheduled) = self.create_queue.pop() {
            let id = scheduled.id;
            let code = scheduled.code;
            let args = scheduled.args;
            let local_adress = ctx
                .add_obj(code.group, code.identifier, code.parent, args)
                .cast()?;
            self.id_table.insert(id, local_adress);
        }

        //clear dead objects
        ctx.remove_garbage().cast()?;

        Ok(())
    }

    fn process(&self, thread_id: usize, dt: u32) -> EngResult<()> {
        //println!("PROCESSING");
        let ctx = self.get_context(thread_id)?;
        ctx.process(dt).cast()
    }

    fn commence(&self) -> EngResult<()> {
        //println!("SWAPPING");
        Ok(())
    }

    fn conclude(&self) -> EngResult<()> {
        //println!("SWAPPING");
        Ok(())
    }

    fn create(&self, code: PartialCode, args: PartialArgs) -> EngResult<usize> {
        let id = self.id_table.len();
        let scheduled = ScheduledCreate { id, code, args };
        self.create_queue.push(scheduled);
        Ok(id)
    }

    fn destroy(&self, id: usize) -> EngResult<()> {
        let (thread_id, local_id) = self
            .id_table
            .remove(&id)
            .ok_or_else(|| INVALID_DESTROY_REQUEST.clone())?
            .1;

        let ctx = self.get_context(thread_id)?;
        ctx.mark_destroy(local_id)
    }
}

///internally used functions
impl Runtime {
    fn get_context(&self, thread_id: usize) -> EngResult<&ThreadCtx> {
        self.context_set
            .get(thread_id)
            .ok_or_else(|| FORBIDDEN_THREAD_ID.clone().with_root_cause(thread_id))
    }
}

struct ScheduledCreate {
    id: usize,
    code: PartialCode,
    args: PartialArgs,
}

mod context;
//mod elements;
mod infra;

#[cfg(test)]
mod tests;

#[macro_use]
use cmoon_commons::*;
use lazy_static::lazy_static;

lazy_static! {
    static ref MDL_TYPE: ModuleType = ModuleType::Runtime("Lua".into());
}

//Implements unsafe interface functions
module_interface!(context::Runtime, MDL_TYPE);

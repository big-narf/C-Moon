use std::collections::HashMap;

use mlua::prelude::*;

use cmoon_commons::*;

use super::*;

pub trait GetRef<T: LuaUserData> {
    fn get_ref<K: IntoLua>(&self, key: K) -> LuaResult<LuaUserDataRef<T>>;

    fn get_ref_mut<K: IntoLua>(&self, key: K) -> LuaResult<LuaUserDataRefMut<T>>;
}

impl<T: LuaUserData + 'static> GetRef<T> for LuaTable {
    fn get_ref<K: IntoLua>(&self, key: K) -> LuaResult<LuaUserDataRef<T>> {
        self.get(key)
    }
    fn get_ref_mut<K: IntoLua>(&self, key: K) -> LuaResult<LuaUserDataRefMut<T>> {
        self.get(key)
    }
}

///Local trait to convert to and fro the core
pub trait FromEng {
    fn from_eng(self, lua: &Lua) -> LuaResult<LuaValue>;
}

pub trait GetAs<T> {
    fn get_as(&self, id: &str) -> LuaResult<T>;
}

impl<T: ToEng> GetAs<T> for LuaTable {
    fn get_as(&self, id: &str) -> LuaResult<T> {
        T::to_eng(self.get(id)?)
    }
}

///Receives an wrapped lua value and clone-converts to an engine value.
pub trait ToEng: Sized + Clone {
    fn to_eng(value: LuaValue) -> LuaResult<Self>;
}

impl ToEng for EngString {
    fn to_eng(value: LuaValue) -> LuaResult<Self> {
        match value {
            LuaValue::String(s) => Ok(Self::from(s.to_str()?.deref())),
            _ => Err(LuaError::FromLuaConversionError {
                from: "not string",
                to: "eng string".into(),
                message: None,
            }),
        }
    }
}

impl FromEng for EngString {
    fn from_eng(self, lua: &Lua) -> LuaResult<LuaValue> {
        self.as_str().into_lua(lua)
    }
}

impl ToEng for Transform {
    fn to_eng(value: LuaValue) -> LuaResult<Self> {
        match value {
            LuaValue::Table(t) => {
                let x = t.get(0)?;
                let y = t.get(1)?;
                let z = t.get(2)?;

                Ok(Self { pos: [x, y, z] })
            }
            _ => Err(LuaError::FromLuaConversionError {
                from: "not table",
                to: "transform".into(),
                message: None,
            }),
        }
    }
}

impl ToEng for DynValue {
    fn to_eng(value: LuaValue) -> LuaResult<DynValue> {
        match value {
            LuaValue::UserData(ud) => {
                if ud.is::<Lunar<AssetHandle>>() {
                    let concrete = ud.borrow::<Lunar<AssetHandle>>()?.clone_inner();
                    Ok(DynValue::Asset(concrete))
                } else if ud.is::<Lunar<BlueprintHandle>>() {
                    let concrete = ud.borrow::<Lunar<BlueprintHandle>>()?.clone_inner();
                    Ok(DynValue::Blueprint(concrete))
                } else {
                    Err(LuaError::runtime("Unimplemented!"))
                }
            }
            LuaValue::Number(n) => Ok(DynValue::Number(n)),
            LuaValue::Integer(n) => Ok(DynValue::Number(n as f64)),
            LuaValue::String(s) => Ok(DynValue::Text(s.to_string_lossy())),
            _ => Err(LuaError::FromLuaConversionError {
                from: "something",
                to: "dyn value".into(),
                message: None,
            }),
        }
    }
}

impl ToEng for InstantiateInfo {
    fn to_eng(value: LuaValue) -> LuaResult<Self> {
        match value {
            LuaValue::Table(t) => {
                let handle = t.get_as("handle")?;
                let obj_id = t.get_as("id")?;
                let parent = t.get_as("parent").ok();
                let transform = t.get_as("transform")?;
                Ok(Self {
                    transform,
                    handle,
                    obj_id,
                    parent,
                    args: Default::default(),
                })
            }
            _ => Err(LuaError::FromLuaConversionError {
                from: "not table",
                to: "instantiate info".into(),
                message: None,
            }),
        }
    }
}

impl ToEng for SignalID {
    fn to_eng(value: LuaValue) -> LuaResult<Self> {
        match value {
            LuaValue::Table(t) => {
                let signal_type = t.raw_get("signal_type")?;
                Ok(match t.raw_get::<Option<LuaValue>>("target")? {
                    Some(target) => Self::Object {
                        target: EngString::to_eng(target)?,
                        id: EngString::to_eng(signal_type)?,
                    },
                    None => Self::Ambient(EngString::to_eng(signal_type)?),
                })
            }
            _ => Err(LuaError::FromLuaConversionError {
                from: "not table",
                to: "signal".into(),
                message: None,
            }),
        }
    }
}

impl ToEng for Signal {
    fn to_eng(value: LuaValue) -> LuaResult<Self> {
        match value {
            LuaValue::Table(t) => {
                let strenght = t.raw_get("strenght")?;
                let delta = t.raw_get("delta")?;
                let attch: EngMap<DynValue> = t.get_as("attatched")?;
                Ok(Self::default()
                    .with_strenght(strenght)
                    .with_delta(delta)
                    .extend(attch))
            }
            _ => Err(LuaError::FromLuaConversionError {
                from: "not table",
                to: "signal".into(),
                message: None,
            }),
        }
    }
}

impl FromEng for Signal {
    fn from_eng(self, lua: &Lua) -> LuaResult<LuaValue> {
        let map = self.into();
        EngMap::from_eng(map, lua)
    }
}

impl<T: FromEng> FromEng for EngMap<T> {
    fn from_eng(self, lua: &Lua) -> LuaResult<LuaValue> {
        let map = lua.create_table()?;
        self.into_iter()
            .try_for_each(|(k, v)| map.raw_set(k.from_eng(lua)?, v.from_eng(lua)?))?;
        map.into_lua(lua)
    }
}

impl<T: ToEng> ToEng for EngMap<T> {
    fn to_eng(value: LuaValue) -> LuaResult<Self> {
        match value {
            LuaValue::Table(t) => t
                .pairs()
                .map(|pair| {
                    let (k, v) = pair?;
                    Ok((EngString::to_eng(k)?, T::to_eng(v)?))
                })
                .collect(),
            _ => Err(LuaError::FromLuaConversionError {
                from: "not table",
                to: "eng map".into(),
                message: None,
            }),
        }
    }
}

impl FromEng for DynValue {
    fn from_eng(self, lua: &Lua) -> LuaResult<LuaValue> {
        match self {
            Self::Number(n) => n.into_lua(lua),
            Self::Text(t) => t.into_lua(lua),
            Self::Asset(h) => Lunar::from(h).into_lua(lua),
            Self::Blueprint(b) => Lunar::from(b).into_lua(lua),
            Self::Data(d) => {
                let s = BString::from(d.to_vec());
                s.into_lua(lua)
            }
            _ => panic!("UINPLEMENTED!!!!"),
        }
    }
}

impl FromEng for AssetData {
    fn from_eng(self, lua: &Lua) -> LuaResult<LuaValue> {
        match self {
            Self::Simple(d) => d.from_eng(lua),
            Self::Composite(m) => m.from_eng(lua),
        }
    }
}

use std::any::Any;
use std::ops::{Deref, DerefMut};
use std::sync::Arc;

use mlua::{prelude::*, BString};

use cmoon_commons::*;

mod collections;
mod convert;
mod error;

pub use collections::*;
pub use convert::*;
pub use error::*;

pub type LunarRef<T> = LuaUserDataRef<Lunar<T>>;

///Wrapper around T that is a lua user data
#[derive(Debug)]
pub struct Lunar<T>(T);

impl<T: Clone> Lunar<T> {
    pub fn clone_inner(&self) -> T {
        self.0.clone()
    }
}

impl<T: Clone> Clone for Lunar<T> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl<T> From<T> for Lunar<T> {
    fn from(value: T) -> Self {
        Self(value)
    }
}

impl<T> Deref for Lunar<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> DerefMut for Lunar<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

/*
impl<T: Any> FromLua for Lunar<T> {
    fn from_lua(value: LuaValue, lua: &Lua) -> LuaResult<Self> {
        match value {
            LuaValue::UserData(ud) => ud.take::<Self>(),
            _ => Err(LuaError::FromLuaConversionError {
                from: "not ud",
                to: "lunar wrapper".to_owned(),
                message: None,
            }),
        }
    }
}*/

impl LuaUserData for Lunar<AssetHandle> {
    fn add_methods<M: LuaUserDataMethods<Self>>(methods: &mut M) {
        methods.add_method("load", |lua, this, ()| {
            let data = this.load().into_lua_err()?;
            data.from_eng(lua)
        })
    }
}

impl LuaUserData for Lunar<BlueprintHandle> {}

use std::ops::{Deref, DerefMut};
use std::sync::PoisonError;

use lazy_static::lazy_static;
use mlua::prelude::*;

use cmoon_commons::*;

///Trait to convert LuaResult to a EngineResult, converting error
pub trait Cast {
    type Target;
    fn cast(self) -> Self::Target;
}

impl<T, E: IntoEngError> Cast for Result<T, E> {
    type Target = EngResult<T>;
    fn cast(self) -> Self::Target {
        self.map_err(|e| e.into_eng_error())
    }
}

pub trait WithRoot {
    fn with_root(self, root: &str) -> Self;
}

impl<T> WithRoot for EngResult<T> {
    fn with_root(self, root: &str) -> Self {
        self.map_err(|e| e.with_root_cause(root))
    }
}

pub trait LuaContext<T> {
    fn lua_context<S: Into<String>>(self, ctx: S) -> LuaResult<T>;
}

impl<T> LuaContext<T> for Option<T> {
    fn lua_context<S: Into<String>>(self, ctx: S) -> LuaResult<T> {
        match self {
            Some(val) => Ok(val),
            None => Err(LuaError::RuntimeError(ctx.into())),
        }
    }
}

impl<T> LuaContext<T> for LuaResult<T> {
    fn lua_context<S: Into<String>>(self, ctx: S) -> LuaResult<T> {
        LuaErrorContext::context(self, ctx.into())
    }
}

///Converts Lua error to engine error
pub trait IntoEngError {
    fn into_eng_error(self) -> EngineError;
}

impl<T> IntoEngError for PoisonError<T> {
    fn into_eng_error(self) -> EngineError {
        LOCK_FAIL.clone()
    }
}

impl IntoEngError for LuaError {
    fn into_eng_error(self) -> EngineError {
        match self {
            LuaError::ToLuaConversionError { from, to, message } => INVALID_TYPE_CONVERTION
                .clone()
                .with_root_cause(format!("{}=>{}", from, to)),
            LuaError::FromLuaConversionError { from, to, message } => INVALID_TYPE_CONVERTION
                .clone()
                .with_root_cause(format!("{}=>{}", from, to)),
            LuaError::MemoryError(s) => MEMORY_FAILURE.clone().with_root_cause(s),
            LuaError::SyntaxError {
                message,
                incomplete_input,
            } => BAD_SYNTAX.clone().with_root_cause(message),
            LuaError::BadArgument {
                to,
                pos,
                name,
                cause,
            } => {
                let msg = format!(
                    "{}@{}",
                    to.unwrap_or("unknown".into()),
                    name.unwrap_or(pos.to_string())
                );
                LUA_EXEC_ERROR.clone().with_root_cause(msg)
            }
            LuaError::RuntimeError(s) => LUA_EXEC_ERROR.clone().with_root_cause(s),
            LuaError::CallbackError { traceback, cause } => {
                println!("ERROR IS :{cause:?}");
                cause.as_ref().clone().into_eng_error()
            }
            LuaError::ExternalError(e) => {
                let concrete = e.downcast_ref().map(|r: &EngineError| r.clone()).unwrap_or(
                    UNINPLEMENTED
                        .clone()
                        .with_root_cause("unknown root lua error"),
                );
                concrete
            }
            _ => UNINPLEMENTED.clone().with_root_cause("Lua error not impl"),
        }
    }
}

macro_rules! static_error {
    ($name:ident, $code:expr, $msg:expr) => {
        lazy_static! {
            pub static ref $name: EngineError = EngineError::default()
                .from_mdl(&crate::MDL_TYPE)
                .with_specifics($code, $msg);
        }
    };
}

//NOTE: 1XX SERIES (INIT ERRORS)
static_error!(INIT_BAD_CONFIG, 101, "Config value missing or malformed");

static_error!(
    CODE_NOT_FOUND,
    102,
    "Runtime didn't received any code to load."
);

//NOTE: 2XX SERIES (OPS ERRORS)
static_error!(FORBIDDEN_THREAD_ID, 201, "Forbidden thread id!");
static_error!(
    INVALID_TYPE_CONVERTION,
    202,
    "Failed to convert between types"
);
static_error!(MEMORY_FAILURE, 203, "A memory error occured!");

static_error!(
    LOCK_FAIL,
    204,
    "An error occurred when trying to acquire a lock!"
);

static_error!(
    CODE_ASSEMBLY_UNKNOWN,
    205,
    "Couldn't find a required module."
);

//NOTE: 3XX SERIES (ALTER ERRORS)
static_error!(
    INVALID_CREATE_REQUEST,
    301,
    "Trying to create undefined script!"
);
static_error!(
    INVALID_DESTROY_REQUEST,
    302,
    "Tried to destroy script that doesn't exist!"
);
static_error!(CREATION_FAILED, 303, "Could not create script!");

//NOTE: 4XX SERIES (PROCESS ERRORS)
static_error!(LUA_EXEC_ERROR, 401, "An Error was found during execution!");
static_error!(BAD_SYNTAX, 402, "The code provided is not valid Lua syntax");

static_error!(
    UNINPLEMENTED,
    999,
    "Something went wrong, but this catboy can't be bothered to tell you what"
);

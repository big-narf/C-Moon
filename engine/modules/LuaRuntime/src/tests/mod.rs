use mlua::prelude::*;

use cmoon_commons::*;

use cmoon_tests::*;

use crate::context;

//#[test]
fn single_threaded_test() {
    let mut app = SingleApp::<context::Runtime>::new(context::Runtime::new);
    app.run_loop();
}

#[test]
fn multi_threaded_test() {
    let mut app =
        MultiApp::<context::Runtime, 5>::new(context::Runtime::new).with_space_fn(create_obj);
    app.run_loop();
}

fn create_obj(mdl: &context::Runtime) -> EngResult<()> {
    let code = PartialCode {
        identifier: "tester".into(),
        group: "tester".into(),
        parent: None,
    };

    //let h = AssetHandle::from(0);
    //let args = [("image".to_string(), DynValue::from(h))].into();
    mdl.create(code, Default::default())?;
    Ok(())
}

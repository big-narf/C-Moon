mode=$1
target=$2

echo "SKIPPING PHYSICS MODULE"
exit 0

if [ "$mode" = "test" ]; then
    echo "Running tests on Component:$(basename "$PWD")"
    cargo test
elif [ "$mode" = "build" ]; then
    echo "Compiling Component:$(basename "$PWD") to $target"
    cargo build 
    mv target/debug/libPhysics.so $target/Physics.so
    cd .. || exit 1
fi


exit 0

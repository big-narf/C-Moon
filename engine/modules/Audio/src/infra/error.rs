use cpal::{BuildStreamError, DevicesError, StreamError};
use std::sync::PoisonError;

use lazy_static::lazy_static;

use cmoon_commons::*;

///Trait to convert LuaResult to a EngineResult, converting error
pub trait Cast {
    type Target;
    fn cast(self) -> Self::Target;
}

impl<T, E: IntoEngError> Cast for Result<T, E> {
    type Target = EngResult<T>;
    fn cast(self) -> Self::Target {
        self.map_err(|e| e.into_eng_error())
    }
}

///Converts Lua error to engine error
pub trait IntoEngError {
    fn into_eng_error(self) -> EngineError;
}

impl<T> IntoEngError for PoisonError<T> {
    fn into_eng_error(self) -> EngineError {
        LOCK_FAIL.clone()
    }
}

impl IntoEngError for BuildStreamError {
    fn into_eng_error(self) -> EngineError {
        STREAM_BUILD_FAIL.clone()
    }
}

macro_rules! static_error {
    ($name:ident, $code:expr, $msg:expr) => {
        lazy_static! {
            pub static ref $name: EngineError = EngineError::default()
                .from_mdl(&crate::MDL_TYPE)
                .with_specifics($code, $msg);
        }
    };
}

static_error!(STREAM_BUILD_FAIL, 101, "Failed to build the audio stream");

static_error!(LOCK_FAIL, 201, "Failed to aqcuire lock");

static_error!(UNKNOWN_OBJ_ID, 301, "Unkown Create ID");

static_error!(MISSING_ARG, 302, "A required argument was not found.");

use std::fmt::Debug;
use std::ops::AddAssign;
use std::sync::atomic::{AtomicU8, AtomicUsize, Ordering};
use std::sync::{Arc, Mutex, OnceLock, RwLock, RwLockReadGuard, RwLockWriteGuard};

use cpal::traits::*;
use cpal::*;

use cmoon_commons::*;

mod error;

pub use error::*;

///Returns the concrete run function
macro_rules! function_for {
    ($format :expr,$generic :ident) => {
        match $format {
            cpal::SampleFormat::I8 => $generic::<i8>,
            cpal::SampleFormat::I16 => $generic::<i16>,
            // cpal::SampleFormat::I24 => run::<I24>(&device, &config.into()),
            cpal::SampleFormat::I32 => $generic::<i32>,
            // cpal::SampleFormat::I48 => run::<I48>(&device, &config.into()),
            cpal::SampleFormat::I64 => $generic::<i64>,
            cpal::SampleFormat::U8 => $generic::<u8>,
            cpal::SampleFormat::U16 => $generic::<u16>,
            // cpal::SampleFormat::U24 => run::<U24>(&device, &config.into()),
            cpal::SampleFormat::U32 => $generic::<u32>,
            // cpal::SampleFormat::U48 => run::<U48>(&device, &config.into()),
            cpal::SampleFormat::U64 => $generic::<u64>,
            cpal::SampleFormat::F32 => $generic::<f32>,
            cpal::SampleFormat::F64 => $generic::<f64>,
            sample_format => panic!("Unsupported sample format '{sample_format}'"),
        }
    };
}

///Audio stream that will joint the audio from all sources and push to the hardware buffer.
pub struct AudioSink {
    device: Device,
    format: SampleFormat,
    config: StreamConfig,
    stream: Stream,
    pub groups: Arc<AudioGroup>,
}

impl AudioSink {
    pub fn new(sample_rate: usize, delta_millis: u32) -> EngResult<Self> {
        let frame_size = sample_rate * delta_millis as usize;

        let groups: Arc<_> = AudioGroup::new(frame_size).into();

        let device = cpal::default_host().default_output_device().unwrap();

        let rate = SampleRate(sample_rate as u32);
        let supported = device
            .supported_output_configs()
            .unwrap()
            .find(|range| range.check_buffer(frame_size))
            .map(|range| range.with_sample_rate(rate))
            .unwrap();

        let format = supported.sample_format();
        let config = supported.config();

        let stream = function_for!(format, init_stream)(&device, &config, groups.clone())?;

        Ok(Self {
            device,
            format,
            config,
            stream,
            groups,
        })
    }
}

unsafe impl Send for AudioSink {}

unsafe impl Sync for AudioSink {}

///This is a sample format indepentent internal generic function that will be called at module
///start
fn init_stream<T: SizedSample + FromSample<f32>>(
    device: &Device,
    config: &StreamConfig,
    acc: Arc<AudioGroup>,
) -> EngResult<Stream> {
    let channels = config.channels as usize;
    let data_fn = move |output: &mut [T], _: &cpal::OutputCallbackInfo| {
        acc.accumulate_to(output, channels);
    };
    let err_fn = |err| eprintln!("an error occurred on audio stream: {}", err);
    let stream = device
        .build_output_stream(config, data_fn, err_fn, None)
        .cast()?;
    stream.play().unwrap();
    Ok(stream)
}

trait CheckBuffer {
    ///Checks if the config supports the buffer size b_size
    fn check_buffer(&self, b_size: usize) -> bool;
}

impl CheckBuffer for cpal::SupportedStreamConfigRange {
    fn check_buffer(&self, b_size: usize) -> bool {
        match self.buffer_size() {
            SupportedBufferSize::Unknown => false,
            SupportedBufferSize::Range { min, max } => {
                b_size >= *min as usize //Checks if the frame is bigger than the audio buffer.
            }
        }
    }
}

///Engine side ring buffer that accumulates the samples comming from the engine
#[derive(Debug)]
pub struct AudioGroup {
    frame_size: usize,
    read_index: AtomicUsize,
    write_frame: AtomicU8,
    frame_0: RwLock<Vec<f32>>,
    frame_1: RwLock<Vec<f32>>,
    frame_2: RwLock<Vec<f32>>,
}

impl AudioGroup {
    ///frame_size is the size of a single audio frame
    pub fn new(frame_size: usize) -> Self {
        let frame_0 = vec![f32::EQUILIBRIUM; 2 * frame_size];
        let frame_1 = frame_0.clone();
        let frame_2 = frame_0.clone();
        Self {
            frame_size,
            read_index: Default::default(),
            write_frame: Default::default(),
            frame_0: frame_0.into(),
            frame_1: frame_1.into(),
            frame_2: frame_2.into(),
        }
    }
}

impl AudioGroup {
    fn get_frame(&self, id: u8) -> &RwLock<Vec<f32>> {
        let id = id % 3;
        if id == 0 {
            &self.frame_0
        } else if id == 1 {
            &self.frame_1
        } else {
            &self.frame_2
        }
    }
}

impl AudioGroup {
    ///Advances the currently writing frame and clears it
    pub fn next_frame(&self) -> EngResult<()> {
        let id = self
            .write_frame
            .fetch_add(1, Ordering::SeqCst)
            .wrapping_add(1);
        let frame = self.get_frame(id);
        frame
            .write()
            .cast()?
            .iter_mut()
            .for_each(|val| *val = f32::EQUILIBRIUM);
        Ok(())
    }
    pub fn len(&self) -> usize {
        self.frame_size
    }

    ///Adds values to the current write frame
    pub fn write_frame(&self) -> EngResult<RwLockWriteGuard<Vec<f32>>> {
        let frame = self.get_frame(self.write_frame.load(Ordering::SeqCst));
        frame.write().cast()
    }

    ///Gets a chunk of audio data with size, advances the read index
    pub fn accumulate_to<T: SizedSample + FromSample<f32>>(
        &self,
        output: &mut [T],
        channels: usize,
    ) {
        //NOTE: read index ops
        let size = output.len();
        let start = self.read_index.fetch_add(size, Ordering::SeqCst);
        println!("{start}");

        //NOTE: get read regions
        let start_frame = (start / self.frame_size) as u8;
        let offset = start % self.frame_size;
        let r1 = self.get_frame(start_frame).read().expect("shat the bed");
        let r2 = self
            .get_frame(start_frame + 1)
            .read()
            .expect("shat the bed");

        let input = r1
            .iter()
            .skip(offset)
            .chain(r2.iter())
            .take(size)
            .map(|&v| T::from_sample(v));
        //NOTE: FILLS ALL CHANNELS WITH THE SAME STUFF
        let frames = output.chunks_mut(channels);
        Iterator::zip(frames, input).for_each(|(frame, value)| {
            frame.fill(value);
        });
    }
}

use core::time;
use std::{
    sync::{Mutex, OnceLock},
    time::Instant,
};

use anyhow::bail;
use cmoon_commons::*;

use cmoon_tests::*;

use crate::context;

static SINE_WAVE: Mutex<SineWave> = Mutex::new(SineWave::new(44100));

#[test]
fn render_test() {
    //let mut app = SingleApp::new(context::AudioRenderer::new);

    let mut app = MultiApp::<_, 3>::new(context::AudioRenderer::new)
        .override_signal_searcher(get_signal)
        .with_space_fn(create_emmiter);

    app.run_loop();
}

fn create_emmiter<T: Module>(mdl: &T) -> EngResult<()> {
    let code = PartialCode {
        group: "emmiter".into(),
        identifier: "emitter".into(),
        parent: None,
    };
    let args = Default::default();
    mdl.create(code, args)?;
    Ok(())
}

fn get_signal(_: &SignalID) -> Option<Signal> {
    let mut wave = SINE_WAVE.lock().unwrap();

    let samples = wave.sample_vec(44100 * 17 * 100);
    println!("fetching samples");
    let signal = Signal::new(0, 0.3).attach("samples", samples);
    Some(signal)
}

#[derive(Debug)]
pub struct SineWave {
    sample_rate: usize,
    clock: f32, //SampleRate,  //ChannelCount,
}

impl SineWave {
    pub const fn new(sample_rate: usize) -> Self {
        Self {
            sample_rate,
            clock: 0.0,
        }
    }

    pub fn fill(&mut self, buff: &mut [f32]) {
        buff.iter_mut().for_each(|v| *v = self.sample());
    }

    pub fn sample_vec(&mut self, size: usize) -> Vec<u8> {
        let buff: Vec<f32> = (0..size).map(|_| self.sample()).collect();
        bytemuck::cast_slice(&buff).to_vec()
    }

    pub fn sample(&mut self) -> f32 {
        let rate = self.sample_rate as f32;
        self.clock = (self.clock + 1.0) % (rate);
        let s = (self.clock * 440.0 * 2.0 * std::f32::consts::PI / rate).sin();
        //println!("sample :{s}");
        s
    }
}

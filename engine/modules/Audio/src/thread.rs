use std::collections::HashMap;
use std::sync::Mutex;

use cmoon_commons::*;

use crate::elements::*;
use crate::infra::*;

pub struct ThreadCtx {
    objects: HashMap<usize, AudioEmmiter>,
    last_id: usize,
    destroy_list: Mutex<Vec<usize>>,
}

impl ThreadCtx {
    pub fn new() -> Self {
        Self {
            objects: Default::default(),
            last_id: Default::default(),
            destroy_list: Default::default(),
        }
    }

    pub fn process(&mut self, signal_searcher: &fn(&SignalID) -> Option<Signal>) -> EngResult<()> {
        self.objects
            .values_mut()
            .try_for_each(|e| e.render(signal_searcher))
    }

    pub fn add_obj(
        &mut self,
        shared: &AudioSink,
        group: EngString,
        id: EngString,
        parent: Option<EngString>,
        mut args: PartialArgs,
    ) -> EngResult<usize> {
        match id.as_str() {
            "emmiter" => {
                let src_sig = args
                    .pop_as("AUDIO_START")
                    .ok_or_else(|| MISSING_ARG.clone().with_root_cause("AUDIO_START"))?;
                let emmiter = AudioEmmiter::new(parent, src_sig, shared.groups.clone());
                let cur_id = self.last_id;
                self.objects.insert(cur_id, emmiter);
                self.last_id += 1;
                Ok(cur_id)
            }
            _ => Err(UNKNOWN_OBJ_ID.clone().with_root_cause(format!("ID={id}"))),
        }
    }

    pub fn mark_destroy(&self, local_id: usize) -> EngResult<()> {
        let mut lock = self.destroy_list.lock().cast()?;
        lock.push(local_id);
        Ok(())
    }

    ///destroy all objects marked for destruction on the current thread
    pub fn remove_garbage(&mut self) -> EngResult<()> {
        let mut lock = self.destroy_list.lock().unwrap();
        lock.drain(..).for_each(|id| {
            self.objects.remove(&id);
        });
        Ok(())
    }
}

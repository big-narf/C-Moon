use std::collections::HashMap;
use std::sync::{Arc, Mutex, RwLock, RwLockReadGuard};

use cpal::traits::*;
use cpal::*;

use crossbeam::queue::SegQueue;
use dashmap::DashMap;

use cmoon_commons::*;

use crate::elements::*;
use crate::infra::*;
use crate::thread::ThreadCtx;

mod misc;
use misc::*;

pub struct AudioRenderer {
    shared: AudioSink,
    get_signal_fn: fn(&SignalID) -> Option<Signal>,

    create_queue: SegQueue<ScheduledCreate>,
    id_table: DashMap<usize, (usize, usize)>,
    locals: Vec<Mutex<ThreadCtx>>,
}

impl AudioRenderer {
    pub fn new(
        handle: CoreHandle,
        n_threads: usize,
        config: ModuleConfig,
        _: Option<HashMap<String, String>>,
    ) -> EngResult<Self> {
        let shared = AudioSink::new(44100, 17)?;

        let locals = (0..n_threads).map(|_| ThreadCtx::new().into()).collect();

        Ok(Self {
            shared,
            get_signal_fn: handle.signal_searcher,
            create_queue: Default::default(),
            id_table: Default::default(),
            locals,
        })
    }
}

impl Module for AudioRenderer {
    fn get_type() -> ModuleType {
        ModuleType::Audio
    }

    fn commence(&self) -> EngResult<()> {
        Ok(())
    }

    fn conclude(&self) -> EngResult<()> {
        self.shared.groups.next_frame()
    }

    fn alter(&self, thread_id: usize) -> EngResult<()> {
        let mut ctx = self.locals.get(thread_id).unwrap().lock().cast()?;
        //create new objects
        while let Some(scheduled) = self.create_queue.pop() {
            let id = scheduled.id;
            let code = scheduled.code;
            let args = scheduled.args;
            let local_adress =
                ctx.add_obj(&self.shared, code.group, code.identifier, code.parent, args)?;
            self.id_table.insert(id, (thread_id, local_adress));
        }

        //clear dead objects
        ctx.remove_garbage()
    }

    fn process(&self, thread_id: usize, dt: u32) -> EngResult<()> {
        let mut ctx = self.locals.get(thread_id).unwrap().lock().cast()?;
        ctx.process(&self.get_signal_fn)
    }
    fn create(&self, code: PartialCode, args: PartialArgs) -> EngResult<usize> {
        let id = self.id_table.len();
        let scheduled = ScheduledCreate { id, code, args };
        self.create_queue.push(scheduled);
        Ok(id)
    }

    fn destroy(&self, id: usize) -> EngResult<()> {
        let (_, (thread_id, local_id)) = self.id_table.remove(&id).unwrap();
        self.locals
            .get(thread_id)
            .unwrap()
            .lock()
            .cast()?
            .mark_destroy(local_id)
    }
}

pub struct ScheduledCreate {
    id: usize,
    code: PartialCode,
    args: PartialArgs,
}

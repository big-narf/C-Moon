#[macro_use]
use cmoon_commons::*;

mod context;
mod elements;
mod infra;
mod thread;

#[cfg(test)]
mod tests;

pub static MDL_TYPE: ModuleType = ModuleType::Audio;

module_interface!(context::AudioRenderer, MDL_TYPE);

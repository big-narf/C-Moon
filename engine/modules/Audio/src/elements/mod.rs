use std::collections::VecDeque;
use std::sync::{Arc, Mutex};

use cmoon_commons::*;

use cpal::*;

use crate::infra::AudioGroup;

//TODO: WHEN I HAVE ALL THE SIGNAL/INSTANTIATION BASICS LOCKED DOWN, CONVERT THIS TO AN ENUM OF
//AMBIENT/LINKED SOUND EMITTERS
pub struct AudioEmmiter {
    parent: Option<EngString>,
    src: SignalID,
    dst: Arc<AudioGroup>,
    buffer: VecDeque<f32>,
    state: EmmiterState,
}
///Trait for generic Spatial audio source
impl AudioEmmiter {
    pub fn new(parent: Option<EngString>, src: SignalID, dst: Arc<AudioGroup>) -> Self {
        Self {
            parent,
            src,
            dst,
            buffer: Default::default(),
            state: Default::default(),
        }
    }

    fn get_samples(&mut self, signal_searcher: &fn(&SignalID) -> Option<Signal>) {
        let signal = (signal_searcher)(&self.src);
        let new_samples = signal
            .as_ref()
            .and_then(|s| s.get_as::<DataBlock>("samples"))
            .map(|s| bytemuck::cast_slice(s));
        match new_samples {
            Some(s) => self.buffer.extend(s),
            None => (),
        }
    }

    //accumulates the signal from this emmiter to the destination group
    pub fn render(&mut self, signal_searcher: &fn(&SignalID) -> Option<Signal>) -> EngResult<()> {
        if self.buffer.len() < self.dst.len() {
            self.get_samples(signal_searcher);
        }
        println!("rendering: {} values in buffer", self.buffer.len());
        println!("frame size:{}", self.dst.len());
        let input = self.buffer.drain(0..self.dst.len());
        let mut frame = self.dst.write_frame()?;
        Iterator::zip(frame.iter_mut(), input).for_each(|(o, i)| {
            *o += i;
        });
        Ok(())
    }
}

#[derive(Debug, Default, Clone)]
pub enum EmmiterState {
    #[default]
    Playing,
    Paused,
}

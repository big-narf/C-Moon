use gilrs::*;

use cmoon_commons::*;

pub struct CtrlContext {}

impl CtrlContext {
    pub fn new() -> EngResult<Self> {
        let ctrl = Gilrs::new().unwrap();
        let gamepad = ctrl.gamepads().next().unwrap();
    }
}

impl Module for CtrlContext {
    fn get_type() -> cmoon_commons::ModuleType {
        cmoon_commons::ModuleType::Control
    }
}

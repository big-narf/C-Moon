mode=$1
target=$2

echo "SKIPPING CONTROLLER MODULE"
exit 0 #remove after testing

if [ "$mode" = "test" ]; then
	echo -e "\033[1;33mRunning tests on :$(basename "$PWD") \033[0m"
	cargo test
elif [ "$mode" = "build" ]; then
	if [ -z "$target" ]; then
		echo -e "\033[1;31mERROR: No project path provided.\033[0m"
		exit 1
	fi
    echo -e "\033[1;33mCompiling Component:$(basename "$PWD") to $target\033[0m"
    cargo build 
    mv target/debug/libVKRenderer.so $target/VKRenderer.so
    cd .. || exit 1
fi


exit 0

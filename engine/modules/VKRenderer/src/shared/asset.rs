use std::collections::HashMap;
use std::fmt::Debug;
use std::sync::{Arc, Mutex, RwLock, Weak};
use std::u64;

use ash::vk;
use ash::{ext, khr};

use cmoon_commons::*;
use dashmap::DashMap;
use rand::Rng;

use crate::elements::*;
use crate::infra::*;

/**This is a high level handler that managers creating and caching asset objects
*/
pub struct ResourceManager {
    asset_cache: DashMap<u32, Weak<ImgAsset>>,

    d_pool: vk::DescriptorPool,
    layout_list: RwLock<Vec<vk::DescriptorSetLayout>>,

    c_pool: vk::CommandPool,
    c_buffer: vk::CommandBuffer,
    fence: vk::Fence,
    semaphore: vk::Semaphore,
}

///Ops functions
impl ResourceManager {
    pub fn new(driver: &VKDriver, max_desc: u32) -> EngResult<Self> {
        let descriptor_sizes = [
            vk::DescriptorPoolSize {
                ty: vk::DescriptorType::UNIFORM_BUFFER,
                descriptor_count: max_desc,
            },
            vk::DescriptorPoolSize {
                ty: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
                descriptor_count: max_desc,
            },
        ];

        let create_info = vk::DescriptorPoolCreateInfo::default()
            .pool_sizes(&descriptor_sizes)
            .flags(vk::DescriptorPoolCreateFlags::FREE_DESCRIPTOR_SET)
            .max_sets(16); //BUG: THIS IS WRONG

        let d_pool = driver.create_desc_pool(&create_info)?;

        let create_info = vk::CommandPoolCreateInfo::default()
            .queue_family_index(0)
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER);
        let c_pool = driver.create_cmd_pool(&create_info)?;

        let create_info = vk::SemaphoreCreateInfo::default();
        let semaphore = driver.create_semaphore(&create_info)?;

        let create_info = vk::FenceCreateInfo::default().flags(vk::FenceCreateFlags::SIGNALED);
        let fence = driver.create_fence(&create_info)?;

        //creates command buffer for copy commands
        let create_info = vk::CommandBufferAllocateInfo::default()
            .command_pool(c_pool)
            .level(vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(1);
        let c_buffer = driver
            .allocate_cmd_buffers(&create_info)?
            .into_iter()
            .next()
            .ok_or_else(|| {
                CMD_BUFFER_ALLOC_FAIL
                    .clone()
                    .with_root_cause("Primary@ResManager")
            })?;

        Ok(Self {
            asset_cache: Default::default(),

            d_pool,
            layout_list: Default::default(),

            c_pool,
            c_buffer,
            fence,
            semaphore,
        })
    }
}

impl Drop for ResourceManager {
    fn drop(&mut self) {
        let driver = VKDriver::instance().unwrap();
        driver.destroy_desc_pool(&mut self.d_pool);
        driver.destroy_semaphore(&mut self.semaphore);
        driver.destroy_fence(&mut self.fence);
        driver.destroy_cmd_pool(&mut self.c_pool);
    }
}

impl ResourceManager {
    ///called by the context, creates the resource and returns the ID value for it.
    pub fn create_texture(&self, image: &AssetHandle) -> EngResult<Arc<ImgAsset>> {
        let mut uniform_color = Vector3 {
            x: 1.0,
            y: 1.0,
            z: 1.0,
            _pad: 0.0,
        };

        let mut rgn = rand::thread_rng();
        rgn.fill(&mut uniform_color);

        //checks if image data is in the cache
        let tag = image.tag();
        let cached = self.asset_cache.get(&tag).and_then(|r| r.upgrade());

        match cached {
            Some(asset) => {
                println!("ASSET CACHE HIT!");
                Ok(asset)
            }

            None => {
                let mut image = image.load()?;
                let rgba: DataBlock = image.get_create_value("rgba")?;
                let width: u32 = image.get_create_value("width")?;
                let height: u32 = image.get_create_value("height")?;
                //tries to unwrap the dynvalues into the correct types
                let dimensions = vk::Extent2D { height, width };
                //pushes pixel data onto a GPU visible buffer
                println!("RGBA LEN:{}", rgba.len());
                let staging_buffer = MapVec::new_from(
                    &rgba,
                    vk::BufferUsageFlags::TRANSFER_SRC,
                    vk::SharingMode::EXCLUSIVE,
                )
                .with_root("STAGING BUFFER IS TOO BIG")?;

                //tries to create the asset object
                self.define_image_layout()?; //NOTE: CHANGE THIS

                let driver = VKDriver::instance().unwrap();
                let layouts = &self.layout_list.read().unwrap()[0..1]; //NOTE:CHANGE THIS
                let asset: Arc<_> = ImgAsset::new(
                    driver,
                    &[uniform_color],
                    dimensions,
                    vk::BufferUsageFlags::UNIFORM_BUFFER,
                    vk::SharingMode::EXCLUSIVE,
                    layouts,
                    self.d_pool.into(),
                )?
                .into();

                //copies data over
                driver.prepare_cmd_buffer(&self.c_buffer, &[self.fence])?;
                driver.push_to_image(
                    &self.c_buffer,
                    staging_buffer.to_buffer(),
                    asset.as_image(),
                    dimensions.into(),
                );

                let driver = VKDriver::instance().unwrap();

                let cmd_buffers = [self.c_buffer];
                //let wait_semaphores = [self.semaphore];
                driver.submit_transfer_cmds(&cmd_buffers, &[], self.fence)?;

                //keeps weak ref on cache
                self.asset_cache.insert(tag, Arc::downgrade(&asset));

                Ok(asset)
            }
        }
    }
}

impl ResourceManager {
    pub fn define_image_layout(&self) -> EngResult<()> {
        let driver = VKDriver::instance().unwrap();
        let desc_layout_bindings = [
            vk::DescriptorSetLayoutBinding {
                descriptor_type: vk::DescriptorType::UNIFORM_BUFFER,
                descriptor_count: 1,
                stage_flags: vk::ShaderStageFlags::FRAGMENT,
                ..Default::default()
            },
            vk::DescriptorSetLayoutBinding {
                binding: 1,
                descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
                descriptor_count: 1,
                stage_flags: vk::ShaderStageFlags::FRAGMENT,
                ..Default::default()
            },
        ];
        let descriptor_info =
            vk::DescriptorSetLayoutCreateInfo::default().bindings(&desc_layout_bindings);

        let layout = driver.create_layout(&descriptor_info)?;
        self.layout_list.write().unwrap().push(layout);
        Ok(())
    }

    ///Pipes the pixels of a CPU visible buffer to a GPU only image
    fn cmds_send_image(
        &self,
        device: &ash::Device,
        src_buffer: vk::Buffer,
        dst_image: vk::Image,
        extent: vk::Extent3D,
    ) {
        let texture_barrier = vk::ImageMemoryBarrier {
            dst_access_mask: vk::AccessFlags::TRANSFER_WRITE,
            new_layout: vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            image: dst_image,
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                level_count: 1,
                layer_count: 1,
                ..Default::default()
            },
            ..Default::default()
        };
        unsafe {
            device.cmd_pipeline_barrier(
                self.c_buffer,
                vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                vk::PipelineStageFlags::TRANSFER,
                vk::DependencyFlags::empty(),
                &[],
                &[],
                &[texture_barrier],
            )
        };
        let buffer_copy_regions = vk::BufferImageCopy::default()
            .image_subresource(
                vk::ImageSubresourceLayers::default()
                    .aspect_mask(vk::ImageAspectFlags::COLOR)
                    .layer_count(1),
            )
            .image_extent(extent);

        unsafe {
            device.cmd_copy_buffer_to_image(
                self.c_buffer,
                src_buffer,
                dst_image,
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                &[buffer_copy_regions],
            )
        };
        let texture_barrier_end = vk::ImageMemoryBarrier {
            src_access_mask: vk::AccessFlags::TRANSFER_WRITE,
            dst_access_mask: vk::AccessFlags::SHADER_READ,
            old_layout: vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            new_layout: vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            image: dst_image,
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                level_count: 1,
                layer_count: 1,
                ..Default::default()
            },
            ..Default::default()
        };
        unsafe {
            device.cmd_pipeline_barrier(
                self.c_buffer,
                vk::PipelineStageFlags::TRANSFER,
                vk::PipelineStageFlags::FRAGMENT_SHADER,
                vk::DependencyFlags::empty(),
                &[],
                &[],
                &[texture_barrier_end],
            );
        }
    }
    /*
    ///Pipes the pixels of an image to a CPU visible buffer. Inverse of Send_image
    pub fn grab_image(&self, src_image: vk::Image, dst_buffer: vk::Buffer, extent: vk::Extent3D) {
        let device = VKDriver::device().unwrap();

        let texture_barrier = vk::ImageMemoryBarrier {
            dst_access_mask: vk::AccessFlags::TRANSFER_WRITE,
            new_layout: vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            image: src_image,
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                level_count: 1,
                layer_count: 1,
                ..Default::default()
            },
            ..Default::default()
        };
        unsafe {
            device.cmd_pipeline_barrier(
                self.c_buffer,
                vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                vk::PipelineStageFlags::TRANSFER,
                vk::DependencyFlags::empty(),
                &[],
                &[],
                &[texture_barrier],
            )
        };
        let buffer_copy_regions = vk::BufferImageCopy::default()
            .image_subresource(
                vk::ImageSubresourceLayers::default()
                    .aspect_mask(vk::ImageAspectFlags::COLOR)
                    .layer_count(1),
            )
            .image_extent(extent);

        //NOTE: THIS IS DEFINETLY WRONG, BUT I AINT CHANGING RN
        unsafe {
            device.cmd_copy_image_to_buffer(
                self.c_buffer,
                src_image,
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                dst_buffer,
                &[buffer_copy_regions],
            )
        };
        let texture_barrier_end = vk::ImageMemoryBarrier {
            src_access_mask: vk::AccessFlags::TRANSFER_WRITE,
            dst_access_mask: vk::AccessFlags::SHADER_READ,
            old_layout: vk::ImageLayout::TRANSFER_DST_OPTIMAL,
            new_layout: vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            image: src_image,
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                level_count: 1,
                layer_count: 1,
                ..Default::default()
            },
            ..Default::default()
        };
        unsafe {
            device.cmd_pipeline_barrier(
                self.c_buffer,
                vk::PipelineStageFlags::TRANSFER,
                vk::PipelineStageFlags::FRAGMENT_SHADER,
                vk::DependencyFlags::empty(),
                &[],
                &[],
                &[texture_barrier_end],
            );
        }
    }*/
}

//NOTE: RAW FUNCTIONS

///This is a smart wrapper around all the stuff needed for a combined image sampler descriptor,
///it's supposed to be kept inside an Arc
pub struct ImgAsset {
    //allocations
    d_pool: Derived<vk::DescriptorPool>,
    desc_sets: Vec<vk::DescriptorSet>,
    layouts: Vec<vk::DescriptorSetLayout>, //NOTE: MAKE THIS INTO A DERIVED

    uniform_mem: GSlice<Vector3>,
    tex_mem: GImage<Color32>,

    //others
    image: vk::Image,
    view: vk::ImageView,
    sampler: vk::Sampler,
    uniform_b: vk::Buffer,
}

impl Debug for ImgAsset {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "ASSET: {{ sets: \"{:?}\", layouts: {:?}, img_mem :{:?} }}",
            self.desc_sets, self.layouts, self.tex_mem
        )
    }
}

impl ImgAsset {
    pub fn new(
        driver: &VKDriver,
        uniform_color: &[Vector3],
        dimensions: vk::Extent2D,

        b_flags: vk::BufferUsageFlags,
        mode: vk::SharingMode,
        layouts: &[vk::DescriptorSetLayout],
        d_pool: Derived<vk::DescriptorPool>,
    ) -> EngResult<Self> {
        //uniform buffer part
        let capacity = uniform_color.len().next_power_of_two();
        let create_info = vk::BufferCreateInfo::default()
            .size((capacity * std::mem::size_of::<Vector3>()) as vk::DeviceSize)
            .usage(b_flags)
            .sharing_mode(mode);
        let uniform_b = driver.create_buffer(&create_info)?;
        let mut uniform_mem: GSlice<Vector3> = driver.allocate_for(&uniform_b)?;
        uniform_mem[0..uniform_color.len()].copy_from_slice(uniform_color);

        driver.bind_buffer(&uniform_b, &uniform_mem)?;

        //Color image part
        let create_info = vk::ImageCreateInfo {
            image_type: vk::ImageType::TYPE_2D,
            format: vk::Format::R8G8B8A8_UNORM,
            extent: dimensions.into(),
            mip_levels: 1,
            array_layers: 1,
            samples: vk::SampleCountFlags::TYPE_1,
            tiling: vk::ImageTiling::OPTIMAL,
            usage: vk::ImageUsageFlags::TRANSFER_DST | vk::ImageUsageFlags::SAMPLED,
            sharing_mode: vk::SharingMode::EXCLUSIVE,
            ..Default::default()
        };
        let image = driver.create_image(&create_info)?;
        //let pixels = (dimensions.width * dimensions.height) as usize;

        let tex_mem = driver.allocate_for(&image)?;

        driver.bind_image(&image, &tex_mem)?;

        let sampler_info = vk::SamplerCreateInfo {
            mag_filter: vk::Filter::LINEAR,
            min_filter: vk::Filter::LINEAR,
            mipmap_mode: vk::SamplerMipmapMode::LINEAR,
            address_mode_u: vk::SamplerAddressMode::MIRRORED_REPEAT,
            address_mode_v: vk::SamplerAddressMode::MIRRORED_REPEAT,
            address_mode_w: vk::SamplerAddressMode::MIRRORED_REPEAT,
            max_anisotropy: 1.0,
            border_color: vk::BorderColor::FLOAT_OPAQUE_WHITE,
            compare_op: vk::CompareOp::NEVER,
            ..Default::default()
        };

        let sampler = driver.create_sampler(&sampler_info)?;

        let format = vk::Format::R8G8B8A8_UNORM;

        let create_info = vk::ImageViewCreateInfo {
            view_type: vk::ImageViewType::TYPE_2D,
            format: format,
            components: vk::ComponentMapping {
                r: vk::ComponentSwizzle::R,
                g: vk::ComponentSwizzle::G,
                b: vk::ComponentSwizzle::B,
                a: vk::ComponentSwizzle::A,
            },
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                level_count: 1,
                layer_count: 1,
                ..Default::default()
            },
            image: image,
            ..Default::default()
        };
        let view = driver.create_view(&create_info)?;

        //descriptor part

        //NOTE: THIS WILL PROLLY BE CHANGED
        let desc_alloc_info = vk::DescriptorSetAllocateInfo::default()
            .descriptor_pool(*d_pool)
            .set_layouts(&layouts);
        let desc_sets = driver.allocate_desc_sets(&desc_alloc_info)?;

        let uniform_desc = vk::DescriptorBufferInfo {
            buffer: uniform_b,
            offset: 0,
            range: (std::mem::size_of::<Vector3>() * capacity) as vk::DeviceSize,
        };

        let tex_descriptor = vk::DescriptorImageInfo {
            image_layout: vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            image_view: view,
            sampler: sampler,
        };

        let write_desc_sets = [
            vk::WriteDescriptorSet {
                dst_set: desc_sets[0],
                descriptor_count: 1,
                descriptor_type: vk::DescriptorType::UNIFORM_BUFFER,
                p_buffer_info: &uniform_desc,
                ..Default::default()
            },
            vk::WriteDescriptorSet {
                dst_set: desc_sets[0],
                dst_binding: 1,
                descriptor_count: 1,
                descriptor_type: vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
                p_image_info: &tex_descriptor,
                ..Default::default()
            },
        ];
        driver.update_desc(&write_desc_sets);

        Ok(Self {
            d_pool,
            desc_sets,
            layouts: layouts.to_vec(),

            uniform_mem,
            tex_mem,

            uniform_b,
            image,
            view,
            sampler,
        })
    }
}

impl ImgAsset {
    pub fn as_image(&self) -> vk::Image {
        self.image
    }

    pub fn as_desc_set<'img>(&'img self) -> &'img [vk::DescriptorSet] {
        &self.desc_sets
    }

    pub fn layouts<'img>(&'img self) -> &'img [vk::DescriptorSetLayout] {
        &self.layouts
    }
}

impl Drop for ImgAsset {
    fn drop(&mut self) {
        let driver = VKDriver::instance().unwrap();

        driver.destroy_buffer(&mut self.uniform_b);
        driver.destroy_sampler(&mut self.sampler);
        driver.destroy_view(&mut self.view);
        driver.destroy_image(&mut self.image);

        driver.free_desc_sets(self.d_pool, &mut self.desc_sets);
        driver.free(&mut self.tex_mem);
        driver.free(&mut self.uniform_mem);
    }
}

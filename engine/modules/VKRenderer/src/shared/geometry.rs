use std::fmt::Debug;
use std::marker::PhantomData;
use std::mem;
use std::ops::DerefMut;
use std::sync::{Arc, Mutex, RwLock, Weak};

use ash::vk;

use bytemuck::{AnyBitPattern, NoUninit, Zeroable};
use dashmap::DashMap;

use cmoon_commons::*;

use super::*;
use crate::infra::*;

///Dedicated allocator for index and vertex data
pub struct GeometryManager<T: Copy> {
    _v_marker: PhantomData<T>,

    vertex_buffer: vk::Buffer,
    vertex_slice: RwLock<GSlice<T>>,
    vertex_max: usize,

    index_buffer: vk::Buffer,
    index_slice: RwLock<GSlice<u32>>,
    index_max: usize,

    free_vertex_regions: Arc<Mutex<Vec<GeoRegion>>>,
    free_index_regions: Arc<Mutex<Vec<GeoRegion>>>,

    geometry_cache: DashMap<u32, Weak<Geometry>>,
}

impl<T: Copy> GeometryManager<T> {
    pub fn new(driver: &VKDriver, index_max: usize, vertex_max: usize) -> EngResult<Self> {
        let vert_size = (vertex_max * mem::size_of::<T>()) as vk::DeviceSize;
        let ind_size = (index_max * mem::size_of::<u32>()) as vk::DeviceSize;

        let create_info = vk::BufferCreateInfo::default()
            .size(vert_size)
            .usage(vk::BufferUsageFlags::VERTEX_BUFFER)
            .sharing_mode(vk::SharingMode::EXCLUSIVE);
        let vertex_buffer = driver.create_buffer(&create_info)?;
        let create_info = vk::BufferCreateInfo::default()
            .size(ind_size)
            .usage(vk::BufferUsageFlags::INDEX_BUFFER)
            .sharing_mode(vk::SharingMode::EXCLUSIVE);
        let index_buffer = driver.create_buffer(&create_info)?;

        let vertex_slice = driver.allocate_for(&vertex_buffer)?;

        driver.bind_buffer(&vertex_buffer, &vertex_slice)?;

        let index_slice = driver.allocate_for(&index_buffer)?;

        driver.bind_buffer(&index_buffer, &index_slice)?;
        Ok(Self {
            _v_marker: PhantomData,

            vertex_buffer,
            vertex_slice: vertex_slice.into(),
            vertex_max,

            index_buffer,
            index_slice: index_slice.into(),
            index_max,

            free_vertex_regions: Mutex::from(vec![GeoRegion {
                offset: 0,
                size: vertex_max,
            }])
            .into(),

            free_index_regions: Mutex::from(vec![GeoRegion {
                offset: 0,
                size: index_max,
            }])
            .into(),
            geometry_cache: Default::default(),
        })
    }
}

impl GeometryManager<VertexFAT> {
    pub fn create_geometry(&self, mesh: &AssetHandle) -> EngResult<Arc<Geometry>> {
        let key = mesh.tag();
        let cached = self.geometry_cache.get(&key).and_then(|r| r.upgrade());

        match cached {
            Some(scheme) => {
                println!("GEOMETRY CACHE HIT!");
                Ok(scheme)
            }
            None => {
                let mut mesh = mesh.load()?;
                let indices: DataBlock = mesh.get_create_value("indices")?;
                println!("indices :{:?}", indices);
                let vertices: DataBlock = mesh.get_create_value("vertices")?;
                let i: Vec<u32> = bytemuck::cast_slice(&indices).to_vec();
                let v: Vec<VertexFAT> = bytemuck::cast_slice(&vertices).to_vec();

                let mut i_lock = self.free_index_regions.lock().unwrap();
                let index_chunk = i_lock
                    .iter_mut()
                    .find(|r| r.size >= i.len())
                    .ok_or_else(|| GEOMETRY_FILLED_UP.clone().with_root_cause("index"))?;

                let mut v_lock = self.free_vertex_regions.lock().unwrap();
                let vertex_chunk = v_lock
                    .iter_mut()
                    .find(|r| r.size >= v.len())
                    .ok_or_else(|| GEOMETRY_FILLED_UP.clone().with_root_cause("vertex"))?;

                let vertex_region = vertex_chunk.front_split(v.len());

                let mut v_slice = self.vertex_slice.write().unwrap();
                v_slice[vertex_region.start()..vertex_region.end()].copy_from_slice(&v);

                //copies indexes
                let index_region = index_chunk.front_split(i.len());
                let mut i_slice = self.index_slice.write().unwrap();
                i_slice[index_region.start()..index_region.end()].copy_from_slice(&i);

                // offsets with initial vertex value
                i_slice[index_region.start()..index_region.end()]
                    .iter_mut()
                    .for_each(|i| *i += vertex_region.start() as u32);

                let geo = Geometry {
                    vertices: vertex_region,
                    indices: index_region,
                    free_index_regions: self.free_index_regions.clone(),
                    free_vertex_regions: self.free_vertex_regions.clone(),
                }
                .into();

                //keeps weak ref on cache
                self.geometry_cache.insert(key, Arc::downgrade(&geo));

                Ok(geo)
            }
        }
    }
}

impl<'geo, T: Copy> GeometryManager<T> {
    pub fn vertices(&'geo self) -> &'geo vk::Buffer {
        &self.vertex_buffer
    }

    pub fn indices(&'geo self) -> &'geo vk::Buffer {
        &self.index_buffer
    }
}

impl<T: Copy> Drop for GeometryManager<T> {
    fn drop(&mut self) {
        let driver = VKDriver::instance().unwrap();
        driver.free(self.index_slice.write().unwrap().deref_mut());
        driver.free(self.vertex_slice.write().unwrap().deref_mut());
        driver.destroy_buffer(&mut self.vertex_buffer);
        driver.destroy_buffer(&mut self.index_buffer);
    }
}

impl<T: Copy> Debug for GeometryManager<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "GEO MANAGER: {{ vert_buffer: \"{:?}\", ind_buffer: {:?} }}",
            self.vertex_buffer, self.index_buffer
        )
    }
}

///This is specifically a region in the geometry buffers
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd)]
struct GeoRegion {
    offset: usize,
    size: usize,
}

impl Ord for GeoRegion {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.size.cmp(&other.size)
    }
}

impl GeoRegion {
    pub fn start(&self) -> usize {
        self.offset
    }

    pub fn end(&self) -> usize {
        self.offset + self.size
    }

    pub fn merge(&mut self, r2: Self) {
        self.offset = usize::min(self.offset, r2.offset);
        self.size = self.size + r2.size;
    }

    ///Returns a region of size from the end of the region
    pub fn back_split(&mut self, size: usize) -> Self {
        let part = Self {
            offset: self.offset + self.size - size,
            size: size,
        };
        self.size -= size;
        part
    }
    ///Returns a region of size from the end of the region
    pub fn front_split(&mut self, size: usize) -> Self {
        let part = Self {
            offset: self.offset,
            size: size,
        };
        self.offset += size;
        self.size -= size;
        part
    }
}

///POS + UV + NORMAL VERTEX DATA
#[derive(Clone, Debug, Copy)]
pub struct VertexFAT {
    pub pos: [f32; 4],
    pub uv: [f32; 3],
    pub normal: [f32; 3],
}

unsafe impl NoUninit for VertexFAT {}
unsafe impl Zeroable for VertexFAT {}
unsafe impl AnyBitPattern for VertexFAT {}

#[derive(Debug)]
pub struct Geometry {
    vertices: GeoRegion,
    indices: GeoRegion,
    free_vertex_regions: Arc<Mutex<Vec<GeoRegion>>>,
    free_index_regions: Arc<Mutex<Vec<GeoRegion>>>,
}

impl Geometry {
    pub fn first_index(&self) -> u32 {
        self.indices.offset as u32
    }
    pub fn index_count(&self) -> u32 {
        self.indices.size as u32
    }
}

impl Drop for Geometry {
    fn drop(&mut self) {
        let mut v_lock = self.free_vertex_regions.lock().unwrap();
        let adjacent = v_lock.iter_mut().find(|free_r| {
            free_r.offset == self.vertices.end() || free_r.end() == self.vertices.offset
        });
        match adjacent {
            Some(r) => r.merge(self.vertices.clone()),
            None => v_lock.push(self.vertices.clone()),
        };

        let mut i_lock = self.free_index_regions.lock().unwrap();
        let adjacent = i_lock.iter_mut().find(|free_r| {
            free_r.offset == self.indices.end() || free_r.end() == self.indices.offset
        });
        match adjacent {
            Some(r) => r.merge(self.indices.clone()),
            None => v_lock.push(self.indices.clone()),
        };
    }
}

use std::ffi::CStr;
use std::fmt::Debug;
use std::mem;
use std::sync::{Arc, RwLock, Weak};

use ash::vk;
use dashmap::*;

use cmoon_commons::*;

use super::*;
use crate::elements::*;
use crate::infra::*;

pub struct SchemeManager {
    layout_list: RwLock<Vec<vk::PipelineLayout>>,
    scheme_cache: DashMap<(u32, u32), Weak<ImgScheme>>,
}

impl SchemeManager {
    pub fn new() -> EngResult<Self> {
        Ok(Self {
            layout_list: Default::default(),
            scheme_cache: Default::default(),
        })
    }
}

impl Drop for SchemeManager {
    fn drop(&mut self) {
        let driver = VKDriver::instance().unwrap();

        self.layout_list
            .write()
            .unwrap()
            .iter_mut()
            .for_each(|layout| driver.destroy_ppl_layout(layout))
    }
}

impl SchemeManager {
    pub fn create_img_pipeline(
        &self,
        target: &RenderTarget,
        vert_shader: &AssetHandle,
        frag_shader: &AssetHandle,
    ) -> EngResult<Arc<ImgScheme>> {
        let key = (vert_shader.tag(), frag_shader.tag());

        let v_ref: DataBlock = vert_shader.load()?.take().ok_or_else(|| {
            MISSING_CREATE_VALUE
                .clone()
                .with_root_cause("vert_shader data")
        })?;
        let f_ref: DataBlock = frag_shader.load()?.take().ok_or_else(|| {
            MISSING_CREATE_VALUE
                .clone()
                .with_root_cause("frag_shader data")
        })?;

        let cached = self.scheme_cache.get(&key).and_then(|r| r.upgrade());

        match cached {
            Some(scheme) => {
                println!("SCHEME CACHE HIT!");
                Ok(scheme)
            }
            None => {
                let driver = VKDriver::instance().unwrap();
                let lock = self.layout_list.read().unwrap();
                let layout = lock[0]; //NOTE: CHANGE THIS LATER

                let scheme =
                    ImgScheme::new(driver, target, layout, v_ref.as_shader(), f_ref.as_shader())?
                        .into();

                //keeps weak ref on cache
                self.scheme_cache.insert(key, Arc::downgrade(&scheme));
                Ok(scheme)
            }
        }
    }

    pub fn define_ppl_layout(&self, res_layouts: &[vk::DescriptorSetLayout]) -> EngResult<()> {
        let driver = VKDriver::instance().unwrap();
        let push_consts = [vk::PushConstantRange::default()
            .stage_flags(vk::ShaderStageFlags::VERTEX)
            .offset(0)
            .size(mem::size_of::<Origin>() as u32)];
        let layout_create_info = vk::PipelineLayoutCreateInfo::default()
            .set_layouts(res_layouts)
            .push_constant_ranges(&push_consts);

        let pipeline_layout = driver.create_ppl_layout(&layout_create_info)?;

        self.layout_list.write().cast()?.push(pipeline_layout);
        Ok(())
    }
}

///This is a smart wrapper around the pipeline and shader modules. It's supposed to be kept inside
///an Arc
pub struct ImgScheme {
    vert_module: vk::ShaderModule,
    frag_module: vk::ShaderModule,

    pipeline: vk::Pipeline,
    layout: Derived<vk::PipelineLayout>,
}

impl Debug for ImgScheme {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "SCHEME {{ vert: \"{:?}\", frag: {:?}, pipeline: {:?}, layout: {:?} }}",
            self.vert_module, self.frag_module, self.pipeline, self.layout
        )
    }
}

impl ImgScheme {
    pub fn new(
        driver: &VKDriver,
        target: &RenderTarget,
        ppl_layout: vk::PipelineLayout,
        vert_shader: &[u32],
        frag_shader: &[u32],
    ) -> EngResult<Self> {
        let vertex_shader_info = vk::ShaderModuleCreateInfo::default().code(vert_shader);

        let frag_shader_info = vk::ShaderModuleCreateInfo::default().code(frag_shader);

        let vert_module = driver.create_shader_mdl(&vertex_shader_info)?;

        let frag_module = driver.create_shader_mdl(&frag_shader_info)?;

        let shader_entry_name = unsafe { CStr::from_bytes_with_nul_unchecked(b"main\0") };
        let shader_stage_create_infos = [
            vk::PipelineShaderStageCreateInfo {
                module: vert_module,
                p_name: shader_entry_name.as_ptr(),
                stage: vk::ShaderStageFlags::VERTEX,
                ..Default::default()
            },
            vk::PipelineShaderStageCreateInfo {
                s_type: vk::StructureType::PIPELINE_SHADER_STAGE_CREATE_INFO,
                module: frag_module,
                p_name: shader_entry_name.as_ptr(),
                stage: vk::ShaderStageFlags::FRAGMENT,
                ..Default::default()
            },
        ];

        let vertex_input_binding_descriptions = [vk::VertexInputBindingDescription {
            binding: 0,
            stride: mem::size_of::<VertexFAT>() as u32,
            input_rate: vk::VertexInputRate::VERTEX,
        }];
        let vertex_input_attribute_descriptions = [
            vk::VertexInputAttributeDescription {
                location: 0,
                binding: 0,
                format: vk::Format::R32G32B32A32_SFLOAT,
                offset: crate::offset_of!(VertexFAT, pos) as u32,
            },
            vk::VertexInputAttributeDescription {
                location: 1,
                binding: 0,
                format: vk::Format::R32G32B32A32_SFLOAT,
                offset: crate::offset_of!(VertexFAT, uv) as u32,
            },
        ];

        let vertex_input_state_info = vk::PipelineVertexInputStateCreateInfo::default()
            .vertex_attribute_descriptions(&vertex_input_attribute_descriptions)
            .vertex_binding_descriptions(&vertex_input_binding_descriptions);
        let vertex_input_assembly_state_info = vk::PipelineInputAssemblyStateCreateInfo {
            topology: vk::PrimitiveTopology::TRIANGLE_LIST,
            ..Default::default()
        };

        let viewport_state_info = vk::PipelineViewportStateCreateInfo::default()
            .scissors(target.scissors())
            .viewports(target.viewports());

        let rasterization_info = vk::PipelineRasterizationStateCreateInfo {
            front_face: vk::FrontFace::COUNTER_CLOCKWISE,
            line_width: 1.0,
            polygon_mode: vk::PolygonMode::FILL,
            ..Default::default()
        };
        let multisample_state_info = vk::PipelineMultisampleStateCreateInfo {
            rasterization_samples: vk::SampleCountFlags::TYPE_1,
            ..Default::default()
        };
        let noop_stencil_state = vk::StencilOpState {
            fail_op: vk::StencilOp::KEEP,
            pass_op: vk::StencilOp::KEEP,
            depth_fail_op: vk::StencilOp::KEEP,
            compare_op: vk::CompareOp::ALWAYS,
            ..Default::default()
        };
        let depth_state_info = vk::PipelineDepthStencilStateCreateInfo {
            depth_test_enable: vk::TRUE,
            depth_write_enable: vk::TRUE,
            depth_compare_op: vk::CompareOp::LESS_OR_EQUAL,
            front: noop_stencil_state,
            back: noop_stencil_state,
            max_depth_bounds: 1.0,
            ..Default::default()
        };
        let color_blend_attachment_states = [vk::PipelineColorBlendAttachmentState {
            blend_enable: vk::TRUE,
            src_color_blend_factor: vk::BlendFactor::SRC_ALPHA,
            dst_color_blend_factor: vk::BlendFactor::ONE_MINUS_SRC_ALPHA,
            color_blend_op: vk::BlendOp::ADD,
            src_alpha_blend_factor: vk::BlendFactor::ONE,
            dst_alpha_blend_factor: vk::BlendFactor::ONE,
            alpha_blend_op: vk::BlendOp::ADD,
            color_write_mask: vk::ColorComponentFlags::RGBA,
        }];
        let color_blend_state = vk::PipelineColorBlendStateCreateInfo::default()
            .logic_op(vk::LogicOp::CLEAR)
            .attachments(&color_blend_attachment_states);

        let dynamic_state = [vk::DynamicState::VIEWPORT, vk::DynamicState::SCISSOR];
        let dynamic_state_info =
            vk::PipelineDynamicStateCreateInfo::default().dynamic_states(&dynamic_state);

        let graphic_pipeline_info = vk::GraphicsPipelineCreateInfo::default()
            .stages(&shader_stage_create_infos)
            .vertex_input_state(&vertex_input_state_info)
            .input_assembly_state(&vertex_input_assembly_state_info)
            .viewport_state(&viewport_state_info)
            .rasterization_state(&rasterization_info)
            .multisample_state(&multisample_state_info)
            .depth_stencil_state(&depth_state_info)
            .color_blend_state(&color_blend_state)
            .dynamic_state(&dynamic_state_info)
            .layout(ppl_layout)
            .render_pass(*target.renderpass());

        let graphics_pipelines = driver.create_graphic_ppls(&[graphic_pipeline_info])?;

        let pipeline = graphics_pipelines[0];

        Ok(Self {
            vert_module,
            frag_module,
            pipeline,
            layout: ppl_layout.into(),
        })
    }
}

impl ImgScheme {
    pub fn get_layout(&self) -> vk::PipelineLayout {
        *self.layout
    }

    pub fn as_pipeline(&self) -> vk::Pipeline {
        self.pipeline
    }
}

impl Drop for ImgScheme {
    fn drop(&mut self) {
        let driver = VKDriver::instance().unwrap();
        driver.destroy_ppl(&mut self.pipeline);
        driver.destroy_shader_mdl(&mut self.frag_module);
        driver.destroy_shader_mdl(&mut self.vert_module);
    }
}

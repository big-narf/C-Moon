use std::sync::atomic::{AtomicU32, AtomicU8, AtomicUsize, Ordering};
use std::sync::{Mutex, MutexGuard};

use ash::vk;
use ash::{ext, khr};

use raw_window_handle::*;

use cmoon_commons::*;

use super::*;
use crate::infra::*;
use crate::thread::*;

///this is a convenience wrapper around the swapchain and it's images
pub struct Presenter {
    current_frame_id: AtomicUsize,

    surface: vk::SurfaceKHR,

    swapchain: FATSwapchain,
    color_views: Vec<vk::ImageView>,
    depth_image: vk::Image,

    depth_view: vk::ImageView,
    depth_memory: GImage<Depth16>,

    target: RenderTarget,

    frame_free: vk::Fence,       //Signaled when the frame can be acquired
    present_done: vk::Semaphore, //Signaled when the present op has ended

    //illustrator stuff
    cmd_available: vk::Fence, //Signaled when the command buffer can be used
    render_done: Vec<vk::Semaphore>, //Signaled when the render op has ended

    wait_mask: Vec<vk::PipelineStageFlags>,

    primary_pool: vk::CommandPool,
    primary_buffer: vk::CommandBuffer,

    derived_buffers: Derived<Vec<vk::CommandBuffer>>,
}

impl Presenter {
    pub fn new(
        display_handle: RawDisplayHandle,
        window_handle: RawWindowHandle,
        height: u32,
        width: u32,
        derived_buffers: Derived<Vec<vk::CommandBuffer>>,
        queue_id: u32,
        wait_mask: &[vk::PipelineStageFlags],
    ) -> EngResult<Self> {
        let driver = VKDriver::instance().unwrap();

        let surface = driver.create_surface(display_handle, window_handle)?;

        let surface_info = driver.surface_info(&surface).cast()?;

        let mut img_count = surface_info.capabilities.min_image_count + 1;
        if surface_info.capabilities.max_image_count > 0
            && img_count > surface_info.capabilities.max_image_count
        {
            img_count = surface_info.capabilities.max_image_count;
        }

        let surface_resolution = match surface_info.capabilities.current_extent.width {
            u32::MAX => vk::Extent2D { width, height },
            _ => surface_info.capabilities.current_extent,
        };

        let pre_transform = if surface_info
            .capabilities
            .supported_transforms
            .contains(vk::SurfaceTransformFlagsKHR::IDENTITY)
        {
            vk::SurfaceTransformFlagsKHR::IDENTITY
        } else {
            surface_info.capabilities.current_transform
        };

        let present_mode = surface_info
            .present_modes
            .iter()
            .cloned()
            .find(|&mode| mode == vk::PresentModeKHR::MAILBOX)
            .unwrap_or(vk::PresentModeKHR::FIFO);

        let swapchain_create_info = vk::SwapchainCreateInfoKHR::default()
            .surface(surface)
            .min_image_count(img_count)
            .image_color_space(surface_info.format.color_space)
            .image_format(surface_info.format.format)
            .image_extent(surface_resolution)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
            .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
            .pre_transform(pre_transform)
            .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(present_mode)
            .clipped(true)
            .image_array_layers(1);

        let swapchain = driver.create_swapchain(&swapchain_create_info).cast()?;

        /*
         * IMAGES
         */
        let color_views: Vec<vk::ImageView> = swapchain
            .images
            .iter()
            .map(|&image| {
                let create_view_info = vk::ImageViewCreateInfo::default()
                    .view_type(vk::ImageViewType::TYPE_2D)
                    .format(surface_info.format.format)
                    .components(vk::ComponentMapping {
                        r: vk::ComponentSwizzle::R,
                        g: vk::ComponentSwizzle::G,
                        b: vk::ComponentSwizzle::B,
                        a: vk::ComponentSwizzle::A,
                    })
                    .subresource_range(vk::ImageSubresourceRange {
                        aspect_mask: vk::ImageAspectFlags::COLOR,
                        base_mip_level: 0,
                        level_count: 1,
                        base_array_layer: 0,
                        layer_count: 1,
                    })
                    .image(image);
                driver.create_view(&create_view_info)
            })
            .collect::<EngResult<_>>()?;

        let depth_format = vk::Format::D16_UNORM;
        let create_info = vk::ImageCreateInfo::default()
            .image_type(vk::ImageType::TYPE_2D)
            .format(depth_format)
            .extent(surface_resolution.into())
            .mip_levels(1)
            .array_layers(1)
            .samples(vk::SampleCountFlags::TYPE_1)
            .tiling(vk::ImageTiling::OPTIMAL)
            .usage(vk::ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT)
            .sharing_mode(vk::SharingMode::EXCLUSIVE);

        let depth_image = driver.create_image(&create_info)?;

        let depth_memory = driver.allocate_for(&depth_image)?;

        driver.bind_image(&depth_image, &depth_memory)?;

        let create_info = vk::ImageViewCreateInfo::default()
            .subresource_range(
                vk::ImageSubresourceRange::default()
                    .aspect_mask(vk::ImageAspectFlags::DEPTH)
                    .level_count(1)
                    .layer_count(1),
            )
            .image(depth_image)
            .format(depth_format)
            .view_type(vk::ImageViewType::TYPE_2D);

        let depth_view = driver.create_view(&create_info)?;

        let target = RenderTarget::new(
            &driver,
            surface_resolution,
            surface_info.format.format,
            &color_views,
            depth_format,
            &depth_view,
        )?;
        /*
         *       Synchronization
         */
        let create_info = vk::SemaphoreCreateInfo::default();
        let present_done = driver.create_semaphore(&create_info)?;
        let create_info = vk::FenceCreateInfo::default().flags(vk::FenceCreateFlags::SIGNALED);
        let frame_free = driver.create_fence(&create_info)?;

        let create_info = vk::CommandPoolCreateInfo::default()
            .queue_family_index(queue_id)
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER);
        let primary_pool = driver.create_cmd_pool(&create_info)?;
        let create_info = vk::CommandBufferAllocateInfo::default()
            .command_pool(primary_pool)
            .level(vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(1);
        let primary_buffer = driver
            .allocate_cmd_buffers(&create_info)?
            .into_iter()
            .next()
            .ok_or_else(|| {
                CMD_BUFFER_ALLOC_FAIL
                    .clone()
                    .with_root_cause("Primary@Illustrator")
            })?;

        //NOTE: SYNCING OBJECTS
        let create_info = vk::FenceCreateInfo::default().flags(vk::FenceCreateFlags::SIGNALED);
        let cmd_available = driver.create_fence(&create_info)?;

        let create_info = vk::SemaphoreCreateInfo::default();
        let render_done = [driver.create_semaphore(&create_info)?].into();

        Ok(Self {
            current_frame_id: Default::default(),

            surface,

            swapchain,
            color_views,

            depth_image,
            depth_view,
            depth_memory,

            target,

            present_done,
            frame_free,

            cmd_available,
            primary_pool,
            primary_buffer,
            render_done,
            derived_buffers,
            wait_mask: wait_mask.to_owned(),
        })
    }
}

impl Presenter {
    ///Swaps the currently drawn image and starts the main cmd buffer
    pub fn next_frame(&self) -> EngResult<()> {
        //NOTE: CHANGE CURRENT FRAME
        let driver = VKDriver::instance().unwrap();

        let wait_fences = [self.frame_free, self.cmd_available];
        let (present_index, sub_obptimal) = driver.acquire_image(
            &self.swapchain,
            &wait_fences,
            &self.present_done,
            &self.frame_free,
        )?;
        self.current_frame_id.store(present_index, Ordering::SeqCst);

        driver.reset_primary(&self.primary_buffer)?; //, &[self.cmd_available])?;

        self.target
            .begin(driver, &self.primary_buffer, present_index as usize)
    }

    ///pushes the framebuffer to the screen when it's done rendering. It's supposed to be called by
    ///a background thread.
    pub fn present(&self) -> EngResult<()> {
        let driver = VKDriver::instance().unwrap();

        //sends commands to GPU
        driver.finish_primary(&self.primary_buffer, &self.derived_buffers)?;
        let cmd_buffs = [self.primary_buffer];
        let wait_list = [self.present_done];

        driver.submit_graphic_cmds(
            &cmd_buffs,
            &wait_list,
            &self.wait_mask,
            self.cmd_available,
            &self.render_done,
        )?;

        let indices = [self.current_frame_id.load(Ordering::SeqCst) as u32];

        driver.submit_present(&indices, &self.swapchain, &self.render_done)?;
        Ok(())
    }
}

impl<'p> Presenter {
    pub fn color(&'p self) -> &'p [vk::ImageView] {
        &self.color_views
    }

    pub fn depth(&'p self) -> &'p vk::ImageView {
        &self.depth_view
    }
    pub fn swapchain(&'p self) -> &'p vk::SwapchainKHR {
        &self.swapchain
    }

    pub fn semaphore(&'p self) -> vk::Semaphore {
        self.present_done
    }

    pub fn target(&'p self) -> &'p RenderTarget {
        &self.target
    }

    pub fn current_frame(&'p self) -> usize {
        self.current_frame_id.load(Ordering::SeqCst)
    }
}

impl Drop for Presenter {
    fn drop(&mut self) {
        let driver = VKDriver::instance().unwrap();
        driver.wait_idle().unwrap();

        driver.free(&mut self.depth_memory);

        driver.destroy_view(&mut self.depth_view);
        driver.destroy_image(&mut self.depth_image);

        self.color_views
            .iter_mut()
            .for_each(|v| driver.destroy_view(v));

        //swapchain cleanup
        driver.destroy_swapchain(&mut self.swapchain);

        driver.destroy_surface(&mut self.surface);
        //synchro cleanup
        driver.destroy_semaphore(&mut self.present_done);

        driver.destroy_cmd_pool(&mut self.primary_pool);

        self.render_done
            .iter_mut()
            .for_each(|s| driver.destroy_semaphore(s));
        driver.destroy_fence(&mut self.cmd_available);
    }
}

///Wrapper around a renderpass and related shit
pub struct RenderTarget {
    renderpass: vk::RenderPass,
    framebuffers: Vec<vk::Framebuffer>,
    resolution: vk::Extent2D,
    viewports: Vec<vk::Viewport>,
    scissors: Vec<vk::Rect2D>,
}

impl RenderTarget {
    pub fn new(
        driver: &VKDriver,
        resolution: vk::Extent2D,
        color_format: vk::Format,
        color_views: &[vk::ImageView],
        depth_format: vk::Format,
        depth_view: &vk::ImageView,
    ) -> EngResult<Self> {
        let renderpass_attachments = [
            vk::AttachmentDescription {
                format: color_format,
                samples: vk::SampleCountFlags::TYPE_1,
                load_op: vk::AttachmentLoadOp::CLEAR,
                store_op: vk::AttachmentStoreOp::STORE,
                final_layout: vk::ImageLayout::PRESENT_SRC_KHR,
                ..Default::default()
            },
            vk::AttachmentDescription {
                format: depth_format,
                samples: vk::SampleCountFlags::TYPE_1,
                load_op: vk::AttachmentLoadOp::CLEAR,
                initial_layout: vk::ImageLayout::UNDEFINED, //DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                //WARN: THIS IS PROBABLY WRONG, BUT GIVES A VALIDATION ERROR OTHERWISE
                final_layout: vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                ..Default::default()
            },
        ];
        let color_attachment_refs = [vk::AttachmentReference {
            attachment: 0,
            layout: vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
        }];
        let depth_attachment_ref = vk::AttachmentReference {
            attachment: 1,
            layout: vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        };
        let dependencies = [vk::SubpassDependency {
            src_subpass: vk::SUBPASS_EXTERNAL,
            src_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
            dst_access_mask: vk::AccessFlags::COLOR_ATTACHMENT_READ
                | vk::AccessFlags::COLOR_ATTACHMENT_WRITE,
            dst_stage_mask: vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
            ..Default::default()
        }];

        let subpass = vk::SubpassDescription::default()
            .color_attachments(&color_attachment_refs)
            .depth_stencil_attachment(&depth_attachment_ref)
            .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS);

        let renderpass_create_info = vk::RenderPassCreateInfo::default()
            .attachments(&renderpass_attachments)
            .subpasses(std::slice::from_ref(&subpass))
            .dependencies(&dependencies);

        let renderpass = driver.create_renderpass(&renderpass_create_info)?;

        let framebuffers = color_views
            .iter()
            .map(|&present_image_view| {
                let framebuffer_attachments = [present_image_view, *depth_view];
                let frame_buffer_create_info = vk::FramebufferCreateInfo::default()
                    .render_pass(renderpass)
                    .attachments(&framebuffer_attachments)
                    .width(resolution.width)
                    .height(resolution.height)
                    .layers(1);

                driver.create_framebuffer(&frame_buffer_create_info)
            })
            .collect::<EngResult<Vec<_>>>()?;

        let viewports = vec![vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: resolution.width as f32,
            height: resolution.height as f32,
            min_depth: 0.0,
            max_depth: 1.0,
        }];
        let scissors = vec![resolution.into()];

        Ok(Self {
            renderpass,
            framebuffers,
            resolution,
            viewports,
            scissors,
        })
    }

    pub fn begin(
        &self,
        driver: &VKDriver,
        c_buffer: &vk::CommandBuffer,
        current_frame: usize,
    ) -> EngResult<()> {
        let clear_values = [
            vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [0.0, 0.0, 0.0, 1.0],
                },
            },
            vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0,
                },
            },
        ];

        let framebuffer = self.framebuffers.get(current_frame).unwrap();
        let render_pass_begin_info = vk::RenderPassBeginInfo::default()
            .render_pass(self.renderpass)
            .framebuffer(*framebuffer)
            .render_area(self.resolution.into())
            .clear_values(&clear_values);

        driver.begin_renderpass(c_buffer, &render_pass_begin_info);
        Ok(())
    }
}

impl<'tar> RenderTarget {
    pub fn framebuffer(&'tar self, id: usize) -> &'tar vk::Framebuffer {
        &self.framebuffers[id]
    }

    pub fn renderpass(&'tar self) -> &'tar vk::RenderPass {
        &self.renderpass
    }

    pub fn viewports(&'tar self) -> &'tar [vk::Viewport] {
        &self.viewports
    }

    pub fn scissors(&'tar self) -> &'tar [vk::Rect2D] {
        &self.scissors
    }

    pub fn resolution(&self) -> vk::Extent2D {
        self.resolution
    }
}

impl Drop for RenderTarget {
    fn drop(&mut self) {
        let driver = VKDriver::instance().unwrap();

        self.framebuffers
            .iter_mut()
            .for_each(|f| driver.destroy_framebuffer(f));
        driver.destroy_renderpass(&mut self.renderpass);
    }
}

/*
///NOTE: MOVE THESE TO THE PRESENTER, WILL GIVE ME A BETTER LOGIC FLOW OF THE SYSTEM
///This is a wrapper around a set of command buffers and associated synchro primitives.
pub struct Illustrator {
    cmd_available: vk::Fence,
    render_done: Vec<vk::Semaphore>,
    wait_mask: Vec<vk::PipelineStageFlags>,

    primary_pool: vk::CommandPool,
    primary_buffer: vk::CommandBuffer,

    //cmd_pools: Vec<vk::CommandPool>,
    ctx_list: Vec<DrawCtx>,
    secondary_buff_derive: Derived<Vec<vk::CommandBuffer>>, //these are not supposed to be droped by the
                                                            //illustrator
}

///Basic utilities
impl Illustrator {
    pub fn new(n_threads: usize, wait_mask: &[vk::PipelineStageFlags]) -> EngResult<Self> {
        let queue_id = 0; //TODO: REMOVE THIS TO SOME OTHER PLACE

        let device = VKDriver::device().unwrap();

        //NOTE: PRIMARY OBJECTS
        let create_info = vk::CommandPoolCreateInfo::default()
            .queue_family_index(queue_id)
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER);
        let primary_pool = unsafe { device.create_command_pool(&create_info, None) }.cast()?;
        let create_info = vk::CommandBufferAllocateInfo::default()
            .command_pool(primary_pool)
            .level(vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(1);
        let primary_buffer = unsafe { device.allocate_command_buffers(&create_info) }
            .cast()?
            .into_iter()
            .next()
            .ok_or_else(|| {
                CMD_BUFFER_ALLOC_FAIL
                    .clone()
                    .with_root_cause("Primary@Illustrator")
            })?;

        //NOTE: SECONDARY OBJECTS
        let ctx_list: Vec<_> = (0..n_threads)
            .map(|_| DrawCtx::new(device, queue_id))
            .collect::<EngResult<_>>()?;

        let secondary_buff_derive: Derived<Vec<_>> =
            ctx_list.iter().map(|ctx| ctx.derived_buffer()).collect();

        //NOTE: SYNCING OBJECTS
        let create_info = vk::FenceCreateInfo::default().flags(vk::FenceCreateFlags::SIGNALED);
        let cmd_available = unsafe { device.create_fence(&create_info, None) }.cast()?;

        let create_info = vk::SemaphoreCreateInfo::default();
        let render_done = [unsafe { device.create_semaphore(&create_info, None) }.cast()?].into();

        Ok(Self {
            wait_mask: wait_mask.to_owned(),

            primary_pool,
            primary_buffer,

            render_done,
            cmd_available,

            ctx_list,
            secondary_buff_derive,
        })
    }

    pub fn schedule_destroy(&self, thread_id: usize, local_id: usize) {
        self.ctx_list
            .get(thread_id)
            .unwrap()
            .schedule_destroy(local_id);
    }
}

///Main thread called functions (single use per frame)
impl Illustrator {
    ///Starts the render pass and binds the geometry group to the pipeline
    pub fn begin_render(
        &self,
        device: &ash::Device,
        framebuffer: &vk::Framebuffer,
        area: vk::Rect2D,
        renderpass: &vk::RenderPass,
        viewports: &[vk::Viewport],
        scissors: &[vk::Rect2D],
        geo: &GeometryManager<VertexFAT>,
    ) -> VkResult<()> {
        //Sets up main command buffer

        unsafe {
            //wait for GPU render fence
            device.wait_for_fences(&[self.cmd_available], true, u64::MAX)?;
            device.reset_command_buffer(
                self.primary_buffer,
                vk::CommandBufferResetFlags::RELEASE_RESOURCES,
            )?;

            let begin_info = vk::CommandBufferBeginInfo::default()
                .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

            device.begin_command_buffer(self.primary_buffer, &begin_info)?;
        }

        let clear_values = [
            vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [0.0, 0.0, 0.0, 1.0],
                },
            },
            vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0,
                },
            },
        ];

        let render_pass_begin_info = vk::RenderPassBeginInfo::default()
            .render_pass(*renderpass)
            .framebuffer(*framebuffer)
            .render_area(area)
            .clear_values(&clear_values);

        //Common commands to all threads
        unsafe {
            /*self.device
                .cmd_set_viewport(self.primary_buffer, 0, viewports);
            self.device
                .cmd_set_scissor(self.primary_buffer, 0, scissors);*/
            device.cmd_begin_render_pass(
                self.primary_buffer,
                &render_pass_begin_info,
                vk::SubpassContents::SECONDARY_COMMAND_BUFFERS,
            );
        }
        Ok(())
    }

    ///This is a function called by the main thread.
    pub fn end_render(
        &self,
        device: &ash::Device,
        queue: &vk::Queue,
        wait_semaphores: &[vk::Semaphore],
    ) -> VkResult<()> {
        //synchronization and submit commands

        let b = [self.primary_buffer];
        let submit_info = vk::SubmitInfo::default()
            .wait_semaphores(wait_semaphores)
            .wait_dst_stage_mask(&self.wait_mask)
            .command_buffers(&b)
            //.command_buffers(&self.cmd_buffers)
            .signal_semaphores(&self.render_done);

        unsafe {
            //sends commands to GPU
            device.cmd_execute_commands(self.primary_buffer, &self.secondary_buff_derive);
            device.cmd_end_render_pass(self.primary_buffer);
            device.end_command_buffer(self.primary_buffer)?;
            device.reset_fences(&[self.cmd_available])?;
            device.queue_submit(*queue, &[submit_info], self.cmd_available)?;
        };

        Ok(())
    }
}

impl<'i> Illustrator {
    pub fn render_done(&'i self) -> &'i [vk::Semaphore] {
        &self.render_done
    }

    ///S
    pub fn get_draw(
        &'i self,
        thread_id: usize,
        framebuffer: &vk::Framebuffer,
        area: vk::Rect2D,
        renderpass: &vk::RenderPass,
        viewports: &[vk::Viewport],
        scissors: &[vk::Rect2D],
        geo: &GeometryManager<VertexFAT>,
    ) -> EngResult<&'i DrawCtx> {
        let c_buffer = self
            .cmd_buffers
            .get(thread_id)
            .ok_or_else(|| INVALID_THREAD_ID.clone().with_root_cause(thread_id))?;

        unsafe {
            //wait for GPU render fence
            self.device
                .wait_for_fences(&[self.cmd_available], true, u64::MAX)
                .cast()?;

            self.device
                .reset_command_buffer(*c_buffer, vk::CommandBufferResetFlags::RELEASE_RESOURCES)
                .cast()?;

            let inheritance = vk::CommandBufferInheritanceInfo::default().render_pass(*renderpass);

            let begin_info = vk::CommandBufferBeginInfo::default()
                .flags(
                    vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT
                        | vk::CommandBufferUsageFlags::RENDER_PASS_CONTINUE,
                )
                .inheritance_info(&inheritance);

            self.device
                .begin_command_buffer(*c_buffer, &begin_info)
                .cast()?;
        }

        /*
        let clear_values = [
            vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [0.0, 0.0, 0.0, 1.0],
                },
            },
            vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0,
                },
            },
        ];

        let render_pass_begin_info = vk::RenderPassBeginInfo::default()
            .render_pass(*renderpass)
            .framebuffer(*framebuffer)
            .render_area(area)
            .clear_values(&clear_values);*/

        //Common commands to all threads
        unsafe {
            self.device.cmd_set_viewport(*c_buffer, 0, viewports);

            self.device.cmd_set_scissor(*c_buffer, 0, scissors);

            self.device
                .cmd_bind_vertex_buffers(*c_buffer, 0, &[*geo.vertices()], &[0]);
            self.device
                .cmd_bind_index_buffer(*c_buffer, *geo.indices(), 0, vk::IndexType::UINT32);
            /*
            self.device.cmd_begin_render_pass(
                *c_buffer,
                &render_pass_begin_info,
                vk::SubpassContents::INLINE,
            );*/
        }

        Ok(DrawGroup {
            device: &self.device,
            c_buffer,
        })
    }
}

impl Drop for Illustrator {
    fn drop(&mut self) {
        unsafe {
            self.cmd_pools
                .iter_mut()
                .for_each(|p| self.device.destroy_command_pool(*p, None));

            self.render_done
                .iter_mut()
                .for_each(|s| self.device.destroy_semaphore(*s, None));
            self.device.destroy_fence(self.cmd_available, None);
        }
    }
}*/

use cmoon_commons::*;

mod asset;
mod geometry;
mod render; //NOTE: MOVE THIS TO CONTEXT
mod shader;

pub use asset::*;
pub use geometry::*;
pub use render::*;
pub use shader::*;

use crate::infra::VKDriver;

pub struct SharedManager {
    pub geometry: GeometryManager<VertexFAT>,
    pub asset: ResourceManager,
    pub scheme: SchemeManager,
}

impl SharedManager {
    pub fn new(
        driver: &VKDriver,
        max_desc: u32,
        index_max: usize,
        vertex_max: usize,
    ) -> EngResult<Self> {
        let geometry = GeometryManager::new(driver, index_max, vertex_max)?;
        let asset = ResourceManager::new(driver, max_desc as u32)?;
        let scheme = SchemeManager::new()?;
        Ok(Self {
            geometry,
            asset,
            scheme,
        })
    }
}

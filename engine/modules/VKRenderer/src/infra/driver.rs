use std::ffi::{c_char, CStr};
use std::ops::Deref;
use std::sync::{Arc, OnceLock};

use ash::ext;
use ash::khr::{self, swapchain};
use ash::vk;
use ash::{Entry, Instance};
use raw_window_handle::{RawDisplayHandle, RawWindowHandle};

use cmoon_commons::*;

use super::*;

#[macro_export]
macro_rules! offset_of {
    ($base:path, $field:ident) => {{
        #[allow(unused_unsafe)]
        unsafe {
            let b: $base = mem::zeroed();
            std::ptr::addr_of!(b.$field) as isize - std::ptr::addr_of!(b) as isize
        }
    }};
}

pub(super) fn find_memorytype_index(
    memory_req: &vk::MemoryRequirements,
    memory_prop: &vk::PhysicalDeviceMemoryProperties,
    flags: vk::MemoryPropertyFlags,
) -> Option<u32> {
    memory_prop.memory_types[..memory_prop.memory_type_count as _]
        .iter()
        .enumerate()
        .find(|(index, memory_type)| {
            (1 << index) & memory_req.memory_type_bits != 0
                && memory_type.property_flags & flags == flags
        })
        .map(|(index, _memory_type)| index as _)
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd)]
pub(super) struct Region {
    pub(super) offset: vk::DeviceSize,
    pub(super) size: vk::DeviceSize,
}

impl Ord for Region {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.size.cmp(&other.size)
    }
}

impl Region {
    pub fn end(&self) -> vk::DeviceSize {
        self.offset + self.size
    }

    pub fn merge(&mut self, r2: Self) {
        self.offset = u64::min(self.offset, r2.offset);
        self.size = self.size + r2.size;
    }

    ///Returns a region of size from the end of the region
    pub fn back_split(&mut self, size: vk::DeviceSize) -> Self {
        let part = Self {
            offset: self.offset + self.size - size,
            size: size,
        };
        self.size -= size;
        part
    }
    ///Returns a region of size from the end of the region
    pub fn front_split(&mut self, size: vk::DeviceSize) -> Self {
        let part = Self {
            offset: self.offset,
            size: size,
        };
        self.offset += size;
        self.size -= size;
        part
    }

    ///Splits the region in half, return the back half as result
    pub fn back_bissection(&mut self) -> Self {
        let half_size = self.size >> 1;
        let part = Self {
            offset: self.offset + half_size,
            size: half_size,
        };
        self.size = half_size;
        part
    }

    ///Splits the region in half, return the front half as result
    pub fn front_bissection(&mut self) -> Self {
        let half_size = self.size >> 1;
        let part = Self {
            offset: self.offset,
            size: half_size,
        };
        self.offset += half_size;
        self.size = half_size;
        part
    }
}

#[derive(Clone, Copy)]
pub struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
    pub _pad: f32,
}

impl Fill for Vector3 {
    fn try_fill<R: rand::Rng + ?Sized>(&mut self, rng: &mut R) -> Result<(), rand::Error> {
        self.x = rng.gen_range(0.0..1.0);
        self.y = rng.gen_range(0.0..1.0);
        self.z = rng.gen_range(0.0..1.0);
        self._pad = rng.gen_range(0.0..1.0);
        Ok(())
    }
}

pub struct SurfaceInfo {
    pub format: vk::SurfaceFormatKHR,
    pub capabilities: vk::SurfaceCapabilitiesKHR,
    pub present_modes: Vec<vk::PresentModeKHR>,
}

///Wrapper around swapchain + images
pub struct FATSwapchain {
    pub swapchain: vk::SwapchainKHR,
    pub images: Vec<vk::Image>,
}

impl Deref for FATSwapchain {
    type Target = vk::SwapchainKHR;
    fn deref(&self) -> &Self::Target {
        &self.swapchain
    }
}

pub fn enumerate_required_extensions(
    display_handle: RawDisplayHandle,
) -> EngResult<&'static [*const c_char]> {
    match display_handle {
        RawDisplayHandle::Windows(_) => {
            const WINDOWS_EXTS: [*const c_char; 2] = [
                khr::surface::NAME.as_ptr(),
                khr::win32_surface::NAME.as_ptr(),
            ];
            Ok(&WINDOWS_EXTS)
        }

        RawDisplayHandle::Wayland(_) => {
            const WAYLAND_EXTS: [*const c_char; 2] = [
                khr::surface::NAME.as_ptr(),
                khr::wayland_surface::NAME.as_ptr(),
            ];
            Ok(&WAYLAND_EXTS)
        }

        RawDisplayHandle::Xlib(_) => {
            const XLIB_EXTS: [*const c_char; 2] = [
                khr::surface::NAME.as_ptr(),
                khr::xlib_surface::NAME.as_ptr(),
            ];
            Ok(&XLIB_EXTS)
        }

        RawDisplayHandle::Xcb(_) => {
            const XCB_EXTS: [*const c_char; 2] =
                [khr::surface::NAME.as_ptr(), khr::xcb_surface::NAME.as_ptr()];
            Ok(&XCB_EXTS)
        }

        RawDisplayHandle::Android(_) => {
            const ANDROID_EXTS: [*const c_char; 2] = [
                khr::surface::NAME.as_ptr(),
                khr::android_surface::NAME.as_ptr(),
            ];
            Ok(&ANDROID_EXTS)
        }

        /*
        RawDisplayHandle::AppKit(_) | RawDisplayHandle::UiKit(_) => {
            const METAL_EXTS: [*const c_char; 2] = [
                khr::Surface::NAME.as_ptr(),
                ext::MetalSurface::NAME.as_ptr(),
            ];
            &METAL_EXTS
        }*/
        _ => Err(SURFACE_CREATE_FAIL
            .clone()
            .with_root_cause("System Not supported")),
    }
}

///This is just a thin wrapper around a Vulkan value that is dropped on a struct that isn't this
///one.
#[derive(Debug)]
pub struct Derived<T>(T);

impl<T: Clone> Clone for Derived<T> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl<T: Copy> Copy for Derived<T> {}

impl<T> Deref for Derived<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> From<T> for Derived<T> {
    fn from(value: T) -> Self {
        Self(value)
    }
}

impl<T, C: FromIterator<T>> FromIterator<Derived<T>> for Derived<C> {
    fn from_iter<I: IntoIterator<Item = Derived<T>>>(iter: I) -> Self {
        let col: C = iter.into_iter().map(|v| v.0).collect();
        Self(col)
    }
}

pub trait AsShader {
    fn as_shader(&self) -> &[u32];
}

impl AsShader for [u8] {
    fn as_shader(&self) -> &[u32] {
        unsafe { std::slice::from_raw_parts(self.as_ptr() as *const u32, self.len() >> 2) }
    }
}

pub trait GetCreateValue<T> {
    fn get_create_value(&mut self, name: &str) -> EngResult<T>;
}

impl<T: FromDyn> GetCreateValue<T> for AssetData {
    fn get_create_value(&mut self, name: &str) -> EngResult<T> {
        self.take_field(name)
            .ok_or_else(|| MISSING_CREATE_VALUE.clone().with_root_cause(name))
    }
}

impl<T: FromDyn> GetCreateValue<T> for EngMap<DynValue> {
    fn get_create_value(&mut self, name: &str) -> EngResult<T> {
        self.pop_as(name)
            .ok_or_else(|| MISSING_CREATE_VALUE.clone().with_root_cause(name))
    }
}

pub use std::sync::PoisonError;

pub use ash::prelude::VkResult;
pub use ash::vk::Result as VKError;
pub use ash::LoadingError;

use cmoon_commons::{EngResult, EngineError};

///Converts Vulkan error to engine error
pub trait IntoEngError {
    fn into_eng_error(self) -> EngineError;
}

impl IntoEngError for VKError {
    fn into_eng_error(self) -> EngineError {
        match self {
            Self::ERROR_EXTENSION_NOT_PRESENT | Self::ERROR_LAYER_NOT_PRESENT => {
                MISSING_FEATURE.clone()
            }
            Self::NOT_READY => THING_NOT_READY.clone(),
            Self::ERROR_OUT_OF_HOST_MEMORY => OUT_OF_MEMORY.clone().with_root_cause("Host"),
            Self::ERROR_OUT_OF_DEVICE_MEMORY => OUT_OF_MEMORY.clone().with_root_cause("Device"),
            Self::ERROR_OUT_OF_POOL_MEMORY => OUT_OF_MEMORY.clone().with_root_cause("Pool"),
            _ => UNIMPLEMENTED.clone(),
        }
    }
}

impl<T> IntoEngError for PoisonError<T> {
    fn into_eng_error(self) -> EngineError {
        LOCK_ERROR.clone()
    }
}

impl IntoEngError for LoadingError {
    fn into_eng_error(self) -> EngineError {
        API_LOAD_FAIL.clone()
    }
}

pub trait Cast {
    type Target;
    fn cast(self) -> Self::Target;
}

impl<T, E: IntoEngError> Cast for Result<T, E> {
    type Target = EngResult<T>;
    fn cast(self) -> Self::Target {
        self.map_err(|e| e.into_eng_error())
    }
}

pub trait WithRoot {
    fn with_root<S: ToString>(self, msg: S) -> Self;
}
impl<T> WithRoot for EngResult<T> {
    fn with_root<S: ToString>(self, msg: S) -> Self {
        self.map_err(|e| e.with_root_cause(msg))
    }
}

macro_rules! static_error {
    ($name:ident, $code:expr, $msg:expr) => {
        lazy_static::lazy_static! {
            pub static ref $name: EngineError = EngineError::default()
                .from_mdl(&crate::MDL_TYPE)
                .with_specifics($code, $msg);
        }
    };
}

//NOTE: X1XX SERIES (INIT ERRORS)
static_error!(
    CMD_BUFFER_ALLOC_FAIL,
    101,
    "Could not allocate command buffer"
);

static_error!(API_LOAD_FAIL, 102, "Vulkan library not found or malformed!");

static_error!(
    NO_SUITABLE_PDEVICE,
    103,
    "Couldn't find a suitable Physical device"
);

static_error!(
    SURFACE_CREATE_FAIL,
    104,
    "Could not create a surface for rendering"
);
static_error!(
    MISSING_FEATURE,
    105,
    "A Critical Vulkan extension or layer couldn't be loaded"
);

static_error!(
    INFRA_INIT_FAIL,
    106,
    "Couldnt initialize Renderer infrastructure"
);

static_error!(MISSING_CONFIG_FIELD, 107, "Field in config not found");

//NOTE: X2XX SERIES (OPS ERRORS)
static_error!(
    ALLOCATION_TOO_BIG,
    201,
    "Allocation is bigger than the allowed by the allocator"
);
static_error!(OUT_OF_MEMORY, 202, "Vulkan ran out of memory!");

static_error!(
    NO_SUITABLE_MEMORY,
    203,
    "Couldn't get a memory segment with the id required."
);
static_error!(DROP_FAILED, 204, "An error ocurred while droping a value!");

static_error!(
    FRAME_CAPTURE_FAIL,
    205,
    "Couldn't grab hold of current framebuffer"
);

static_error!(
    LOCK_ERROR,
    206,
    "Failed trying to grab lock to shared object"
);

static_error!(INVALID_THREAD, 207, "Trying to access an invalid thread id");

static_error!(
    INVALID_THREAD_ID,
    207,
    "Trying to access resources from an unknown thread."
);
//NOTE: X4XX SERIES (CREATE ERRORS)

static_error!(MISSING_CREATE_VALUE, 401, "Needed value not found");

static_error!(INVALID_PARTIAL_TYPE, 402, "Invalid partial object type");

static_error!(
    INVALID_DESC_SET_ID,
    403,
    "Trying to acquire an invalid descriptor set id"
);

static_error!(GEOMETRY_FILLED_UP, 404, "Geometry buffer is filled up");

//NOTE: X5XX SERIES (DESTROY ERRORS)

static_error!(
    OBJ_DONT_EXIST,
    501,
    "Trying to destroy an object that doenst exists!"
);

static_error!(THING_NOT_READY, 991, "Not ready!");

static_error!(
    UNIMPLEMENTED,
    999,
    "The VK renderer found an unknown error!"
);

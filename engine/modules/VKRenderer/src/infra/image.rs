use std::collections::{HashMap, HashSet};
use std::marker::PhantomData;
use std::mem;
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::{Arc, Mutex, OnceLock};

use ash::vk;

use cmoon_commons::*;

use super::*;

pub struct TexAllocator {
    mem_property: vk::PhysicalDeviceMemoryProperties,
    page_size: vk::DeviceSize,
    mem_pages: Mutex<HashMap<u32, TexChunk>>,
    counter: AtomicU32,
}

impl TexAllocator {
    pub fn new(
        mem_property: vk::PhysicalDeviceMemoryProperties,
        page_size: vk::DeviceSize,
    ) -> EngResult<Self> {
        Ok(Self {
            mem_property,
            page_size,
            mem_pages: Default::default(),
            counter: Default::default(),
        })
    }
}

///Debug functions
impl TexAllocator {
    ///Checks info about the currently used pages
    pub fn check_pages(&self) {
        let mut alloc_total = 0;
        let mut frag_sum = 0.0;
        let pages = self.mem_pages.lock().unwrap();
        pages.values().enumerate().for_each(|(n, page)| {
            let (total, max, frag) = page.memory_stats();
            println!("PAGE {n}: Free Total :{total} . Max Region :{max} . Usable: {frag} %");
            alloc_total += total;
            frag_sum += frag;
        });
        let used_mem = pages.len() as u64 * self.page_size;
        let frag_avg = frag_sum / (pages.len() as f64);
        println!(
            "TOTAL FREE MEMORY : {alloc_total} out of {used_mem}. Average Usability: {frag_avg} %"
        );
    }
}

///Generic functions
impl TexAllocator {
    pub unsafe fn allocate<T: Copy>(
        &self,
        device: &ash::Device,
        mem_req: &vk::MemoryRequirements,
    ) -> EngResult<GImage<T>> {
        let flags = vk::MemoryPropertyFlags::DEVICE_LOCAL;

        //let size = (mem::size_of::<T>() * pixels) as vk::DeviceSize;
        //let align = mem_req.alignment;
        if mem_req.size > self.page_size {
            Err(ALLOCATION_TOO_BIG
                .clone()
                .with_root_cause(format!("{}>MAX({})", mem_req.size, self.page_size)))
        } else {
            let memory_type_index = find_memorytype_index(mem_req, &self.mem_property, flags)
                .ok_or_else(|| NO_SUITABLE_MEMORY.clone())?;
            let mut pages = self.mem_pages.lock().unwrap();
            let (id, reg, memory) = pages
                .iter_mut()
                .find_map(|(&id, page)| {
                    if page.type_index != memory_type_index {
                        None
                    } else {
                        let region = page.get_region(mem_req.size, mem_req.alignment)?;
                        let memory = page.as_memory();
                        Some((id, region, memory))
                    }
                })
                .unwrap_or_else(|| {
                    let mut page = TexChunk::new(device, self.page_size as u64, memory_type_index)
                        .expect("PAGE ALLOCATION ERROR");
                    let region = page
                        .get_region(mem_req.size, mem_req.alignment)
                        .expect("HOW DID THIS ERROR'D?!");
                    let memory = page.as_memory();
                    let id = self.counter.fetch_add(1, Ordering::SeqCst);
                    pages.insert(id, page);
                    (id, region, memory)
                });

            Ok(GImage {
                page_id: id,
                mem: memory,
                region: reg,
                _marker: PhantomData,
            })
        }
    }

    pub unsafe fn free<T: Copy>(&self, device: &ash::Device, tex: &mut GImage<T>) {
        let mut pages = self.mem_pages.lock().unwrap();
        let page = pages.get_mut(&tex.page_id).unwrap();
        page.free_region(tex.region.clone());
        if page.free_total == self.page_size {
            let mut page = pages.remove(&tex.page_id).unwrap();
            page.destroy(device);
        }
    }
}

///GPU local chunk of memory for images
#[derive(Debug)]
struct TexChunk {
    type_index: u32,
    mem: vk::DeviceMemory,
    free_total: vk::DeviceSize,
    free_list: Vec<Region>,
}

impl TexChunk {
    pub unsafe fn new(
        device: &ash::Device,
        size: vk::DeviceSize,
        type_index: u32,
    ) -> VkResult<Self> {
        let allocate_info = vk::MemoryAllocateInfo::default()
            .allocation_size(size)
            .memory_type_index(type_index);
        let mem = device.allocate_memory(&allocate_info, None)?;
        let region = Region { offset: 0, size };
        Ok(Self {
            type_index,
            mem,
            free_total: size,
            free_list: vec![region],
        })
    }

    pub fn as_memory(&self) -> vk::DeviceMemory {
        self.mem
    }

    pub unsafe fn destroy(&mut self, device: &ash::Device) {
        device.free_memory(self.mem, None);
    }

    pub fn get_region(&mut self, size: vk::DeviceSize, align: vk::DeviceSize) -> Option<Region> {
        let size = size.next_power_of_two();
        if size > self.free_total {
            None
        } else {
            //finds a region that works
            let r_index = self.free_list.iter().enumerate().find_map(|(i, r)| {
                let is_aligned = r.offset % align == 0;
                let can_fit = r.size >= size;
                if is_aligned && can_fit {
                    Some(i)
                } else {
                    None
                }
            })?;
            //pops, alters, and reinserts regions
            let mut cur_region = self.free_list.swap_remove(r_index);
            //front splits region until it's minimum needed size
            while size <= cur_region.size >> 1 {
                let f_region = cur_region.front_bissection();
                self.free_list.push(cur_region);
                cur_region = f_region;
            }
            self.free_total -= cur_region.size;
            Some(cur_region)
        }
    }

    pub fn free_region(&mut self, region: Region) {
        self.free_total += region.size;
        let adjacent = self
            .free_list
            .iter_mut()
            .find(|free_r| free_r.offset == region.end() || free_r.end() == region.offset);
        match adjacent {
            Some(r) => r.merge(region),
            None => self.free_list.push(region),
        }
    }

    ///Returns Total free memory, max region and frag percentage
    pub fn memory_stats(&self) -> (u64, u64, f64) {
        let max_reg = self.free_list.iter().max();
        //let reg_min = self.free_list.iter().min().unwrap().size;

        let reg_total = self.free_list.iter().fold(0, |acc, r| acc + r.size);
        let (max_size, frag_coef) = match max_reg {
            None => (0, 100.0),
            Some(r) => {
                let max = r.size;
                let frag = (100.0 * max as f64) / (reg_total as f64);
                (max, frag)
            }
        };
        (reg_total, max_size, frag_coef)
    }
}

pub type Color32 = u32;
pub type Depth16 = u16;

#[derive(Debug)]
pub struct GImage<T: Copy> {
    page_id: u32,
    mem: vk::DeviceMemory,
    region: Region,
    _marker: PhantomData<T>,
}

impl<T: Copy> GImage<T> {
    pub fn offset<'s>(&'s self) -> vk::DeviceSize {
        self.region.offset as vk::DeviceSize
    }
    pub fn as_memory(&self) -> vk::DeviceMemory {
        self.mem
    }
}

///this is a smart wrapper around a GPU local image
//#[derive(Debug)]
pub struct Texture<T: Copy> {
    flags: vk::ImageUsageFlags,
    mode: vk::SharingMode,
    dimensions: vk::Extent2D,

    image: vk::Image,
    view: vk::ImageView,
    sampler: vk::Sampler,

    allocation: GImage<T>,
}

impl<T: Copy> Texture<T> {
    pub fn new(
        driver: &VKDriver,
        dimensions: vk::Extent2D,
        i_flags: vk::ImageUsageFlags,
        mode: vk::SharingMode,
    ) -> EngResult<Self> {
        let create_info = vk::ImageCreateInfo {
            image_type: vk::ImageType::TYPE_2D,
            format: vk::Format::R8G8B8A8_UNORM,
            extent: dimensions.into(),
            mip_levels: 1,
            array_layers: 1,
            samples: vk::SampleCountFlags::TYPE_1,
            tiling: vk::ImageTiling::OPTIMAL,
            usage: vk::ImageUsageFlags::TRANSFER_DST | vk::ImageUsageFlags::SAMPLED,
            sharing_mode: vk::SharingMode::EXCLUSIVE,
            ..Default::default()
        };
        let image = driver.create_image(&create_info)?;
        let pixels = (dimensions.width * dimensions.height) as usize;

        let allocation = driver.allocate_for(&image)?;
        driver.bind_image(&image, &allocation)?;

        let sampler_info = vk::SamplerCreateInfo {
            mag_filter: vk::Filter::LINEAR,
            min_filter: vk::Filter::LINEAR,
            mipmap_mode: vk::SamplerMipmapMode::LINEAR,
            address_mode_u: vk::SamplerAddressMode::MIRRORED_REPEAT,
            address_mode_v: vk::SamplerAddressMode::MIRRORED_REPEAT,
            address_mode_w: vk::SamplerAddressMode::MIRRORED_REPEAT,
            max_anisotropy: 1.0,
            border_color: vk::BorderColor::FLOAT_OPAQUE_WHITE,
            compare_op: vk::CompareOp::NEVER,
            ..Default::default()
        };

        let sampler = driver.create_sampler(&sampler_info)?;

        let format = vk::Format::R8G8B8A8_UNORM;

        let create_info = vk::ImageViewCreateInfo {
            view_type: vk::ImageViewType::TYPE_2D,
            format: format,
            components: vk::ComponentMapping {
                r: vk::ComponentSwizzle::R,
                g: vk::ComponentSwizzle::G,
                b: vk::ComponentSwizzle::B,
                a: vk::ComponentSwizzle::A,
            },
            subresource_range: vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                level_count: 1,
                layer_count: 1,
                ..Default::default()
            },
            image: image,
            ..Default::default()
        };
        let view = driver.create_view(&create_info)?;
        Ok(Self {
            flags: i_flags,
            mode,
            image,
            allocation,
            dimensions,

            view,
            sampler,
        })
    }
}

impl<T: Copy> Drop for Texture<T> {
    fn drop(&mut self) {
        let driver = VKDriver::instance().unwrap();
        driver.destroy_sampler(&mut self.sampler);
        driver.destroy_view(&mut self.view);
        driver.destroy_image(&mut self.image);
        driver.free(&mut self.allocation);
    }
}

impl<T: Copy> Texture<T> {
    pub fn as_image(&self) -> &vk::Image {
        &self.image
    }
    pub fn as_view(&self) -> &vk::ImageView {
        &self.view
    }
    pub fn as_sampler(&self) -> &vk::Sampler {
        &self.sampler
    }
}

impl<T: Copy> Texture<T> {
    pub fn descriptor(&self) -> vk::DescriptorImageInfo {
        vk::DescriptorImageInfo {
            image_layout: vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            image_view: self.view,
            sampler: self.sampler,
        }
    }
}

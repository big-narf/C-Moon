use std::collections::HashMap;
use std::ffi::c_void;
use std::marker::PhantomData;
use std::mem;
use std::ops::{Deref, DerefMut};
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::{Arc, Mutex, OnceLock};

use ash::vk;

use cmoon_commons::*;

use super::*;

///This is a singleton allocator struct for mapped buffers
//#[derive(Debug)]
pub struct MappedAllocator {
    pub(self) mem_property: vk::PhysicalDeviceMemoryProperties,
    page_size: vk::DeviceSize,
    pub(self) mem_pages: Mutex<HashMap<u32, MappedChunk>>,
    counter: AtomicU32,
}

impl MappedAllocator {
    pub fn new(
        mem_property: vk::PhysicalDeviceMemoryProperties,
        page_size: vk::DeviceSize,
    ) -> EngResult<Self> {
        Ok(Self {
            mem_property,
            page_size,
            mem_pages: Default::default(),
            counter: Default::default(),
        })
    }

    ///Checks info about the currently used pages
    pub fn check_pages(&self) {
        let mut alloc_total = 0;
        let mut frag_sum = 0.0;
        let pages = self.mem_pages.lock().unwrap();
        pages.values().enumerate().for_each(|(n, page)| {
            let (total, max, frag) = page.memory_stats();
            println!("PAGE {n}: Free Total :{total} . Max Region :{max} . Usable: {frag} %");
            alloc_total += total;
            frag_sum += frag;
        });
        let used_mem = pages.len() as u64 * self.page_size;
        let frag_avg = frag_sum / (pages.len() as f64);
        println!(
            "TOTAL FREE MEMORY : {alloc_total} out of {used_mem}. Average Usability: {frag_avg} %"
        );
    }
}

///Generic functions
impl MappedAllocator {
    pub unsafe fn allocate<T: Clone>(
        &self,
        device: &ash::Device,
        mem_req: &vk::MemoryRequirements,
    ) -> EngResult<GSlice<T>> {
        let flags = vk::MemoryPropertyFlags::HOST_COHERENT | vk::MemoryPropertyFlags::HOST_VISIBLE;
        //let size = (mem::size_of::<T>() * len) as vk::DeviceSize;
        //let align = mem_req.alignment;
        if mem_req.size > self.page_size {
            Err(ALLOCATION_TOO_BIG
                .clone()
                .with_root_cause(format!("{}>MAX({})", mem_req.size, self.page_size)))
        } else {
            let memory_type_index = find_memorytype_index(mem_req, &self.mem_property, flags)
                .ok_or_else(|| NO_SUITABLE_MEMORY.clone())?;
            let mut pages = self.mem_pages.lock().unwrap();
            pages
                .iter_mut()
                .find_map(|(&id, page)| {
                    if page.type_index != memory_type_index {
                        None
                    } else {
                        let region = page.get_region(mem_req.size, mem_req.alignment)?;
                        Some(GSlice {
                            page_id: id,
                            memory: page.as_derived(),
                            map_ptr: page.map_ptr(),
                            region,
                            _marker: PhantomData,
                        })
                    }
                })
                .map_or_else(
                    || {
                        let mut page =
                            MappedChunk::new(device, self.page_size as u64, memory_type_index)
                                .cast()?;
                        let id = self.counter.fetch_add(1, Ordering::SeqCst);
                        let map_ptr = page.map_ptr();
                        let region = page
                            .get_region(mem_req.size, mem_req.alignment)
                            .expect("new mapped chunk couldnt provide region");
                        let memory = page.as_derived();
                        pages.insert(id, page);
                        Ok(GSlice {
                            page_id: id,
                            memory,
                            map_ptr,
                            region,
                            _marker: PhantomData,
                        })
                    },
                    Ok,
                )
        }
    }

    pub unsafe fn free<T: Clone>(&self, device: &ash::Device, slice: &mut GSlice<T>) {
        let mut pages = self.mem_pages.lock().unwrap();
        let page = pages.get_mut(&slice.page_id).unwrap();
        page.free_region(slice.region.clone());
        if page.free_total == self.page_size {
            let mut page = pages.remove(&slice.page_id).unwrap();
            page.destroy(device);
        }
    }

    pub unsafe fn trim<T: Clone>(
        &self,
        device: &ash::Device,
        slice: &mut GSlice<T>,
        new_len: usize,
    ) {
        let mut pages = self.mem_pages.lock().unwrap();
        let page = pages.get_mut(&slice.page_id).unwrap();
        let size = (new_len * mem::size_of::<T>()) as vk::DeviceSize;
        page.free_region(slice.region.back_split(size));
        if page.free_total == self.page_size {
            let mut page = pages.remove(&slice.page_id).unwrap();
            page.destroy(device);
        }
    }
}

///Drop-like function called when the driver goes out
impl MappedAllocator {
    pub(super) unsafe fn destroy(&mut self, device: &ash::Device) {
        let mut pages = self.mem_pages.lock().unwrap();
        pages.values_mut().for_each(|page| {
            page.destroy(device);
        })
    }
}

///GPU memory page mmaped to the CPU. The underlying memory is always neutrally aligned.
#[derive(Debug)]
struct MappedChunk {
    type_index: u32,
    mem: vk::DeviceMemory,
    map_ptr: *mut c_void,
    free_total: vk::DeviceSize,
    free_list: Vec<Region>,
}

unsafe impl Send for MappedChunk {}

impl MappedChunk {
    pub unsafe fn new(
        device: &ash::Device,
        size: vk::DeviceSize,
        type_index: u32,
    ) -> VkResult<Self> {
        let allocate_info = vk::MemoryAllocateInfo::default()
            .allocation_size(size)
            .memory_type_index(type_index);
        let mem = device.allocate_memory(&allocate_info, None)?;
        let map_ptr = device.map_memory(mem, 0, size as u64, vk::MemoryMapFlags::empty())?;
        let region = Region { offset: 0, size };
        Ok(Self {
            type_index,
            mem,
            map_ptr,
            free_total: size,
            free_list: vec![region],
        })
    }

    pub unsafe fn destroy(&mut self, device: &ash::Device) {
        device.unmap_memory(self.mem);
        device.free_memory(self.mem, None);
    }

    pub fn map_ptr(&self) -> *mut c_void {
        self.map_ptr
    }

    pub fn as_derived(&self) -> Derived<vk::DeviceMemory> {
        self.mem.into()
    }

    pub fn get_region(&mut self, size: vk::DeviceSize, align: vk::DeviceSize) -> Option<Region> {
        let size = size.next_power_of_two();
        if size > self.free_total {
            None
        } else {
            //finds a region that works
            let r_index = self.free_list.iter().enumerate().find_map(|(i, r)| {
                let is_aligned = r.offset % align == 0;
                let can_fit = r.size >= size;
                if is_aligned && can_fit {
                    Some(i)
                } else {
                    None
                }
            })?;
            //pops, alters, and reinserts regions
            let mut cur_region = self.free_list.swap_remove(r_index);
            //front splits region until it's minimum needed size
            while size <= cur_region.size >> 1 {
                let f_region = cur_region.front_bissection();
                self.free_list.push(cur_region);
                cur_region = f_region;
            }
            self.free_total -= cur_region.size;
            Some(cur_region)
        }
    }

    pub fn free_region(&mut self, region: Region) {
        self.free_total += region.size;
        let adjacent = self
            .free_list
            .iter_mut()
            .find(|free_r| free_r.offset == region.end() || free_r.end() == region.offset);
        match adjacent {
            Some(r) => r.merge(region),
            None => self.free_list.push(region),
        }
    }

    ///Returns Total free memory, max region and frag percentage
    pub fn memory_stats(&self) -> (u64, u64, f64) {
        let reg_total = self.free_list.iter().fold(0, |acc, r| acc + r.size);
        let reg_max = self.free_list.iter().max().unwrap().size;
        //let reg_min = self.free_list.iter().min().unwrap().size;
        let frag_coef = if reg_total == 0 {
            100.0
        } else {
            (100.0 * reg_max as f64) / (reg_total as f64)
        };
        (reg_total, reg_max, frag_coef)
    }
}

///slice to an underlying mapped memory region
#[derive(Debug)]
pub struct GSlice<T: Clone> {
    page_id: u32,
    memory: Derived<vk::DeviceMemory>,
    pub(self) map_ptr: *mut c_void,
    pub(self) region: Region,
    pub(self) _marker: PhantomData<T>,
}

unsafe impl<T: Clone> Send for GSlice<T> {}

unsafe impl<T: Clone> Sync for GSlice<T> {}

impl<T: Clone> GSlice<T> {
    ///Returns the whole underlying slice
    fn as_slice(&self) -> &[T] {
        unsafe {
            let ptr = self.map_ptr.offset(self.region.offset as isize);
            let len = self.region.size as usize / mem::size_of::<T>();
            core::slice::from_raw_parts(ptr.cast(), len)
        }
    }

    ///Returns the whole array, even uinit elements (prolly should be marked as unsafe)
    fn as_slice_mut(&mut self) -> &mut [T] {
        unsafe {
            let ptr = self.map_ptr.offset(self.region.offset as isize);
            let len = self.region.size as usize / mem::size_of::<T>();
            core::slice::from_raw_parts_mut(ptr.cast(), len)
        }
    }

    pub fn as_memory(&self) -> vk::DeviceMemory {
        *self.memory
    }
    pub fn offset<'s>(&'s self) -> vk::DeviceSize {
        self.region.offset as u64
    }
}

impl<T: Clone> Deref for GSlice<T> {
    type Target = [T];
    fn deref(&self) -> &Self::Target {
        self.as_slice()
    }
}
impl<T: Clone> DerefMut for GSlice<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.as_slice_mut()
    }
}
///Vec like type for GPU mapped memory
pub struct MapVec<T: Clone> {
    flags: vk::BufferUsageFlags,
    mode: vk::SharingMode,
    buffer: vk::Buffer,
    slice: GSlice<T>,
    count: usize,
}

impl<T: Clone> MapVec<T> {
    ///This is the base function for creating MapVecs
    pub fn new_with_capacity(
        capacity: usize,
        b_flags: vk::BufferUsageFlags,
        mode: vk::SharingMode,
        driver: &VKDriver,
    ) -> EngResult<Self> {
        let create_info = vk::BufferCreateInfo::default()
            .size((mem::size_of::<T>() * capacity) as u64)
            .usage(b_flags)
            .sharing_mode(mode);
        let buffer = driver.create_buffer(&create_info)?;
        let slice = driver.allocate_for(&buffer)?;
        driver.bind_buffer(&buffer, &slice)?;
        Ok(Self {
            flags: b_flags,
            mode,
            buffer,
            slice,
            count: 0,
        })
    }

    pub fn new_from(
        src: &[T],
        b_flags: vk::BufferUsageFlags,
        mode: vk::SharingMode,
    ) -> EngResult<Self> {
        let capacity = src.len().next_power_of_two();
        let driver = VKDriver::instance().unwrap();
        let mut m_vec = Self::new_with_capacity(capacity, b_flags, mode, driver)?;

        let slice = &mut m_vec.slice;
        let copy_region = &mut slice[0..src.len()];
        copy_region.clone_from_slice(src);
        Ok(m_vec)
    }

    /*
    pub fn push_block(&mut self, values: &[T]) {
        let new_count = self.count + values.len();
        if new_count > self.slice.len() {
            //allocates new slice and copy old values
            let capacity = new_count.next_power_of_two();
            let create_info = vk::BufferCreateInfo::default()
                .size((mem::size_of::<T>() * new_count) as vk::DeviceSize)
                .usage(self.flags)
                .sharing_mode(self.mode);
            let buffer =
                unsafe { self.allocator.device.create_buffer(&create_info, None) }.unwrap();

            let mem_req = unsafe { self.allocator.device.get_buffer_memory_requirements(buffer) };
            let mut n_slice = unsafe { self.allocator.allocate(capacity, &mem_req) }.unwrap();
            n_slice[0..self.count].clone_from_slice(&self.slice[0..self.count]);
            unsafe {
                self.allocator.free(&mut self.slice);
                self.allocator.device.destroy_buffer(self.buffer, None);
            };
            self.buffer = buffer;
            self.slice = n_slice;
            unsafe {
                self.allocator.device.bind_buffer_memory(
                    self.buffer,
                    self.allocator.page_of(&self.slice),
                    self.slice.offset(),
                )
            }
            .unwrap();
        };
        self.slice[self.count..new_count].clone_from_slice(values);
        self.count = new_count;
    }*/

    /*
    ///pushes value to the end of the vector, may need to reallocate the underlying buffer.
    ///NOTE: REWORK THIS FUNCTION
    pub fn push(&mut self, value: T) {
        let new_count = self.count + 1;
        if new_count > self.slice.len() {
            //allocates new slice and copy old values
            let capacity = new_count.next_power_of_two();
            let create_info = vk::BufferCreateInfo::default()
                .size((mem::size_of::<T>() * new_count) as vk::DeviceSize)
                .usage(self.flags)
                .sharing_mode(self.mode);
            let buffer =
                unsafe { self.allocator.device.create_buffer(&create_info, None) }.unwrap();

            let mem_req = unsafe { self.allocator.device.get_buffer_memory_requirements(buffer) };
            let mut n_slice = unsafe { self.allocator.allocate(capacity, &mem_req) }.unwrap();
            n_slice[0..self.count].clone_from_slice(&self.slice[0..self.count]);
            unsafe {
                self.allocator.free(&mut self.slice);
                self.allocator.device.destroy_buffer(self.buffer, None);
            };
            self.buffer = buffer;
            self.slice = n_slice;
            unsafe {
                self.allocator.device.bind_buffer_memory(
                    self.buffer,
                    self.allocator.page_of(&self.slice),
                    self.slice.offset(),
                )
            }
            .unwrap();
        };
        self.slice[self.count] = value;
        self.count = new_count;
    }*/

    ///pushes value to the end of the vector without allocating new buffer, Returns value if it
    ///fails
    pub fn try_push(&mut self, value: T) -> Result<usize, T> {
        let new_count = self.count + 1;
        if new_count > self.slice.len() {
            Err(value)
        } else {
            self.slice[self.count] = value;
            self.count = new_count;
            Ok(new_count)
        }
    }

    /*
    pub fn pop(&mut self) -> Option<T> {
        if self.count == 0 {
            None
        } else {
            if 2 * self.count < self.slice.len() {
                //resizes if capacity is too big for current size
                let capacity = self.count.next_power_of_two();
                let fat = self.slice.len() - capacity;
                unsafe { self.allocator.trim(&mut self.slice, fat) };
            };
            self.count -= 1;
            let out = self.slice[self.count].clone();
            Some(out)
        }
    }*/
}

///Utility methods
impl<'vec, T: Clone> MapVec<T> {
    pub fn to_buffer(&'vec self) -> vk::Buffer {
        self.buffer
    }

    pub fn as_slice(&'vec self) -> &'vec [T] {
        &self.slice[0..self.count]
    }

    pub fn as_slice_mut(&'vec mut self) -> &'vec mut [T] {
        &mut self.slice[0..self.count]
    }

    pub fn len(&self) -> usize {
        self.count
    }

    pub fn capacity(&self) -> usize {
        self.slice.len()
    }
}

impl<T: Clone> MapVec<T> {
    pub fn uniform_descriptor(&self) -> vk::DescriptorBufferInfo {
        vk::DescriptorBufferInfo {
            buffer: self.buffer,
            offset: 0,
            range: (std::mem::size_of::<T>() * self.len()) as vk::DeviceSize,
        }
    }
}

impl<T: Clone> Drop for MapVec<T> {
    fn drop(&mut self) {
        let driver = VKDriver::instance().unwrap();
        driver.destroy_buffer(&mut self.buffer);
        driver.free(&mut self.slice);
    }
}

use std::ffi::{c_char, CStr};
use std::ops::Deref;
use std::sync::{Arc, OnceLock};
use std::u64;

use ash::ext;
use ash::khr;
use ash::vk;

use raw_window_handle::{RawDisplayHandle, RawWindowHandle};

use cmoon_commons::*;

mod collections;
mod debug;
mod driver;
mod error;
mod image;
mod mapped;

pub use collections::*;
pub use driver::*;
pub use error::*;
pub use image::*;
pub use mapped::*;
use rand::Fill;

use crate::elements::Origin;
use crate::shared::{Geometry, ImgAsset, ImgScheme};

//#[derive(Debug)]
pub struct VKDriver {
    entry: ash::Entry,
    instance: ash::Instance,
    pdevice: vk::PhysicalDevice,
    device: ash::Device,

    queue: vk::Queue,
    queue_id: u32,

    surface_loader: khr::surface::Instance,
    swapchain_loader: khr::swapchain::Device,

    map_allocator: MappedAllocator,
    tex_allocator: TexAllocator,
}

static DRIVER_INSTANCE: OnceLock<VKDriver> = OnceLock::new();

///Init functions
impl VKDriver {
    const VULKAN_SDK: &CStr =
        unsafe { CStr::from_bytes_with_nul_unchecked(b"VK_LAYER_KHRONOS_validation\0") };
    const DEVICE_SORTED: &CStr =
        unsafe { CStr::from_bytes_with_nul_unchecked(b"VK_LAYER_MESA_device_select\0") };

    pub fn init(display_handle: RawDisplayHandle, page_size: vk::DeviceSize) -> EngResult<()> {
        //let entry = Entry::linked();
        let entry = unsafe { ash::Entry::load() }.cast()?;

        let app_info = vk::ApplicationInfo {
            api_version: vk::make_api_version(0, 1, 3, 0),
            s_type: vk::StructureType::APPLICATION_INFO,
            ..Default::default()
        };

        let layers = [Self::VULKAN_SDK.as_ptr(), Self::DEVICE_SORTED.as_ptr()];

        let mut extension_names = enumerate_required_extensions(display_handle)
            .unwrap()
            .to_vec();
        extension_names.push(ext::debug_utils::NAME.as_ptr());

        let create_info = vk::InstanceCreateInfo::default()
            .application_info(&app_info)
            .enabled_extension_names(&extension_names)
            .enabled_layer_names(&layers);

        let instance = unsafe { entry.create_instance(&create_info, None) }.cast()?;

        let pdevice_list = unsafe { instance.enumerate_physical_devices() }.cast()?;

        //Grabs first suitable Graphics card
        //WARNING: THIS MAY NEED TO BE CHANGED TO CHECK IF GRAPHICS CARD SUPPORTS SURFACES

        let (pdevice, queue_family_index) = pdevice_list
            .iter()
            .find_map(|pdevice| unsafe {
                instance
                    .get_physical_device_queue_family_properties(*pdevice)
                    .iter()
                    .enumerate()
                    .find_map(|(index, info)| {
                        let support_flag = info.queue_flags.contains(vk::QueueFlags::GRAPHICS);
                        /*&& surface_loader
                        .get_physical_device_surface_support(
                            *pdevice,
                            index as u32,
                            surface,
                        )
                        .unwrap();*/
                        if support_flag {
                            Some((*pdevice, index))
                        } else {
                            None
                        }
                    })
            })
            .ok_or_else(|| NO_SUITABLE_PDEVICE.clone())?;

        let queue_family_index = queue_family_index as u32;

        //creates virtual vulkan device
        let queue_create_info = [vk::DeviceQueueCreateInfo::default()
            .queue_family_index(queue_family_index)
            .queue_priorities(&[1.0])];

        let device_extensions = [khr::swapchain::NAME.as_ptr()];

        let device_create_info = vk::DeviceCreateInfo::default()
            .queue_create_infos(&queue_create_info)
            .enabled_extension_names(&device_extensions);
        let device =
            unsafe { instance.create_device(pdevice, &device_create_info, None) }.cast()?;

        let queue = unsafe { device.get_device_queue(queue_family_index, 0) };

        let memory_property = unsafe { instance.get_physical_device_memory_properties(pdevice) };

        let map_allocator = MappedAllocator::new(memory_property, page_size)?;
        let tex_allocator = TexAllocator::new(memory_property, page_size)?;

        let surface_loader = khr::surface::Instance::new(&entry, &instance);
        let swapchain_loader = khr::swapchain::Device::new(&instance, &device);

        DRIVER_INSTANCE
            .set(Self {
                entry,
                instance,
                pdevice,
                device,

                queue,
                queue_id: queue_family_index,

                surface_loader,
                swapchain_loader,

                map_allocator,
                tex_allocator,
            })
            .map_err(|_| INFRA_INIT_FAIL.clone().with_root_cause("driver"))?;
        Ok(())
    }

    pub fn init_headless(page_size: vk::DeviceSize) -> EngResult<()> {
        //let entry = Entry::linked();
        let entry = unsafe { ash::Entry::load() }.cast()?;

        let app_info = vk::ApplicationInfo {
            api_version: vk::make_api_version(0, 1, 3, 0),
            s_type: vk::StructureType::APPLICATION_INFO,
            ..Default::default()
        };

        let layers = [Self::VULKAN_SDK.as_ptr(), Self::DEVICE_SORTED.as_ptr()];

        let mut extension_names = [
            khr::surface::NAME.as_ptr(),
            khr::xlib_surface::NAME.as_ptr(),
        ]
        .to_vec();
        extension_names.push(ext::debug_utils::NAME.as_ptr());

        let create_info = vk::InstanceCreateInfo::default()
            .application_info(&app_info)
            .enabled_extension_names(&extension_names)
            .enabled_layer_names(&layers);

        let instance = unsafe { entry.create_instance(&create_info, None) }.cast()?;

        let pdevice_list = unsafe { instance.enumerate_physical_devices() }.cast()?;

        //Grabs first suitable Graphics card
        //WARNING: THIS MAY NEED TO BE CHANGED TO CHECK IF GRAPHICS CARD SUPPORTS SURFACES

        let (pdevice, queue_family_index) = pdevice_list
            .iter()
            .find_map(|pdevice| unsafe {
                instance
                    .get_physical_device_queue_family_properties(*pdevice)
                    .iter()
                    .enumerate()
                    .find_map(|(index, info)| {
                        let support_flag = info.queue_flags.contains(vk::QueueFlags::GRAPHICS);
                        /*&& surface_loader
                        .get_physical_device_surface_support(
                            *pdevice,
                            index as u32,
                            surface,
                        )
                        .unwrap();*/
                        if support_flag {
                            Some((*pdevice, index))
                        } else {
                            None
                        }
                    })
            })
            .ok_or_else(|| NO_SUITABLE_PDEVICE.clone())?;

        let queue_family_index = queue_family_index as u32;

        //creates virtual vulkan device
        let queue_create_info = [vk::DeviceQueueCreateInfo::default()
            .queue_family_index(queue_family_index)
            .queue_priorities(&[1.0])];

        let device_extensions = [khr::swapchain::NAME.as_ptr()];

        let device_create_info = vk::DeviceCreateInfo::default()
            .queue_create_infos(&queue_create_info)
            .enabled_extension_names(&device_extensions);
        let device =
            unsafe { instance.create_device(pdevice, &device_create_info, None) }.cast()?;

        let queue = unsafe { device.get_device_queue(queue_family_index, 0) };

        let memory_property = unsafe { instance.get_physical_device_memory_properties(pdevice) };

        let map_allocator = MappedAllocator::new(memory_property, page_size)?;
        let tex_allocator = TexAllocator::new(memory_property, page_size)?;

        let surface_loader = khr::surface::Instance::new(&entry, &instance);
        let swapchain_loader = khr::swapchain::Device::new(&instance, &device);

        DRIVER_INSTANCE
            .set(Self {
                entry,
                instance,
                pdevice,
                device,

                queue,
                queue_id: queue_family_index,

                surface_loader,
                swapchain_loader,

                map_allocator,
                tex_allocator,
            })
            .map_err(|_| INFRA_INIT_FAIL.clone().with_root_cause("driver"))?;
        Ok(())
    }
}

///Instance access functions
impl VKDriver {
    pub fn instance() -> Option<&'static Self> {
        DRIVER_INSTANCE.get()
    }
}

impl VKDriver {
    pub fn wait_idle(&self) -> EngResult<()> {
        unsafe { self.device.device_wait_idle().cast() }
    }
}

///submit commands functions
impl VKDriver {
    ///will end the buffers and submit them to the GPU
    pub fn submit_transfer_cmds(
        &self,
        cmd_buffers: &[vk::CommandBuffer],
        wait_semaphores: &[vk::Semaphore],
        fence: vk::Fence,
    ) -> EngResult<()> {
        unsafe {
            cmd_buffers
                .iter()
                .try_for_each(|c_buff| self.device.end_command_buffer(*c_buff))
                .cast()?;

            //synchronization and submit commands
            let submit_info = vk::SubmitInfo::default()
                .wait_semaphores(wait_semaphores)
                .command_buffers(&cmd_buffers);

            self.device
                .queue_submit(self.queue, &[submit_info], fence)
                .cast()?;
            self.device.wait_for_fences(&[fence], true, u64::MAX).cast()
        }
    }

    ///will end the buffers and submit them to the GPU
    pub fn submit_graphic_cmds(
        &self,
        cmd_buffers: &[vk::CommandBuffer],
        wait_semaphores: &[vk::Semaphore],
        wait_mask: &[vk::PipelineStageFlags],
        signal_fence: vk::Fence,
        signal_semaphores: &[vk::Semaphore],
    ) -> EngResult<()> {
        unsafe {
            /*
            cmd_buffers
                .iter()
                .try_for_each(|c_buff| self.device.end_command_buffer(*c_buff))
                .cast()?;*/

            //synchronization and submit commands
            let submit_info = vk::SubmitInfo::default()
                .wait_semaphores(wait_semaphores)
                .wait_dst_stage_mask(wait_mask)
                .command_buffers(&cmd_buffers)
                .signal_semaphores(signal_semaphores);

            self.device
                .queue_submit(self.queue, &[submit_info], signal_fence)
                .cast()
        }
    }

    pub fn submit_present(
        &self,
        indices: &[u32],
        chain: &FATSwapchain,
        wait_semaphores: &[vk::Semaphore],
    ) -> EngResult<bool> {
        let chains = [chain.swapchain];
        let present_info = vk::PresentInfoKHR::default()
            .wait_semaphores(wait_semaphores) //NOTE: CHANGE THIS
            .swapchains(&chains)
            .image_indices(&indices);
        unsafe {
            self.swapchain_loader
                .queue_present(self.queue, &present_info)
        }
        .cast()
    }

    pub fn acquire_image(
        &self,
        swapchain: &FATSwapchain,
        wait_fences: &[vk::Fence],
        signal_semaphore: &vk::Semaphore,
        signal_fence: &vk::Fence,
    ) -> EngResult<(usize, bool)> {
        unsafe {
            self.device
                .wait_for_fences(wait_fences, true, u64::MAX)
                .cast()?;
            self.device.reset_fences(wait_fences).cast()?;
            self.swapchain_loader
                .acquire_next_image(
                    *swapchain.deref(),
                    std::u64::MAX,
                    *signal_semaphore,
                    *signal_fence, //vk::Fence::null(),
                )
                .map(|(i, b)| (i as usize, b))
                .cast()
        }
    }

    pub fn reset_primary(&self, c_buffer: &vk::CommandBuffer) -> EngResult<()> {
        unsafe {
            self.device
                .reset_command_buffer(*c_buffer, vk::CommandBufferResetFlags::RELEASE_RESOURCES)
                .cast()?;

            let begin_info = vk::CommandBufferBeginInfo::default()
                .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

            self.device
                .begin_command_buffer(*c_buffer, &begin_info)
                .cast()
        }
    }

    ///Executes the secondaries after waiting or the wait fences, then resets the fences
    pub fn finish_primary(
        &self,
        c_buffer: &vk::CommandBuffer,
        secondaries: &Derived<Vec<vk::CommandBuffer>>,
    ) -> EngResult<()> {
        unsafe {
            self.device.cmd_execute_commands(*c_buffer, &secondaries);
            self.device.cmd_end_render_pass(*c_buffer);
            self.device.end_command_buffer(*c_buffer).cast()
        }
    }

    pub fn start_secondary(
        &self,
        c_buffer: &vk::CommandBuffer,
        renderpass: &vk::RenderPass,
        framebuffer: &vk::Framebuffer,
        viewports: &[vk::Viewport],
        scissors: &[vk::Rect2D],
        index_buffer: &vk::Buffer,
        vertex_buffer: &vk::Buffer,
    ) -> EngResult<()> {
        let inheritance = vk::CommandBufferInheritanceInfo::default()
            .render_pass(*renderpass)
            .framebuffer(*framebuffer);
        let info = vk::CommandBufferBeginInfo::default()
            .flags(vk::CommandBufferUsageFlags::RENDER_PASS_CONTINUE)
            .inheritance_info(&inheritance);
        unsafe {
            self.device.begin_command_buffer(*c_buffer, &info).cast()?;
            self.device.cmd_set_viewport(*c_buffer, 0, viewports);
            self.device.cmd_set_scissor(*c_buffer, 0, scissors);
            self.device
                .cmd_bind_index_buffer(*c_buffer, *index_buffer, 0, vk::IndexType::UINT32);
            self.device
                .cmd_bind_vertex_buffers(*c_buffer, 0, &[*vertex_buffer], &[0]);
        }
        Ok(())
    }

    pub fn finish_secondary(&self, c_buffer: &vk::CommandBuffer) -> EngResult<()> {
        unsafe { self.device.end_command_buffer(*c_buffer) }.cast()
    }

    ///inserts render commands into the c_buffer
    pub fn render_obj(
        &self,
        c_buffer: &vk::CommandBuffer,
        transform: Transform,
        geometry: &Geometry,
        scheme: &ImgScheme,
        asset: &ImgAsset,
    ) {
        let origin: Origin = transform.into();
        println!("rendering at {:?}", origin);
        unsafe {
            self.device.cmd_push_constants(
                *c_buffer,
                scheme.get_layout(),
                vk::ShaderStageFlags::VERTEX,
                0,
                &origin.bytes_ref(),
            );

            self.device.cmd_bind_descriptor_sets(
                *c_buffer,
                vk::PipelineBindPoint::GRAPHICS,
                scheme.get_layout(),
                0,
                asset.as_desc_set(),
                &[],
            );

            self.device.cmd_bind_pipeline(
                *c_buffer,
                vk::PipelineBindPoint::GRAPHICS,
                scheme.as_pipeline(),
            );

            self.device.cmd_draw_indexed(
                *c_buffer,
                geometry.index_count(),
                1,
                geometry.first_index(),
                0,
                1,
            );
        }
    }
    ///Pipes the pixels of a CPU visible buffer to a GPU only image
    pub fn push_to_image(
        &self,
        c_buffer: &vk::CommandBuffer,
        src_buffer: vk::Buffer,
        dst_image: vk::Image,
        extent: vk::Extent3D,
    ) {
        unsafe {
            let texture_barrier = vk::ImageMemoryBarrier {
                dst_access_mask: vk::AccessFlags::TRANSFER_WRITE,
                new_layout: vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                image: dst_image,
                subresource_range: vk::ImageSubresourceRange {
                    aspect_mask: vk::ImageAspectFlags::COLOR,
                    level_count: 1,
                    layer_count: 1,
                    ..Default::default()
                },
                ..Default::default()
            };
            self.device.cmd_pipeline_barrier(
                *c_buffer,
                vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                vk::PipelineStageFlags::TRANSFER,
                vk::DependencyFlags::empty(),
                &[],
                &[],
                &[texture_barrier],
            );

            let buffer_copy_regions = vk::BufferImageCopy::default()
                .image_subresource(
                    vk::ImageSubresourceLayers::default()
                        .aspect_mask(vk::ImageAspectFlags::COLOR)
                        .layer_count(1),
                )
                .image_extent(extent);

            self.device.cmd_copy_buffer_to_image(
                *c_buffer,
                src_buffer,
                dst_image,
                vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                &[buffer_copy_regions],
            );

            let texture_barrier_end = vk::ImageMemoryBarrier {
                src_access_mask: vk::AccessFlags::TRANSFER_WRITE,
                dst_access_mask: vk::AccessFlags::SHADER_READ,
                old_layout: vk::ImageLayout::TRANSFER_DST_OPTIMAL,
                new_layout: vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                image: dst_image,
                subresource_range: vk::ImageSubresourceRange {
                    aspect_mask: vk::ImageAspectFlags::COLOR,
                    level_count: 1,
                    layer_count: 1,
                    ..Default::default()
                },
                ..Default::default()
            };
            self.device.cmd_pipeline_barrier(
                *c_buffer,
                vk::PipelineStageFlags::TRANSFER,
                vk::PipelineStageFlags::FRAGMENT_SHADER,
                vk::DependencyFlags::empty(),
                &[],
                &[],
                &[texture_barrier_end],
            );
        }
    }

    /// Starts the rendepass and sets viewport+scissors
    pub fn begin_renderpass(&self, c_buffer: &vk::CommandBuffer, info: &vk::RenderPassBeginInfo) {
        unsafe {
            self.device.cmd_begin_render_pass(
                *c_buffer,
                info,
                vk::SubpassContents::SECONDARY_COMMAND_BUFFERS,
            );
        }
    }
}

///Object create functions
impl VKDriver {
    pub fn graphics_queue_id(&self) -> u32 {
        self.queue_id
    }

    pub fn create_surface(
        &self,
        display_handle: RawDisplayHandle,
        window_handle: RawWindowHandle,
    ) -> EngResult<vk::SurfaceKHR> {
        match (display_handle, window_handle) {
            (RawDisplayHandle::Windows(_), RawWindowHandle::Win32(window)) => {
                println!("Running on windows!");
                let surface_desc = vk::Win32SurfaceCreateInfoKHR::default()
                    .hinstance(
                        window
                            .hinstance
                            .ok_or_else(|| {
                                SURFACE_CREATE_FAIL.clone().with_root_cause("No hinstance")
                            })?
                            .get(),
                    )
                    .hwnd(window.hwnd.get());
                let surface_fn = khr::win32_surface::Instance::new(&self.entry, &self.instance);
                unsafe { surface_fn.create_win32_surface(&surface_desc, None) }.cast()
            }

            (RawDisplayHandle::Wayland(display), RawWindowHandle::Wayland(window)) => {
                let surface_desc = vk::WaylandSurfaceCreateInfoKHR::default()
                    .display(display.display.as_ptr())
                    .surface(window.surface.as_ptr());
                let surface_fn = khr::wayland_surface::Instance::new(&self.entry, &self.instance);
                unsafe { surface_fn.create_wayland_surface(&surface_desc, None) }.cast()
            }
            (RawDisplayHandle::Xlib(display), RawWindowHandle::Xlib(window)) => {
                let surface_desc = vk::XlibSurfaceCreateInfoKHR::default()
                    .dpy(
                        display
                            .display
                            .ok_or_else(|| {
                                SURFACE_CREATE_FAIL.clone().with_root_cause("no display")
                            })?
                            .as_ptr(),
                    )
                    .window(window.window);

                let surface_fn = khr::xlib_surface::Instance::new(&self.entry, &self.instance);

                unsafe { surface_fn.create_xlib_surface(&surface_desc, None) }.cast()
            }

            (RawDisplayHandle::Xcb(display), RawWindowHandle::Xcb(window)) => {
                let surface_desc = vk::XcbSurfaceCreateInfoKHR::default()
                    .connection(
                        display
                            .connection
                            .ok_or_else(|| {
                                SURFACE_CREATE_FAIL
                                    .clone()
                                    .with_root_cause("No XServer connection")
                            })?
                            .as_ptr(),
                    )
                    .window(window.window.into());
                let surface_fn = khr::xcb_surface::Instance::new(&self.entry, &self.instance);
                unsafe { surface_fn.create_xcb_surface(&surface_desc, None) }.cast()
            }

            (RawDisplayHandle::Android(_), RawWindowHandle::AndroidNdk(window)) => {
                let surface_desc = vk::AndroidSurfaceCreateInfoKHR::default()
                    .window(window.a_native_window.as_ptr());
                let surface_fn = khr::android_surface::Instance::new(&self.entry, &self.instance);
                unsafe { surface_fn.create_android_surface(&surface_desc, None) }.cast()
            }

            _ => Err(SURFACE_CREATE_FAIL
                .clone()
                .with_root_cause("System not Supported!")),
        }
    }

    pub fn destroy_surface(&self, surface: &mut vk::SurfaceKHR) {
        unsafe {
            self.surface_loader.destroy_surface(*surface, None);
        }
    }

    pub fn surface_info(&self, surface: &vk::SurfaceKHR) -> VkResult<SurfaceInfo> {
        let info = unsafe {
            let surface = *surface;
            let format = self
                .surface_loader
                .get_physical_device_surface_formats(self.pdevice, surface)?[0];

            let capabilities = self
                .surface_loader
                .get_physical_device_surface_capabilities(self.pdevice, surface)?;

            let present_modes = self
                .surface_loader
                .get_physical_device_surface_present_modes(self.pdevice, surface)?;

            SurfaceInfo {
                format,
                capabilities,
                present_modes,
            }
        };
        Ok(info)
    }

    pub fn create_swapchain(&self, info: &vk::SwapchainCreateInfoKHR) -> VkResult<FATSwapchain> {
        unsafe {
            let swapchain = self.swapchain_loader.create_swapchain(&info, None)?;

            let images = self.swapchain_loader.get_swapchain_images(swapchain)?;

            Ok(FATSwapchain { swapchain, images })
        }
    }

    pub fn destroy_swapchain(&self, swapchain: &mut FATSwapchain) {
        unsafe {
            self.swapchain_loader
                .destroy_swapchain(swapchain.swapchain, None)
        };
    }
}

///Functions that wrap device functions
impl VKDriver {
    pub fn create_cmd_pool(&self, info: &vk::CommandPoolCreateInfo) -> EngResult<vk::CommandPool> {
        unsafe { self.device.create_command_pool(&info, None) }.cast()
    }
    pub fn destroy_cmd_pool(&self, pool: &mut vk::CommandPool) {
        unsafe {
            self.device.destroy_command_pool(*pool, None);
        }
    }

    pub fn create_desc_pool(
        &self,
        info: &vk::DescriptorPoolCreateInfo,
    ) -> EngResult<vk::DescriptorPool> {
        unsafe { self.device.create_descriptor_pool(&info, None) }.cast()
    }

    pub fn destroy_desc_pool(&self, pool: &mut vk::DescriptorPool) {
        unsafe {
            self.device.destroy_descriptor_pool(*pool, None);
        }
    }

    pub fn allocate_cmd_buffers(
        &self,
        info: &vk::CommandBufferAllocateInfo,
    ) -> EngResult<Vec<vk::CommandBuffer>> {
        unsafe { self.device.allocate_command_buffers(&info) }.cast()
    }

    pub fn create_buffer(&self, info: &vk::BufferCreateInfo) -> EngResult<vk::Buffer> {
        unsafe { self.device.create_buffer(&info, None) }.cast()
    }
    pub fn destroy_buffer(&self, buff: &mut vk::Buffer) {
        unsafe { self.device.destroy_buffer(*buff, None) };
    }

    pub fn bind_buffer<T: Clone>(&self, buff: &vk::Buffer, slice: &GSlice<T>) -> EngResult<()> {
        unsafe {
            self.device
                .bind_buffer_memory(*buff, slice.as_memory(), slice.offset())
        }
        .cast()
    }

    pub fn create_image(&self, info: &vk::ImageCreateInfo) -> EngResult<vk::Image> {
        unsafe { self.device.create_image(&info, None) }.cast()
    }

    pub fn destroy_image(&self, img: &mut vk::Image) {
        unsafe {
            self.device.destroy_image(*img, None);
        }
    }
    pub fn bind_image<T: Copy>(&self, img: &vk::Image, slice: &GImage<T>) -> EngResult<()> {
        unsafe {
            self.device
                .bind_image_memory(*img, slice.as_memory(), slice.offset())
        }
        .cast()
    }

    pub fn create_view(&self, info: &vk::ImageViewCreateInfo) -> EngResult<vk::ImageView> {
        unsafe { self.device.create_image_view(&info, None) }.cast()
    }

    pub fn destroy_view(&self, view: &mut vk::ImageView) {
        unsafe {
            self.device.destroy_image_view(*view, None);
        }
    }

    pub fn create_renderpass(&self, info: &vk::RenderPassCreateInfo) -> EngResult<vk::RenderPass> {
        unsafe { self.device.create_render_pass(&info, None) }.cast()
    }
    pub fn destroy_renderpass(&self, renderpass: &mut vk::RenderPass) {
        unsafe {
            self.device.destroy_render_pass(*renderpass, None);
        }
    }

    pub fn create_framebuffer(
        &self,
        info: &vk::FramebufferCreateInfo,
    ) -> EngResult<vk::Framebuffer> {
        unsafe { self.device.create_framebuffer(&info, None) }.cast()
    }

    pub fn destroy_framebuffer(&self, framebuffer: &mut vk::Framebuffer) {
        unsafe { self.device.destroy_framebuffer(*framebuffer, None) }
    }

    pub fn create_layout(
        &self,
        info: &vk::DescriptorSetLayoutCreateInfo,
    ) -> EngResult<vk::DescriptorSetLayout> {
        unsafe { self.device.create_descriptor_set_layout(&info, None) }.cast()
    }

    pub fn create_ppl_layout(
        &self,
        info: &vk::PipelineLayoutCreateInfo,
    ) -> EngResult<vk::PipelineLayout> {
        unsafe { self.device.create_pipeline_layout(info, None) }.cast()
    }
    pub fn destroy_ppl_layout(&self, layout: &mut vk::PipelineLayout) {
        unsafe {
            self.device.destroy_pipeline_layout(*layout, None);
        }
    }

    pub fn create_sampler(&self, info: &vk::SamplerCreateInfo) -> EngResult<vk::Sampler> {
        unsafe { self.device.create_sampler(info, None) }.cast()
    }
    pub fn destroy_sampler(&self, sampler: &mut vk::Sampler) {
        unsafe {
            self.device.destroy_sampler(*sampler, None);
        }
    }

    pub fn create_shader_mdl(
        &self,
        info: &vk::ShaderModuleCreateInfo,
    ) -> EngResult<vk::ShaderModule> {
        unsafe { self.device.create_shader_module(info, None) }.cast()
    }

    pub fn destroy_shader_mdl(&self, module: &mut vk::ShaderModule) {
        unsafe { self.device.destroy_shader_module(*module, None) };
    }

    pub fn create_graphic_ppls(
        &self,
        info: &[vk::GraphicsPipelineCreateInfo],
    ) -> EngResult<Vec<vk::Pipeline>> {
        unsafe {
            self.device
                .create_graphics_pipelines(vk::PipelineCache::null(), info, None)
        }
        .map_err(|_| {
            UNIMPLEMENTED
                .clone()
                .with_root_cause("pipeline creation shat the bed")
        })
    }

    pub fn destroy_ppl(&self, ppl: &mut vk::Pipeline) {
        unsafe { self.device.destroy_pipeline(*ppl, None) };
    }

    pub fn create_semaphore(&self, info: &vk::SemaphoreCreateInfo) -> EngResult<vk::Semaphore> {
        unsafe { self.device.create_semaphore(&info, None) }.cast()
    }
    pub fn destroy_semaphore(&self, semaphore: &mut vk::Semaphore) {
        unsafe {
            self.device.device_wait_idle().unwrap();
            self.device.destroy_semaphore(*semaphore, None);
        }
    }

    pub fn create_fence(&self, info: &vk::FenceCreateInfo) -> EngResult<vk::Fence> {
        unsafe { self.device.create_fence(&info, None) }.cast()
    }
    pub fn destroy_fence(&self, fence: &mut vk::Fence) {
        unsafe {
            self.device.destroy_fence(*fence, None);
        }
    }
}

///Functions that prolly should be moved to an Allocator trait impl
impl VKDriver {
    pub fn allocate_desc_sets(
        &self,
        info: &vk::DescriptorSetAllocateInfo,
    ) -> EngResult<Vec<vk::DescriptorSet>> {
        unsafe { self.device.allocate_descriptor_sets(info) }.cast()
    }

    pub fn update_desc(&self, write: &[vk::WriteDescriptorSet]) {
        unsafe { self.device.update_descriptor_sets(&write, &[]) };
    }

    pub fn free_desc_sets(
        &self,
        pool: Derived<vk::DescriptorPool>,
        sets: &mut [vk::DescriptorSet],
    ) {
        unsafe { self.device.free_descriptor_sets(*pool, sets) }
            .expect("failed to free descriptors");
    }
}

///OTHER STUFF
impl VKDriver {
    ///prepares the commander to be able to receive commands
    pub fn prepare_cmd_buffer(
        &self,
        c_buffer: &vk::CommandBuffer,
        fences: &[vk::Fence],
    ) -> EngResult<()> {
        unsafe {
            self.device.wait_for_fences(fences, true, u64::MAX).cast()?;

            self.device.reset_fences(fences).cast()?;

            self.device
                .reset_command_buffer(*c_buffer, vk::CommandBufferResetFlags::RELEASE_RESOURCES)
                .cast()?;

            let begin_info = vk::CommandBufferBeginInfo::default()
                .flags(vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

            self.device
                .begin_command_buffer(*c_buffer, &begin_info)
                .cast()
        }
    }
}

impl Drop for VKDriver {
    fn drop(&mut self) {
        unsafe {
            self.map_allocator.destroy(&self.device);
            self.device.destroy_device(None);
            self.instance.destroy_instance(None);
        }
    }
}

pub trait Allocator<T> {
    type Object;
    ///Allocates an uninitialized array of len elements
    fn allocate(&self, mem_req: &vk::MemoryRequirements) -> EngResult<T>;
    fn allocate_for(&self, obj: &Self::Object) -> EngResult<T>;
    fn free(&self, slice: &mut T);
}

impl<T: Clone> Allocator<GSlice<T>> for VKDriver {
    type Object = vk::Buffer;
    fn allocate(&self, mem_req: &vk::MemoryRequirements) -> EngResult<GSlice<T>> {
        unsafe { self.map_allocator.allocate(&self.device, &mem_req) }
    }

    fn allocate_for(&self, obj: &Self::Object) -> EngResult<GSlice<T>> {
        unsafe {
            let mem_req = self.device.get_buffer_memory_requirements(*obj);
            self.map_allocator.allocate(&self.device, &mem_req)
        }
    }

    fn free(&self, slice: &mut GSlice<T>) {
        unsafe { self.map_allocator.free(&self.device, slice) };
    }
}

impl<T: Copy> Allocator<GImage<T>> for VKDriver {
    type Object = vk::Image;
    fn allocate(&self, mem_req: &vk::MemoryRequirements) -> EngResult<GImage<T>> {
        unsafe { self.tex_allocator.allocate(&self.device, &mem_req) }
    }

    fn allocate_for(&self, obj: &Self::Object) -> EngResult<GImage<T>> {
        unsafe {
            let mem_req = self.device.get_image_memory_requirements(*obj);
            self.tex_allocator.allocate(&self.device, &mem_req)
        }
    }

    fn free(&self, slice: &mut GImage<T>) {
        unsafe { self.tex_allocator.free(&self.device, slice) };
    }
}

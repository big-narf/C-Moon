#[derive(Debug)]
pub struct ReuseList<T> {
    inner: Vec<Option<T>>,
}

impl<T> ReuseList<T> {
    pub fn iter(&self) -> impl Iterator<Item = &T> {
        self.inner.iter().filter_map(|op| op.as_ref())
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut T> {
        self.inner.iter_mut().filter_map(|op| op.as_mut())
    }
}

impl<T> Default for ReuseList<T> {
    fn default() -> Self {
        Self {
            inner: Default::default(),
        }
    }
}

impl<T> ReuseList<T> {
    ///Inserts the value, returns the index that it was inserted on.
    pub fn insert(&mut self, value: T) -> usize {
        let slot = self
            .inner
            .iter_mut()
            .enumerate()
            .find(|(_, op)| op.is_none());
        match slot {
            Some((index, op)) => {
                op.replace(value);
                index
            }
            None => {
                let index = self.inner.len();
                self.inner.push(Some(value));
                index
            }
        }
    }

    pub fn remove(&mut self, index: usize) -> Option<T> {
        self.inner.get_mut(index).and_then(|op| op.take())
    }

    pub fn clear(&mut self) {
        self.inner.clear();
    }

    pub fn get(&self, index: usize) -> Option<&T> {
        self.inner.get(index).and_then(|op| op.as_ref())
    }

    pub fn get_mut(&mut self, index: usize) -> Option<&mut T> {
        self.inner.get_mut(index).and_then(|op| op.as_mut())
    }
}
///List that will reuse empty slots if possible
#[derive(Debug)]
pub struct SlotList<T> {
    inner: Vec<Slot<T>>,
}

impl<T> Default for SlotList<T> {
    fn default() -> Self {
        Self {
            inner: Default::default(),
        }
    }
}

impl<T> SlotList<T> {
    ///Reserves an slot in the list, returns the slot index.
    pub fn reserve(&mut self) -> usize {
        let reserved = self.inner.iter_mut().enumerate().find_map(|(index, slot)| {
            if slot.try_reserve() {
                Some(index)
            } else {
                None
            }
        });
        match reserved {
            Some(index) => index,
            None => {
                let index = self.inner.len();
                self.inner.push(Slot::Reserved);
                index
            }
        }
    }

    pub fn used_ids(&self) -> impl Iterator<Item = usize> + '_ {
        self.inner
            .iter()
            .enumerate()
            .filter_map(|(i, s)| if s.is_used() { Some(i) } else { None })
    }

    pub fn iter(&self) -> impl Iterator<Item = &T> {
        self.inner.iter().filter_map(|op| op.as_ref())
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut T> {
        self.inner.iter_mut().filter_map(|op| op.as_mut())
    }
}

impl<T> SlotList<T> {
    pub fn insert(&mut self, index: usize, value: T) {
        self.inner[index] = Slot::Filled(value);
    }

    pub fn remove(&mut self, index: usize) -> Option<T> {
        self.inner.get_mut(index)?.take()
    }

    pub fn clear(&mut self) {
        self.inner.clear();
    }

    pub fn get(&self, index: usize) -> Option<&T> {
        self.inner.get(index).and_then(|op| op.as_ref())
    }

    pub fn get_mut(&mut self, index: usize) -> Option<&mut T> {
        self.inner.get_mut(index).and_then(|op| op.as_mut())
    }
}

#[derive(Debug)]
enum Slot<T> {
    Empty,     //not in used
    Reserved,  //index used but obj not created.
    Filled(T), //object
}

impl<T> Slot<T> {
    pub fn is_used(&self) -> bool {
        match self {
            Self::Empty => false,
            _ => true,
        }
    }

    pub fn take(&mut self) -> Option<T> {
        let val = std::mem::replace(self, Slot::Empty);
        val.into_inner()
    }

    pub fn try_reserve(&mut self) -> bool {
        match self {
            Self::Empty => {
                *self = Self::Reserved;
                true
            }
            _ => false,
        }
    }

    pub fn into_inner(self) -> Option<T> {
        match self {
            Self::Filled(v) => Some(v),
            _ => None,
        }
    }

    pub fn as_ref(&self) -> Option<&T> {
        match self {
            Self::Filled(v) => Some(v),
            _ => None,
        }
    }

    pub fn as_mut(&mut self) -> Option<&mut T> {
        match self {
            Self::Filled(v) => Some(v),
            _ => None,
        }
    }
}

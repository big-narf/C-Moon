use super::*;
use crate::context::*;
use crate::infra::*;

//#[test]
fn res_test() {
    let size = 2u64.pow(20);
    VKDriver::init_headless(size).unwrap();

    println!("page size is :{size}");

    println!("END OF TEST!!!");
}

/*
//#[test]
fn img_alloc() {
    let driver = VKDriver::new_headless().unwrap();

    let size = 2u64.pow(13);
    let allocator = TexAllocator::new(&driver, size).into();

    println!("page size is :{size}");
    {
        let n_tex = 50;
        let dim_range = 8..=32;

        let mut rng = rand::thread_rng();
        let mut textures = Vec::new();
        //randomly creates and destroys textures
        (1..=100).for_each(|_| {
            if rng.gen_bool(0.8) {
                let dim = rng.gen_range(dim_range.clone());
                let dimensions = vk::Extent2D {
                    width: dim,
                    height: dim,
                };
                //println!("creating!!!");
                let t = Texture::<Color32>::new(
                    dimensions,
                    vk::ImageUsageFlags::TRANSFER_DST,
                    vk::SharingMode::EXCLUSIVE,
                    &allocator,
                )
                .unwrap();
                textures.push(t);
            } else {
                if textures.len() != 0 {
                    let index = rng.gen_range(0..textures.len());
                    textures.swap_remove(index);
                }
            };
        });
        allocator.check_pages();
        println!("Using {} textures", textures.len());
    }

    allocator.check_pages();
}
//#[test]
fn allocation_test() {
    let driver = VKDriver::new_headless().unwrap();

    let allocator = MappedAllocator::new(&driver, 512).into();

    let src = [10, 2, 3, 4, 10000, 2];
    let mut v = MapVec::new_from(
        &src,
        vk::BufferUsageFlags::TRANSFER_SRC,
        vk::SharingMode::EXCLUSIVE,
        &allocator,
    )
    .unwrap();

    println!("vec :{:?}", v.as_slice());

    v.push(10);

    println!("vec :{:?}", v.as_slice());

    println!("size: {}, capacity :{}", v.len(), v.capacity());
    //allocator.check_pages();
    allocator.check_pages();
    drop(v);
    allocator.check_pages();
}

//#[test]
fn frag_test() {
    let driver = VKDriver::new_headless().unwrap();

    let allocator = MappedAllocator::new(&driver, 512).into();

    {
        let n_lists = 50;
        let range = 1..=10;

        let mut rng = rand::thread_rng();
        let mut lists: Vec<MapVec<u32>> = (1..=n_lists)
            .map(|_| {
                MapVec::new_with_capacity(
                    16,
                    vk::BufferUsageFlags::TRANSFER_SRC,
                    vk::SharingMode::EXCLUSIVE,
                    &allocator,
                )
                .unwrap()
            })
            .collect();

        (1..=1000).for_each(|_| {
            let index = rng.gen_range(0..lists.len());
            let list = lists.get_mut(index).unwrap();
            if rng.gen_bool(0.8) {
                list.push(rng.gen());
            } else {
                let _ = list.pop();
            };
        });

        allocator.check_pages();
    };

    allocator.check_pages();
}*/

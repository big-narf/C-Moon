use std::collections::HashMap;
use std::sync::{Arc, OnceLock, RwLock};

use ash::*;
use image::EncodableLayout;
use rand::rngs::ThreadRng;
use rand::{random, Rng};

use cmoon_commons::*;
use cmoon_tests::*;

use crate::context;
use crate::shared::VertexFAT;

mod alloc;
mod debug;
mod thread;

use debug::*;
use thread::*;

//#[test]
fn map_test() {
    let (_, map) = rust_info(1);

    println!("map is :{map:?}");
    map.get("mesh").unwrap();
}

#[test]
fn render_test() {
    let mut app = MultiApp::<_, 3>::new(context::Renderer::new)
        .with_space_fn(create_image)
        .with_enter_fn(destroy_image);

    //let mut app = MultiApp::<_, 3>::new(context::Renderer::new);

    app.run_loop();
}

fn destroy_image<T: Module>(mdl: &T) -> EngResult<()> {
    let mut rng = ThreadRng::default();
    let id = rng.gen_range(0..10);
    match mdl.destroy(id) {
        Ok(()) => println!("destroyed obj {id}"),
        Err(e) => println!("error trying to destroy obj {id} : {e:?}"),
    }

    Ok(())
}

fn create_image<T: Module>(mdl: &T) -> EngResult<()> {
    let (code, args) = rust_info(0);
    mdl.create(code, args)?;
    Ok(())
}

fn rust_info(marker: u32) -> (PartialCode, PartialArgs) {
    let image = AssetHandle::new(0, image_loader);

    let mesh = AssetHandle::new(1, mesh_loader);

    let f_shader = AssetHandle::new(2, frag_loader);

    let v_shader = AssetHandle::new(3, vert_loader);

    let code = PartialCode {
        group: Default::default(),
        identifier: "sprite".into(),
        parent: Some("TESTER".into()),
    };
    let mut args = EngMap::default();
    args.try_insert("frag_shader", f_shader);
    args.try_insert("vert_shader", v_shader);
    args.try_insert("image", image);
    args.try_insert("mesh", mesh);

    (code, args)
}

fn image_loader(_: u32) -> EngResult<AssetData> {
    let image = image::load_from_memory(&cmoon_tests::bytes_from_project!("image.png"))
        .unwrap()
        .to_rgba8();
    let width = image.width();
    let height = image.height();

    let rgba = Data::from(image.as_bytes());

    let mut data = EngMap::default();
    data.replace("width", width);
    data.replace("height", height);
    data.replace("rgba", rgba);

    Ok(AssetData::Composite(data))
}

fn mesh_loader(_: u32) -> EngResult<AssetData> {
    println!("LOADING MESH!!!!!!!");
    let index_data = [0u32, 1, 2, 2, 3, 0];

    let indices = Data::from(bytemuck::cast_slice(&index_data));

    let (x1, x2, y1, y2) = if true {
        (-0.0, 1.0, -1.0, 0.5)
    } else {
        (-1.0, 0.0, -1.0, 0.5)
    };

    let vertex_data = [
        VertexFAT {
            pos: [x1, y1, 0.0, 1.0],
            uv: [0.0, 0.0, 0.0],
            normal: [0.0, 0.0, 0.0],
        },
        VertexFAT {
            pos: [x1, y2, 0.0, 1.0],
            uv: [0.0, 1.0, 0.0],
            normal: [0.0, 0.0, 0.0],
        },
        VertexFAT {
            pos: [x2, y2, 0.0, 1.0],
            uv: [1.0, 1.0, 0.0],
            normal: [0.0, 0.0, 0.0],
        },
        VertexFAT {
            pos: [x2, y1, 0.0, 1.0],
            uv: [1.0, 0.0, 0.0],

            normal: [0.0, 0.0, 0.0],
        },
    ];

    let vertices = Data::from(bytemuck::cast_slice(&vertex_data));

    let map = EngMap::from_iter([("indices", indices), ("vertices", vertices)]);

    Ok(AssetData::Composite(map))
}

fn frag_loader(_: u32) -> EngResult<AssetData> {
    let shader = Data::from(cmoon_tests::bytes_from_project!("texture.frag.spv"));
    Ok(AssetData::Simple(shader.into()))
}

fn vert_loader(_: u32) -> EngResult<AssetData> {
    let shader = Data::from(cmoon_tests::bytes_from_project!("texture.vert.spv"));
    Ok(AssetData::Simple(shader.into()))
}

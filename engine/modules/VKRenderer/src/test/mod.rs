use std::collections::HashMap;
use std::sync::{Arc, OnceLock, RwLock};

use ash::*;
use image::EncodableLayout;
use rand::rngs::ThreadRng;
use rand::{random, Rng};

use cmoon_commons::*;
use cmoon_tests::*;

use crate::context;
use crate::shared::VertexFAT;

mod alloc;
mod debug;
mod thread;

use debug::*;
use thread::*;
#[test]
fn render_test() {
    let mut app = MultiApp::<_, 3>::new(context::Renderer::new)
        .with_space_fn(create_image)
        .with_enter_fn(destroy_image);

    //let mut app = MultiApp::<_, 3>::new(context::Renderer::new);

    app.run_loop();
}

fn destroy_image<T: Module>(mdl: &T) -> EngResult<()> {
    let mut rng = ThreadRng::default();
    let id = rng.gen_range(0..10);
    match mdl.destroy(id) {
        Ok(()) => println!("destroyed obj {id}"),
        Err(e) => println!("error trying to destroy obj {id} : {e:?}"),
    }

    Ok(())
}

fn create_image<T: Module>(mdl: &T) -> EngResult<()> {
    let (code, args) = rust_info(0);
    mdl.create(code, args)?;
    Ok(())
}

fn rust_info(marker: u32) -> (PartialCode, PartialArgs) {
    let image = AssetHandle::new(0, image_loader);

    let mesh = AssetHandle::new(1, mesh_loader);

    let f_shader = AssetHandle::new(2, frag_loader);

    let v_shader = AssetHandle::new(3, vert_loader);

    let code = PartialCode {
        group: Default::default(),
        identifier: "sprite".into(),
        parent: Some("TESTER".into()),
    };
    let args = [
        ("frag_shader".into(), f_shader.into()),
        ("vert_shader".into(), v_shader.into()),
        ("image".into(), image.into()),
        ("mesh".into(), mesh.into()),
    ]
    .into_iter()
    .collect();
    (code, args)
}

fn image_loader(_: u32) -> EngResult<Arc<AssetData>> {
    let image = image::load_from_memory(&cmoon_tests::bytes_from_project!("image.png"))
        .unwrap()
        .to_rgba8();
    let width: DynValue = (image.width()).into();
    let height: DynValue = (image.height()).into();

    let rgba: DynValue = image.as_bytes().to_vec().into();

    let data: AssetData = AssetData::Composite(
        [
            ("width".to_string(), width),
            ("height".to_string(), height),
            ("rgba".to_string(), rgba),
        ]
        .into(),
    );
    Ok(Arc::new(data))
}

fn mesh_loader(_: u32) -> EngResult<Arc<AssetData>> {
    let index_data = [0u32, 1, 2, 2, 3, 0];

    let indices: Vec<u8> = bytemuck::cast_slice(&index_data).into();

    let (x1, x2, y1, y2) = if true {
        (-0.0, 1.0, -1.0, 0.5)
    } else {
        (-1.0, 0.0, -1.0, 0.5)
    };

    let vertex_data = [
        VertexFAT {
            pos: [x1, y1, 0.0, 1.0],
            uv: [0.0, 0.0, 0.0],
            normal: [0.0, 0.0, 0.0],
        },
        VertexFAT {
            pos: [x1, y2, 0.0, 1.0],
            uv: [0.0, 1.0, 0.0],
            normal: [0.0, 0.0, 0.0],
        },
        VertexFAT {
            pos: [x2, y2, 0.0, 1.0],
            uv: [1.0, 1.0, 0.0],
            normal: [0.0, 0.0, 0.0],
        },
        VertexFAT {
            pos: [x2, y1, 0.0, 1.0],
            uv: [1.0, 0.0, 0.0],

            normal: [0.0, 0.0, 0.0],
        },
    ];

    let vertices: Vec<u8> = bytemuck::cast_slice(&vertex_data).into();

    let data = AssetData::Composite(
        [
            ("indices".to_string(), indices.into()),
            ("vertices".to_string(), vertices.into()),
        ]
        .into(),
    );
    Ok(Arc::new(data))
}

fn frag_loader(_: u32) -> EngResult<Arc<AssetData>> {
    let shader = cmoon_tests::bytes_from_project!("texture.frag.spv");
    let data = AssetData::Simple(shader.into());
    Ok(Arc::new(data))
}

fn vert_loader(_: u32) -> EngResult<Arc<AssetData>> {
    let shader = cmoon_tests::bytes_from_project!("texture.vert.spv");
    let data = AssetData::Simple(shader.into());
    Ok(Arc::new(data))
}

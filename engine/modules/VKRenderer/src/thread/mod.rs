use ash::vk;
use ash::{ext, khr};

use crossbeam::queue::SegQueue;

use cmoon_commons::*;

use crate::elements::*;
use crate::infra::*;
use crate::shared::*;

///this is thread local ctx with command buffer and local objects
pub struct DrawCtx {
    c_pool: vk::CommandPool,
    c_buffer: vk::CommandBuffer,
    objects: ReuseList<RenderObject>,
    destroy_list: SegQueue<usize>,

    get_transform_fn: fn(&str) -> Option<Transform>,
}

impl DrawCtx {
    pub fn new(driver: &VKDriver, core_handle: &CoreHandle) -> EngResult<Self> {
        //NOTE: SECONDARY O
        let queue_id = driver.graphics_queue_id();
        let create_info = vk::CommandPoolCreateInfo::default()
            .queue_family_index(queue_id)
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER);
        let c_pool = driver.create_cmd_pool(&create_info)?;
        //creates thread command buffers
        let create_info = vk::CommandBufferAllocateInfo::default()
            .command_pool(c_pool)
            .level(vk::CommandBufferLevel::SECONDARY)
            .command_buffer_count(1);
        let c_buffer = driver
            .allocate_cmd_buffers(&create_info)?
            .into_iter()
            .next()
            .ok_or_else(|| {
                CMD_BUFFER_ALLOC_FAIL
                    .clone()
                    .with_root_cause("Secondary@Illustrator")
            })?;

        Ok(Self {
            c_pool,
            c_buffer,
            objects: Default::default(),
            destroy_list: Default::default(),
            get_transform_fn: core_handle.transform_getter,
        })
    }
    pub fn derived_buffer(&self) -> Derived<vk::CommandBuffer> {
        self.c_buffer.into()
    }

    pub fn schedule_destroy(&self, id: usize) {
        self.destroy_list.push(id);
    }

    pub fn process_local(
        &self,
        target: &RenderTarget,
        frame_id: usize,
        index_buffer: &vk::Buffer,
        vertex_buffer: &vk::Buffer,
    ) -> EngResult<()> {
        let driver = VKDriver::instance().unwrap();

        let renderpass = target.renderpass();
        let framebuffer = target.framebuffer(frame_id);
        let viewports = target.viewports();
        let scissors = target.scissors();
        //let framebuffer = target.
        driver.start_secondary(
            &self.c_buffer,
            renderpass,
            framebuffer,
            viewports,
            scissors,
            index_buffer,
            vertex_buffer,
        )?;
        //does a draw command for each object in the scene
        self.objects.iter().try_for_each(|obj| -> EngResult<()> {
            match obj {
                RenderObject::Sprite(s) => {
                    let origin = (self.get_transform_fn)(&s.parent).unwrap();
                    //println!("Rendering obj at {origin:?}");
                    driver.render_obj(&self.c_buffer, origin, &s.geometry, &s.scheme, &s.asset);

                    Ok(())
                }
                _ => Err(UNIMPLEMENTED.clone()),
            }
        })?;

        driver.finish_secondary(&self.c_buffer)?;

        Ok(())
    }
}

///Alter related functions
impl DrawCtx {
    pub fn add_obj(
        &mut self,
        shared: &SharedManager,
        target: &RenderTarget,
        group: EngString,
        identifer: EngString,
        parent: Option<EngString>,
        mut args: PartialArgs,
    ) -> EngResult<usize> {
        let obj: RenderObject = match identifer.as_str() {
            "sprite" => {
                let mesh: AssetHandle = args.get_create_value("mesh")?;
                let geometry = shared.geometry.create_geometry(&mesh)?;

                //NOTE: CREATE IMAGE RESOURCE
                let image: AssetHandle = args.get_create_value("image")?;
                let asset = shared.asset.create_texture(&image)?;

                shared.scheme.define_ppl_layout(&asset.layouts())?;

                let f_shader: AssetHandle = args.get_create_value("frag_shader")?;
                let v_shader: AssetHandle = args.get_create_value("vert_shader")?;
                let scheme = shared
                    .scheme
                    .create_img_pipeline(target, &v_shader, &f_shader)?;

                Sprite::new(parent.unwrap(), geometry, scheme, asset).into()
            }
            t_name => unimplemented!("shit!"), //Err(INVALID_PARTIAL_TYPE.clone().with_root_cause(t_name)),
        };

        let id = self.objects.insert(obj);

        Ok(id)
    }

    pub fn collect_garbage(&mut self) {
        while let Some(index) = self.destroy_list.pop() {
            self.objects.remove(index);
        }
    }
}

impl Drop for DrawCtx {
    fn drop(&mut self) {
        let driver = VKDriver::instance().unwrap();
        driver.destroy_cmd_pool(&mut self.c_pool);
    }
}

use cmoon_commons::*;

mod context;
mod elements;
mod infra;
mod shared;
mod thread;

#[cfg(test)]
mod test;

pub static MDL_TYPE: ModuleType = ModuleType::Renderer;

//implements the C interface in the format that the engine core expects
cmoon_commons::module_interface!(context::Renderer, MDL_TYPE);

use std::collections::HashMap;
use std::ops::{Deref, DerefMut};
use std::sync::{Arc, Mutex, RwLock, RwLockReadGuard, RwLockWriteGuard};

use ash::vk;
use ash::Device;
use ash::{ext, khr};

use crossbeam::queue::SegQueue;
use dashmap::DashMap;
use raw_window_handle::*;

use cmoon_commons::*;

use crate::elements::*;
use crate::infra::*;
use crate::shared::*;
use crate::thread::*;

///Renderer is the local_ctx for the vkrenderer.
//#[derive(Debug)]
pub struct Renderer {
    //thread locals
    ctx_list: ThreadVec<DrawCtx>,
    //Swapchain
    presenter: Presenter,

    //Descriptors
    shared: SharedManager,

    //object list
    id_table: Mutex<SlotList<(usize, usize)>>,

    create_list: SegQueue<ScheduledCreate>,
}

///Module interface functions
impl Renderer {
    ///Returns new instance of vkrenderer
    pub fn new(
        handle: CoreHandle,
        n_threads: usize,
        config: ModuleConfig,
        _: Option<HashMap<String, String>>,
    ) -> EngResult<Self> {
        //NOTE: INITS INFRASTRUCTURE

        let page_size = config.get("MAPPED_PAGE_SIZE").ok_or_else(|| {
            MISSING_CONFIG_FIELD
                .clone()
                .with_root_cause("MAPPED_PAGE_SIZE")
        })?;

        VKDriver::init(handle.display_handle, page_size)?;

        let driver = VKDriver::instance()
            .ok_or_else(|| UNIMPLEMENTED.clone().with_root_cause("device error"))?;

        let ctx_list: ThreadVec<_> = (0..n_threads)
            .map(|_| DrawCtx::new(driver, &handle))
            .collect::<EngResult<_>>()?;

        let buff_derive = ctx_list.iter().map(|ctx| ctx.derived_buffer()).collect();

        let presenter = Presenter::new(
            handle.display_handle,
            handle.window_handle,
            700,
            700,
            buff_derive,
            driver.graphics_queue_id(),
            &[vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT],
        )?;

        let index_max = config
            .get("index_max")
            .ok_or_else(|| MISSING_CONFIG_FIELD.clone().with_root_cause("index_max"))?;
        let vertex_max = config
            .get("vertex_max")
            .ok_or_else(|| MISSING_CONFIG_FIELD.clone().with_root_cause("vertex_max"))?;

        let max_desc = config
            .get("max_desc")
            .ok_or_else(|| MISSING_CONFIG_FIELD.clone().with_root_cause("max_desc"))?;

        let shared = SharedManager::new(&driver, max_desc, index_max, vertex_max)?;

        Ok(Self {
            ctx_list,
            presenter,

            shared,

            id_table: Default::default(),

            create_list: Default::default(),
        })
    }
}

impl Module for Renderer {
    fn get_type() -> ModuleType {
        crate::MDL_TYPE.clone()
    }

    ///will create a triangle + pipeline
    fn create(&self, code: PartialCode, args: PartialArgs) -> EngResult<usize> {
        let id = self.id_table.lock().cast()?.reserve(); //this will fuck me up, but oh well....

        let create = ScheduledCreate { id, code, args };
        self.create_list.push(create);
        Ok(id)
    }

    fn destroy(&self, address: usize) -> EngResult<()> {
        let (thread_id, local_id) =
            self.id_table
                .lock()
                .cast()?
                .remove(address)
                .ok_or_else(|| {
                    OBJ_DONT_EXIST
                        .clone()
                        .with_root_cause(format!("id={address}"))
                })?;
        self.ctx_list.try_get(thread_id)?.schedule_destroy(local_id);
        Ok(())
    }

    fn alter(&self, thread_id: usize) -> EngResult<()> {
        let mut ctx = self.ctx_list.get_mut(thread_id).unwrap();

        //create new objects
        while let Some(scheduled) = self.create_list.pop() {
            let id = scheduled.id;
            let code = scheduled.code;
            let args = scheduled.args;
            let target = self.presenter.target();
            let local_adress = ctx.add_obj(
                &self.shared,
                target,
                code.group,
                code.identifier,
                code.parent,
                args,
            )?;
            self.id_table
                .lock()
                .cast()?
                .insert(id, (thread_id, local_adress));
        }

        //Destroys removed objects.
        ctx.collect_garbage();

        println!("alter {thread_id} done!");
        Ok(())
    }

    fn process(&self, thread_id: usize, dt: u32) -> EngResult<()> {
        //gets thread local buffer
        let draw = self.ctx_list.get(thread_id).unwrap();
        //does a draw command for each object in the local thread
        let target = self.presenter.target();
        let frame_id = self.presenter.current_frame();
        let index_buffer = self.shared.geometry.indices();
        let vertex_buffer = self.shared.geometry.vertices();
        draw.process_local(target, frame_id, index_buffer, vertex_buffer)?;

        println!("process {thread_id} done!");
        Ok(())
    }

    fn commence(&self) -> EngResult<()> {
        self.id_table
            .lock()
            .cast()?
            .used_ids()
            .for_each(|id| println!("valid id:{}", id));
        println!("Commencing renderer");
        self.presenter.next_frame()?;
        Ok(())
    }

    fn conclude(&self) -> EngResult<()> {
        println!("Concluding renderer");
        self.presenter.present()
    }
}

pub struct ScheduledCreate {
    id: usize,
    code: PartialCode,
    args: PartialArgs,
}

///conveniance struct for lists of thread local ctxs
pub struct ThreadVec<T>(Vec<RwLock<T>>);

impl<T> ThreadVec<T> {
    pub fn get(&self, index: usize) -> Option<RwLockReadGuard<'_, T>> {
        self.0.get(index).and_then(|lock| lock.read().ok())
    }

    pub fn try_get(&self, index: usize) -> EngResult<RwLockReadGuard<'_, T>> {
        let lock = self
            .0
            .get(index)
            .ok_or_else(|| INVALID_THREAD.clone().with_root_cause(index))?;
        lock.read().cast()
    }

    pub fn get_mut(&self, index: usize) -> Option<RwLockWriteGuard<'_, T>> {
        self.0.get(index).and_then(|lock| lock.write().ok())
    }

    pub fn iter(&self) -> ThreadIter<'_, T> {
        ThreadIter {
            src: self,
            cur: Default::default(),
        }
    }
}

impl<T> FromIterator<T> for ThreadVec<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        Self(iter.into_iter().map(|val| val.into()).collect())
    }
}

struct ThreadIter<'src, T> {
    src: &'src ThreadVec<T>,
    cur: usize,
}
impl<'src, T> Iterator for ThreadIter<'src, T> {
    type Item = RwLockReadGuard<'src, T>;
    fn next(&mut self) -> Option<Self::Item> {
        let item = self.src.get(self.cur);
        self.cur += 1;
        item
    }
}

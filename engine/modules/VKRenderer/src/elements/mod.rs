use std::ffi::CStr;
use std::mem;
use std::sync::{Arc, Weak};

use ash::vk;
use ash::vk::DescriptorSetLayout;
use ash::Device;

use cmoon_commons::*;
pub use misc::*;
use rand::Rng;

use crate::context::*;

#[macro_use]
use crate::infra::*;
use crate::shared::RenderTarget;
use crate::shared::*;

mod misc;
mod target;

///Render object is a generic object that can be rendered to a target
pub enum RenderObject {
    Camera,
    Sprite(Sprite),
}

//NOTE: CHANGE THIS TO HOLD Arcs probably

#[derive(Debug)]
pub struct Sprite {
    pub parent: EngString,
    pub geometry: Arc<Geometry>,
    pub scheme: Arc<ImgScheme>,
    pub asset: Arc<ImgAsset>,
}

impl Sprite {
    pub fn new(
        parent: EngString,
        geometry: Arc<Geometry>,
        scheme: Arc<ImgScheme>,
        asset: Arc<ImgAsset>,
    ) -> Self {
        Self {
            parent,
            geometry,
            scheme,
            asset,
        }
    }
}

impl Into<RenderObject> for Sprite {
    fn into(self) -> RenderObject {
        RenderObject::Sprite(self)
    }
}

#[derive(Clone, Debug, Copy)]
pub struct Vertex {
    pub pos: [f32; 4],
    pub color: [f32; 4],
}

#[derive(Clone, Debug, Copy, Default)]
pub struct Origin {
    pub pos: [f32; 4],
}

impl From<Transform> for Origin {
    fn from(value: Transform) -> Self {
        let pos = [value.pos[0], value.pos[1], value.pos[2], 1.0];
        Self { pos }
    }
}

impl Origin {
    pub fn ones() -> Self {
        Self {
            pos: [1.0, 1.0, 1.0, 0.0],
        }
    }

    pub fn zeroes() -> Self {
        Self {
            pos: [0.0, 0.0, 0.0, 0.0],
        }
    }

    pub fn random() -> Self {
        let mut rng = rand::thread_rng();
        let x = rng.gen_range(0.0..1.0);
        let y = rng.gen_range(0.0..1.0);
        Self {
            pos: [x, y, 1.0, 0.0],
        }
    }
}

impl Origin {
    pub fn bytes_ref<'a>(&'a self) -> &'a [u8] {
        unsafe {
            std::slice::from_raw_parts(
                self as *const Self as *const u8,
                std::mem::size_of::<Self>(),
            )
        }
    }
}

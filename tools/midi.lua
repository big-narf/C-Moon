local loader = {}

---NOTE: Will import the MIDI messages and time deltas as two arrays of fixed sized elements.
---messages are 4 byte long, if there is no data2 it is just zeroed.
---deltas are regular u32. If we need to make space, we compressing the fucker.
midi = {
	extension = "obj$",
	scheme = {
		messages = "compressed_data",
		deltas = "compressed_data",
	},
}

function midi.import(file)
	print("importing midi :", file)

	local messages, deltas = loader.load(file:text())

	local data = {
		messages = to_u32_bytes(messages),
		deltas = to_u32_bytes(deltas),
	}

	local imports = {
		[file:name()] = data,
	}
	return imports
end

---@class reader
---@field bytes string,
---@field i integer,
local reader = {}
reader({ __index = reader })

---comment
---@param bytes string
---@return reader
function reader.new(bytes)
	return setmetatable({ bytes = bytes, i = 0 }, reader)
end

---returns the current read byte.
---@return integer
function reader:position()
	return self.i
end

---returns true if the reader hasen't reached pos yet.
---@param pos integer
---@return boolean
function reader:is_before(pos)
	return self.i < pos
end

---skips size bytes , unchecked
---@param size integer
function reader:skip(size)
	self.i = self.i + size
end

---skips to the position, unchecked
---@param pos integer
function reader:skip_to(pos)
	self.i = pos
end

---returns size next bytes as a string
---@param size number
---@return string
function reader:read_bytes(size)
	local bytes = self.bytes.sub(self.i, self.i + size)
	self.i = self.i + size
	return bytes
end

function reader:read_4cc()
	local v = string.sub(self.bytes, self.i, self.i + 4)
	self.i = self.i + 4
	return v
end

function reader:read_u8()
	local b = string.byte(string.sub(self.bytes, self.i, self.i))
	self.i = self.i + 1
	return b
end

---reads i32 integer. Assumes small endianess if not specified.
---@param big_end boolean?
---@return integer
function reader:read_i32(big_end)
	print("I IS :", self.i)
	local b1, b2, b3, b4 = string.byte(string.sub(self.bytes, self.i, self.i + 4))
	self.i = self.i + 4
	if big_end then
		return b4 + (b3 * 256) + (b2 * 65536) + (b1 * 16777216) - 2147483647
	else
		return b1 + (b2 * 256) + (b3 * 65536) + (b4 * 16777216) - 2147483647
	end
end

---reads u16 integer. Assumes small endianess if not specified.
---@param big_end boolean?
---@return integer
function reader:read_u16(big_end)
	local b1, b2 = string.byte(string.sub(self.bytes, self.i, self.i + 2))
	self.i = self.i + 2
	if big_end then
		return b2 + (b1 * 256)
	else
		return b1 + (b2 * 256)
	end
end

---reads i16 integer. Assumes small endianess if not specified.
---@param big_end boolean?
---@return integer
function reader:read_i16(big_end)
	local b1, b2 = string.byte(string.sub(self.bytes, self.i, self.i + 2))
	self.i = self.i + 2
	if big_end then
		return b2 + (b1 * 256) - 32767
	else
		return b1 + (b2 * 256) - 32767
	end
end

---this is the most ungodly shit ever
---@return integer
function reader:read_ivar()
	local b1, b2, b3, b4 = string.byte(string.sub(self.bytes, self.i, self.i + 4))
	local acc = 0
	acc = acc + b1
	if b1 < 0x80 then -- 128
		self.i = self.i + 1
		return acc
	end
	acc = (acc - 0x80) * 0x80 -- removes lead bit and shifts value

	acc = acc + b2
	if b2 < 0x80 then
		self.i = self.i + 2
		return acc
	end
	acc = (acc - 0x80) * 0x80

	acc = acc + b3
	if b3 < 0x80 then
		self.i = self.i + 3
		return acc
	end
	acc = (acc - 0x80) * 0x80

	acc = acc + b4
	if b4 < 0x80 then
		self.i = self.i + 4
		return acc
	end
	acc = (acc - 0x80) * 0x80
	error("Ivar bigger than 4 bytes")
end

local TRACK_HEADER = "MTrk"

---@class message
local message = {}

function message.common(status, data1)
	return { channel = (status / 0x0F) * 0x0F, command = (status / 0xF0) * 0xF0, data1 = data1, data2 = 0 }
end

function message.common2(status, data1, data2, loop_type)
	local channel = (status / 0x0F) * 0x0F
	local command = (status / 0xF0) * 0xF0
end

---returns the message formated as a u32.
function message:flatten()
	return self.data2 + (self.data1 * 256) + (self.command * 65536) + (self.channel * 16777216)
end

function read_track(r)
	--chunk header
	assert(r:read_4cc() == TRACK_HEADER, "Not a track chunk")
	local chunk_end = r:read_i32(true)
	chunk_end = chunk_end + r:position()

	local cur_tick, last_status = 0, 0
	local msgs, ticks = {}, {}
	repeat
		--loop
		local delta = r:read_ivar()
		local first = r:read_u8()

		cur_tick = cur_tick + delta

		if first > 0x80 then
			local cmd = math.floor(last_status / 0xF0) * 0xF0
			if cmd == 0xC0 or cmd == 0xD0 then ---NOTE: REGULAR MESSAGES
				table.insert(msgs, message.common(last_status, first))
				table.insert(ticks, cur_tick)
			else
				local data2 = r:read_u8()
				table.insert(msgs, message.common2(last_status, first, data2, loop_type))
			end
		end
		last_status = first
	until true
end

---comment
---@param source string
---@return unknown
function loader.load(source)
	local r = reader.new(source)
	assert(r:read_4cc() == TRACK_HEADER, "Not a track chunk")
	local size = r:read_i32(true)
	assert(size == 6, "track header size is invalid.")
	local format = r:read_i16(true)

	assert(format == 0 or format == 1, "track format is not supported")

	local track_count = r:read_i16(true)
	local resolution = r:read_i16(true)

	for i = 0, track_count do
		local msgs, ticks = read_track(r)
	end
end

return loader

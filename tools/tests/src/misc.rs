use std::path::{Path, PathBuf};
use std::sync::Barrier;
use std::sync::atomic::{AtomicBool, AtomicUsize, Ordering};

use dirs::home_dir;
use lazy_static::lazy_static;
use raw_window_handle::*;

use cmoon_commons::*;

lazy_static! {
    pub static ref DEFAULT_PROJECT: PathBuf =
        home_dir().expect("NO HOME DIR").join("C-Moon/project");
}

#[derive(Debug)]
pub struct Synchronizer {
    n_threads: usize,
    frame_barrier: Barrier,
    n_idle: AtomicUsize,
    end_flag: AtomicBool,
}

impl Synchronizer {
    pub fn new(n_threads: usize) -> Self {
        Self {
            n_threads,
            frame_barrier: Barrier::new(n_threads + 1),
            n_idle: Default::default(),
            end_flag: Default::default(),
        }
    }
}

impl Synchronizer {
    ///Resets the counter and unblocks the slaves
    pub fn reset(&self) {
        self.n_idle.store(0, Ordering::SeqCst);
        self.frame_barrier.wait();
    }

    pub fn finish(&self) {
        self.end_flag.store(true, Ordering::SeqCst);
        self.frame_barrier.wait();
    }

    ///Checks if the frame can be reseted already
    pub fn check(&self) -> bool {
        let v = self.n_idle.load(Ordering::SeqCst);
        v >= self.n_threads
    }

    pub fn slave_wait(&self) -> bool {
        if self.end_flag.load(Ordering::SeqCst) {
            true
        } else {
            let n = self.n_idle.fetch_add(1, Ordering::SeqCst);
            self.frame_barrier.wait();
            false
        }
    }
}

#[macro_export]
macro_rules! bytes_from_project {
    ($file :expr) => {{
        use std::path::PathBuf;
        use $crate::DEFAULT_PROJECT;
        let p = std::env::var("PROJECT_PATH")
            .map(|s| PathBuf::from(s))
            .unwrap_or(DEFAULT_PROJECT.clone())
            .join($file);
        println!("{p:?}");
        let mut file = std::fs::File::open(p).expect("open error");
        let mut buf = Vec::new();
        std::io::Read::read_to_end(&mut file, &mut buf).expect("read error");
        buf
    }};
}

#[macro_export]
macro_rules! str_from_project {
    ($file :expr) => {{
        use std::path::PathBuf;
        use $crate::DEFAULT_PROJECT;
        let p = std::env::var("PROJECT_PATH")
            .map(|s| PathBuf::from(s))
            .unwrap_or(DEFAULT_PROJECT.clone())
            .join($file);

        println!("{p:?}");
        let mut file = std::fs::File::open(p).expect("open error");
        let mut buf = String::new();
        std::io::Read::read_to_string(&mut file, &mut buf).expect("read error");
        buf
    }};
}

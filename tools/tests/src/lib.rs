use std::collections::HashMap;
use std::io::Read;
use std::sync::{Arc, OnceLock, RwLock};
use std::thread::JoinHandle;

use lazy_static::lazy_static;
use raw_window_handle::*;
use winit::application::ApplicationHandler;
use winit::event::*;
use winit::event_loop::*;
use winit::keyboard::KeyCode;
use winit::platform::x11::EventLoopBuilderExtX11;
use winit::window::{Window, WindowAttributes};

use cmoon_commons::*;

mod misc;

pub use misc::*;

pub type InnitFn<T> =
    fn(CoreHandle, usize, ModuleConfig, Option<HashMap<String, String>>) -> EngResult<T>;

#[derive(Debug)]
pub struct SingleApp<T: Module> {
    innit: InnitFn<T>,
    ctx: OnceLock<Arc<T>>,
    win: Option<Window>,
    on_space: Option<fn(&T) -> EngResult<()>>,
    on_enter: Option<fn(&T) -> EngResult<()>>,
}

impl<T: Module> SingleApp<T> {
    const N_THREADS: usize = 1;

    pub fn new(innit: InnitFn<T>) -> Self {
        Self {
            innit,
            ctx: OnceLock::new(),
            win: None,
            on_space: Default::default(),
            on_enter: Default::default(),
        }
    }

    pub fn with_space_fn(mut self, on_space: fn(&T) -> EngResult<()>) -> Self {
        self.on_space = Some(on_space);
        self
    }

    pub fn with_enter_fn(mut self, on_enter: fn(&T) -> EngResult<()>) -> Self {
        self.on_enter = Some(on_enter);
        self
    }

    pub fn run_loop(&mut self) {
        let event_loop = EventLoop::builder().with_any_thread(true).build().unwrap();

        println!("event loop starting!");

        event_loop.run_app(self).unwrap();

        println!("exited event loop!");
    }

    /// does an alter + process cycle and then swaps frame
    fn advance_frame(&self) {
        let ctx = self.ctx.get().unwrap();
        ctx.commence().unwrap();
        //worker part
        {
            ctx.alter(0).unwrap();
            ctx.process(0, 10).unwrap();
        }
        //main thread part
        ctx.conclude().unwrap();
    }
}
impl<T: Module> ApplicationHandler for SingleApp<T> {
    fn resumed(&mut self, event_loop: &winit::event_loop::ActiveEventLoop) {
        // PARAMS
        let width = 700;
        let height = 700;
        let att = WindowAttributes::default()
            .with_title("Ash - Example")
            .with_inner_size(winit::dpi::LogicalSize::new(
                f64::from(width),
                f64::from(height),
            ));
        let window = event_loop.create_window(att).unwrap();

        let mock = CoreHandle::mock(
            window.display_handle().unwrap().as_raw(),
            window.window_handle().unwrap().as_raw(),
        );

        let ctx: Arc<_> = (self.innit)(
            mock,
            Self::N_THREADS,
            ModuleConfig::config_for(T::get_type()),
            None,
        )
        .unwrap()
        .into();

        //ctx.commence().unwrap();

        self.ctx.set(ctx.clone()).ok().unwrap();

        self.win = Some(window);
    }

    fn about_to_wait(&mut self, event_loop: &ActiveEventLoop) {
        self.advance_frame();
    }

    fn window_event(
        &mut self,
        event_loop: &winit::event_loop::ActiveEventLoop,
        window_id: winit::window::WindowId,
        event: winit::event::WindowEvent,
    ) {
        match event {
            WindowEvent::CloseRequested => {
                self.win = None;
                event_loop.exit();
            }
            WindowEvent::KeyboardInput {
                device_id,
                event,
                is_synthetic,
            } => {
                let ctx: &T = self.ctx.get().unwrap();

                if event.physical_key == KeyCode::Space {
                    self.on_space.as_ref().map(|func| (func)(ctx).unwrap());
                }
                if event.physical_key == KeyCode::Enter {
                    self.on_enter.as_ref().map(|func| (func)(ctx).unwrap());
                }
            }
            _ => (),
        }
    }

    fn exiting(&mut self, event_loop: &winit::event_loop::ActiveEventLoop) {}
}

//#[derive(Default)]
pub struct MultiApp<T: Module + 'static, const N_THREADS: usize> {
    innit: InnitFn<T>,
    ctx: OnceLock<Arc<T>>,
    sync: Arc<Synchronizer>,
    win: Option<Window>,
    threads: Vec<JoinHandle<()>>,
    on_space: Option<fn(&T) -> EngResult<()>>,
    on_enter: Option<fn(&T) -> EngResult<()>>,
    on_update: Option<fn(&T) -> EngResult<()>>,

    //core overrides
    signal_searcher: Option<fn(&SignalID) -> Option<Signal>>,
}

impl<T: Module + 'static, const N_THREADS: usize> MultiApp<T, N_THREADS> {
    pub fn new(innit: InnitFn<T>) -> Self {
        Self {
            innit,
            ctx: OnceLock::new(),
            sync: Synchronizer::new(N_THREADS).into(),
            win: None,
            threads: Default::default(),
            on_space: Default::default(),
            on_enter: Default::default(),
            on_update: Default::default(),

            signal_searcher: Default::default(),
        }
    }

    pub fn with_space_fn(mut self, on_space: fn(&T) -> EngResult<()>) -> Self {
        self.on_space = Some(on_space);
        self
    }

    pub fn with_enter_fn(mut self, on_enter: fn(&T) -> EngResult<()>) -> Self {
        self.on_enter = Some(on_enter);
        self
    }

    pub fn override_signal_searcher(
        mut self,
        signal_searcher: fn(&SignalID) -> Option<Signal>,
    ) -> Self {
        self.signal_searcher = Some(signal_searcher);
        self
    }

    pub fn run_loop(&mut self) {
        let event_loop = EventLoop::builder().with_any_thread(true).build().unwrap();

        println!("event loop starting!");

        event_loop.run_app(self).unwrap();

        println!("ALL IS OK, EXITED MAIN LOOP!!!!!");
    }

    fn advance_frame(&self) {
        while !self.sync.check() {}
        self.ctx.get().unwrap().conclude().unwrap();
        self.ctx.get().unwrap().commence().unwrap();
        self.sync.reset();
    }

    fn try_advance_frame(&self) {
        if self.sync.check() {
            let ctx = self.ctx.get().unwrap();
            self.on_update.as_ref().map(|func| func(ctx).unwrap());
            ctx.conclude().unwrap();
            ctx.commence().unwrap();
            self.sync.reset();
        }
    }
}
impl<T: Module + 'static, const N_THREADS: usize> ApplicationHandler for MultiApp<T, N_THREADS> {
    fn resumed(&mut self, event_loop: &winit::event_loop::ActiveEventLoop) {
        // PARAMS
        let width = 700;
        let height = 700;
        let att = WindowAttributes::default()
            .with_title("TEST CORE")
            .with_inner_size(winit::dpi::LogicalSize::new(
                f64::from(width),
                f64::from(height),
            ));
        let window = event_loop.create_window(att).unwrap();

        let mut mock = CoreHandle::mock(
            window.display_handle().unwrap().as_raw(),
            window.window_handle().unwrap().as_raw(),
        );

        match self.signal_searcher {
            Some(func) => {
                mock = mock.with_sig_finder(func);
            }
            None => (),
        };

        let ctx: Arc<_> = (self.innit)(
            mock,
            N_THREADS,
            ModuleConfig::config_for(T::get_type()),
            None,
        )
        .unwrap()
        .into();

        self.ctx.set(ctx).ok().unwrap();

        let ctx = self.ctx.get().unwrap();
        let synchro = &self.sync;

        //NOTE: THREADS ARE SPAWNED AND BLOCK AT START OF LOOP

        println!("[TEST]:SPAWNING THREADS");
        let threads: Vec<_> = (0..N_THREADS)
            .map(|thread_id| spawn_thread(ctx, synchro, thread_id))
            .collect();

        //NOTE: RUN CTX INIT, THEN UNBLOCK THE THREADS TO START WORKING

        println!("[TEST]:INITIALIZING CONTEXT STATE");
        ctx.commence().unwrap();
        self.sync.reset();
        println!("[TEST]:THREADS UNBLOCKED!");

        self.threads = threads;
        self.win = Some(window);
    }

    fn about_to_wait(&mut self, event_loop: &ActiveEventLoop) {
        self.try_advance_frame();
    }

    fn window_event(
        &mut self,
        event_loop: &winit::event_loop::ActiveEventLoop,
        window_id: winit::window::WindowId,
        event: winit::event::WindowEvent,
    ) {
        match event {
            WindowEvent::CloseRequested => {
                self.win = None;
                event_loop.exit();
            }
            WindowEvent::KeyboardInput {
                device_id,
                event,
                is_synthetic,
            } => {
                let ctx: &T = self.ctx.get().unwrap();

                if event.physical_key == KeyCode::Space {
                    self.on_space.as_ref().map(|func| (func)(ctx).unwrap());
                }
                if event.physical_key == KeyCode::Enter {
                    self.on_enter.as_ref().map(|func| (func)(ctx).unwrap());
                }
            }

            _ => (),
        }
    }

    fn exiting(&mut self, event_loop: &winit::event_loop::ActiveEventLoop) {
        while !self.sync.check() {}
        self.sync.finish();
        self.threads.drain(..).try_for_each(|h| h.join()).unwrap();
        println!("finished everything");
        std::thread::sleep(std::time::Duration::from_millis(1000));
    }
}

lazy_static! {
    static ref NOT_IMPLEMENTED: EngineError = EngineError::default()
        .from_core()
        .with_specifics(999, "MOCK!!!");
}

fn spawn_thread<T: Module + 'static>(
    ctx: &Arc<T>,
    synchro: &Arc<Synchronizer>,
    thread_id: usize,
) -> JoinHandle<()> {
    let ctx = ctx.clone();
    let synchro = synchro.clone();
    std::thread::spawn(move || {
        while !synchro.slave_wait() {
            ctx.alter(thread_id).unwrap();
            ctx.process(thread_id, 16).unwrap();
        }
        println!("exited loop!!!!");
    })
}

trait Mock {
    fn mock(display_handle: RawDisplayHandle, window_handle: RawWindowHandle) -> Self;

    fn with_sig_finder(self, func: fn(&SignalID) -> Option<Signal>) -> Self;
}

impl Mock for cmoon_commons::CoreHandle {
    fn mock(display_handle: RawDisplayHandle, window_handle: RawWindowHandle) -> Self {
        let signal_emitter = |_: SignalID, _: Signal| {
            println!("EMITTING SIGNAL!");
        };
        let signal_searcher = |_: &SignalID| None;

        let partial_creator = |info: CreateInfo| Err(NOT_IMPLEMENTED.clone());
        let partial_destroyer = |_: GlobalAdress| {
            Err(EngineError::default()
                .from_core()
                .with_specifics(999, "TEST CORE"))
        };

        let instantiator = |info: InstantiateInfo| Err(NOT_IMPLEMENTED.clone());

        let eliminator = |_: &str| Err(NOT_IMPLEMENTED.clone());

        let transform_getter = |_: &str| Some(Transform::default());
        let transform_setter = |_: &str, _: Transform| Ok(());

        Self {
            display_handle,
            window_handle,

            signal_emitter,
            signal_searcher,

            partial_creator,
            partial_destroyer,

            instantiator,
            eliminator,

            transform_getter,
            transform_setter,
        }
    }

    fn with_sig_finder(mut self, func: fn(&SignalID) -> Option<Signal>) -> Self {
        self.signal_searcher = func;
        self
    }
}

use serde::{Deserialize, Serialize};
use std::{collections::hash_map::HashMap, fs::File, io::Read, path::Path, str::FromStr};

use anyhow::{Context, Result};

use cmoon_commons::*;

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct Blueprint {
    pub parametrics: Vec<(String, String)>, //this is a name/type pair
    pub statics: HashMap<String, Property>, //these are type metadata
    pub behaviours: Vec<BehaviourMeta>,     //these are assembly/type pairs
    pub children: Vec<Relation>,            //these are db values
}

impl Blueprint {
    pub fn from_file(path: &Path) -> Result<Self> {
        let mut file = File::open(path)?;
        let mut json = String::new();
        file.read_to_string(&mut json)?;
        Ok(serde_json::from_str(&json)?)
    }
}

impl Blueprint {
    pub fn with_behaviours<I: IntoIterator<Item = BehaviourMeta>>(mut self, values: I) -> Self {
        self.behaviours = values.into_iter().collect();
        self
    }
    pub fn with_statics<I: IntoIterator<Item = (String, Property)>>(mut self, values: I) -> Self {
        self.statics = values.into_iter().collect();
        self
    }
    pub fn with_children<I: IntoIterator<Item = Relation>>(mut self, values: I) -> Self {
        self.children = values.into_iter().collect();
        self
    }
}

///This is conceptually an entry on the hierarchy table
#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct Relation {
    pub handle: String,
    pub child_id: String,
    pub transform: Transform,
}

impl FromStr for Relation {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (handle, remain) = s.split_once(':').context("no ':' found")?;
        let (child_id, remain) = remain.split_once('@').context("no '@' found")?;
        let coords: Vec<_> = remain.split('/').collect();
        let x: f32 = coords
            .get(0)
            .and_then(|s| s.parse().ok())
            .unwrap_or_default();
        let y = coords
            .get(1)
            .and_then(|s| s.parse().ok())
            .unwrap_or_default();
        let z = coords
            .get(2)
            .and_then(|s| s.parse().ok())
            .unwrap_or_default();
        let transform = Transform { pos: [x, y, z] };
        Ok(Self {
            handle: handle.to_string(),
            child_id: child_id.to_string(),
            transform,
        })
    }
}

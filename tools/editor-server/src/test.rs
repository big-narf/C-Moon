use std::path::PathBuf;

use dirs::home_dir;
use mlua::prelude::*;

use crate::importer::{FileImporter, LuaFile, LuaPattern};

#[test]
fn import_test() {
    let root = home_dir().unwrap().join("C-Moon/tools/editor-server/");
    let importer = FileImporter::new(root).unwrap();

    importer.enumerate_importers().unwrap();

    let f1 = "TestDir/something/aux.a.test";

    println!("F1 IS :{}", f1.to_string());

    //let x = importer.import(f1).unwrap();
}

//#[test]
fn match_test() {
    let root = home_dir().unwrap().join("C-Moon/tools/editor-server/");
    let res_folder = root.join("standard/");
    let importers = root.join("importers/");
    let lua = Lua::new();

    let path = PathBuf::from("TestDir/something/aux.a.test");

    let mut f1 = LuaFile::new(res_folder, path);

    let p1 = LuaPattern::new_external("TestDir/.*%.test$", false, &lua).unwrap();
    let p2 = LuaPattern::new_external("test", false, &lua).unwrap();

    let x1 = p1.find(&mut f1);
    println!("INDEX IS :{x1:?}");
}

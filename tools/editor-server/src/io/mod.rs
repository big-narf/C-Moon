use std::env;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::io::{stdin, BufRead, BufReader, Write};
use std::path::{Path, PathBuf};
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;
use std::sync::{Arc, Mutex};
use std::thread::JoinHandle;

use anyhow::Result;
use clap::ArgAction;
use clap::ArgGroup;
use clap::ValueHint;
use console::{style, StyledObject};
use gettext::Catalog;
use lazy_static::lazy_static;

use crate::commands;

//commons args types
lazy_static! {
    static ref LOCAL_PATH: clap::Arg = clap::Arg::new("local_path")
        .required(true)
        .value_hint(ValueHint::FilePath);
    static ref PROJECT_PATH: clap::Arg = clap::Arg::new("project_path")
        .required(true)
        .value_hint(ValueHint::DirPath);
}

lazy_static! {
    pub static ref CLI_COMMANDS: clap::Command = clap::Command::new("editor")
        .subcommand(
            clap::Command::new("new")
                .short_flag('n')
                .long_flag("new")
                .arg(PROJECT_PATH.clone())
                .arg(
                    clap::Arg::new("empty")
                        .short('e')
                        .action(ArgAction::SetTrue)
                )
                .arg(
                    clap::Arg::new("no-config")
                        .long("no-config")
                        .action(ArgAction::SetTrue)
                )
                .arg(clap::Arg::new("open").short('o'))
        )
        .subcommand(
            clap::Command::new("open")
                .short_flag('o')
                .long_flag("open")
                .arg(PROJECT_PATH.clone())
        );
}
//editor commands
lazy_static! {
    pub static ref EDITOR_COMMANDS: clap::Command = {
        clap::Command::new("editor").subcommands([
            clap::Command::new("create").args([
                LOCAL_PATH.clone(),
                clap::Arg::new("import")
                    .short('i')
                    .action(ArgAction::SetTrue),
                clap::Arg::new("clone")
                    .require_equals(true)
                    .long("clone")
                    .short('C')
                    .value_hint(ValueHint::FilePath),
                clap::Arg::new("behaviours")
                    .long("behaviours")
                    .require_equals(true)
                    .value_delimiter(';'),
                clap::Arg::new("statics")
                    .long("statics")
                    .short('s')
                    .require_equals(true)
                    .value_delimiter(';'),
                clap::Arg::new("children")
                    .long("children")
                    .short('c')
                    .require_equals(true)
                    .value_delimiter(';'),
                clap::Arg::new("statics_json")
                    .short('S')
                    .long("statics-json")
                    .require_equals(true),
            ]),
            clap::Command::new("launch"),
            clap::Command::new("purge"),
            clap::Command::new("close"),
            clap::Command::new("import").args([
                LOCAL_PATH.clone().required(false).default_value(""),
                clap::Arg::new("force")
                    .short('f')
                    .action(ArgAction::SetTrue),
            ]),
            clap::Command::new("config").args([LOCAL_PATH.clone().default_value("")]),
        ])
    };
}
lazy_static! {
    pub static ref LANG_FOLDER: PathBuf = env::current_exe()
        .expect("FATAL ERROR: Couldn't get current executable location.")
        .parent()
        .expect("FATAL ERROR: Couldn't get current executable location.")
        .join("languages");
}

///Wrapper around bufreader that is smart
pub struct SmartReader {
    output_queue: Arc<Mutex<Vec<String>>>,
    close_tag: Arc<AtomicBool>,
    thread_handle: Option<JoinHandle<()>>,
}

impl SmartReader {
    pub fn new<T: Read + Send + 'static>(target: T) -> Self {
        let output_queue: Arc<Mutex<Vec<String>>> = Default::default();
        let close_tag: Arc<AtomicBool> = Default::default();

        let tag = close_tag.clone();
        let queue = output_queue.clone();
        let thread_handle = std::thread::spawn(move || {
            let mut reader = BufReader::new(target);
            let mut buffer = String::default();
            while !tag.load(Ordering::SeqCst) {
                buffer.clear();
                let r = reader.read_line(&mut buffer);
                match r {
                    Ok(_) => {
                        let mut lock = queue.lock().unwrap();
                        lock.push(buffer.trim_end().to_string());
                    }
                    Err(_) => (),
                }
            }
        })
        .into();
        Self {
            output_queue,
            close_tag,
            thread_handle,
        }
    }
    pub fn check_line(&self) -> Option<String> {
        let mut lock = self.output_queue.lock().unwrap();
        lock.pop()
    }
}

impl Drop for SmartReader {
    fn drop(&mut self) {
        self.close_tag.store(true, Ordering::SeqCst);
        self.thread_handle.take().unwrap().join().unwrap();
    }
}

#[derive(Debug, Clone)]
pub struct OutputHandler {
    catalog: Catalog,
    eng_info_header: StyledObject<String>,
    eng_error_header: StyledObject<String>,

    edit_info_header: StyledObject<String>,
    edit_error_header: StyledObject<String>,
}

impl Default for OutputHandler {
    fn default() -> Self {
        //gets system language
        let lang = env::var("LANG").unwrap_or("en_US.UTF-8".into());
        //let path=;
        let catalog = File::open(LANG_FOLDER.join(lang).with_extension(".mo"))
            .map_or(Ok(Catalog::empty()), |f| Catalog::parse(f))
            .unwrap_or(Catalog::empty());

        let eng_info_header = style("[ENGINE INFO]:".to_owned()).cyan().bold();
        let eng_error_header = style("[ENGINE ERROR]:".to_owned()).red().bold();
        let edit_info_header = style("[EDITOR INFO]:".to_owned()).cyan().bold();
        let edit_error_header = style("[EDITOR ERROR]:".to_owned()).red().bold();
        Self {
            catalog,
            eng_info_header,
            eng_error_header,
            edit_info_header,
            edit_error_header,
        }
    }
}

impl OutputHandler {
    pub fn editor_result(&self, r: Result<String>) {
        match r {
            Ok(s) => {
                self.editor_info(s);
            }
            Err(e) => {
                let s = e.to_string();
                self.editor_error(s);
            }
        }
    }

    pub fn stdout_result<S: ToString>(&self, r: Result<S>) {
        match r {
            Ok(s) => {
                self.engine_info(s.to_string());
            }
            Err(e) => {
                let s = e.to_string();
                self.editor_error(s);
            }
        }
    }

    pub fn stderr_result<S: ToString>(&self, r: Result<S>) {
        match r {
            Ok(s) => {
                self.engine_error(s.to_string());
            }
            Err(e) => {
                let s = e.to_string();
                self.editor_error(s);
            }
        }
    }

    pub fn editor_infos<I: IntoIterator<Item = String>>(&self, i: I) {
        i.into_iter().for_each(|s| self.editor_info(s))
    }
    pub fn editor_info(&self, info: String) {
        let content = style(self.catalog.gettext(&info)).cyan().italic();
        println!("{}{}", self.edit_info_header, content);
    }

    pub fn editor_error(&self, info: String) {
        let content = style(self.catalog.gettext(&info)).red().italic();
        eprintln!("{}{}", self.edit_error_header, content);
    }

    pub fn engine_info(&self, info: String) {
        let content = style(self.catalog.gettext(&info)).cyan().italic();
        println!("{}{}", self.eng_info_header, content);
    }
    pub fn engine_error(&self, info: String) {
        let content = style(self.catalog.gettext(&info)).red().italic();
        eprintln!("{}{}", self.eng_error_header, content);
    }
}

trait Input {
    fn get_cmds(&mut self) -> Result<Vec<String>>;
}

impl Input for env::Args {
    fn get_cmds(&mut self) -> Result<Vec<String>> {
        Ok(self.collect())
    }
}

///This is a wrapper for the command line arguments
#[derive(Default)]
struct InParser {
    buf: String,
}

impl Input for InParser {
    fn get_cmds(&mut self) -> Result<Vec<String>> {
        self.buf.clear();
        stdin().read_line(&mut self.buf)?;
        let cmds = self.buf.split_whitespace();
        let list = ["editor"]
            .into_iter()
            .chain(cmds)
            .map(|s| s.to_owned())
            .collect();
        Ok(list)
    }
}

///Struct to handle command chaining from inputs
pub struct InputHandler {
    commands: clap::Command,
    input: Box<dyn Input>,
    chain_slot: Vec<String>,
}

impl Default for InputHandler {
    fn default() -> Self {
        //let input = Box::from(stdin());
        let input = Box::from(env::args());
        Self {
            commands: CLI_COMMANDS.clone(),
            chain_slot: Default::default(),
            input,
        }
    }
}

impl InputHandler {
    pub fn as_editor_handler(mut self) -> Self {
        self.commands = EDITOR_COMMANDS.clone();
        self.input = Box::from(InParser::default());
        self
    }

    pub fn as_cli_handler(mut self) -> Self {
        self.commands = CLI_COMMANDS.clone();
        self
    }

    pub fn arguments(&mut self) -> Result<clap::ArgMatches> {
        let cmds = match self.chain_slot.pop() {
            Some(val) => val.split_whitespace().map(|s| s.to_owned()).collect(),
            None => self.input.get_cmds()?,
        };
        let matches = self.commands.clone().try_get_matches_from(cmds.clone())?;
        Ok(matches)
    }

    pub fn schedule_cmd(&mut self, cmds: &str) {
        self.chain_slot.push(cmds.to_string());
    }
}

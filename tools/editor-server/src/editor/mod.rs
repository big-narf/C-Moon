use std::collections::hash_map::HashMap;
use std::env;
use std::fs;
use std::io::{Read, Write};
use std::path::PathBuf;
use std::process::{Command as OSCommand, Stdio};
use std::sync::Arc;
use std::time::Duration;

use anyhow::*;
use lazy_static::lazy_static;

use cmoon_commons::*;

use crate::blueprint::*;
use crate::importer::*;
use crate::io::*;
use crate::misc::*;

lazy_static! {
    static ref ENGINE_PATH: PathBuf = {
        let mut path = env::current_exe()
            .expect("FATAL ERROR: Couldn't get current executable location.")
            .ancestors()
            .nth(2)
            .expect("FATAL ERROR: Couldn't get current executable location.")
            .join("engine/core");
        path.set_file_name("core");
        path
    };
}

pub struct Project {
    root: PathBuf,
    importer: FileImporter,

    out_handler: Arc<OutputHandler>,
    in_handler: InputHandler,
    close_flag: bool,
}

/*
* EDITOR LOOP FUNCTIONS
*/
impl Project {
    pub fn new(
        root: PathBuf,
        in_handler: InputHandler,
        out_handler: Arc<OutputHandler>,
    ) -> Result<Self> {
        let importer = FileImporter::new(root.clone())?;

        //displays loaded importers
        out_handler.editor_infos(importer.enumerate_importers()?);

        let in_handler = in_handler.as_editor_handler();

        Ok(Self {
            root,
            importer,
            out_handler,
            in_handler,

            close_flag: false,
        })
    }
}
impl Project {
    ///Gets the matches from stdin and routes to the corresponding functions. Prints info to stdout
    ///when the command returns.
    pub fn loop_commands(&mut self) -> Result<String> {
        while !self.close_flag {
            let res = {
                //gets argumentsrado
                let matches = self.in_handler.arguments().unwrap_or_default();
                //matches to command and return results as strings
                //NOTE: CHANGE THESE FUNCTIONS TO RETURN VECS OF FORMATED STRING IN THE FUTURE
                match matches.subcommand() {
                    Some(("launch", args)) => self.launch(args),
                    Some(("close", args)) => self.close(args),
                    Some(("create", args)) => self.create(args),
                    Some(("purge", args)) => self.purge(args),
                    Some(("import", args)) => self.import(args),
                    Some(("config", args)) => self.config(args),
                    _ => Err(anyhow!("Unrecognized command: !")),
                }
            };
            self.out_handler.editor_result(res);
        }
        println!("stuff is fucked here");
        Ok("Editor state exited succesefully".to_owned())
    }
}

/**
* TOP LEVEL COMMANDS
*/
impl Project {
    fn launch(&mut self, _: &clap::ArgMatches) -> Result<String> {
        let engine = ENGINE_PATH.to_owned();
        let proj = self.root.join("assets.db");
        println!("Path to Engine is :{engine:?}");
        println!("Path to Project assets is :{proj:?}");
        let mut process = OSCommand::new(engine)
            .arg(proj)
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()?;

        let out_reader = SmartReader::new(process.stdout.take().unwrap());
        let err_reader = SmartReader::new(process.stderr.take().unwrap());

        loop {
            //println!("CHECKING STD_OUT");
            //checks output
            match out_reader.check_line() {
                Some(s) => {
                    self.out_handler.engine_info(s);
                }
                None => (),
            }

            //println!("CHECKING STD_ERR");
            //checks errors
            match err_reader.check_line() {
                Some(s) => {
                    self.out_handler.engine_error(s);
                }
                None => (),
            }

            //println!("Waiting!!!!!!");
            //checks if engine has exited
            match process.try_wait().unwrap() {
                Some(status) => break,
                None => (),
            }
        }

        Ok("Running engine".to_owned())
    }
    ///clears out everything from the assets database, keeps engine compatible schema
    fn purge(&mut self, _: &clap::ArgMatches) -> Result<String> {
        self.importer.purge()?;
        Ok(format!("Project database wiped clean!"))
    }

    fn close(&mut self, _: &clap::ArgMatches) -> Result<String> {
        self.close_flag = true;
        Ok("Closing editor".to_string())
    }
    ///creates a blueprint file at the designated relative path.
    fn create(&mut self, args: &clap::ArgMatches) -> Result<String> {
        let res_folder = self.root.clone().join("resources");
        let local_path = args
            .get_one::<String>("local_path")
            .context("No Local Path provided")?;
        let mut file_path = res_folder.clone().join(local_path);
        file_path.set_extension("blu");

        let mut blu = match args.get_one::<String>("clone") {
            Some(s) => {
                let src = res_folder.clone().join(s);
                Blueprint::from_file(&src)?
            }
            None => Blueprint::default(),
        };

        match args.get_many::<String>("behaviours") {
            Some(list) => {
                let list: Vec<_> = list.map(|s| s.parse()).collect::<Result<_, _>>()?;
                blu = blu.with_behaviours(list)
            }
            None => (),
        }

        match args.get_many::<String>("statics") {
            Some(list) => {
                let list: Vec<_> = list
                    .map(|s| {
                        let (name, value) = s.split_once(':').context("Couldn't split")?;
                        Ok((name.to_owned(), value.parse()?))
                    })
                    .collect::<Result<_, _>>()?;
                blu = blu.with_statics(list);
            }
            None => (),
        }

        match args.get_many::<String>("children") {
            Some(list) => {
                let list: Vec<_> = list
                    .map(|s| {
                        let rel = s.parse()?;
                        Ok(rel)
                    })
                    .collect::<Result<_, _>>()?;
                blu = blu.with_children(list);
            }
            None => (),
        }

        let stringified = serde_json::to_string_pretty(&blu)?;
        fs::File::create(file_path)?.write_all(stringified.as_bytes())?;

        if args.get_flag("import") {
            let import_cmd = format!("import {}", local_path);
            self.in_handler.schedule_cmd(&import_cmd);
        }
        Ok(format!("Blueprint created!"))
    }

    fn import(&mut self, args: &clap::ArgMatches) -> Result<String> {
        let local_path = args
            .get_one::<String>("local_path")
            .context("No Path provided!")?;

        let is_forced = args.get_flag("force");

        self.importer.import(local_path, is_forced)?;
        Ok(format!("Import Successful!"))
    }

    fn config(&mut self, args: &clap::ArgMatches) -> Result<String> {
        //path manipulation
        let local_path = args
            .get_one::<String>("local_path")
            .context("No Path provided!")?;
        let file_path = self.root.clone().join(local_path);
        self.importer.set_config(&file_path)?;
        Ok(format!(
            "Engine configurations set as file: :{:?}",
            file_path.file_name().context("no file name found!")?
        ))
    }
}

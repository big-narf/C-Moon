use std::env;
use std::fmt::{Debug, Display};
use std::fs;
use std::io::Write;
use std::path::PathBuf;
use std::str::FromStr;
use std::sync::Arc;
use std::vec::IntoIter;

use anyhow::{Context, Result};
use clap::ArgMatches;
use fs_extra::dir::CopyOptions;
use lazy_static::lazy_static;

use console::{style, StyledObject};

use fs_extra::dir;
use rusqlite::Connection;

use cmoon_commons::*;

use crate::editor;
use crate::io::{InputHandler, OutputHandler};

//miscellanious statics
lazy_static! {
    static ref IMPORTER_SOURCE: PathBuf = env::current_exe()
        .expect("FATAL ERROR: Couldn't get current executable location.")
        .parent()
        .expect("FATAL ERROR: Couldn't get current executable location.")
        .join("importers");
    static ref STDLIB_SOURCE: PathBuf = env::current_exe()
        .expect("FATAL ERROR: Couldn't get current executable location.")
        .parent()
        .expect("FATAL ERROR: Couldn't get current executable location.")
        .join("standard");
}

pub fn new(
    args: &ArgMatches,
    i_handler: InputHandler,
    o_handler: Arc<OutputHandler>,
) -> Result<String> {
    let raw_p = args
        .get_one::<String>("project_path")
        .context("No Path provided")?;
    let root = PathBuf::from_str(&raw_p)?;
    fs::create_dir(root.clone())?;
    fs::create_dir(root.clone().join("resources"))?;

    //tries to initialize project with standard objects
    if !args.get_flag("empty") {
        let copy_options = CopyOptions::new();

        //tries to copy standard library
        dir::copy(
            STDLIB_SOURCE.to_owned(),
            root.clone().join("resources"),
            &copy_options,
        )?;
        //tries to copy standard importers
        dir::copy(IMPORTER_SOURCE.to_owned(), root.clone(), &copy_options)?;
    }
    let db = Connection::open(root.clone().join("assets.db"))?;
    db.execute_batch(CORE_TABLE_DEFINITIONS)?;

    //tries to create standard config file for the engine
    if !args.get_flag("no-config") {
        let config = EngineConfig::default();

        let mut config_file = std::fs::File::create(root.clone().join("config.config"))?;
        let json = serde_json::to_string_pretty(&config).unwrap();
        config_file.write_all(json.as_bytes())?;
        set_config(&db, json)?;
    }

    Ok("Created project Successefully".to_owned())
}

pub fn open(
    args: &ArgMatches,
    i_handler: InputHandler,
    o_handler: Arc<OutputHandler>,
) -> Result<String> {
    let raw_p = args
        .get_one::<String>("project_path")
        .context("No Path provided")?;
    let root = PathBuf::from_str(&raw_p)?;
    let mut ctx = editor::Project::new(root, i_handler, o_handler)?;
    ctx.loop_commands()
}

pub fn set_config(db: &Connection, json: String) -> Result<()> {
    db.prepare(SET_ENGINE_CONFIG)?
        .execute(rusqlite::named_params! {":config":json})?;
    Ok(())
}

pub trait AsOutput<T: Display, E: Debug> {
    ///Converts command results into formated objects
    fn as_output(self) -> Vec<StyledObject<String>>;
}

impl<T: Display, E: Debug> AsOutput<T, E> for Result<T, E> {
    fn as_output(self) -> Vec<StyledObject<String>> {
        vec![match self {
            Ok(val) => style(format!("{val}")).green(),
            Err(val) => style(format!("{val:?}")).red(),
        }]
    }
}

impl<T: Display, E: Debug, O: AsOutput<T, E>> AsOutput<T, E> for IntoIter<O> {
    fn as_output(self) -> Vec<StyledObject<String>> {
        self.flat_map(|val| val.as_output()).collect()
    }
}

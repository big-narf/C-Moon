use std::collections::HashMap;
use std::fs;
use std::io::Read;
use std::ops::Deref;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::sync::Arc;

use mlua::BString;
use mlua::{prelude::*, Variadic};

//use cmoon_commons::*;

use crate::io::OutputHandler;
use crate::misc::*;

use super::*;

pub(super) fn prepared_lua() -> LuaResult<Lua> {
    let lua = Lua::new_with(
        LuaStdLib::ALL_SAFE,
        LuaOptions::default().catch_rust_panics(false),
    )?;

    lua.globals()
        .set("to_f32_bytes", LuaFunction::wrap(to_f32_bytes))?;

    lua.globals()
        .set("to_u32_bytes", LuaFunction::wrap(to_u32_bytes))?;

    Ok(lua)
}

pub enum IntFmt {
    U8(bool),
    I8(bool),
    U16(bool),
    I16(bool),
    U32(bool),
    I32(bool),
    U64(bool),
    I64(bool),
}

macro_rules! order_bytes {
    ($value:ident,$format:ty,$is_little:expr) => {
        if $is_little {
            ($value as $format).to_le_bytes()
        } else {
            ($value as $format).to_be_bytes()
        }
        .to_vec()
    };
}

impl IntFmt {
    fn as_bytes(&self, value: LuaInteger) -> Vec<u8> {
        match self {
            Self::U8(is_little) => order_bytes!(value, u8, *is_little),
            Self::I8(is_little) => order_bytes!(value, i8, *is_little),
            Self::U16(is_little) => order_bytes!(value, u16, *is_little),
            Self::I16(is_little) => order_bytes!(value, i16, *is_little),
            Self::U32(is_little) => order_bytes!(value, u32, *is_little),
            Self::I32(is_little) => order_bytes!(value, i32, *is_little),
            Self::U64(is_little) => order_bytes!(value, u64, *is_little),
            Self::I64(is_little) => order_bytes!(value, i64, *is_little),
        }
    }
}

pub enum NumFmt {
    F32(bool),
    F64(bool),
}

pub enum StrFmt {
    FixedStr(usize),
}

pub enum ValueFmt {
    Integer(IntFmt),
    Number(NumFmt),
    String(StrFmt),
}

pub struct TokenList(Vec<ValueFmt>);

enum LuaPackable {
    Integer(LuaInteger),
    Number(LuaNumber),
}
impl FromLua for LuaPackable {
    fn from_lua(value: LuaValue, lua: &Lua) -> LuaResult<Self> {
        match value {
            LuaValue::Integer(i) => Ok(Self::Integer(i)),
            LuaValue::Number(n) => Ok(Self::Number(n)),
            _ => unreachable!(),
        }
    }
}

fn thing(tokens: Vec<ValueFmt>, values: Vec<LuaPackable>) {
    let output: Vec<u8> = Iterator::zip(tokens.into_iter(), values.into_iter())
        .flat_map(|(token, value)| match (token, value) {
            (ValueFmt::Integer(format), LuaPackable::Integer(i)) => format.as_bytes(i),
            _ => unreachable!(),
        })
        .collect();
}

/*
impl FromLua for ByteFmt {
    fn from_lua(value: LuaValue, lua: &Lua) -> LuaResult<Self> {
        let str = value.as_str().lua_context("pattern is not a string")?;
        str.parse()
    }
}

impl FromStr for ByteFmt {
    type Err = LuaError;
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let out = Vec::new();
        //s.chars().
    }
}


///Impls more or less conformant string.un/pack fuctions
pub fn lua_pack(fmt: ByteFmt, values: Variadic<LuaPackable>) {
    let mut buffer = Vec::new();
    let values = values.into_iter();
    fmt.0
        .into_iter()
        .try_for_each(|(big_end, v_fmt)| {
            let val = values.next().lua_context("not enough values")?;
            Ok(())
        })
        .unwrap();
}
pub fn lua_unpack(fmt: LuaString, bytes: BString) {}*/

///Convert reasonable lua values to a byteslice of given numeric representation
pub fn to_u32_bytes(table: LuaTable) -> LuaResult<BString> {
    let v: LuaResult<Vec<_>> = table
        .sequence_values::<LuaEither<LuaNumber, LuaInteger>>()
        .collect();

    Ok(v?
        .into_iter()
        .flat_map(|either| {
            match either {
                LuaEither::Left(f) => f as u32,
                LuaEither::Right(i) => i as u32,
            }
            .to_le_bytes()
        })
        .collect())
}

///Convert reasonable lua values to a byteslice of given numeric representation
pub fn to_f32_bytes(table: LuaTable) -> LuaResult<BString> {
    let v: LuaResult<Vec<_>> = table
        .sequence_values::<LuaEither<LuaNumber, LuaInteger>>()
        .collect();

    Ok(v?
        .into_iter()
        .flat_map(|either| {
            match either {
                LuaEither::Left(f) => f as f32,
                LuaEither::Right(i) => i as f32,
            }
            .to_le_bytes()
        })
        .collect())
}

#[derive(Clone)]
pub(super) struct ImportAction {
    pub(super) is_forced: bool,
    pub(super) outputter: indicatif::ProgressBar,
    pub(super) database: Arc<Mutex<Connection>>,
    pub(super) importer: LuaTable,
    pub(super) matched_list: LuaTable,  // Map<Path,Vec<Pattern>>
    pub(super) imported_list: LuaTable, // Map<Path,bool>
}

impl ImportAction {
    ///returns the result of a single import action
    pub fn import(
        &self,
        lua: &Lua,
        src: LuaUserDataRef<LuaFile>,
    ) -> LuaResult<Option<ImportTable>> {
        self.outputter
            .set_prefix(format!("[IMPORTING: {}]", src.to_string()));

        let file_path = src
            .rel_path
            .to_str()
            .lua_context("file path is not valid")?;

        if self.is_imported(&src)? {
            Ok(None)
        } else {
            //NOTE: THIS IS FUCKING ASS, PROLLY NEED TO REFACTOR
            let used_matches: LuaTable = self.used_matches(file_path).and_then(|op| match op {
                Some(v) => Ok(v),
                None => lua.create_table(),
            })?;
            self.importer.best_match(&src, used_matches).map_or_else(
                || Ok(None),
                |func| {
                    let imports = func.call(src.clone())?;

                    self.imported_list.set(file_path, true)?;
                    Ok(Some(imports))
                },
            )
        }
    }

    pub fn imported_files(&self) -> LuaResult<Vec<String>> {
        self.imported_list
            .pairs::<String, bool>()
            .map(|r| r.map(|(k, v)| k))
            .collect()
    }

    ///checks if the current file has already been imported
    fn is_imported(&self, src: &LuaFile) -> LuaResult<bool> {
        let rel = src.rel_path.to_str().lua_context("path is shit")?;
        let in_db = self
            .database
            .lock()
            .unwrap()
            .check_imported(&src.root_path, rel)
            .into_lua_err()?;
        let in_mem = self.imported_list.contains_key(rel)?;
        if self.is_forced {
            Ok(in_mem)
        } else {
            Ok(in_db || in_mem)
        }
    }

    fn used_matches(&self, rel_path: &str) -> LuaResult<Option<LuaTable>> {
        self.matched_list.raw_get(rel_path)
    }
}

impl Drop for ImportAction {
    fn drop(&mut self) {
        let n_files = self.imported_list.raw_len();
        self.outputter
            .finish_with_message(format!("Imported {n_files} files"));
    }
}

trait BestMatch {
    fn best_match(
        &self,
        file: &LuaFile,
        excluding: LuaTable,
    ) -> Option<LuaUserDataRef<ImportFunction>>;
}

impl BestMatch for LuaTable {
    ///Matches the best match to the file path that is not on the exclusion list.
    fn best_match(
        &self,
        file: &LuaFile,
        excluding: LuaTable,
    ) -> Option<LuaUserDataRef<ImportFunction>> {
        self.pairs::<LuaUserDataRef<LuaPattern>, LuaUserDataRef<ImportFunction>>()
            .filter_map(|pair| {
                let (p, func) = pair.ok()?;
                if excluding.contains_key(&p.pattern).ok()? {
                    None
                } else {
                    let (len, _, _) = p.find(file)?;
                    Some((len, p, func))
                }
            })
            .max_by(|p1, p2| p1.0.cmp(&p2.0))
            .and_then(|(n, p, f)| {
                excluding.set(p.pattern.clone(), true).ok()?;
                //println!("BEST MATCH IS: {}", p.pattern.to_string_lossy());
                Some(f)
            })
    }
}

///Wrapper around the lua function and relevant meta-data
#[derive(Debug)]
pub(super) struct ImportFunction {
    import_fn: LuaFunction, //returns a table [obj_name]=obj_data
    scheme: ImportScheme,
}

impl ImportFunction {
    pub fn new(import_fn: LuaFunction, scheme: ImportScheme) -> Self {
        Self { import_fn, scheme }
    }

    /// Returns all the imports for the given function
    pub fn call(&self, file: LuaFile) -> LuaResult<ImportTable> {
        let import_list: HashMap<String, LuaValue> = self.import_fn.call(file)?;
        import_list
            .into_iter()
            .map(|(name, val)| {
                let obj = self.scheme.validate(&val)?;
                Ok((name, obj))
            })
            .collect()
    }
}

impl LuaUserData for ImportFunction {}

#[derive(Debug)]
pub(super) enum ImportScheme {
    Deferred,
    Assembly(String),
    Blueprint,
    SimpleAsset(SchemeField),
    ComplexAsset(HashMap<String, SchemeField>),
}
impl FromStr for ImportScheme {
    type Err = LuaError;
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "deferred" => Ok(Self::Deferred),
            val if val.starts_with("assembly") => {
                let lang = val
                    .split_whitespace()
                    .nth(1)
                    .lua_context("No language specified")?;
                Ok(Self::Assembly(lang.to_owned()))
            }
            "blueprint" => Ok(Self::Blueprint),
            val if val.starts_with("asset") => {
                let field = val
                    .split_whitespace()
                    .nth(1)
                    .lua_context("no asset type specified")?
                    .parse()?;
                Ok(Self::SimpleAsset(field))
            }
            _ => Err(LuaError::runtime("some stuff went down")),
        }
    }
}

impl FromLua for ImportScheme {
    fn from_lua(value: LuaValue, lua: &Lua) -> LuaResult<Self> {
        match value {
            LuaValue::Nil => Ok(ImportScheme::Deferred),
            LuaValue::String(s) => Ok(s.to_str()?.parse()?),
            LuaValue::Table(t) => {
                let map: LuaResult<HashMap<_, _>> = t.pairs::<String, SchemeField>().collect();
                Ok(ImportScheme::ComplexAsset(map?))
            }
            _ => Err(LuaError::FromLuaConversionError {
                from: "some shit",
                to: "import scheme".to_owned(),
                message: None,
            }),
        }
    }
}

#[derive(Debug)]
pub(super) enum SchemeField {
    Number,
    Text,
    CompressedText,
    Data,
    CompressedData,
}

impl FromStr for SchemeField {
    type Err = LuaError;
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        match s {
            "number" => Ok(Self::Number),
            "text" => Ok(Self::Text),
            "compressed_text" => Ok(Self::CompressedText),
            "data" => Ok(Self::Data),
            "compressed_data" => Ok(Self::CompressedData),
            _ => Err(LuaError::FromLuaConversionError {
                from: "bad string",
                to: "scheme field".to_owned(),
                message: Some(format!("STRING IS :{s}")),
            }),
        }
    }
}

impl FromLua for SchemeField {
    fn from_lua(value: LuaValue, lua: &Lua) -> LuaResult<Self> {
        let val = value
            .as_str()
            .lua_context("scheme field is not a string!")?;
        val.parse()
    }
}

impl SchemeField {
    fn validate(&self, val: &LuaValue) -> LuaResult<AssetField> {
        Ok(match self {
            Self::Number => {
                let number = val
                    .as_f64()
                    .or_else(|| val.as_i64().map(|i| i as f64))
                    .lua_context("FUCK ISNT A NUMBER")?;
                AssetField::Number(number)
            }
            Self::Text => AssetField::Text(val.as_str().lua_context("FUCK ISNT TXT")?.to_string()),
            Self::CompressedText => AssetField::CompressedText(
                val.as_str().lua_context("FUCK ISNT STRIGN")?.to_string(),
            ),
            Self::Data => AssetField::Data(
                val.as_string()
                    .lua_context("DATA ISNT STRING")?
                    .as_bytes()
                    .to_owned(),
            ),
            Self::CompressedData => AssetField::CompressedData(
                val.as_string()
                    .lua_context("ZE COMPRRRRREZZED DATUM IZT ZNOT A ZTRING")?
                    .as_bytes()
                    .to_owned(),
            ),
        })
    }
}

impl ImportScheme {
    pub fn validate(&self, val: &LuaValue) -> LuaResult<ImportObj> {
        Ok(match self {
            Self::Deferred => val.as_userdata().lua_context("error")?.take()?,
            Self::Assembly(lang) => ImportObj::Assembly {
                lang: lang.to_owned(),
                code: val
                    .as_str()
                    .lua_context("THIS SHIT AINT A STRING BROTHER!!!!!!!")?
                    .to_string(),
            },
            Self::Blueprint => ImportObj::Blueprint(
                val.as_str()
                    .lua_context("THIS BLUEPRINT ISNT A FUCKING STRING, WTF?")?
                    .to_string(),
            ),
            Self::SimpleAsset(field) => ImportObj::Asset(Asset::Simple(field.validate(val)?)),
            Self::ComplexAsset(scheme) => {
                let t = val
                    .as_table()
                    .lua_context("why in fuckles is this not a table?????????")?;
                let fields: LuaResult<_> = scheme
                    .iter()
                    .map(|(name, field)| {
                        let val: LuaValue = t.get(name.deref())?;
                        Ok((name.to_owned(), field.validate(&val)?))
                    })
                    .collect();
                ImportObj::Asset(Asset::Complex(fields?))
            }
        })
    }
}

impl LuaUserData for ImportObj {}

#[derive(Debug)]
pub struct LuaPattern {
    pattern: LuaString,
    find_fn: LuaFunction,
    is_recursive: bool,
}

impl LuaPattern {
    pub fn new(pattern: LuaString, is_recursive: bool, lua: &Lua) -> LuaResult<Self> {
        let find_fn = lua.globals().get::<LuaTable>("string")?.get("find")?;
        Ok(Self::new_from_parts(pattern, find_fn, is_recursive))
    }

    pub fn new_from_parts(pattern: LuaString, find_fn: LuaFunction, is_recursive: bool) -> Self {
        Self {
            pattern,
            find_fn,
            is_recursive,
        }
    }

    pub fn new_external<S: ToString>(pattern: S, is_recursive: bool, lua: &Lua) -> LuaResult<Self> {
        let find_fn = lua.globals().get::<LuaTable>("string")?.get("find")?;
        let pattern = pattern
            .to_string()
            .into_lua(lua)?
            .as_string()
            .lua_context("idk")?
            .clone();
        Ok(Self::new_from_parts(pattern, find_fn, is_recursive))
    }

    /// Check if this pattern matches the path, returns the size of the pattern, and the indexes of
    /// start and finish of the match.
    pub fn find(&self, file: &LuaFile) -> Option<(usize, usize, usize)> {
        let (i, j) = self
            .find_fn
            .call::<(usize, usize)>((file.rel_path.clone(), self.pattern.clone()))
            .ok()?;
        let len = self.pattern.to_str().ok()?.len();
        Some((len, i, j))
    }
}

impl ToString for LuaPattern {
    fn to_string(&self) -> String {
        self.pattern.to_string_lossy()
    }
}

impl LuaUserData for LuaPattern {}

trait LuaContext<T> {
    fn lua_context<S: Into<String>>(self, ctx: S) -> LuaResult<T>;
}

impl<T> LuaContext<T> for Option<T> {
    fn lua_context<S: Into<String>>(self, ctx: S) -> LuaResult<T> {
        match self {
            Some(val) => Ok(val),
            None => Err(LuaError::RuntimeError(ctx.into())),
        }
    }
}

impl<T> LuaContext<T> for LuaResult<T> {
    fn lua_context<S: Into<String>>(self, ctx: S) -> LuaResult<T> {
        match self {
            Ok(val) => Ok(val),
            Err(e) => Err(e.context(ctx.into())),
        }
    }
}

trait AsRelative {
    fn as_relative(&self, root: &Path) -> Option<&Path>;
}

impl AsRelative for Path {
    fn as_relative(&self, root: &Path) -> Option<&Path> {
        self.strip_prefix(root).ok()
    }
}
#[derive(Debug, Clone)]
pub struct LuaFile {
    root_path: PathBuf,
    rel_path: PathBuf,
}

impl LuaFile {
    pub fn new<P: Into<PathBuf>, S: Into<PathBuf>>(root: P, rel_path: S) -> Self {
        Self {
            root_path: root.into(),
            rel_path: rel_path.into(),
        }
    }

    fn path(&self) -> PathBuf {
        self.root_path.join(&self.rel_path)
    }

    fn child(&self, name: &str) -> LuaResult<Self> {
        let mut v = self.clone();
        v.rel_path = v.rel_path.join(name);
        Ok(v)
    }

    pub fn contents(&self) -> LuaResult<Option<Vec<Self>>> {
        let path = &self.path();
        Ok(if !path.is_dir() {
            None
        } else {
            let dir = fs::read_dir(path)?;
            let list: Vec<_> = dir
                .filter_map(|e| {
                    let entry = e.ok()?;
                    let rel_path = entry.path().as_relative(&self.root_path)?.to_path_buf();
                    let child = Self {
                        root_path: self.root_path.clone(),
                        rel_path,
                    };
                    Some(child)
                })
                .collect();
            Some(list)
        })
    }

    pub fn parent(&self) -> LuaResult<Self> {
        let path = self.path();
        let path = path
            .parent()
            .and_then(|p| p.as_relative(&self.root_path))
            .lua_context("Error occurred trying to get parent")?;
        let parent = Self {
            root_path: self.root_path.clone(),
            rel_path: path.to_owned(),
        };
        Ok(parent)
    }

    ///reads of the file to a lua string that may or may not be utf8.
    pub fn read_all(&self) -> LuaResult<BString> {
        let mut buffer: Vec<u8> = Default::default();
        let mut file = fs::File::open(&self.path()).into_lua_err()?;
        file.read_to_end(&mut buffer).into_lua_err()?;
        Ok(buffer.into())
    }
}

impl ToString for LuaFile {
    fn to_string(&self) -> String {
        self.path().to_string_lossy().to_string()
    }
}

impl LuaUserData for LuaFile {
    fn add_methods<M: LuaUserDataMethods<Self>>(methods: &mut M) {
        methods.add_meta_method(LuaMetaMethod::ToString, |_, this, ()| Ok(this.to_string()));

        methods.add_method("text", |_, this, ()| this.read_all());

        methods.add_method("parent", |_, this, ()| this.parent());

        methods.add_method("child", |_, this, val: LuaString| {
            this.child(&val.to_str()?)
        });
        methods.add_method("contents", |_, this, ()| this.contents());

        //meta data methods
        methods.add_method("name", |_, this, ()| {
            let name = this
                .path()
                .main_name()
                .lua_context("No main name found!")?
                .to_owned();
            Ok(name)
        });

        methods.add_method("file_name", |_, this, ()| {
            this.rel_path
                .file_name()
                .map(|s| s.to_owned())
                .lua_context("couldnt get file name!!!")
        });
        methods.add_method("extension", |_, this, ()| {
            let name: Suffix = this
                .path()
                .suffix()
                .lua_context("No main name found!")?
                .into();
            Ok(name)
        });
    }
}

impl LuaUserData for Suffix {
    fn add_methods<M: LuaUserDataMethods<Self>>(methods: &mut M) {
        methods.add_method("full", |_, this, ()| Ok(this.full()));
        methods.add_method("identifier", |_, this, ()| {
            let ident = this
                .identifier()
                .lua_context("Couldn't get main identifier of extension!")?;
            Ok(ident)
        });
    }
}

#[derive(PartialEq, Eq, Hash, Clone, Debug)]
pub enum ImportType {
    Blueprint,
    Resource(Option<String>),
    Assembly(String),
}

impl FromStr for ImportType {
    type Err = LuaError;
    fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
        let mut parts = s.split_whitespace();
        match parts.next().lua_context("Import type is empty string!!!")? {
            "Blueprint" => Ok(Self::Blueprint),
            "Resource" => {
                let format = parts.next().map(|s| s.to_string());
                Ok(Self::Resource(format))
            }

            "Assembly" => {
                let lang = parts
                    .next()
                    .lua_context("Import error: Trying to import assembly without language tag")?;
                Ok(Self::Assembly(lang.to_owned()))
            }
            _ => Err(LuaError::FromLuaConversionError {
                from: "string",
                to: "ImportType".to_owned(),
                message: Some("Invalid type code".to_owned()),
            }),
        }
    }
}

impl FromLua for ImportType {
    fn from_lua(value: LuaValue, lua: &Lua) -> LuaResult<Self> {
        match value {
            LuaValue::String(s) => s.to_str()?.parse(),
            _ => Err(LuaError::FromLuaConversionError {
                from: "Not a string",
                to: "ImportType".to_owned(),
                message: Some("Invalid type code".to_owned()),
            }),
        }
    }
}
///this is an output value from the lua importer, it informs where the importer will put the raw
///byte data inside the database
#[derive(PartialEq, Eq, Hash)]
pub struct MetaData {
    pub src_file: String,
    pub name: String,
    pub import_type: ImportType,
}
impl FromLua for MetaData {
    fn from_lua(value: LuaValue, lua: &Lua) -> LuaResult<Self> {
        match value {
            LuaValue::Table(table) => {
                let name = table.get("name").lua_context(" name field not found")?;
                let import_type = table
                    .get("import_type")
                    .lua_context("import_type field not found")?;
                let src_file = table.raw_get("file")?;
                Ok(Self {
                    name,
                    src_file,
                    import_type,
                })
            }
            _ => Err(LuaError::FromLuaConversionError {
                from: "Not a Table",
                to: "MetaData".to_owned(),
                message: Some("Meta data must be create from lua table only".to_owned()),
            }),
        }
    }
}

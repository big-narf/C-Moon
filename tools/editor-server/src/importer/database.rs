use std::collections::HashMap;
use std::fs;
use std::path::Path;
use std::str::FromStr;
use std::time::SystemTime;

use anyhow::Context;
use anyhow::Result;
use chrono::DateTime;
use chrono::Local;
use chrono::NaiveDateTime;
use rusqlite::*;

use cmoon_commons::*;

use crate::blueprint::*;
use crate::misc::*;

use super::*;

pub trait DBExt {
    ///Checks if the last imported time is after last file modification
    fn check_imported(&self, root: &Path, local_path: &str) -> Result<bool>;
    ///High level db push function

    fn push_artifacts<I: IntoIterator<Item = (String, ImportObj)>>(
        &self,
        mut list: I,
    ) -> Result<()> {
        list.into_iter()
            .try_for_each(|(k, v)| self.push_artifact(k, v))
    }
    fn push_artifact(&self, name: String, obj: ImportObj) -> Result<()>;

    fn register_imports(&self, files: Vec<String>) -> Result<()>;

    fn purge(&self) -> Result<()>;
}

impl DBExt for Connection {
    /// Returns true if it has already been imported since the last modification on de OS level.
    fn check_imported(&self, root: &Path, local_path: &str) -> Result<bool> {
        //NOTE: CHECKS IF LAST MODIFIED IS ALREADY LAST IMPORTED
        let mut stmt = self.prepare_cached(CHECK_LAST_IMPORT)?;
        let mut rows = stmt.query(named_params! {":path":local_path})?;
        let entry = rows.next()?;

        match entry {
            Some(row) => {
                let db_date: DateTime<Local> = row
                    .get_ref("LastImported")?
                    .as_str()
                    .context("some shit")?
                    .parse()?;
                let file_date: DateTime<Local> =
                    std::fs::metadata(root.join(local_path))?.modified()?.into();
                Ok(db_date > file_date)
            }
            None => Ok(false),
        }
    }

    fn push_artifact(&self, name: String, obj: ImportObj) -> Result<()> {
        match obj {
            ImportObj::Asset(data) => push_asset(self, name, data),
            ImportObj::Blueprint(json) => push_blueprint(self, name, json),
            ImportObj::Assembly { lang, code } => push_assembly(self, name, lang, code),
        }
    }

    fn register_imports(&self, files: Vec<String>) -> Result<()> {
        let dtime = Local::now().to_string();
        let mut stmt = self.prepare_cached(REGISTER_IMPORT)?;
        files.into_iter().try_for_each(|file| {
            stmt.execute(named_params! {":path":file,":dtime":dtime})?;
            Ok(())
        })
    }

    fn purge(&self) -> Result<()> {
        let mut stmt = self.prepare("SELECT name FROM sqlite_master WHERE type='table'")?;
        let mut rows = stmt.query(())?;
        let mut names: Vec<String> = Vec::new();
        while let Some(row) = rows.next()? {
            names.push(row.get(0)?);
        }
        for name in names {
            self.execute(format!("DELETE FROM {}", name).as_str(), ())?;
        }
        Ok(())
    }
}

fn push_blueprint(db: &Connection, name: String, json: String) -> Result<()> {
    let blu: Blueprint = serde_json::from_str(&json)?;
    db.prepare_cached(IMPORT_BLUEPRINT)?
        .execute(named_params! {
            ":name": name,
            ":statics":serde_json::to_string(&blu.statics)?,
            ":parametrics":serde_json::to_string(&blu.parametrics)?,
            ":behaviours":serde_json::to_string(&blu.behaviours)?,
        })?;

    let mut stmt = db.prepare_cached(IMPORT_RELATIONS)?;
    blu.children.iter().try_for_each(
        |Relation {
             handle,
             child_id,
             transform,
         }|
         -> Result<()> {
            let transform = serde_json::to_string(&transform)?;
            stmt.execute(named_params! {
                ":handle":handle,
                ":transform":transform,
                ":parent": name,
                ":child":child_id,
            })?;
            Ok(())
        },
    )?;
    Ok(())
}

fn push_asset(db: &Connection, name: String, asset: Asset) -> Result<()> {
    let mut stmt = db.prepare_cached(IMPORT_RESOURCE)?;
    let bytes = asset.to_bytes();
    //println!("BYTES ARE:");
    //bytes.iter().for_each(|b| print!("{:02x }   ,", b));
    stmt.execute(named_params! { ":name": name,":data":bytes})?;
    Ok(())
}

fn push_assembly(db: &Connection, assembly: String, lang: String, code: String) -> Result<()> {
    let mut stmt = db.prepare_cached(IMPORT_ASSEMBLY)?;
    stmt.execute(named_params! { ":assembly": assembly,":lang":lang, ":code":code})?;
    Ok(())
}

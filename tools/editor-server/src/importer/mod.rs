use std::collections::HashMap;
use std::fs;
use std::io::Read;
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex};

//use parking_lot::Re
use rusqlite::Connection;

use anyhow::{Context, Result};
use mlua::prelude::*;
use walkdir::WalkDir;

use cmoon_commons::*;

mod database;
mod interpreter;

use database::*;
pub use interpreter::*;

use crate::io::OutputHandler;
use crate::misc::PathExt;

pub struct FileImporter {
    res_folder: PathBuf,
    importers: LuaTable,
    lua: Lua,
    db: Arc<Mutex<Connection>>,
}

impl FileImporter {
    pub fn new(root: PathBuf) -> Result<Self> {
        let db = Arc::from(Mutex::from(Connection::open(root.join("assets.db"))?));
        let importer_path = root.join("importers");
        let res_folder = root.join("resources");

        let wd = WalkDir::new(importer_path);
        let lua = interpreter::prepared_lua()?;
        let env_meta = lua.create_table()?;
        env_meta.set("__index", lua.globals())?;

        let tables: Vec<_> = wd
            .into_iter()
            .filter_map(|e| {
                let entry = e.ok()?;
                println!("path is {:?}", entry.path());
                let chunk_name = entry
                    .path()
                    .file_name()
                    .unwrap()
                    .to_string_lossy()
                    .to_string();
                let chunk = fs::read_to_string(entry.into_path()).ok()?;

                let env: LuaTable = lua.create_table().ok()?;
                env.set_metatable(Some(env_meta.clone()));

                lua.load(chunk)
                    .set_environment(env.clone())
                    .set_name(chunk_name)
                    .exec()
                    .ok()?;

                Some(env)
            })
            .collect();

        let importers = lua.create_table()?;

        tables
            .iter()
            .flat_map(|t| t.pairs::<LuaValue, LuaTable>())
            .filter_map(|pair| pair.ok())
            .try_for_each(|(k, imptr)| {
                let p: LuaString = imptr.raw_get("extension")?;
                let is_recursive: bool = imptr.raw_get("is_recursive").unwrap_or(false);
                let pattern = LuaPattern::new(p, is_recursive, &lua)?;
                let importer =
                    ImportFunction::new(imptr.raw_get("import")?, imptr.raw_get("scheme")?);
                //println!("PATTERN :{:?}, IMPORTER: {:?}", pattern, importer);
                importers.set(pattern, importer)
            })?;

        Ok(Self {
            res_folder,
            lua,
            importers,
            db,
        })
    }

    pub fn purge(&self) -> Result<()> {
        self.db.lock().unwrap().purge()
    }

    ///This is the high level import function that will return nothing
    pub fn import<P: Into<PathBuf>>(&self, local_path: P, is_forced: bool) -> Result<()> {
        //Some type manipulation shennaningans

        let lua_val = LuaFile::new(&self.res_folder, local_path).into_lua(&self.lua)?;
        let file_ref = LuaUserDataRef::<LuaFile>::from_lua(lua_val, &self.lua)?;

        println!("importing:{}", file_ref.to_string());

        let out = indicatif::ProgressBar::new_spinner();
        out.enable_steady_tick(std::time::Duration::from_millis(100));
        let act = self.new_action(is_forced, out)?;

        let results: Option<ImportTable> = act.import(&self.lua, file_ref).into_lua_err()?;

        match results {
            Some(t) => {
                let lock = self.db.lock().unwrap();
                lock.push_artifacts(t)?;
                lock.register_imports(act.imported_files()?)?;
                Ok(())
            }
            None => Ok(()),
        }
    }

    pub fn set_config(&self, file_path: &Path) -> Result<()> {
        //importer invocation
        let mut config_file = std::fs::File::open(file_path)?;
        let mut buffer = String::new();
        config_file.read_to_string(&mut buffer)?;
        //let config: EngineConfig = serde_json::from_reader(config_file)?;
        let lock = self.db.lock().unwrap();
        crate::commands::set_config(&lock, buffer)
    }
}

impl FileImporter {
    pub fn enumerate_importers(&self) -> LuaResult<Vec<String>> {
        self.importers
            .pairs::<LuaUserDataRef<LuaPattern>, LuaValue>()
            .map(|pairs| {
                let (p, i) = pairs?;
                Ok(format!("Loaded importer for: {}", p.to_string()))
            })
            .collect()
    }

    fn new_action(&self, is_forced: bool, out: indicatif::ProgressBar) -> LuaResult<ImportAction> {
        //Some type manipulation shennaningans
        let act = ImportAction {
            outputter: out.clone(),
            database: self.db.clone(),
            importer: self.importers.clone(),
            imported_list: self.lua.create_table()?,
            matched_list: self.lua.create_table()?,
            is_forced,
        };

        let lua_act = act.clone();
        let import_fn = self
            .lua
            .create_function(move |lua, file| Ok(lua_act.import(lua, file)))?;
        self.lua.globals().set("import", import_fn)?;

        let print_fn = LuaFunction::wrap(move |var: LuaMultiValue| {
            let msg =
                var.iter()
                    .try_fold(String::default(), |mut acc, v| -> LuaResult<String> {
                        acc.push_str(&v.to_string()?);
                        acc.push_str("  ");
                        Ok(acc)
                    })?;
            out.set_message(msg);
            Ok(())
        });
        self.lua.globals().set("print", print_fn)?;
        Ok(act)
    }
}

///High level list of name=obj
type ImportTable = HashMap<String, ImportObj>;

enum ImportObj {
    Assembly { lang: String, code: String },
    Blueprint(String),
    Asset(Asset),
}

enum Asset {
    Simple(AssetField),
    Complex(HashMap<String, AssetField>),
}

impl Asset {
    pub fn to_bytes(self) -> Vec<u8> {
        let mut data: Vec<u8> = Default::default();
        match self {
            Self::Simple(field) => {
                data.push(SIMPLE_BYTE);
                Self::push_field(&mut data, field);
                data
            }
            Self::Complex(table) => {
                data.push(COMPOSITE_BYTE);
                table.into_iter().for_each(|(name, field)| {
                    data.extend(name.bytes());
                    data.push(ID_TERMINATOR_BYTE);
                    Self::push_field(&mut data, field);
                });
                data
            }
        }
    }

    fn push_field(data: &mut Vec<u8>, field: AssetField) {
        match field {
            AssetField::Number(n) => {
                data.extend([NUMBER_TAG]);
                data.extend((std::mem::size_of::<f64>() as u32).to_le_bytes());
                data.extend(n.to_le_bytes());
            }
            AssetField::Data(d) => {
                data.extend([RAW_DATA_TAG]);
                data.extend((d.len() as u32).to_le_bytes());
                data.extend(d);
            }
            AssetField::CompressedData(d) => {
                data.extend([COMPRESS_DATA_TAG]);
                let d = deflate::deflate_bytes(&d);
                data.extend((d.len() as u32).to_le_bytes());
                data.extend(d);
            }
            AssetField::Text(t) => {
                data.extend([RAW_TEXT_TAG]);
                let d = t.as_bytes();
                data.extend((d.len() as u32).to_le_bytes());
                data.extend(d);
            }
            AssetField::CompressedText(t) => {
                data.extend([COMPRESS_TEXT_TAG]);
                let d = deflate::deflate_bytes(t.as_bytes());
                data.extend((d.len() as u32).to_le_bytes());
                data.extend(d);
            }
        }
    }
}

enum AssetField {
    Number(f64),
    Text(String),
    CompressedText(String),
    Data(Vec<u8>),
    CompressedData(Vec<u8>),
}

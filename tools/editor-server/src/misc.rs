use std::path::{Path, PathBuf};

use mlua::AsChunk;

///keeps parts of the suffix, from last to first
#[derive(Clone)]
pub struct Suffix {
    inner: String,
    cur: usize,
}

impl Suffix {
    pub fn full(&self) -> String {
        self.inner.clone()
    }
    pub fn nth_parts(&self, n: usize) -> Option<&str> {
        self.inner.splitn(n, '.').last()
    }

    pub fn identifier(&self) -> Option<String> {
        self.inner.split_once(".").map(|(s, _)| s.to_owned())
    }
}

impl From<&str> for Suffix {
    fn from(value: &str) -> Self {
        Self {
            inner: value.to_owned(),
            cur: 0,
        }
    }
}

impl Iterator for Suffix {
    type Item = String;
    fn next(&mut self) -> Option<Self::Item> {
        self.cur += 1;
        let ext = self.nth_parts(self.cur)?;
        if ext.len() > 0 {
            Some(ext.to_owned())
        } else {
            None
        }
    }
}

pub trait PathExt {
    ///Returns the base name of the file on the path and the whole suffix after the first dot.
    fn suffix(&self) -> Option<Suffix>;

    fn main_name(&self) -> Option<&str>;
}

impl PathExt for PathBuf {
    fn suffix(&self) -> Option<Suffix> {
        let name = self.file_name()?.to_str()?;
        let raw = name.split_once(".")?.1;
        Some(raw.into())
    }

    fn main_name(&self) -> Option<&str> {
        let name = self.file_name()?.to_str()?;
        Some(name.split_once(".")?.0)
    }
}

impl PathExt for Path {
    fn suffix(&self) -> Option<Suffix> {
        let name = self.file_name()?.to_str()?;
        let raw = name.split_once(".")?.1;
        Some(raw.into())
    }

    fn main_name(&self) -> Option<&str> {
        let name = self.file_name()?.to_str()?;
        Some(name.split_once(".")?.0)
    }
}

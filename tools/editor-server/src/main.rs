use std::sync::Arc;

use anyhow::{Context, Result};
use io::{InputHandler, OutputHandler};

mod blueprint;
mod commands;
mod editor;
mod importer;
mod io;
mod misc;

#[cfg(test)]
mod test;

fn main() {
    let mut i_handler = InputHandler::default();
    let o_handler = Arc::from(OutputHandler::default());
    let edit_out = o_handler.clone();

    let matches = i_handler.arguments().unwrap();

    let res = {
        match matches.subcommand() {
            Some(("new", args)) => commands::new(args, i_handler, o_handler),
            Some(("open", args)) => commands::open(args, i_handler, o_handler),
            _ => Ok("INVALID COMMAND".to_owned()),
        }
    };

    edit_out.editor_result(res);
}

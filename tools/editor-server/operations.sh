mode=$1
target=$2

set -e

if [ "$mode" = "test" ]; then
    echo "Running tests on Component:$(basename "$PWD")"
    cargo test
elif [ "$mode" = "build" ]; then
    echo -e "\033[1;33m[Compiling Component:$(basename "$PWD") to $target]\033[0m"    
    cargo build 
    mv target/debug/editor-server $target/editor-server
    cd .. || exit 1
fi


exit 0

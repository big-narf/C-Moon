loader = require("soundfont")

file = io.open("font.sf2", "r")

--file = io.open("arachno.sf2", "r")

text = file:read("*all")
file:close()

t1 = os.clock()
font = loader.load(text)
t2 = os.clock()
print("delta time:", (t2 - t1) * 1000)

print("target:", 1000 / 60)

shader_frag = {
	extension = "frag$",
	scheme = "deferred",
}
function shader_frag.import(file)
	print("Compiling shader:", file)

	local out_name = string.format("%s.spv", file:file_name())
	local out = file:parent():child(out_name)
	local cmd = string.format("glslangValidator -V %s -o %s", file, out)
	os.execute(cmd)
	print("PATH IS", out)
	local res = {}
	for name, artifact in pairs(import(out)) do
		res[name .. "_frag"] = artifact
	end
	return res
end

shader_vert = {
	extension = "vert$",
	scheme = "deferred",
}
function shader_vert.import(file)
	print("Compiling shader:", file)
	local out_name = string.format("%s.spv", file:file_name())
	local out = file:parent():child(out_name)
	local cmd = string.format("glslangValidator -V %s -o %s", file, out)
	os.execute(cmd)
	print("PATH IS", out)
	local res = {}
	for name, artifact in pairs(import(out)) do
		res[name .. "_vert"] = artifact
	end
	return res
end

spv = {
	extension = ".spv$",
	scheme = "asset data",
}
--receives "LuaFile" userdata, and returns table with metadata/binary data pairs
function spv.import(file)
	return { [file:name()] = file:text() }
end

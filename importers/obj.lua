local loader = {}

mesh = {
	extension = "obj$",
	scheme = {
		indices = "compressed_data",
		vertices = "compressed_data",
	},
}
function mesh.import(file)
	print("importing mesh :", file)

	local vertices, indices = loader.load(file:text())

	local data = {
		vertices = to_f32_bytes(vertices:flatten()),
		indices = to_u32_bytes(indices),
	}

	local imports = {
		[file:name()] = data,
	}
	return imports
end

function loader.load(source)
	local lines = {}

	for line in string.gmatch(source, "([^\r\n]+)") do
		table.insert(lines, line)
	end

	return loader.parse(lines)
end

local function string_split(s, d)
	local t = {}
	local i = 0
	local f
	local match = "(.-)" .. d .. "()"

	if string.find(s, d) == nil then
		return { s }
	end

	for sub, j in string.gmatch(s, match) do
		i = i + 1
		t[i] = sub
		f = j
	end

	if i ~= 0 then
		t[i + 1] = string.sub(s, f)
	end

	return t
end

local has_uv, has_normal = false, false

local vertex_meta = {}
vertex_meta.__index = vertex_meta
function vertex_meta:flatten()
	local flat = {}
	--insert v
	table.insert(flat, self.v.x)
	table.insert(flat, self.v.y)
	table.insert(flat, self.v.z)
	table.insert(flat, self.v.w)

	if has_uv then
		table.insert(flat, self.vt.u or 0)
		table.insert(flat, self.vt.v or 0)
		table.insert(flat, self.vt.w or 0)
	end

	if has_normal then
		table.insert(flat, self.vn.x or 0)
		table.insert(flat, self.vn.y or 0)
		table.insert(flat, self.vn.z or 0)
	end
	return flat
end

local vertices = {}
---pushes a vertex to the vertex list, returns its index
function vertices:push(v, vt, vn)
	--checks for duplicates
	for index, vertex in ipairs(self) do
		if v == vertex.v then
			return index
		end
	end
	--insert if novel
	has_uv = (vt ~= nil) or has_uv
	has_normal = (vn ~= nil) or has_normal
	table.insert(self, setmetatable({ v = v, vt = vt, vn = vn }, vertex_meta))
	return #self
end

---extends dst table with the contents of src table
local function extend(dst, src)
	for _, val in ipairs(src) do
		table.insert(dst, val)
	end
end

function vertices:flatten()
	local result = {}
	for _, vertex in ipairs(self) do
		extend(result, vertex:flatten())
	end
	return result
end

local indices = setmetatable({}, {
	__tostring = function(self)
		local s = "indices::"
		for index, value in ipairs(self) do
			s = s .. ";" .. value
		end
		return s
	end,
})

local function valid_triangle(v1, v2, v3)
	--error("NOT IMPLEMENTED!!!!")
	return true
end

---NOTE: THIS ONLY WORKS FOR SIMPLE CONVEX FACES, OTHERS NEED TO BE PROCESSED FIRST
---@param face any
local function triangulate(face)
	while #face >= 3 do
		if valid_triangle(vertices[face[-1]], vertices[face[0]], vertices[face[1]]) then
			table.insert(indices, face[-1])
			table.insert(indices, face[0])
			table.insert(indices, face[1])
			table.remove(face) --pops last vertex
		else
			error("invalid triangle in the face")
		end
	end
end

function loader.parse(object)
	local meta = {
		__index = function(self, i)
			if i < 0 then
				return rawget(self, #self + i)
			else
				return rawget(self, i)
			end
		end,
	}
	local obj = {
		v = setmetatable({}, meta), -- List of vertices - x, y, z, [w]=1.0
		vt = setmetatable({}, meta), -- Texture coordinates - u, v, [w]=0
		vn = setmetatable({}, meta), -- Normals - x, y, z
		vp = setmetatable({}, meta), -- Parameter space vertices - u, [v], [w]
		f = {}, -- Faces
	}

	for _, line in ipairs(object) do
		local l = string_split(line, "%s+")

		if l[1] == "v" then
			local v = {
				x = tonumber(l[2]),
				y = tonumber(l[3]),
				z = tonumber(l[4]),
				w = tonumber(l[5]) or 1.0,
			}
			table.insert(obj.v, v)
		elseif l[1] == "vt" then
			local vt = {
				u = tonumber(l[2]),
				v = tonumber(l[3]),
				w = tonumber(l[4]) or 0,
			}
			table.insert(obj.vt, vt)
		elseif l[1] == "vn" then
			local vn = {
				x = tonumber(l[2]),
				y = tonumber(l[3]),
				z = tonumber(l[4]),
			}
			table.insert(obj.vn, vn)
		elseif l[1] == "vp" then
			local vp = {
				u = tonumber(l[2]),
				v = tonumber(l[3]),
				w = tonumber(l[4]),
			}
			table.insert(obj.vp, vp)
		elseif l[1] == "f" then
			local f_meta = {
				__index = function(self, i)
					if i <= 0 then
						return rawget(self, #self + i)
					elseif i > #self then
						return rawget(self, i - #self)
					end
					return rawget(self, i)
				end,
				__tostring = function(self)
					local s = ""
					for index, value in ipairs(self) do
						s = s .. ";" .. value
					end
					return s
				end,
			}
			local face = setmetatable({}, f_meta)

			for i = 2, #l do
				local split = string_split(l[i], "/")

				local index=tonumber(split[1])
				assert(index,"found null vertex index")
				local v = obj.v[index]
				local index=tonumber(split[2])
				assert(index,"found null UV index")
				local vt = obj.vt[index]
				local index=tonumber(split[3])
				assert(index,"found null normal index")
				local vn = obj.vn[index]

				local index = vertices:push(v, vt, vn)

				table.insert(face, index)
			end

			--print(face)
			table.insert(obj.f, face)
		end
	end

	for index, face in ipairs(obj.f) do
		triangulate(face)
	end
	--return obj
	return vertices, indices
end

return loader

--high level generic importer for folders in general
general = {
	extension = ".*",
	is_recursive = false,
	scheme = "deferred",
}
--receives "LuaFile" userdata, and returns table with metadata/binary data pairs
function general.import(file)
	local contents = file:contents()
	local imports = {}
	for i, path in pairs(contents) do
		--collects individual imports into a single file
		local imp = import(path)
		if imp then
			for name, obj in pairs(imp) do
				local index=0
				local dedup_name=name
				while imports[dedup_name] do
					index = index + 1
        				dedup_name = name .. "_" .. index
        			end
				imports[dedup_name] = obj
			end
		else
			print("couldn't import from Path:", path)
		end
	end
	return imports
end

local loader = {}

---NOTE: Imports as a table with three lists:
---samples = raw audio data
---instruments = metadata that indexes the samples
---presets = metadata that indexes the instruments.
---So playing a not goes preset => instrument => raw sample, appliying effects and the like.
soundfont = {
	extension = "sf2$",
	scheme = {
		samples = "compressed_data",
		instrument = "compressed_data",
		presets = "compressed_data",
	},
}
function soundfont.import(file)
	print("importing mesh :", file)

	local vertices, indices = loader.load(file:text())

	local data = {
		vertices = to_f32_bytes(vertices:flatten()),
		indices = to_u32_bytes(indices),
	}

	local imports = {
		[file:name()] = data,
	}
	return imports
end

--- reader class with convenient type reading functions
---@class reader
---@field bytes string,
---@field i integer,
local reader = {}
reader.__index = reader

---comment
---@param bytes string
---@return reader
function reader.new(bytes)
	return setmetatable({ bytes = bytes, i = 1 }, reader)
end

---returns the current read byte.
---@return integer
function reader:position()
	return self.i
end

---returns true if the reader hasen't reached pos yet.
---@param pos integer
---@return boolean
function reader:is_before(pos)
	return self.i < pos
end

---skips size bytes , unchecked
---@param size integer
function reader:skip(size)
	self.i = self.i + size
end

---skips to the position, unchecked
---@param pos integer
function reader:skip_to(pos)
	self.i = pos
end

---returns size next bytes as a string
---@param size number
---@return string
function reader:read_bytes(size)
	local bytes = string.sub(self.bytes, self.i, self.i + size)
	self.i = self.i + size
	return bytes
end

function reader:read_4cc()
	local v = string.sub(self.bytes, self.i, self.i + 3)
	self.i = self.i + 4
	return v
end

function reader:read_u8()
	local b = string.byte(string.sub(self.bytes, self.i, self.i))
	self.i = self.i + 1
	return b
end

---reads i32 integer. Assumes small endianess if not specified.
---@param big_end boolean?
---@return integer
function reader:read_i32(big_end)
	local n = self:read_u32(big_end)
	if n > 0x7FFFFFFF then
		return n - 0x100000000
	else
		return n
	end
end

---reads i32 integer. Assumes small endianess if not specified.
---@param big_end boolean?
---@return integer
function reader:read_u32(big_end)
	local b1, b2, b3, b4 = string.byte(self.bytes, self.i, self.i + 3)
	self.i = self.i + 4
	if big_end then
		return b4 + (b3 * 0x100) + (b2 * 0x10000) + (b1 * 0x1000000)
	else
		return b1 + (b2 * 0x100) + (b3 * 0x10000) + (b4 * 0x1000000)
	end
end

---reads u16 integer. Assumes small endianess if not specified.
---@param big_end boolean?
---@return integer
function reader:read_u16(big_end)
	local b1, b2 = string.byte(self.bytes, self.i, self.i + 1)
	self.i = self.i + 2
	if big_end then
		return b2 + (b1 * 0x100)
	else
		return b1 + (b2 * 0x100)
	end
end

---reads u16 integer. Assumes small endianess if not specified.
---@param big_end boolean?
---@return integer
function reader:read_i16(big_end)
	local n = self:read_u16(big_end)
	if n > 0x7FFF then
		return n - 0x10000
	else
		return n
	end
end

---this is the most ungodly shit ever
---@return integer
function reader:read_ivar()
	local b1, b2, b3, b4 = string.byte(self.bytes, self.i + 1, self.i + 4)
	local acc = 0
	acc = acc + b1
	if b1 < 0x80 then -- 128
		self.i = self.i + 1
		return acc
	end
	acc = (acc - 0x80) * 0x80 -- removes lead bit and shifts value

	acc = acc + b2
	if b2 < 0x80 then
		self.i = self.i + 2
		return acc
	end
	acc = (acc - 0x80) * 0x80

	acc = acc + b3
	if b3 < 0x80 then
		self.i = self.i + 3
		return acc
	end
	acc = (acc - 0x80) * 0x80

	acc = acc + b4
	if b4 < 0x80 then
		self.i = self.i + 4
		return acc
	end
	acc = (acc - 0x80) * 0x80
	error("Ivar bigger than 4 bytes")
end

---NOTE: HEADERS CONSTANTS
local SF2_HEADER = "RIFF"
local FORM_TYPE = "sfbk"
local LIST_CHUNK = "LIST"

local TYPE_INFO = "INFO"
local TYPE_SAMPLE = "sdta"
local TYPE_PARAMS = "pdta"

---comment
---@param r reader
local function read_info(r)
	local s = r:read_4cc()
	assert(s == LIST_CHUNK, "Couldn't find LIST chunk.")
	local chunk_end = r:read_i32()
	chunk_end = chunk_end + r.i
	assert(r:read_4cc() == TYPE_INFO, "LIST chunk need to have type INFO ")

	--[[	while r:is_before(chunk_end) do
		local id = r:read_4cc()
		local size = r:read_i32()
	end]]
	--
	print("info end:", chunk_end)

	r:skip_to(chunk_end) --NOTE: META DATA IGNORING.
end

---comment
---@param r reader
---@return integer[]
local function read_samples(r)
	assert(r:read_4cc() == LIST_CHUNK, "Couldn't find LIST chunk.")
	local chunk_end = r:read_i32()
	chunk_end = chunk_end + r:position()
	assert(r:read_4cc() == TYPE_SAMPLE, "LIST chunk need to have type sample ")

	local meta = {
		__index = function(self, k)
			local value = rawget(self, k)
			print(#self.data)
			if not value then
				--NOTE: lazyly evaluates a i16 small endian
				local b1, b2 = string.byte(self.data, (2 * k) - 1, 2 * k)
				print("b1=", b1, "b2=", b2)
				local number = b1 + (b2 * 0x100)
				if number > 0x7FFF then
					value = number - 0x10000
				else
					value = number
				end
				rawset(self, k, value)
			end
			return value
		end,
	}

	local samples = {}
	while r:is_before(chunk_end) do
		local id = r:read_4cc()
		local size = r:read_i32()
		if id == "smpl" then
			samples.bits_per_sample = 16
			samples.data = r:read_bytes(size)
		else
			error("sample type not supported")
		end
	end
	return setmetatable(samples, meta)
end

---@type table<string,fun(r :reader, list :table,size:integer)>
local PARAM_FUNC = {
	phdr = function(r, list, size)
		assert(size ~= 0 and size % 38 == 0, "preset list has invalid lenght.")
		local preset_infos = {}
		local count = size / 38
		for i = 1, count, 1 do
			local info = {
				name = r:read_bytes(20),
				patch_number = r:read_u16(),
				bank_number = r:read_u16(),
				start_index = r:read_u16() + 1,
				library = r:read_i32(),
				genre = r:read_i32(),
				morphology = r:read_i32(),
			}
			info.id = info.bank_number * 0x10000 + info.patch_number
			--	print("preset:", info.name)
			--	print("zone start:", info.start_index)
			table.insert(preset_infos, info)
		end
		list.preset_infos = preset_infos
	end,
	pbag = function(r, list, size)
		assert(size % 4 == 0, "preset zone list has invalid lenght.")
		local count = size / 4
		local zones = {}
		for i = 1, count, 1 do
			local zone = {
				gen_index = r:read_u16() + 1,
				mod_index = r:read_u16() + 1,
			}
			table.insert(zones, zone)
		end

		list.preset_zones = zones
	end,
	pmod = function(r, list, size)
		--NOTE: This just discarts the data.
		assert(size % 10 == 0, "modulator list has invalid lenght.")
		r:skip(size)
	end,
	pgen = function(r, list, size)
		assert(size % 4 == 0, "generator list has invalid lenght.")
		local count = size / 4
		local gens = {}
		for i = 1, count, 1 do
			local gen = {
				type = r:read_u16(),
				value = r:read_u16(),
			}
			table.insert(gens, gen)
		end
		list.preset_gens = gens
		---NOTE:MAYBE NEEDS TO DISCART THE LAST ONE, IDK.
	end,
	inst = function(r, list, size)
		assert(size % 22 == 0, "instrument list has invalid lenght.")
		local count = size / 22
		local inst_infos = {}
		for i = 1, count, 1 do
			local info = {
				name = r:read_bytes(20),
				start_index = r:read_u16() + 1,
			}
			--	print("instrument:", info.name, "id:", i)
			table.insert(inst_infos, info)
		end
		list.inst_infos = inst_infos
	end,
	ibag = function(r, list, size)
		assert(size % 4 == 0, "instrument zone list has invalid lenght.")
		local count = size / 4
		local zones = {}
		for i = 1, count, 1 do
			local zone = {
				gen_index = r:read_u16() + 1,
				mod_index = r:read_u16() + 1,
			}
			table.insert(zones, zone)
		end

		list.inst_zones = zones
	end,
	imod = function(r, list, size)
		--NOTE: This just discarts the data.
		assert(size % 10 == 0, "modulator list has invalid lenght.")
		r:skip(size)
	end,
	igen = function(r, list, size)
		assert(size % 4 == 0, "generator list has invalid lenght.")
		local count = size / 4
		local gens = {}
		for i = 1, count, 1 do
			local gen = {
				type = r:read_u16(),
				value = r:read_u16(),
			}
			table.insert(gens, gen)
		end
		list.inst_gens = gens
	end,
	shdr = function(r, list, size)
		assert(size % 46 == 0, "sample header list has invalid lenght.")
		local count = size / 46

		local headers = {}
		for i = 1, count, 1 do
			local header = {
				name = r:read_bytes(20),
				start_index = r:read_i32() + 1,
				end_index = r:read_i32() + 1,
				start_loop = r:read_i32() + 1,
				end_loop = r:read_i32() + 1,
				sample_rate = r:read_i32(),
				pitch_o = r:read_u8(),
				pitch_c = r:read_u8(),
				link = r:read_u16(),
				type = r:read_u16(),
			}
			table.insert(headers, header)
		end
		--NOTE: MAYBE NEEDS TO DROP THE LAST ONE, IDK.
		list.sample_headers = headers
	end,
}

---@class slice<T>: { [integer]: T }
---@field src T[]
---@field offset integer
---@field count integer
local slice = {}
---comment
---@generic T
---@param src T[]
---@param offset integer
---@param count integer
---@return slice
function slice.new(src, offset, count)
	assert(type(offset) == "number" and type(count) == "number", "slice params are invalid")
	return setmetatable({ src = src, offset = offset - 1, count = count }, slice)
end
function slice:__index(key)
	if key > self.count or key < 1 then
		return nil
	else
		--print("key", key, "offset", self.offset, "index:", self.offset + key)
		return self.src[self.offset + key]
	end
end
function slice:__len()
	return self.count
end
function slice:__ipairs()
	return function(t, k)
		local v = t[k + 1]
		if v then
			return k + 1, v
		end
	end, self, 0
end

local gen_table = {
	init_filter_cutoff = 8,
	delay_mod_lfo = 21,
	delay_vib_lfo = 23,
	delay_mod_env = 25,
	attack_mod_env = 26,
	hold_mod_env = 27,
	decay_mod_env = 28,
	rel_mod_env = 30,
	delay_vol_env = 33,
	attack_vol_env = 34,
	hold_vol_env = 35,
	decay_vol_env = 36,
	rel_vol_env = 38,

	instrument = 41,

	key_range = 43,
	vel_range = 44,
	key_number = 46,
	velocity = 47,
	scale_tune = 56,
	over_root_key = 58,

	sample_id = 53,
}

---@class region
local region = {}
region.__index = region
region.__add = function(rhs, lhs)
	local result = {}
	for i = 1, 61 do
		table.insert(result, rhs[i] + lhs[i])
	end
	return setmetatable(result, region)
end
region.__tostring = function(self)
	local s = ""
	for index, value in ipairs(self) do
		s = s .. index .. "=" .. value .. "//"
	end
	return s
end

---comment
---@return region
function region.inst_default()
	local reg = {
		[gen_table.init_filter_cutoff + 1] = 13500,
		[gen_table.delay_mod_lfo + 1] = -12000,
		[gen_table.delay_vib_lfo + 1] = -12000,
		[gen_table.delay_mod_env + 1] = -12000,
		[gen_table.attack_mod_env + 1] = -12000,
		[gen_table.hold_mod_env + 1] = -12000,
		[gen_table.decay_mod_env + 1] = -12000,
		[gen_table.rel_mod_env + 1] = -12000,
		[gen_table.delay_mod_env + 1] = -12000,
		[gen_table.attack_vol_env + 1] = -12000,
		[gen_table.hold_vol_env + 1] = -12000,
		[gen_table.decay_vol_env + 1] = -12000,
		[gen_table.rel_vol_env + 1] = -12000,

		[gen_table.key_range + 1] = 0x7F00,
		[gen_table.vel_range + 1] = 0x7F00,
		[gen_table.key_number + 1] = -1,
		[gen_table.velocity + 1] = -1,
		[gen_table.scale_tune + 1] = 100,
		[gen_table.over_root_key + 1] = -1,
	}
	for i = 1, 61, 1 do
		reg[i] = reg[i] or 0
	end
	return setmetatable(reg, region)
end

---comment
---@return region
function region.preset_default()
	local reg = {
		[gen_table.key_range + 1] = 0x7F00,
		[gen_table.vel_range + 1] = 0x7F00,
	}
	for i = 1, 61, 1 do
		reg[i] = reg[i] or 0
	end
	return setmetatable(reg, region)
end

---fills this region with the values from the zone given
---@param zone table
---@return region
function region:fill(zone)
	for index, gen in ipairs(zone) do
		--print(gen.type, gen.value)
		self[gen.type + 1] = gen.value
	end
	return self
end

---@class soundfont
---@field preset_infos table[]
---@field preset_zones table[]
---@field preset_gens table[]
---@field inst_infos table[]
---@field inst_zones table[]
---@field inst_gens table[]
---@field inst_cache table<integer, region[]>
---@field preset_cache table<integer, region[]>
local soundfont = {}
soundfont.__index = soundfont

---comment
---@param r reader
---@return soundfont
function soundfont.read_from(r)
	assert(r:read_4cc() == LIST_CHUNK, "Couldn't find LIST chunk.")
	local chunk_end = r:read_i32()
	chunk_end = chunk_end + r:position()
	assert(r:read_4cc() == TYPE_PARAMS, "LIST chunk need to have type params")

	local params = {}
	while r:is_before(chunk_end) do
		local id = r:read_4cc()
		local size = r:read_i32()
		local func = PARAM_FUNC[id]
		assert(func, "found unknown param id")
		func(r, params, size)
	end

	assert(params.preset_infos, "missing PHDR section.")
	assert(params.preset_zones, "missing PBAG section.")
	assert(params.preset_gens, "missing PGEN section.")
	assert(params.inst_infos, "missing INST section.")
	assert(params.inst_zones, "missing IBAG section.")
	assert(params.inst_gens, "missing IGEN section.")
	assert(params.sample_headers, "missing SHDR section.")

	params.inst_cache = {}
	params.preset_cache = {}
	return setmetatable(params, soundfont)
end

---lazyly evaluates the instrument regions for the signal
---@param inst_id integer
---@return region[]
function soundfont:inst_reg(inst_id)
	local regions = self.inst_cache[inst_id]
	if not regions then
		--NOTE: LAZYLY COMPUTES REGIONS.

		local info_cur = self.inst_infos[inst_id]
		local info_next = self.inst_infos[inst_id + 1]

		print("info cur is:", info_cur.name)
		print("info prev is:", self.inst_infos[inst_id].name)
		print("info next is:", info_next.name)
		---NOTE: CREATES ALL THE ZONES OF THE INSTRUMENT.
		local zones = {}
		for i = info_cur.start_index, info_next.start_index - 1, 1 do
			--	print("inst gen index:", i)
			local zone_info = self.inst_zones[i]
			local gen_count = info_next.start_index - info_cur.start_index
			--	print("count :", gen_count)
			table.insert(zones, slice.new(self.inst_gens, zone_info.gen_index, gen_count))
		end

		regions = {}
		---NOTE: ASSUMES THE FIRST ONE IS THE GLOBAL, CHECKS IF IT IS OTHERWISE
		local base = zones[1]
		local start = 2
		--print("base last:", base[#base].type)
		if #base == 0 or base[#base].type ~= gen_table.sample_id then
			base = {}
			start = 1
		end

		for i = start, #zones, 1 do
			local added = zones[i]
			local reg = region.inst_default():fill(base):fill(added)
			table.insert(regions, reg)
		end

		--NOTE: CACHES ZONES
		self.inst_cache[inst_id] = regions
	end
	return regions
end

---lazyly evaluates the instrument regions for the signal
---@param preset_id integer
---@return region[]
function soundfont:preset_reg(preset_id)
	local regions = self.preset_cache[preset_id]
	if not regions then
		--NOTE: ADD THIS BACK LATTER
		--[[
		local info_index = 1
		for index, value in ipairs(self.preset_infos) do
			if value.id == preset_id then
				info_index = index
				break
			elseif info_index > value.id then
				info_index = index
			end
		end]]

		local info_index = preset_id
		local info_cur = self.preset_infos[info_index]
		print("preset is:", info_cur.name)
		local info_next = self.preset_infos[info_index + 1]

		---NOTE: CREATES ALL THE ZONES OF THE INSTRUMENT.
		local zones = {}

		local first_gen = nil
		print("cur start:", info_cur.start_index, "next start:", info_next.start_index)
		for i = info_cur.start_index, info_next.start_index - 1 do
			local cur_zone = self.preset_zones[i]
			local next_zone = self.preset_zones[i + 1]
			local gen_count = next_zone.gen_index - cur_zone.gen_index
			first_gen = cur_zone.gen_index
			local s = slice.new(self.preset_gens, cur_zone.gen_index, gen_count)
			table.insert(zones, s)
		end

		print("first from slice:", zones[1][1].value)

		print("first generator:", self.preset_gens[first_gen].value)
		print("first type:", self.preset_gens[first_gen].type)
		print("prev generator:", self.preset_gens[first_gen - 1].value)
		print("next generator:", self.preset_gens[first_gen + 1].value)

		regions = {}
		---NOTE: ASSUMES THE FIRST ONE IS THE GLOBAL, CHECKS IF IT IS OTHERWISE
		local base = zones[1]
		local start = 2
		if #base ~= 0 or base[#base].type == gen_table.instrument then
			base = {}
			start = 1
		end

		for i = start, #zones do
			print("new region")
			local added = zones[i]
			local reg = region.preset_default():fill(base):fill(added)
			table.insert(regions, reg)
			print(reg[gen_table.instrument + 1])
		end

		--NOTE: CACHES ZONES
		self.preset_cache[preset_id] = regions
	end
	return regions
end

---@class synth
---@field sample_rate integer
---@field samples integer[]
---@field presets preset[]
---
---@field channels
---@field voices
local synth = {}

function synth:note_on(channel, key, velocity)
	local channel = self.channels[channel]
	local preset_code = channel.bank_number * 0x10000 + channel.patch_number

	local preset = self:preset_reg(preset_code)

	for _, preset_region in ipairs(preset) do
		local id = preset_region[gen_table.instrument + 1] + 1
		--print("0-indexed instrument:", id - 1)
		local inst = self:inst_reg(id)
		for _, inst_region in ipairs(inst) do
			local sum_region = inst_region + preset_region
		end
	end
end

function synth:note_off(channel, key) end

---comment
---@param source string
---@return unknown
function loader.load(source)
	local r = reader.new(source)
	assert(r:read_4cc() == SF2_HEADER, "Invalid Soundfont header")
	local size = r:read_i32()
	print("index is:", r.i, "size is:", size)
	local form = r:read_4cc()
	assert(form == FORM_TYPE, "Invalid Soundfont form type")

	local font = {}
	read_info(r) --NOTE: JUST IGNORES INFO.
	local samples = read_samples(r)
	print("sample read")

	local params = soundfont.read_from(r)
	math.randomseed(os.clock())
	local id = math.random(#params.preset_infos)
	print("0-indexed preset:", id - 1)
	local preset = params:preset_reg(id)

	for _, preset_region in ipairs(preset) do
		local id = preset_region[gen_table.instrument + 1] + 1
		print("0-indexed instrument:", id - 1)
		local inst = params:inst_reg(id)

		for _, inst_region in ipairs(inst) do
			local sum_region = inst_region + preset_region
			print("key ranges:", inst_region[gen_table.key_range + 1], preset_region[gen_table.key_range + 1])
		end
	end
	print("size:", #source, "read:", r.i - 1)
	return font
end

---NOTE: IM SURE THIS SHIT WILL FUCK ME UP WITH ENDIENNESS

return loader

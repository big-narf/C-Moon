script = {
	extension = "lua$",
	is_recursive = false,
	scheme = "assembly Lua",
}
function script.import(file)
	return { [file:name()] = file:text() }
end

assembly = {
	extension = "%.[^%.]*%.lua$",
	is_recursive = false,
	scheme = "deferred",
}
--receives "LuaFile" userdata, and returns table with metadata/binary data pairs
function assembly.import(file)
	print("importing assembly :", file)

	local code = "some stuff"

	local meta = {
		name = file:name(),
		import_type = "Assembly Lua",
	}

	return { [meta] = code }
end

blu = {
	extension = "blu$",
	scheme = "blueprint",
}

--receives "LuaFile" userdata, and returns table with metadata/binary data pairs
function blu.import(file)
	local imports = {
		[file:name()] = file:text(),
	}
	return imports
end

#!/bin/sh

if [ -z $1 ]; then
    mode="build"
else
    mode=$1
fi

set -e

case $mode in
    "build")
    	export RUSTFLAGS=-Awarnings
    	flag=$2
        engine="$HOME/engine_test"
        project="$HOME/test"

        if [ "$flag" = "clean" ]; then
            rm -r $engine || true
           # rm -r $project || true
        fi

        if [ ! -d "$engine" ]; then
            mkdir -p "$engine"
        fi

        if [  -d "engine" ]; then
            cd engine
            ./operations.sh "$mode" "$engine/engine"
        fi
        cd ..

        #compiles and copies editor
        if [  -d "tools/editor-server" ]; then
            cd tools/editor-server
            mkdir -p "$engine/tools"
            ./operations.sh "$mode" "$engine/tools"
        fi
        cd ../..
        
        if [  -d "importers" ]; then
           cp -rn importers "$engine/tools"
    		
        fi
        
        if [  -d "standard" ]; then
            cp -rn standard "$engine/tools"
        fi
        
        if [  -d "languages" ]; then
            cp -rn languages "$engine/tools"
        fi

        if [ "$flag" = "run" ]; then
            ./launch.sh $engine $project
        fi
        ;;
        
        
        
    "test")
    	echo "$2"
        export PROJECT_PATH="$HOME/C-Moon/project/"
        if [ -z $2 ];then
        for folder in engine/modules ; do
  			cargo test 
        		cd .. || exit 1
		done
        	
        else
        	cd engine/modules/$2 || exit 1
        	cargo test -- --nocapture
        	exit 0
	fi
        echo "all tests done."
        ;;
        
        
         "push")
        msg=$2
        git commit -a -m "$msg"
        git submodule update --recursive --remote
        git push
        ;;
    *)
        echo "INVALID MODE!"
        exit 1
        ;;
esac
    exit 0

#!/bin/sh

engine=$1
project=$2

editor="$engine/tools/editor-server"


if [ -d $project ]; then 
rm -r $project
fi

#creates test project if none exists
#if [ ! -d $project ]; then
	echo "Creating new project"
	$editor -n $project
		#moves test files to the project folder
		cp -r project/* "$project/resources/"
	{
		#imports standard libraries
		echo "import standard/standard.lua"	
		echo "import standard/tester.lua"
		
		#import test files
		echo "import image.png"
		#echo "import font.sf2"
		#echo "import texture.frag.spv"
		#echo "import texture.vert.spv"
		
		#creates and imports test blueprint
		tester="Manager.blu"
		statics="{\"image\":{\"Asset\":1}}" #;[\"font\",{\"Resource\":2}]"
		
		behavior="Lua-tester-tester"
		echo "create $tester --behaviours=$behavior --statics=$statics"
		echo "import $tester"
		
		#tries launching engine
		echo "launch"
		echo "close"
	} | $editor -o $project
#else
#	echo "Opening existing project"
#	{
#		echo "launch"
#		echo "close"
#	} | $editor -o $project
#fi

exit 0

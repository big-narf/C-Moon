mod database;
pub use database::*;

pub const NUMBER_TAG: u8 = 0x01;
pub const RAW_TEXT_TAG: u8 = 0x02;
pub const COMPRESS_TEXT_TAG: u8 = 0x03;
pub const RAW_DATA_TAG: u8 = 0x04;
pub const COMPRESS_DATA_TAG: u8 = 0x05;

pub const SIMPLE_BYTE: u8 = 0xA;
pub const COMPOSITE_BYTE: u8 = 0xB;
pub const ID_TERMINATOR_BYTE: u8 = 0xFF;

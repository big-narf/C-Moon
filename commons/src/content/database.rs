#[cfg(feature = "editor")]
mod editor_stmts {

    //table definitions
    pub static CORE_TABLE_DEFINITIONS: &str = r#"CREATE TABLE Blueprints (
                                                Blueprint_ID TEXT PRIMARY KEY,
                                                Statics JSON NOT NULL,
                                                Parametrics JSON,
                                                Behaviours JSON
                                                );
                                                CREATE TABLE Hierarchy (
                                                Handle TEXT NOT NULL,
                                                Transform JSON NOT NULL,
                                                Args JSON,
                                                Parent_ID TEXT NOT NULL,
                                                Child_ID TEXT NOT NULL,
                                                FOREIGN KEY (Parent_ID) REFERENCES Blueprints (Blueprint_ID) ON DELETE CASCADE,
                                                FOREIGN KEY (Child_ID) REFERENCES Blueprints (Blueprint_ID) ON DELETE CASCADE,
                                                UNIQUE (Parent_ID, Handle)
                                                );
                                                CREATE TABLE Resources (
                                                Resource_ID INTEGER PRIMARY KEY,
                                                Name TEXT NOT NULL,
                                                Data BLOB NOT NULL
                                                );
                                                CREATE TABLE Assemblies  (
                                                Assembly_ID TEXT PRIMARY KEY,
                                                Language TEXT NOT NULL,
                                                Code TEXT NOT NULL
                                                ); 
                                                CREATE TABLE FileRefs (
                                                FilePath TEXT PRIMARY KEY,
                                                LastImported TEXT NOT NULL
                                               );
                                               CREATE TABLE Configurations (
                                               Encoded JSON
                                               );"#;

    //Importer queries
    pub static CHECK_LAST_IMPORT: &str = r#"
                                            SELECT LastImported
                                            FROM FileRefs 
                                            WHERE FilePath = :path;
                                            "#;

    pub static REGISTER_IMPORT: &str = r#"INSERT OR REPLACE INTO FileRefs 
                                                (FilePath,LastImported) 
                                                VALUES (:path,:dtime)"#;
    pub static IMPORT_BLUEPRINT: &str = r#"INSERT OR REPLACE INTO Blueprints 
                                                (Blueprint_ID,Statics,Parametrics,Behaviours) 
                                                VALUES (:name,:statics,:parametrics,:behaviours)"#;
    pub static IMPORT_RELATIONS: &str = r#"INSERT OR REPLACE INTO Hierarchy 
                                                (Handle,Transform,Parent_ID,Child_ID) 
                                                VALUES (:handle,:transform,:parent,:child)"#;
    pub static IMPORT_RESOURCE: &str = r#"INSERT OR REPLACE INTO Resources 
                                                (Name,Data) 
                                                VALUES (:name,:data)"#;
    pub static IMPORT_ASSEMBLY: &str = r#"INSERT OR REPLACE INTO Assemblies
                                                (Assembly_ID,Language,Code)
                                                Values(:assembly,:lang,:code);"#;
    pub static SET_ENGINE_CONFIG: &str = r#"INSERT INTO Configurations (Encoded) VALUES (:config)"#;
}
#[cfg(feature = "editor")]
pub use editor_stmts::*;

#[cfg(feature = "core")]
mod core_stmts {
    ///This is the main sql query that will recursively grab all the blueprint data from the db.
    ///WARNING: This will most certainly fuck up the first few times, needs to be checked.
    pub static REC_GRAB_TREE: &str = r#"WITH RECURSIVE RecTree AS (
                                SELECT 
					    blueprints.Blueprint_ID as Blueprint_ID,
					    :root_name AS tree_path,
					    :root_args AS Args,
                                            json_array(json(:root_transform)) AS Transforms,
					    Blueprints.Parametrics as Parametrics,
					    Blueprints.Statics as Statics,
					    Blueprints.Behaviours as Behaviours
                                FROM Blueprints
                                WHERE blueprints.Blueprint_ID = :root

                                UNION ALL

                                SELECT 
                                            child.Blueprint_ID,
					    tree.tree_path || '/' || h.Handle,
					    h.Args,
                                            json_insert(tree.Transforms,'$[#]',json(h.Transform)),
					    child.Parametrics,
					    child.Statics,
					    child.Behaviours
                                FROM RecTree tree
                                JOIN Hierarchy h ON h.Parent_ID = tree.Blueprint_ID
                                JOIN Blueprints child ON h.Child_ID = child.Blueprint_ID
                                )
                                SELECT * FROM RecTree;"#;

    pub static GRAB_RESOURCES: &str = r#"SELECT Data 
                        	FROM 
                        	Resources
                        	WHERE Resource_ID = :resource;"#;

    pub static GRAB_ASSEMBLIES: &str = r#"SELECT Code,Assembly_ID 
                        	FROM Assemblies 
                        	WHERE Language= :lang;"#;

    pub static GRAB_CONFIG: &str = r#" SELECT Encoded as Encoded FROM Configurations"#;
}
#[cfg(feature = "core")]
pub use core_stmts::*;

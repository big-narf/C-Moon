use std::error::Error;
use std::fmt::{Debug, Display};

use super::*;

pub type EngResult<T> = Result<T, EngineError>;

/**This is a major error type that is moved around the engine interface.
It is composed of a four digit error {code} so it can be easyly translated.
In case translation fails it has a {default_msg} string that can be displayed
An optional {root} string can be provided for specifics (ex: script variable error)
*/
#[derive(Clone, Default)]
pub struct EngineError {
    maj_code: u32,
    minor_code: u32,
    root: Option<String>,
    default_msg: String,
}

impl Display for EngineError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let code = self.maj_code + self.minor_code;
        let mut s = format!("ERROR#{}: {}", code, self.default_msg);
        match &self.root {
            Some(r) => s.push_str(&format!("[{r}]")),
            None => (),
        }
        f.write_str(&s)
    }
}

impl Debug for EngineError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self, f)
    }
}

impl Error for EngineError {}

///Dynamically created errors
impl EngineError {
    ///Creates an error originated on the core
    pub fn from_core(mut self) -> Self {
        self.maj_code = 9000;
        self
    }

    ///Creates an error originated on a module
    pub fn from_mdl(mut self, mdl: &ModuleType) -> Self {
        let major_code = match mdl {
            ModuleType::Renderer => 1000,
            ModuleType::Control => 2000,
            ModuleType::Physics => 3000,
            ModuleType::Audio => 4000,
            ModuleType::Runtime(_) => 5000,
            ModuleType::Custom(_) => 6000,
        };
        self.maj_code = major_code;
        self
    }

    pub fn with_specifics<S: ToString>(mut self, minor_code: u32, msg: S) -> Self {
        self.minor_code = minor_code % 1000;
        self.default_msg = msg.to_string();
        self
    }

    pub fn with_root_cause<S: ToString>(mut self, cause: S) -> Self {
        self.root = Some(cause.to_string());
        self
    }
}

///General struct for parse errors, I can't be fucked to deal with that now
#[derive(Debug, Default, Clone)]
pub struct ParseError {}

impl Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Ok(())
    }
}

impl Error for ParseError {}

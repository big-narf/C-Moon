use std::collections::HashMap;
use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::create::CreateInfo;
use crate::general::*;
use crate::info::EngineInfo;
use crate::instantiate::*;
use crate::interface::EngMap;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct EngineConfig {
    pub n_threads: usize,
    pub frame_delta: u32,

    pub init_mdl: ModuleType,
    pub init_group: String,
    pub init_id: String,
    pub init_args: HashMap<EngString, Property>,

    pub mdlconfigs: HashMap<String, ModuleConfig>,
}

impl Default for EngineConfig {
    fn default() -> Self {
        let n_threads = 3;
        let frame_delta = 016; //60 fps

        let init_mdl = ModuleType::Runtime("Lua".into());
        let init_group = "sound".into();
        let init_id = "synth".into();
        let init_args = [("soundfont".into(), Property::Asset(1))].into();
        let mdlconfigs = [
            (
                ModuleType::Runtime("Lua".into()).to_string(),
                ModuleConfig::runtime_config(),
            ),
            (
                ModuleType::Renderer.to_string(),
                ModuleConfig::renderer_config(),
            ),
            (ModuleType::Audio.to_string(), ModuleConfig::audio_config()),
        ]
        .into();

        Self {
            n_threads,
            frame_delta,

            init_mdl,
            init_group,
            init_id,
            init_args,
            mdlconfigs,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CoreConfig {
    pub n_threads: usize,
    pub target_framerate: u32,
    //NOTE: PROLLY ADD THE COMMON SIGNAL DEFS HERE
}

impl Default for CoreConfig {
    fn default() -> Self {
        Self {
            n_threads: 3,
            target_framerate: 60,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct ModuleConfig(HashMap<String, String>);

impl ModuleConfig {
    pub fn config_for(mtype: ModuleType) -> Self {
        match mtype {
            ModuleType::Renderer => Self::renderer_config(),
            ModuleType::Runtime(_) => Self::runtime_config(),
            _ => Self::default(),
        }
    }

    fn runtime_config() -> Self {
        let inner = [
            ("process_fn".to_owned(), "standard/process".to_owned()),
            ("create_fn".to_owned(), "standard/create".to_owned()),
            ("destroy_fn".to_owned(), "standard/destroy".to_owned()),
        ]
        .into();
        Self(inner)
    }

    fn renderer_config() -> Self {
        let inner = [
            ("light_mode".to_owned(), "deferred".into()),
            ("max_desc".to_owned(), 16.to_string()),
            ("index_max".to_owned(), 1024.to_string()),
            ("vertex_max".to_owned(), 1024.to_string()),
            ("MAPPED_PAGE_SIZE".to_owned(), 2u64.pow(23).to_string()),
        ]
        .into();
        Self(inner)
    }

    fn audio_config() -> Self {
        let inner = [
            ("SAMPLE_RATE".to_owned(), 44100.to_string()),
            ("max_desc".to_owned(), 16.to_string()),
            ("index_max".to_owned(), 1024.to_string()),
            ("vertex_max".to_owned(), 1024.to_string()),
            ("MAPPED_PAGE_SIZE".to_owned(), 2u64.pow(23).to_string()),
        ]
        .into();
        Self(inner)
    }
}

impl ModuleConfig {
    pub fn with_field(mut self, key: &str, value: &str) -> Self {
        self.0.insert(key.into(), value.into());
        self
    }

    ///Returns None if the key has not been found, or couldn't parse the value
    pub fn get<E: Send + Sync + Error + 'static, T: FromStr<Err = E>>(
        &self,
        key: &str,
    ) -> Option<T> {
        self.0.get(key).and_then(|s| s.parse().ok())
    }
}

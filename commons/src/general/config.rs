use std::collections::HashMap;
use std::error::Error;

use serde::{Deserialize, Serialize};

use crate::general::*;
use crate::instantiate::*;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct EngineConfig {
    pub n_threads: usize,
    pub start_obj: String,
    pub start_args: HashMap<String, Property>,

    pub mdlconfigs: HashMap<String, ModuleConfig>,
}

impl Default for EngineConfig {
    fn default() -> Self {
        let n_threads = 3;
        let start_obj = "Manager".to_string();
        let start_args = [("image".to_string(), Property::Asset(1))].into();
        let mdlconfigs = [
            (
                ModuleType::Runtime("Lua".into()).to_string(),
                ModuleConfig::runtime_config(),
            ),
            (
                ModuleType::Renderer.to_string(),
                ModuleConfig::renderer_config(),
            ),
        ]
        .into();

        Self {
            n_threads,
            start_obj,
            start_args,
            mdlconfigs,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct CoreConfig {
    pub n_threads: usize,
    pub target_framerate: u32,
    //NOTE: PROLLY ADD THE COMMON SIGNAL DEFS HERE
}

impl Default for CoreConfig {
    fn default() -> Self {
        Self {
            n_threads: 3,
            target_framerate: 60,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct ModuleConfig(HashMap<String, String>);

impl ModuleConfig {
    pub fn config_for(mtype: ModuleType) -> Self {
        match mtype {
            ModuleType::Renderer => Self::renderer_config(),
            ModuleType::Runtime(_) => Self::runtime_config(),
            _ => Self::default(),
        }
    }

    fn runtime_config() -> Self {
        let inner = [("init".to_owned(), "standard".to_owned())].into();
        Self(inner)
    }

    fn renderer_config() -> Self {
        let inner = [
            ("light_mode".to_owned(), "deferred".into()),
            ("max_desc".to_owned(), 16.to_string()),
            ("index_max".to_owned(), 1024.to_string()),
            ("vertex_max".to_owned(), 1024.to_string()),
            ("MAPPED_PAGE_SIZE".to_owned(), 2u64.pow(23).to_string()),
        ]
        .into();
        Self(inner)
    }

    fn audio_config() -> Self {
        let inner = [
            ("SAMPLE_RATE".to_owned(), 44100.to_string()),
            ("max_desc".to_owned(), 16.to_string()),
            ("index_max".to_owned(), 1024.to_string()),
            ("vertex_max".to_owned(), 1024.to_string()),
            ("MAPPED_PAGE_SIZE".to_owned(), 2u64.pow(23).to_string()),
        ]
        .into();
        Self(inner)
    }
}

impl ModuleConfig {
    pub fn with_field(mut self, key: &str, value: &str) -> Self {
        self.0.insert(key.into(), value.into());
        self
    }

    ///Returns None if the key has not been found, or couldn't parse the value
    pub fn get<E: Send + Sync + Error + 'static, T: FromStr<Err = E>>(
        &self,
        key: &str,
    ) -> Option<T> {
        self.0.get(key).and_then(|s| s.parse().ok())
    }
}

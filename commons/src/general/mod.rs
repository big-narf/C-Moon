use std::ops::{Add, AddAssign};
use std::str::FromStr;

use serde::{Deserialize, Serialize};

mod config;
mod error;

pub use config::*;
pub use error::*;

use crate::interface::EngString;

#[repr(C)]
#[derive(Clone, Hash, PartialEq, Eq, Serialize, Deserialize, Debug, Default)]
pub enum ModuleType {
    #[default]
    Renderer,
    Control,
    Physics,
    Audio,
    Runtime(EngString),
    Custom(EngString),
}

impl FromStr for ModuleType {
    type Err = ParseError;
    fn from_str(value: &str) -> Result<Self, Self::Err> {
        Ok(match value {
            "renderer" => Self::Renderer,
            "control" => Self::Control,
            "physics" => Self::Physics,
            "audio" => Self::Audio,
            _ => {
                let (mdl, ident) = value.split_once(" ").ok_or_else(|| ParseError::default())?;
                match mdl {
                    "runtime" => Ok(Self::Runtime(ident.into())),
                    "custom" => Ok(Self::Custom(ident.into())),
                    _ => Err(ParseError::default()),
                }?
            }
        })
    }
}

impl ToString for ModuleType {
    fn to_string(&self) -> String {
        match self {
            Self::Renderer => "renderer".to_owned(),
            Self::Control => "control".to_owned(),
            Self::Physics => "physics".to_owned(),
            Self::Audio => "audio".to_owned(),
            Self::Runtime(lang) => format!("runtime {lang}"),
            Self::Custom(name) => format!("custom {name}"),
        }
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct Transform {
    pub pos: [f32; 3],
}

///This is a prototype for a 2D specific transform.
#[derive(Debug, Clone, Copy, Default)]
pub struct Transform2D {
    pub pos: [f32; 2],
    pub rot: f32,
    pub scale: [f32; 2],
}

///This is a prototype for a 3D specific transform.
#[derive(Debug, Clone, Copy, Default)]
pub struct Transform3D {
    pub pos: [f32; 3],
    pub rot: [f32; 4],
    pub scale: [f32; 3],
}

impl Add for Transform {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        let mut pos = [0.0; 3];
        for i in 0..3 {
            pos[i] = self.pos[i] + rhs.pos[i];
        }
        Self { pos }
    }
}

impl AddAssign for Transform {
    fn add_assign(&mut self, rhs: Self) {
        for i in 0..3 {
            self.pos[i] += rhs.pos[i];
        }
    }
}

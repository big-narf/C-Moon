use std::collections::HashMap;
use std::fmt::{Debug, Display};

use std::ops::Deref;
use std::str::FromStr;
use std::sync::Arc;

use crate::general::*;
use crate::instantiate::*;
use crate::interface::{EngMap, EngString, SignalID};

///Info object that encapsulates everything needed to create a partial object.
#[repr(C)]
#[derive(Debug, Default)]
pub struct CreateInfo {
    module: ModuleType,
    group: EngString,
    identifier: EngString,
    parent: Option<EngString>,
    attachments: EngMap<DynValue>,
}

impl FromStr for CreateInfo {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (group, id) = s.split_once('/').ok_or_else(|| ParseError {})?;
        Ok(Self::default().with_group(group).with_id(id))
    }
}

impl From<BehaviourMeta> for CreateInfo {
    fn from(value: BehaviourMeta) -> Self {
        Self {
            module: ModuleType::Runtime(value.lang),
            group: value.assembly,
            identifier: value.identifier,
            parent: Default::default(),
            attachments: Default::default(),
        }
    }
}

///Utility functions
impl CreateInfo {
    pub fn parent(&self) -> Option<&str> {
        self.parent.as_ref().map(|s| s.as_str())
    }

    pub fn into_parts(self) -> (ModuleType, PartialCode, PartialArgs) {
        let module = self.module;
        let code = PartialCode {
            group: self.group,
            identifier: self.identifier,
            parent: self.parent,
        };
        let args = self.attachments;
        (module, code, args)
    }
}

///Building functions
impl CreateInfo {
    pub fn attach<S: Into<EngString>, V: Into<DynValue>>(mut self, name: S, value: V) -> Self {
        self.attachments.replace(name.into(), value.into());
        self
    }

    pub fn attach_many<S, V, I>(mut self, list: I) -> Self
    where
        S: Into<EngString>,
        V: Into<DynValue>,
        I: IntoIterator<Item = (S, V)>,
    {
        self.attachments.extend(list);
        self
    }

    pub fn with_id<S: Into<EngString>>(mut self, id: S) -> Self {
        self.identifier = id.into();
        self
    }

    pub fn with_group<S: Into<EngString>>(mut self, group: S) -> Self {
        self.group = group.into();
        self
    }

    pub fn with_parent<S: AsRef<str>>(mut self, parent: Option<S>) -> Self {
        self.parent = parent.map(|v| v.as_ref().into());
        self
    }

    pub fn with_mdl(mut self, mdl: ModuleType) -> Self {
        self.module = mdl;
        self
    }
}

pub struct PartialCode {
    pub group: EngString,
    pub identifier: EngString,
    pub parent: Option<EngString>,
}

pub type PartialArgs = EngMap<DynValue>;

/** DynValue is the main object used for object creation in the engine
*
*   It represents a weak reference to either an object owned by the core object or by some other
*   module, lika a closure, for instance.
*   PropRef needs to represend all the DB types from properties + runtime objects like closures and
*   the like
*/
#[derive(Debug, Clone)]
pub enum DynValue {
    Number(f64),
    Text(String),
    Asset(AssetHandle),         //DB Ref to resource
    Blueprint(BlueprintHandle), //DB Ref to Blueprint

    //variant from Runtime elements
    Signal(SignalID),
    Transform(Transform),
    Data(DataBlock), //Raw byte data
}

impl DynValue {
    fn as_solid<T: FromDyn>(self) -> Option<T> {
        T::from_dyn(self)
    }
}

impl From<AssetHandle> for DynValue {
    fn from(value: AssetHandle) -> Self {
        Self::Asset(value)
    }
}

impl From<DataBlock> for DynValue {
    fn from(value: DataBlock) -> Self {
        Self::Data(value)
    }
}

impl From<u32> for DynValue {
    fn from(value: u32) -> Self {
        Self::Number(value as f64)
    }
}

///Unpacks a reference inside a dynamic value.
pub trait DynDeref {
    fn dyn_deref(value: &DynValue) -> Option<&Self>;
}

impl DynDeref for String {
    fn dyn_deref(value: &DynValue) -> Option<&Self> {
        match value {
            DynValue::Text(t) => Some(t),
            _ => None,
        }
    }
}

impl DynDeref for AssetHandle {
    fn dyn_deref(value: &DynValue) -> Option<&Self> {
        match value {
            DynValue::Asset(a) => Some(a),
            _ => None,
        }
    }
}

impl DynDeref for DataBlock {
    fn dyn_deref(value: &DynValue) -> Option<&Self> {
        match value {
            DynValue::Data(b) => Some(b),
            _ => None,
        }
    }
}

impl DynDeref for SignalID {
    fn dyn_deref(value: &DynValue) -> Option<&Self> {
        match value {
            DynValue::Signal(s) => Some(s),
            _ => None,
        }
    }
}

///Consumes a dynamic value and returns the underlying unpacked value.
pub trait FromDyn {
    fn from_dyn(value: DynValue) -> Option<Self>
    where
        Self: Sized;
}

impl FromDyn for Property {
    fn from_dyn(value: DynValue) -> Option<Self>
    where
        Self: Sized,
    {
        match value {
            DynValue::Number(n) => Some(Property::Number(n)),
            DynValue::Text(t) => Some(Property::Text(t)),
            DynValue::Asset(h) => Some(Property::Asset(h.tag)),
            DynValue::Blueprint(id) => Some(Property::Blueprint(id.0)),
            _ => None,
        }
    }
}

impl FromDyn for AssetHandle {
    fn from_dyn(value: DynValue) -> Option<Self>
    where
        Self: Sized,
    {
        match value {
            DynValue::Asset(a) => Some(a),
            _ => None,
        }
    }
}

impl FromDyn for SignalID {
    fn from_dyn(value: DynValue) -> Option<Self>
    where
        Self: Sized,
    {
        match value {
            DynValue::Signal(s) => Some(s),
            _ => None,
        }
    }
}

impl FromDyn for DataBlock {
    fn from_dyn(value: DynValue) -> Option<Self>
    where
        Self: Sized,
    {
        match value {
            DynValue::Data(b) => Some(b),
            _ => None,
        }
    }
}

impl FromDyn for u32 {
    fn from_dyn(value: DynValue) -> Option<Self>
    where
        Self: Sized,
    {
        match value {
            DynValue::Number(n) => Some(n as Self),
            _ => None,
        }
    }
}

#[derive(Debug, Clone)]
pub enum AssetData {
    Simple(DynValue),
    Composite(EngMap<DynValue>),
}

impl AssetData {
    pub fn get_field<T: DynDeref>(&self, name: &str) -> Option<&T> {
        match self {
            Self::Simple(_) => None,
            Self::Composite(t) => t.get_as(name),
        }
    }

    pub fn get<T: DynDeref>(&self) -> Option<&T> {
        match self {
            Self::Simple(d) => T::dyn_deref(d),
            Self::Composite(_) => None,
        }
    }

    pub fn take_field<T: FromDyn>(&mut self, name: &str) -> Option<T> {
        match self {
            Self::Simple(_) => None,
            Self::Composite(t) => t.pop_as(name),
        }
    }
    pub fn take<T: FromDyn>(self) -> Option<T> {
        match self {
            Self::Simple(d) => T::from_dyn(d),
            Self::Composite(_) => None,
        }
    }
}

//NOTE: ENGINE CONCRETE TYPES.

///Cheaply cloned read only data array
#[derive(Clone, Debug)]
pub struct DataBlock {
    inner: Arc<Vec<u8>>,
}

impl DataBlock {}

impl From<Vec<u8>> for DataBlock {
    fn from(value: Vec<u8>) -> Self {
        Self {
            inner: value.into(),
        }
    }
}

impl From<&[u8]> for DataBlock {
    fn from(value: &[u8]) -> Self {
        Self {
            inner: value.to_vec().into(),
        }
    }
}

impl Deref for DataBlock {
    type Target = [u8];
    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

///Wrapper type around an identifier for a game asset
#[derive(Debug, Clone)]
pub struct AssetHandle {
    tag: u32,
    loader: fn(u32) -> EngResult<AssetData>,
}

impl AssetHandle {
    pub fn new(tag: u32, loader: fn(u32) -> EngResult<AssetData>) -> Self {
        Self { tag, loader }
    }
    pub fn tag(&self) -> u32 {
        self.tag
    }

    pub fn load(&self) -> EngResult<AssetData> {
        (self.loader)(self.tag)
    }
}

///Wrapper type around an identifier for a game asset
#[derive(Debug, Clone)]
pub struct BlueprintHandle(String);

impl BlueprintHandle {
    pub fn new(name: String) -> Self {
        Self(name)
    }
}

impl From<String> for BlueprintHandle {
    fn from(value: String) -> Self {
        Self(value)
    }
}

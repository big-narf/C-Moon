use std::fmt::{Debug, Display, Write};
use std::str::FromStr;

use serde::{Deserialize, Serialize};

use crate::general::*;
use crate::interface::EngString;

/*NOTE: THIS MODULE IS INTENDED FOR ALL THE OBJECTS RELATED TO THE INSTANTIATION PROCESS OF A
/  GAMEOBJECT. SINCE GAME OBJECT NEED TO ALLOW FOR INSTANTIATION FROM SERIALIZED PARAMS FROM THE DATABASE
/  THE OBJECTS HERE ARE MAINLY CONCEARNED WITH THAT MANNER OF CREATE BEHAVIOUR
/
/
*/

///This is a convinience struct to create the root object of a hierarchy. It encodes the same
///informations as an entry to the "Hierarchy" table on the DB. Conceptually it is just a
///dynamically created
///version of an entry on that table that can take any value at runtime.
#[derive(Clone, Debug, Default)]
pub struct InstantiateInfo {
    pub handle: EngString,
    pub obj_id: EngString,
    pub parent: Option<EngString>,
    pub transform: Transform,
    pub args: Vec<Property>,
}

impl InstantiateInfo {
    pub fn with_id(mut self, id: &str) -> Self {
        self.obj_id = id.into();
        self
    }

    pub fn with_name(mut self, name: &str) -> Self {
        self.handle = name.into();
        self
    }

    pub fn with_parent(mut self, parent: &str) -> Self {
        self.parent = Some(parent.into());
        self
    }

    pub fn with_arg(mut self, value: Property) -> Self {
        self.args.push(value);
        self
    }

    pub fn with_transform(mut self, transform: Transform) -> Self {
        self.transform = transform;
        self
    }
}

impl InstantiateInfo {
    pub fn root_name(&self) -> String {
        match &self.parent {
            Some(p) => format!("{}_{}", p, self.handle),
            None => self.handle.to_string(),
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct BehaviourMeta {
    pub lang: EngString,
    pub assembly: EngString,
    pub identifier: EngString,
}

impl FromStr for BehaviourMeta {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split("-");
        let lang = parts.next().ok_or_else(|| ParseError {})?.into();
        let assembly = parts.next().ok_or_else(|| ParseError {})?.into();
        let identifier = parts.next().ok_or_else(|| ParseError {})?.into();
        Ok(Self {
            lang,
            assembly,
            identifier,
        })
    }
}
///Dyn value is an ops type that is meant to be used by the editor to create blueprints and by the
///runtime as arguments for parametric instantiation.
#[derive(PartialEq, Debug, Clone, Serialize, Deserialize)]
pub enum Property {
    Number(f64),
    Text(String),
    Asset(u32),        //DB Ref to resource
    Blueprint(String), //DB Ref to Blueprint
}

impl Property {
    pub fn get_type(&self) -> PropertyType {
        match self {
            Self::Number(_) => PropertyType::Number,
            Self::Text(_) => PropertyType::Text,
            Self::Blueprint(_) => PropertyType::Blueprint,
            Self::Asset(_) => PropertyType::Asset,
        }
    }
}

impl FromStr for Property {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (t, v) = s.split_once('#').ok_or(ParseError {})?;
        Ok(match t {
            "number" => Self::Number(v.parse().map_err(|e| ParseError {})?),
            "text" => Self::Text(v.to_string()),
            "asset" => Self::Asset(v.parse().map_err(|e| ParseError {})?),
            "blueprint" => Self::Blueprint(v.to_string()),
            _ => Err(ParseError {})?,
        })
    }
}

impl ToString for Property {
    fn to_string(&self) -> String {
        match self {
            Self::Number(n) => format!("number#{n}"),
            Self::Text(t) => format!("text#{t}"),
            Self::Asset(n) => format!("asset#{n}"),
            Self::Blueprint(s) => format!("blueprint#{s}"),
        }
    }
}

#[derive(PartialEq, Debug, Clone, Copy, Serialize, Deserialize)]
pub enum PropertyType {
    Number,
    Text,
    Asset,     //DB Ref to resource
    Blueprint, //DB Ref to Blueprint
}
impl std::fmt::Display for PropertyType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Number => f.write_str("Number"),
            Self::Text => f.write_str("Text"),
            Self::Asset => f.write_str("Asset"),
            Self::Blueprint => f.write_str("Blueprint"),
        }
    }
}

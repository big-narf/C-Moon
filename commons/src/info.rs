use std::fmt::Display;

use crate::general::*;

///This is a struct meant for non-error loggin in a translation friendly manner.
#[derive(Clone, Default, Debug)]
pub enum EngineInfo {
    #[default]
    Nothing,
    Warn(EngineError),
}

impl Display for EngineInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Nothing => f.write_str("nothing to say"),
            Self::Warn(e) => {
                let s = format!("WARNING#:()");
                f.write_str(&s)
            }
        }
    }
}

use super::*;

#[derive(Clone, Debug, Default)]
pub struct Signal {
    strenght: f32,
    derivate: f32,
    attach: EngMap<DynValue>,
}

impl Signal {
    ///Creates a new signal marked as emmited at moment.
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_strenght(mut self, strenght: f32) -> Self {
        self.strenght = strenght.clamp(0.0, 1.0);
        self
    }

    pub fn with_delta(mut self, delta: f32) -> Self {
        self.derivate = delta.clamp(-1.0, 1.0);
        self
    }

    pub fn attach<K: Into<EngString>, V: Into<DynValue>>(mut self, key: K, value: V) -> Self {
        self.attach.replace(key, value);
        self
    }

    pub fn extend<K, V, I>(mut self, values: I) -> Self
    where
        I: IntoIterator<Item = (K, V)>,
        K: Into<EngString>,
        V: Into<DynValue>,
    {
        self.attach.extend(values);
        self
    }
}

impl Signal {
    pub fn is_above(&self, threshold: f32) -> bool {
        self.strenght > threshold
    }

    pub fn is_bellow(&self, threshold: f32) -> bool {
        self.strenght < threshold
    }

    ///returns true if it passed the threshold in the last frame.
    pub fn rose_above(&self, threshold: f32) -> bool {
        let last_val = self.strenght - self.derivate;
        self.strenght > threshold && last_val <= threshold
    }

    ///returns true if it passed the threshold in the last frame.
    pub fn fell_bellow(&self, threshold: f32) -> bool {
        let last_val = self.strenght - self.derivate;
        self.strenght < threshold && last_val >= threshold
    }
}

impl Signal {
    ///Evaluates self and superposes with the new signal. if self is a dead signal, other is
    ///assigned to self
    pub fn superpose(&mut self, other: Self) {
        self.strenght += other.strenght;
        self.derivate += other.derivate;
        self.attach.extend(other.attach);
    }

    ///Returns true if the signal is dead at the current moment.
    pub fn advance(&mut self, frames_passed: usize) -> bool {
        let delta = self.derivate * frames_passed as f32;
        self.strenght = (self.strenght + delta).clamp(0.0, 1.0);
        if self.strenght >= 1.0 {
            self.derivate = 0.0;
        }
        if self.strenght <= 0.0 {
            true
        } else {
            false
        }
    }

    pub fn adv_compose(&mut self, other: Self, frames_passed: usize) {
        let delta = self.derivate * frames_passed as f32;
        let old_signal = (self.strenght + delta).clamp(0.0, 1.0);
        if old_signal <= 0.0 {
            *self = other;
        } else {
            self.superpose(other);
        }
    }
}

impl Signal {
    ///Clones the value at key and converts to the given type.
    pub fn get_as<T: DynDeref>(&self, key: &str) -> Option<&T> {
        self.attach.get_as(key)
    }

    ///grabs the value at the key without cloning.
    pub fn pop_as<T: FromDyn>(&mut self, key: &str) -> Option<T> {
        self.attach.pop_as(key)
    }
}

impl Into<EngMap<DynValue>> for Signal {
    fn into(self) -> EngMap<DynValue> {
        self.attach
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub enum SignalID {
    Ambient(EngString),
    Object { target: EngString, id: EngString },
}

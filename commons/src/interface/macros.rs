pub use paste::paste;

/**implements the unsafe part of module interaction with engine compatible functions based on the module's local context
* Receives:
* a type [$local_ctx] that has a new method start method of
* and a boolean [$exclusive_flag] that identifies if the module is thread safe or not
* auto
*/
#[macro_export]
macro_rules! module_interface {
    ($local_ctx :ty, $mdl_type :expr) => {
        use std::collections::HashMap;
        use std::ffi::c_void;
        use std::sync::{Arc, Mutex};
        use $crate::*;

        /*
         * MODULE IMPORTING RELATED FUNCTIONS
         */
        /**Creates a [$local_ctx] instance linked to [raw_ctx] from the engine
         * It's important that [$local_ctx].new returns a pointer to a simple Self instance, if it
         * is a Result, the engine will segfault
         */
        #[no_mangle]
        pub unsafe extern "C" fn initialize_module(
            handle: CoreHandle,
            n_threads: usize,
            config: ModuleConfig,
            load: Option<HashMap<String, String>>,
        ) -> EngResult<ModuleHandle> {
            let og_mdl = Arc::new(<$local_ctx>::new(handle, n_threads, config, load)?);
            //gets rid of rust types for the sake of C compatible interface
            let ptr = Arc::into_raw(og_mdl) as *const c_void;
            let mdl = Arc::from_raw(ptr);

            let handle = ModuleHandle {
                module_ptr: mdl.clone(),

                create_fn: create,
                destroy_fn: destroy,

                process_op: process,
                alter_op: alter,
                commence_op: commence,
                conclude_op: conclude,
            };

            Ok(handle)
        }

        #[no_mangle]
        pub unsafe extern "C" fn module_type() -> ModuleType {
            $mdl_type.clone()
        }

        fn alter(raw_mdl: *const c_void, thread_id: usize) -> EngResult<()> {
            let mdl: &$local_ctx = unsafe { std::mem::transmute(raw_mdl) };
            mdl.alter(thread_id)
        }
        fn process(raw_mdl: *const c_void, thread_id: usize, dt: u32) -> EngResult<()> {
            let mdl: &$local_ctx = unsafe { std::mem::transmute(raw_mdl) };
            mdl.process(thread_id, dt)
        }

        fn commence(raw_mdl: *const c_void) -> EngResult<()> {
            let mdl: &$local_ctx = unsafe { std::mem::transmute(raw_mdl) };
            mdl.commence()
        }

        fn conclude(raw_mdl: *const c_void) -> EngResult<()> {
            let mdl: &$local_ctx = unsafe { std::mem::transmute(raw_mdl) };
            mdl.conclude()
        }

        fn create(
            raw_mdl: *const c_void,
            code: PartialCode,
            args: PartialArgs,
        ) -> EngResult<usize> {
            let mdl: &$local_ctx = unsafe { std::mem::transmute(raw_mdl) };
            mdl.create(code, args)
        }

        fn destroy(raw_mdl: *const c_void, id: usize) -> EngResult<()> {
            let mdl: &$local_ctx = unsafe { std::mem::transmute(raw_mdl) };
            mdl.destroy(id)
        }
    };
}

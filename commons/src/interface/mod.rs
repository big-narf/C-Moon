use std::ffi::c_void;
use std::sync::{Arc, Weak};

use raw_window_handle::{RawDisplayHandle, RawWindowHandle};
use serde::{Deserialize, Serialize};
use smol_str::format_smolstr;

use crate::create::*;
use crate::general::*;
use crate::instantiate::*;

mod signal;
pub use signal::*;

#[cfg(feature = "module")]
mod macros;

#[cfg(feature = "module")]
pub use macros::*;

///String type for performance critial strings in the engine.
pub type EngString = smol_str::SmolStr;

///This is an optimized map like object for reasonable use cases
#[derive(Debug, Clone)]
pub struct EngMap<T>(smallvec::SmallVec<[(EngString, T); 16]>);

///Internal primitive functions
impl<T> EngMap<T> {
    ///Inserts into the map without checking if there is a value for the key already.
    fn insert_unchecked<K: Into<EngString>, V: Into<T>>(&mut self, key: K, value: V) {
        self.0.push((key.into(), value.into()));
    }

    ///Returns the index that has the given key
    fn index_of_key(&self, key: &str) -> Option<usize> {
        self.0
            .iter()
            .enumerate()
            .find_map(|(n, (k, v))| if k == key { Some(n) } else { None })
    }

    ///Removes the pair at index.
    fn remove_indexed(&mut self, index: usize) -> T {
        self.0.swap_remove(index).1
    }

    ///returns a reference to the value at the key, with the index in the array.
    fn get_indexed(&self, index: usize) -> Option<&T> {
        self.0.get(index).map(|(_, v)| v)
    }

    ///returns a reference to the value at the key, with the index in the array.
    fn get_mut_indexed(&mut self, index: usize) -> Option<&mut T> {
        self.0.get_mut(index).map(|(_, v)| v)
    }
}

///Public api functions
impl<T> EngMap<T> {
    pub fn new() -> Self {
        let inner = smallvec::SmallVec::new();
        Self(inner)
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn get(&self, key: &str) -> Option<&T> {
        let index = self.index_of_key(key)?;
        self.get_indexed(index)
    }

    pub fn get_mut(&mut self, key: &str) -> Option<&mut T> {
        let index = self.index_of_key(key)?;
        self.get_mut_indexed(index)
    }

    pub fn remove(&mut self, key: &str) -> Option<T> {
        let index =
            self.0
                .iter()
                .enumerate()
                .find_map(|(index, pair)| if pair.0 == key { Some(index) } else { None })?;
        Some(self.remove_indexed(index))
    }

    ///Inserts the value into the key if it is empty. return the value if it fails
    pub fn try_insert<K: Into<EngString>, V: Into<T>>(&mut self, key: K, value: V) -> Option<V> {
        let key = key.into();
        match self.get(&key) {
            Some(_) => Some(value),
            None => {
                self.insert_unchecked(key, value);
                None
            }
        }
    }

    ///Inserts the value into the key, returns the old value if there is one.
    pub fn replace<K: Into<EngString>, V: Into<T>>(&mut self, key: K, value: V) -> Option<T> {
        let key = key.into();
        let value = value.into();
        match self.get_mut(&key) {
            Some(old) => Some(std::mem::replace(old, value)),
            None => {
                self.insert_unchecked(key, value);
                None
            }
        }
    }

    ///Inserts the value in the given key or operates on the current and new values
    pub fn insert_or<K, V, F>(&mut self, key: K, value: V, func: F)
    where
        K: Into<EngString>,
        V: Into<T>,
        F: Fn(&mut T, T),
    {
        let key = key.into();
        match self.get_mut(&key) {
            None => self.insert_unchecked(key, value),
            Some(old) => (func)(old, value.into()),
        }
    }
}

///Multi element functions
impl<T> EngMap<T> {
    pub fn get_prefixed<'pre, 'src: 'pre>(
        &'src self,
        prefix: &'pre str,
    ) -> impl Iterator<Item = &'src T> + 'pre {
        self.0
            .iter()
            .filter_map(move |(k, v)| if k.starts_with(prefix) { Some(v) } else { None })
    }

    pub fn get_mut_prefixed<'pre, 'src: 'pre>(
        &'src mut self,
        prefix: &'pre str,
    ) -> impl Iterator<Item = &'src mut T> + 'pre {
        self.0
            .iter_mut()
            .filter_map(move |(k, v)| if k.starts_with(prefix) { Some(v) } else { None })
    }

    ///Inserts the value with the key or a postfixed key (ex : key_0, key_1 .....). Garantees to
    ///insert
    pub fn over_insert<K: Into<EngString>, V: Into<T>>(&mut self, key: K, value: V) {
        let mut key = key.into();
        let mut counter = 0;
        loop {
            if self.index_of_key(&key).is_none() {
                //found unused name
                self.insert_unchecked(key, value);
                break;
            } else {
                //needs to change the name
                key = format_smolstr!("{key}_{counter}");
                counter += 1;
            }
        }
    }

    ///Extends the map with the contents of the iterator. Doesn't override duplicate keys.
    pub fn extend<S: Into<EngString>, V: Into<T>, I: IntoIterator<Item = (S, V)>>(
        &mut self,
        iter: I,
    ) {
        iter.into_iter().for_each(|(s, v)| {
            self.over_insert(s, v);
        });
    }
}

impl<T> IntoIterator for EngMap<T> {
    type Item = (EngString, T);
    type IntoIter = smallvec::IntoIter<[(EngString, T); 16]>;
    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl<T, S, V> FromIterator<(S, V)> for EngMap<T>
where
    S: Into<EngString>,
    V: Into<T>,
{
    fn from_iter<I: IntoIterator<Item = (S, V)>>(iter: I) -> Self {
        Self(
            iter.into_iter()
                .map(|(s, v)| (s.into(), v.into()))
                .collect(),
        )
    }
}

impl EngMap<DynValue> {
    ///Tries to unpack the value at the key and returns a reference without cloning.
    pub fn get_as<T: DynDeref>(&self, key: &str) -> Option<&T> {
        T::dyn_deref(self.get(key)?)
    }

    ///Removes the value at the key from the map and returns an owned unpacked value.
    pub fn pop_as<T: FromDyn>(&mut self, key: &str) -> Option<T> {
        T::from_dyn(self.remove(key)?)
    }

    ///Convenience function that returns all the the elements with a given prefix that are of the
    ///given type
    pub fn get_group_of<'pre, 'src: 'pre, T: DynDeref + 'static>(
        &'src self,
        prefix: &'pre str,
    ) -> impl Iterator<Item = &'src T> + 'pre {
        self.get_prefixed(prefix)
            .filter_map(|value| T::dyn_deref(value))
    }
}

impl<T> Default for EngMap<T> {
    fn default() -> Self {
        Self::new()
    }
}

//NOTE: ENGINE CORE RELATED INTERFACE ELEMENTS

#[repr(C)]
#[derive(Clone, Debug)]
pub struct CoreHandle {
    pub window_handle: RawWindowHandle,
    pub display_handle: RawDisplayHandle,

    pub signal_emitter: fn(SignalID, Signal),
    pub signal_searcher: fn(&SignalID) -> Option<Signal>,

    pub partial_creator: fn(CreateInfo) -> EngResult<()>,
    pub partial_destroyer: fn(GlobalAdress) -> EngResult<()>,

    pub instantiator: fn(InstantiateInfo) -> EngResult<()>,
    pub eliminator: fn(&str) -> EngResult<()>,

    pub transform_getter: fn(&str) -> Option<Transform>,
    pub transform_setter: fn(&str, Transform) -> EngResult<()>,
}

///Runtime called functions
impl CoreHandle {
    pub fn emit_signal(&self, id: SignalID, signal: Signal) {
        (self.signal_emitter)(id, signal)
    }

    pub fn search_signal(&self, signal_id: &SignalID) -> Option<Signal> {
        (self.signal_searcher)(signal_id)
    }
}

///This is a trait for the functions of the shared part of the module
pub trait Module: Send + Sync {
    fn get_type() -> ModuleType;

    fn alter(&self, thread_id: usize) -> EngResult<()>;

    fn process(&self, thread_id: usize, dt: u32) -> EngResult<()>;

    fn commence(&self) -> EngResult<()>;

    fn conclude(&self) -> EngResult<()>;

    ///Creates an object in the module context.
    fn create(&self, code: PartialCode, args: PartialArgs) -> EngResult<usize>;

    fn destroy(&self, id: usize) -> EngResult<()>;
}

///This is a trait for the thread local part of a module
pub trait Worker {
    fn commence(&mut self) -> EngResult<()>;

    fn alter(&mut self) -> EngResult<()>;

    fn process(&mut self, dt: u32) -> EngResult<()>;

    fn conclude(&mut self) -> EngResult<()>;
}

//NOTE: MODULE RELATED INTEFACE ELEMENTS

///This is a handle held by the core for creating/destroying objects with privileged level
#[repr(C)]
#[derive(Clone, Debug)]
pub struct ModuleHandle {
    pub module_ptr: Arc<c_void>,

    pub create_fn: fn(*const c_void, PartialCode, PartialArgs) -> EngResult<usize>,
    pub destroy_fn: fn(*const c_void, usize) -> EngResult<()>,

    pub alter_op: fn(*const c_void, usize) -> EngResult<()>,
    pub process_op: fn(*const c_void, usize, u32) -> EngResult<()>,
    pub commence_op: fn(*const c_void) -> EngResult<()>,
    pub conclude_op: fn(*const c_void) -> EngResult<()>,
}

///Runtime called functions
impl ModuleHandle {
    pub fn create(&self, code: PartialCode, args: PartialArgs) -> EngResult<usize> {
        (self.create_fn)(Arc::as_ptr(&self.module_ptr), code, args)
    }

    pub fn destroy(&self, id: usize) -> EngResult<()> {
        (self.destroy_fn)(Arc::as_ptr(&self.module_ptr), id)
    }
}

///Runtime called functions
impl ModuleHandle {
    ///Called at the launch sequence, initializes the module
    pub fn alter_phase<'mdl>(&'mdl self, thread_id: usize) -> EngResult<()> {
        (self.alter_op)(Arc::as_ptr(&self.module_ptr), thread_id)
    }

    pub fn process_phase<'mdl>(&'mdl self, thread_id: usize, dt: u32) -> EngResult<()> {
        (self.process_op)(Arc::as_ptr(&self.module_ptr), thread_id, dt)
    }

    pub fn commence_phase<'mdl>(&'mdl self) -> EngResult<()> {
        (self.commence_op)(Arc::as_ptr(&self.module_ptr))
    }

    pub fn conclude_phase<'mdl>(&'mdl self) -> EngResult<()> {
        (self.conclude_op)(Arc::as_ptr(&self.module_ptr))
    }
}

#[derive(Clone, Hash, PartialEq, Eq, Serialize, Deserialize, Debug)]
pub struct GlobalAdress {
    pub mdl: ModuleType,
    pub id: usize,
}

impl ToString for GlobalAdress {
    fn to_string(&self) -> String {
        format!("module:{}, id:{}", self.mdl.to_string(), self.id)
    }
}

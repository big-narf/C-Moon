mod content; //shared between core and editor exclusive
mod general;
mod interface; //shared between module and core exclusive //shared between all.

mod create;
mod instantiate;

mod info;
/*
*   This whole file just manages feature-dependent submodules and re-exports all that is needed at the root
*   of the crate.
*
*/

///Core prelude
#[cfg(feature = "core")]
mod exporter {
    pub use crate::content::*;
    pub use crate::general::*;
    pub use crate::interface::*;

    pub use crate::create::*;
    pub use crate::instantiate::*;

    pub use crate::info::*;
}

///module prelude
#[cfg(feature = "module")]
mod exporter {
    pub use crate::general::*;
    pub use crate::interface::*;

    pub use crate::create::*;
    pub use crate::instantiate::*;

    pub use crate::info::*;
}

///editor prelude
#[cfg(feature = "editor")]
mod exporter {
    pub use crate::content::*;
    pub use crate::general::*;

    pub use crate::instantiate::*;
}

#[cfg(any(feature = "core", feature = "editor", feature = "module"))]
pub use exporter::*;
